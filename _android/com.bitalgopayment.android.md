---
wsId: 
title: "BitalGo Multi Wallet"
altTitle: 
authors:

users: 1000
appId: com.bitalgopayment.android
released: 2020-09-10
updated: 2020-09-10
version: "1.0.0"
stars: 3.7
ratings: 78
reviews: 50
size: 9.1M
website: 
repository: 
issue: 
icon: com.bitalgopayment.android.png
bugbounty: 
verdict: stale
date: 2021-09-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


