---
wsId: BinanceUS
title: "Binance.US"
altTitle: 
authors:
- leo
users: 1000000
appId: com.binance.us
released: 2019-12-23
updated: 2021-11-04
version: "2.8.5"
stars: 2.6
ratings: 6199
reviews: 4488
size: Varies with device
website: https://www.binance.us
repository: 
issue: 
icon: com.binance.us.png
bugbounty: 
verdict: custodial
date: 2020-11-17
signer: 
reviewArchive:


providerTwitter: binanceus
providerLinkedIn: binance-us
providerFacebook: BinanceUS
providerReddit: 

redirect_from:
  - /com.binance.us/
---


Binance being a big exchange, the description on Google Play only mentions
security features like FDIC insurance for USD balance but no word on
self-custody. Their website is not providing more information neither. We
assume the app is a custodial offering and therefore **not verifiable**.
