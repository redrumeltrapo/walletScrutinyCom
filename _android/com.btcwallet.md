---
wsId: 
title: "LUNA - Blockchain Wallet"
altTitle: 
authors:

users: 5000
appId: com.btcwallet
released: 2019-08-29
updated: 2019-10-23
version: "8.0.8"
stars: 3.8
ratings: 33
reviews: 21
size: 4.7M
website: 
repository: 
issue: 
icon: com.btcwallet.jpg
bugbounty: 
verdict: obsolete
date: 2021-10-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


