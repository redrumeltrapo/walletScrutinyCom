---
wsId: 
title: "Buy Bitcoin & Crypto Exchange"
altTitle: 
authors:
- leo
- danny
users: 100000
appId: com.changelly.app
released: 2018-08-28
updated: 2021-10-01
version: "2.7.10"
stars: 4.8
ratings: 3646
reviews: 1879
size: 44M
website: 
repository: 
issue: 
icon: com.changelly.app.png
bugbounty: 
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.changelly.app/
  - /posts/com.changelly.app/
---


This app has no wallet feature in the sense that you hold Bitcoins in the app.
