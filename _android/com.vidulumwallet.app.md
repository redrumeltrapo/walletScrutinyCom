---
wsId: Vidulum
title: "Vidulum - Multi-Asset Cryptocurrency Wallet"
altTitle: 
authors:
- leo
users: 5000
appId: com.vidulumwallet.app
released: 2019-04-04
updated: 2019-04-05
version: "1.2"
stars: 4.2
ratings: 99
reviews: 71
size: 174k
website: https://vidulum.app
repository: https://github.com/vidulum/vidulum.app
issue: 
icon: com.vidulumwallet.app.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2020-07-29
  version: "1.2"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nosource

providerTwitter: VidulumApp
providerLinkedIn: 
providerFacebook: VidulumTeam
providerReddit: VidulumOfficial

redirect_from:
  - /com.vidulumwallet.app/
  - /posts/com.vidulumwallet.app/
---


On Google Play we read

> As a multi-asset web wallet, users are in full control of their private keys

so we assume this app is non-custodial.

On their Website we read again

> **Own Your Private Keys**
>
> Private keys for cryptocurrency addresses are created client side through a
  proprietary method and are never stored or sent back to our servers

which is actually very scary. "Proprietary method" means non-standard method, so
if this wallet stops working, there is no way to get your funds recovered on a
different wallet.

On their website there is a link to a [GitHub
repository](https://github.com/vidulum/vidulum.app) but that  only contains one
text file and no code.

The app is closed source and thus **not verifiable**.
