---
wsId: 
title: "xFolio - bitcoin, ethereum, altcoin crypto tracker"
altTitle: 
authors:

users: 100
appId: xfolio.android.app
released: 2019-02-13
updated: 2020-03-08
version: "1.4.2"
stars: 4.3
ratings: 10
reviews: 10
size: 3.2M
website: 
repository: 
issue: 
icon: xfolio.android.app.png
bugbounty: 
verdict: stale
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


