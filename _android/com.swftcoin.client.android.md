---
wsId: swft
title: "SWFT Blockchain"
altTitle: 
authors:
- leo
users: 10000
appId: com.swftcoin.client.android
released: 2018-01-15
updated: 2021-11-05
version: "5.13.21"
stars: 4.5
ratings: 1722
reviews: 948
size: 34M
website: http://www.swft.pro
repository: 
issue: 
icon: com.swftcoin.client.android.png
bugbounty: 
verdict: custodial
date: 2020-04-15
signer: 
reviewArchive:


providerTwitter: SwftCoin
providerLinkedIn: swftcoin
providerFacebook: SWFTBlockchain
providerReddit: 

redirect_from:
  - /com.swftcoin.client.android/
  - /posts/com.swftcoin.client.android/
---


The description on Google Play is full of buzzwords like big data, AI, machine
learning but no clear words on weather this wallet is custodial or not. Given
its strong emphasis on speed and many coins and no words on the usual seed words,
we have to assume it is indeed custodial. Their [FAQ](https://www.swft.pro/#/FAQ)
also sounds more like a custodial exchange than a wallet. This app is certainly
**not verifiable**.
