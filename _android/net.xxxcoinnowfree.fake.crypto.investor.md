---
wsId: 
title: "Fake Crypto Investor"
altTitle: 
authors:

users: 50
appId: net.xxxcoinnowfree.fake.crypto.investor
released: 2021-07-22
updated: 2021-07-22
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: net.xxxcoinnowfree.fake.crypto.investor.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
