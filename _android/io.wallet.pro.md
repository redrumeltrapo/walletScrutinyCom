---
wsId: WalletIOPRO
title: "wallet.io Pro—Multisig Wallet (BTC, ETH, EOS...)"
altTitle: 
authors:
- danny
users: 1000
appId: io.wallet.pro
released: 2019-07-14
updated: 2021-08-04
version: "1.2.9"
stars: 4.4
ratings: 9
reviews: 4
size: 18M
website: https://pro.wallet.io/
repository: https://github.com/wallet-io
issue: 
icon: io.wallet.pro.png
bugbounty: 
verdict: nosource
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: io_wallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Note**: This is the PRO version for:

{% include walletLink.html wallet='android/io.wallet' verdict='true' %}

## App Description

> - One Stop digital asset management
- Two secure asset management solutions
- Manage assets with partners
- Institutional, Multi-signature, Numerous Currencies
- Team Property, Team Confirmation 

## The Site

> Group multisig
- Suitable for multiple parties’ co-management. Avoid assets loss caused by single private key missing. Offer a better solution to multiple parties funding co-managements.
- 'm-of-n' signatures. Set 'n' total private keys and requires 'm' signatures in authorizing a transaction.

## The App

We downloaded the app. Our user credentials are the same with the other wallet.io app. 

After sign-in, we are presented with two options: 

- Group multisig wallet
- Individual multisig wallet

We opted for the individual multisig wallet. 

We then filled up the following fields:

- Wallet name
- Description 
- Private key generation: 
  - Local
  - Wallet.io hardware
- Safe-keeping of the third private key
  - Authority
  - Yourself 

## Contact

We were able to locate their github, but as of this moment, it seems to be outdated, with only six commits in the past year. Additionally, no repositories were related to wallet.io's mobile app.

## Verdict

This app is **self-custodial**, but the source code is **not verifiable**.
