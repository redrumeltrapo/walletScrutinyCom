---
wsId: 
title: "Coinorbis Trade - Cryptocurrency Trading Platform"
altTitle: 
authors:

users: 100
appId: com.coinorbis.trade
released: 2018-11-05
updated: 2018-11-22
version: "2.0.0"
stars: 4.8
ratings: 6
reviews: 1
size: 5.2M
website: 
repository: 
issue: 
icon: com.coinorbis.trade.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


