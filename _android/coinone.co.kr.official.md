---
wsId: 
title: "코인원 - Coinone"
altTitle: 
authors:
- danny
users: 1000000
appId: coinone.co.kr.official
released: 2018-01-28
updated: 2021-10-21
version: "2.9.5"
stars: 3.3
ratings: 6675
reviews: 2810
size: 30M
website: https://coinone.co.kr/
repository: 
issue: 
icon: coinone.co.kr.official.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: CoinoneOfficial
providerLinkedIn: 
providerFacebook: coinone
providerReddit: 

redirect_from:

---


> Hello. This is Team Coinone. Currently, foreigners cannot use the Coinone app, so we recommend using a web browser to use the Coinone service. We apologize for any inconvenience.

It's a Korean app and is not available in English. As the support stated, *foreigners will not be able to use it.*

> Coinone maintains 85% of cold wallets to protect customer assets more safely.

As it's an exchange and there's nothing to suggest otherwise, it is safe to conclude that Coinone is a **custodial** exchange and therefore **not verifiable**.