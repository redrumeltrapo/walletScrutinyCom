---
wsId: 
title: "Alpha Wallet by Ard"
altTitle: 
authors:

users: 10000
appId: com.bastionpay.alphawallet
released: 2019-05-12
updated: 2019-09-04
version: "v2.0.5"
stars: 3.8
ratings: 41
reviews: 14
size: 17M
website: 
repository: 
issue: 
icon: com.bastionpay.alphawallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-25
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


