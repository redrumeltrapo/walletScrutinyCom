---
wsId: 
title: "CardWallet - Cardano Crypto Wallet"
altTitle: 
authors:

users: 5000
appId: fi.cardwallet.android
released: 2021-08-31
updated: 2021-10-12
version: "1.4"
stars: 4.6
ratings: 123
reviews: 69
size: 12M
website: 
repository: 
issue: 
icon: fi.cardwallet.android.png
bugbounty: 
verdict: wip
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


