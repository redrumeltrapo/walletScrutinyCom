---
wsId: 
title: "BitMeet Peer to Peer Crypto Trading"
altTitle: 
authors:
- leo
users: 100
appId: com.goodbarber.bitmeat
released: 2018-01-09
updated: 2018-01-10
version: "1.0"
stars: 2.2
ratings: 6
reviews: 4
size: 13M
website: 
repository: 
issue: 
icon: com.goodbarber.bitmeat.png
bugbounty: 
verdict: defunct
date: 2021-10-30
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-21**: This app is no more.

