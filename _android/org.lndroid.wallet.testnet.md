---
wsId: 
title: "Lndroid.Wallet Testnet"
altTitle: 
authors:

users: 50
appId: org.lndroid.wallet.testnet
released: 2020-03-23
updated: 2020-04-14
version: "0.1.6-testnet"
stars: 0.0
ratings: 
reviews: 
size: 15M
website: 
repository: 
issue: 
icon: org.lndroid.wallet.testnet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


