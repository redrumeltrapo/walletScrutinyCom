---
wsId:
title: "TechPay Exchange - Cryptocurrency Trading App"
altTitle:
authors:
- danny
users: 1000
appId: crypto.base.baseexchange
released: 2019-06-05
updated: 2021-09-20
version: "4.1"
stars: 4.2
ratings: 134
reviews: 66
size: 24M
website:
repository:
issue:
icon: crypto.base.baseexchange.png
bugbounty:
verdict: custodial
date: 2021-11-10
signer:
reviewArchive:


providerTwitter: techpay_io
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---


## App Description

> - Execute trades in several pairs
> - Deposit and withdraw crypto assets

This is a cryptocurrency exchange.

## Verdict

Although the service doesn't seem to have a Terms and Conditions page, AML/KYC and other regulatory documentation, cryptocurrency exchanges that don't claim otherwise, usually are **custodial** and their apps **cannot be verified**.
