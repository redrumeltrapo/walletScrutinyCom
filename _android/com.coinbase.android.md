---
wsId: coinbaseBSB
title: "Coinbase: Buy BTC, Ethereum, SHIB, Bitcoin Cash"
altTitle: 
authors:
- leo
users: 10000000
appId: com.coinbase.android
released: 2013-03-01
updated: 2021-11-08
version: "9.48.4"
stars: 4.4
ratings: 588116
reviews: 164499
size: Varies with device
website: https://coinbase.com
repository: 
issue: 
icon: com.coinbase.android.jpg
bugbounty: 
verdict: custodial
date: 2021-10-12
signer: 
reviewArchive:


providerTwitter: coinbase
providerLinkedIn: coinbase
providerFacebook: Coinbase
providerReddit: CoinBase

redirect_from:
  - /coinbase/
  - /com.coinbase.android/
  - /posts/2019/10/coinbase/
  - /posts/com.coinbase.android/
---


{{ page.title }}, not to be confused with
{% include walletLink.html wallet='android/org.toshi' verdict='true' %}
is one of the top Bitcoin "wallets" on Google Play but beyond the
name, nothing indicates this app to be an actual wallet.

Historically Coinbase was an exchange and like almost all exchanges, they
allowed to hold Bitcoins in trading accounts. Later the Android app was released
and called a "wallet".

(On another historical note, Brian Armstrong, a co-founder of Coinbase did release
an [actual Bitcoin Wallet](https://github.com/barmstrong/bitcoin-android) back
[in June 2011](https://thenextweb.com/mobile/2011/07/06/bitcoin-payments-go-mobile-with-bitcoin-for-android/).
It was open source and downloaded the full blockchain to your phone.)

As the wallet setup does not involve a way to backup private keys, we assume those
private keys are under the sole control of Coinbase, making it
a custodial wallet or non-wallet.

Verdict: This app is **not verifiable**.

**Note:** [Coinbase.com](https://www.coinbase.com/) does link to this app. It
does also link to a [Wallet page](https://wallet.coinbase.com/) that links to
{% include walletLink.html wallet='android/org.toshi' verdict='true' %}
