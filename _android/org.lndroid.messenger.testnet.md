---
wsId: 
title: "Lndroid.Messenger Testnet"
altTitle: 
authors:

users: 10
appId: org.lndroid.messenger.testnet
released: 2020-03-23
updated: 2020-03-23
version: "0.1.3"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: org.lndroid.messenger.testnet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


