---
wsId: 
title: "Coinchat"
altTitle: 
authors:

users: 100000
appId: com.coinchat.coinchat
released: 2018-05-20
updated: 2020-03-11
version: "2.0.2"
stars: 2.9
ratings: 1186
reviews: 747
size: 30M
website: 
repository: 
issue: 
icon: com.coinchat.coinchat.png
bugbounty: 
verdict: stale
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


