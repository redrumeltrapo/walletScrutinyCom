---
wsId: 101Investing
title: "101Investing Online Trading | Forex and Stocks"
altTitle: 
authors:
- danny
users: 10000
appId: com.brand101investing
released: 
updated: 2021-08-25
version: "1.5.69-brand101investing"
stars: 3.7
ratings: 254
reviews: 205
size: 19M
website: https://www.101investing.com
repository: 
issue: 
icon: com.brand101investing.png
bugbounty: 
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: 101Investing_eu
providerLinkedIn: 73520735
providerFacebook: 101Investing
providerReddit: 

redirect_from:

---


## App Description

Also affiliated with MetaTrader 4, 101investing allows users to:

> trade 350+ financial derivatives on Forex, cryptocurrencies, stocks, indices, and commodities.

## The Site

[Cryptocurrency trading](https://www.101investing.com/en/cryptocurrency-trading) is facilitated via CFDs

> CFDs on bitcoin investment and 50+ most popular cryptocurrencies. No need to own, mine, or buy any of those.

[Funding](https://www.101investing.com/en/faq) is made via credit or debit card, bank transfer or e-wallet.

Withdrawals can be done using the same method as funding.

## Verdict

This service **does not allow the user to send or receive bitcoins**. 
