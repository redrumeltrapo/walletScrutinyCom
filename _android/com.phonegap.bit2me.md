---
wsId: bit2me
title: "Bit2Me - Buy and Sell Bitcoin"
altTitle: 
authors:
- leo
users: 100000
appId: com.phonegap.bit2me
released: 2015-01-08
updated: 2021-11-04
version: "2.1.12"
stars: 4.6
ratings: 3508
reviews: 1125
size: 58M
website: https://bit2me.com
repository: 
issue: 
icon: com.phonegap.bit2me.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: bit2me
providerLinkedIn: bit2me
providerFacebook: bit2me
providerReddit: 

redirect_from:
  - /com.phonegap.bit2me/
  - /posts/com.phonegap.bit2me/
---


This appears to be the interface for an exchange. We could not find any claims
about you owning your keys. As a custodial service it is **not verifiable**.
