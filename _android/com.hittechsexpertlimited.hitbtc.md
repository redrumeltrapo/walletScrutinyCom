---
wsId: 
title: "HitBTC – Cryptocurrency Exchange & Trading BTC App"
altTitle: 
authors:
- leo
users: 100000
appId: com.hittechsexpertlimited.hitbtc
released: 2020-03-04
updated: 2021-04-12
version: "3.0.10"
stars: 3.9
ratings: 3591
reviews: 1571
size: 11M
website: https://hitbtc.com
repository: 
issue: 
icon: com.hittechsexpertlimited.hitbtc.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: hitbtc
providerLinkedIn: 
providerFacebook: hitbtc
providerReddit: hitbtc

redirect_from:
  - /com.hittechsexpertlimited.hitbtc/
  - /posts/com.hittechsexpertlimited.hitbtc/
---


On Google Play this app claims

> **High-Level Security**
  Don’t let anybody sneak into your trade: account access is strictly via API
  keys and PIN-code. Plus, advanced encryption technology and highly protected
  cold storage.

Which means it is a custodial service and thus **not verifiable**.
