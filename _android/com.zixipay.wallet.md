---
wsId: 
title: "ZixiPay - Tether Wallet, Bitcoin Wallet"
altTitle: 
authors:

users: 1000
appId: com.zixipay.wallet
released: 2019-12-18
updated: 2021-10-27
version: "1.75"
stars: 4.3
ratings: 27
reviews: 8
size: 14M
website: 
repository: 
issue: 
icon: com.zixipay.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


