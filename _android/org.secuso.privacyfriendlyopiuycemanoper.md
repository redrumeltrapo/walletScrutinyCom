---
wsId: 
title: "Bitcoin wallet"
altTitle: "Bitcoin wallet by Mr. Bald"
authors:
- leo
users: 10
appId: org.secuso.privacyfriendlyopiuycemanoper
released: 
updated: 2021-02-22
version: "2.0.3"
stars: 0.0
ratings: 
reviews: 
size: 13M
website: 
repository: 
issue: 
icon: org.secuso.privacyfriendlyopiuycemanoper.png
bugbounty: 
verdict: defunct
date: 2021-05-11
signer: 
reviewArchive:
- date: 2021-04-13
  version: "2.0.3"
  appHash: 
  gitRevision: 6849790cf3f18653fbe1116b54693fec1419d0ca
  verdict: fewusers


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app disappeared from the Play Store before reaching 1000 downloads.