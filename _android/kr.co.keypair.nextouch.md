---
wsId: 
title: "NExTouch"
altTitle: 
authors:

users: 1000
appId: kr.co.keypair.nextouch
released: 2018-10-01
updated: 2019-07-22
version: "1.0.0.57"
stars: 3.2
ratings: 10
reviews: 6
size: 12M
website: http://www.eunex.co
repository: 
issue: 
icon: kr.co.keypair.nextouch.png
bugbounty: 
verdict: obsolete
date: 2021-10-01
signer: 
reviewArchive:
- date: 2021-03-07
  version: "1.0.0.57"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nosource

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


As far as we can see, this is the same as
{% include walletLink.html wallet='android/kr.co.keypair.keywalletTouch' %} and thus is **not verifiable**.
