---
wsId: 
title: "Crypto Wallet 2020"
altTitle: 
authors:
- leo
users: 10000
appId: com.thinkdevs.cryptomarket
released: 2018-03-29
updated: 2021-09-24
version: "0.0.11"
stars: 3.7
ratings: 136
reviews: 77
size: 5.9M
website: 
repository: 
issue: 
icon: com.thinkdevs.cryptomarket.png
bugbounty: 
verdict: defunct
date: 2021-10-06
signer: 
reviewArchive:
- date: 2020-12-14
  version: "0.0.11"
  appHash: 
  gitRevision: 390ad12adfd0448e851c0112bc5cc9c2a11698b4
  verdict: nowallet
  

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.thinkdevs.cryptomarket/
---


**Update 2021-09-28**: This app is not on the Store anymore.

This is not a wallet:

> Crypto Wallet gives you quick and easy access to cryptocurrency prices, details.
