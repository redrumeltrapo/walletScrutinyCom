---
wsId: 
title: "Jaxx Liberty Bitcoin Wallet"
altTitle: "(Fake) Jaxx Liberty Bitcoin Wallet"
authors:
- emanuel
- leo
users: 100
appId: com.pjaxx.walletmudul
released: 2021-07-05
updated: 2021-07-05
version: "1"
stars: 0.0
ratings: 
reviews: 
size: 10M
website: 
repository: 
issue: 
icon: com.pjaxx.walletmudul.png
bugbounty: 
verdict: defunct
date: 2021-08-04
signer: 
reviewArchive:
- date: 2021-07-24
  version: "1"
  appHash: 
  gitRevision: 8770b9bdf24645be6c837baa253b07d13a2b2ce8
  verdict: fake

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-04**: This fake app was gone 2 days after our fake verdict on
2021-07-26.


This app "{{ page.title }}" clearly tries to imitate
{% include walletLink.html wallet='android/com.liberty.jaxx' verdict='true' %}.
