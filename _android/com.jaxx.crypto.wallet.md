---
wsId: 
title: "Ja­xx Lib­erty: Crypto Wallet"
altTitle: "(Fake) Ja­xx Lib­erty: Crypto Wallet"
authors:
- leo
users: 50
appId: com.jaxx.crypto.wallet
released: 2021-10-11
updated: 2021-10-12
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 19M
website: 
repository: 
issue: 
icon: com.jaxx.crypto.wallet.png
bugbounty: 
verdict: defunct
date: 2021-10-30
signer: 
reviewArchive:
- date: 2021-10-18
  version: "1.0"
  appHash: 
  gitRevision: 70bdd6a4573c03410f6563e850ff522f756f5fdc
  verdict: fake


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-23**: This app is no more.

This is obviously an imitation of
{% include walletLink.html wallet='android/com.liberty.jaxx' verdict='true' %}.
