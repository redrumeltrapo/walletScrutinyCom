---
wsId: XCOEX
title: "XCOEX: Cryptocurrency Wallet"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.xcoex.mobile
released: 2019-01-17
updated: 2021-10-22
version: "1.20.0"
stars: 3.6
ratings: 158
reviews: 127
size: 37M
website: https://xcoex.com/
repository: 
issue: 
icon: com.xcoex.mobile.png
bugbounty: 
verdict: custodial
date: 2021-05-04
signer: 
reviewArchive:


providerTwitter: OfficialXcoex
providerLinkedIn: 
providerFacebook: xcoex
providerReddit: 

redirect_from:

---


This is an exchange based app wallet, meaning it is mainly developed to manage trading on an exchange.
The exchange provider typically stores users bitcoins, partly in cold storage, partly hot.

This leads us to conclude the wallet funds are in control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
