---
wsId: 
title: "Beehives Wallet - Bitcoin Crypto Wallet"
altTitle: 
authors:

users: 1000
appId: tech.vontech.blockchain.jetoken
released: 2019-04-10
updated: 2019-06-21
version: "1.0.12"
stars: 4.6
ratings: 28
reviews: 12
size: 7.6M
website: 
repository: 
issue: 
icon: tech.vontech.blockchain.jetoken.png
bugbounty: 
verdict: defunct
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-09**: This app is no more.
