---
wsId: LiquidPro
title: "Liquid Pro"
altTitle: 
authors:
- danny
users: 50000
appId: com.quoine.liquid
released: 2019-01-10
updated: 2021-11-09
version: "1.17.0"
stars: 3.0
ratings: 657
reviews: 404
size: 60M
website: https://www.liquid.com/
repository: 
issue: 
icon: com.quoine.liquid.png
bugbounty: 
verdict: custodial
date: 2021-10-13
signer: 
reviewArchive:


providerTwitter: Liquid_Global
providerLinkedIn: quoine
providerFacebook: LiquidGlobal
providerReddit: 

redirect_from:

---


## App Description
From Google Play:

> With access to over 100 fiat and crypto currencies, take control and enjoy the full power of Liquid right from your pocket using Liquid Pro.

Liquid Pro is a **licensed exchange**, as quoted below:

> Liquid was the first cryptocurrency exchange licensed to operate in Japan, and we are embarking on becoming one of the first licensed operators in Singapore.

## The Site
[From About Us: Security Measures](liquid.com/company/#company-security):

> We manage digital assets using a combination of cold wallet & Multi-party computation (MPC) technology.

Additionally, [the blog also states](https://blog.liquid.com/how-we-make-liquid-a-secure-cryptocurrency-exchange) that Liquid Pro keeps funds in cold storage.

> On Liquid, 100% of customer funds are stored in cold storage, essentially making Liquid a form of cryptocurrency custody solution. 

## The App
To register to the app, users must enable 2-FA. Before you can fund your account, it is mandatory to verify it first with a Selfie & Identity Document.

## Verdict
This app is clearly **custodial** and thus **not verifiable.**
