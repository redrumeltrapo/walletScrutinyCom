---
wsId:
title: "Bitcoin Wallet & BNB, BEP20, ERC20 Tokens Wallet"
altTitle:
authors:
- danny
users: 5000
appId: com.bitcoin.wallet.cryptopuce
released: 2021-03-07
updated: 2021-05-20
version: "1.2"
stars: 3.7
ratings: 45
reviews: 21
size: 4.0M
website: https://cryptopuce.com/
repository:
issue:
icon: com.bitcoin.wallet.cryptopuce.png
bugbounty:
verdict: nosource
date: 2021-11-10
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---


## App Description

> **Receive, send, store and exchange bitcoin, erc20 tokens and BEP20 tokens Instantly and securely. Cash out or withdraw bitcoin to any debit card Visa or Mastercard or to any bank account.**
>
> - Multi-currency non-custodial and ERC20 & BEP20 tokens wallets
> - Become your own Bank. Take 100% Control of your Funds.
> - Receive, Send, exchange and store crypto, BTC, bitcoin, erc20 tokens and BEP20 tokens securely!​ Transfer from one wallet to another within seconds.

## The Site

Included in the service are:

- Options to buy BTC with VISA and Mastercard
- Avail a Bitcoin debit card
- Buy, sell and exchange
- Non-custodial app
- No sign up required

## Verdict

We were able to test the app, and true enough, the initial setup provided options for creating a bitcoin wallet or restoring a wallet. This wallet had the ability to send or receive. However, we were **not able to find links to the source code on its homepage or [in any GitHub repository](https://github.com/search?q=com.bitcoin.wallet.cryptopuce&type=code)**. 
