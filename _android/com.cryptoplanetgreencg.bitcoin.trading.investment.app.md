---
wsId: 
title: "Bitcoin Crypto Trading Investment"
altTitle: 
authors:

users: 10
appId: com.cryptoplanetgreencg.bitcoin.trading.investment.app
released: 2021-07-28
updated: 2021-07-28
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: com.cryptoplanetgreencg.bitcoin.trading.investment.app.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
