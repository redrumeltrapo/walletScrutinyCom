---
wsId: 
title: "F1EX"
altTitle: 
authors:

users: 50
appId: com.foxone.exchange
released: 2019-03-18
updated: 2019-05-20
version: "1.6.2"
stars: 0.0
ratings: 
reviews: 
size: 8.4M
website: 
repository: 
issue: 
icon: com.foxone.exchange.png
bugbounty: 
verdict: defunct
date: 2021-10-15
signer: 
reviewArchive:
providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-07**: This app is not on Play Store anymore.

