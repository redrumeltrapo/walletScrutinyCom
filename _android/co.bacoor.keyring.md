---
wsId: keyring
title: "KEYRING PRO | Multichain Wallet Connect ETH, BSC"
altTitle: 
authors:

users: 1000
appId: co.bacoor.keyring
released: 2021-01-21
updated: 2021-09-13
version: "1.6.0"
stars: 4.6
ratings: 33
reviews: 20
size: 52M
website: 
repository: 
issue: 
icon: co.bacoor.keyring.png
bugbounty: 
verdict: wip
date: 2021-03-03
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


