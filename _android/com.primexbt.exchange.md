---
wsId: 
title: "PrimeXBT Trade"
altTitle: 
authors:

users: 100000
appId: com.primexbt.exchange
released: 2019-05-08
updated: 2020-07-16
version: "144.5.0-release"
stars: 2.2
ratings: 625
reviews: 470
size: 9.5M
website: 
repository: 
issue: 
icon: com.primexbt.exchange.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


