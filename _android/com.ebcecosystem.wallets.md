---
wsId: 
title: "EBC Wallet"
altTitle: 
authors:
- leo
users: 1000
appId: com.ebcecosystem.wallets
released: 2018-12-07
updated: 2019-01-30
version: "0.0.7"
stars: 4.7
ratings: 97
reviews: 61
size: 11M
website: https://ebc.eco
repository: 
issue: 
icon: com.ebcecosystem.wallets.png
bugbounty: 
verdict: defunct
date: 2021-11-10
signer: 
reviewArchive:
- date: 2019-12-28
  version: "0.0.7"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: custodial

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.ebcecosystem.wallets/
  - /posts/com.ebcecosystem.wallets/
---


**Update 2021-10-26**: This app is no more.

The description of this app talks about how many coins it supports and mentions
of KYC and give the impression it is a custodial service. Their website is
currently down:

Cloudflare says:
> Error 521 Ray ID: 54c2d0873f95d64d • 2019-12-28 10:22:41 UTC
  Web server is down

Absent further information we have to assume this is a custodial service.

Our verdict: **not verifiable**.
