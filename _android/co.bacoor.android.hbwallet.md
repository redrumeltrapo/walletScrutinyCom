---
wsId: bacoorhbwallet
title: "Ethereum Wallet | HB Wallet – DeFi & Buy BTC, ETH"
altTitle: 
authors:
- danny
users: 100000
appId: co.bacoor.android.hbwallet
released: 2017-07-17
updated: 2021-07-08
version: "3.5.0"
stars: 3.9
ratings: 2101
reviews: 902
size: 71M
website: https://www.hb-wallet.com
repository: 
issue: 
icon: co.bacoor.android.hbwallet.png
bugbounty: 
verdict: nobtc
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: HBWallet_Ether
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


We downloaded the app. This is an Ethereum wallet meant for Ethereum transactions. It is possible to buy Bitcoin, but through a third party provider. **No Bitcoins** are stored on this wallet.

