---
wsId: 
title: "BlackFort Wallet & Exchange"
altTitle: 
authors:

users: 1000
appId: exchange.blackfort.wallet
released: 2020-08-31
updated: 2021-10-27
version: "1.3.18"
stars: 4.2
ratings: 62
reviews: 34
size: 17M
website: 
repository: 
issue: 
icon: exchange.blackfort.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


