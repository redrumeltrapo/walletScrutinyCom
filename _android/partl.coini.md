---
wsId: Coini
title: "Coini — Cryptocurrencies"
altTitle: 
authors:
- danny
users: 10000
appId: partl.coini
released: 2018-02-04
updated: 2021-11-04
version: "2.3.5"
stars: 4.6
ratings: 252
reviews: 126
size: 46M
website: https://timopartl.com
repository: 
issue: 
icon: partl.coini.png
bugbounty: 
verdict: nowallet
date: 2020-12-14
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /partl.coini/
---


This app is for portfolio tracking but probably is not in control of private keys.
