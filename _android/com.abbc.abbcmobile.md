---
wsId: 
title: "Aladdin Wallet"
altTitle: 
authors:

users: 10000
appId: com.abbc.abbcmobile
released: 2019-08-08
updated: 2020-03-24
version: "1.3.3"
stars: 4.0
ratings: 628
reviews: 422
size: 4.5M
website: 
repository: 
issue: 
icon: com.abbc.abbcmobile.png
bugbounty: 
verdict: stale
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


