---
wsId: 
title: "Exnovin - اکس نوین | بازار معاملاتی رمزارزها"
altTitle: 
authors:
- danny
users: 50000
appId: io.exnovin.app
released: 2020-12-12
updated: 2021-10-24
version: "2.3.0"
stars: 4.1
ratings: 702
reviews: 267
size: 37M
website: https://exnovin.io/
repository: 
issue: 
icon: io.exnovin.app.png
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: Exnovin_io
providerLinkedIn: exnovin
providerFacebook: exnovin.io
providerReddit: 

redirect_from:

---


There are two apps with the same name of {% include walletLink.html wallet='com.exnovin' verdict='true' %}. Both appear to be created by the same developer [Team App E](https://play.google.com/store/apps/developer?id=Team+App+E). Both are Iranian. However, both have different website domains. This specific app links to [exnovin.io](https://exnovin.io).

This is an exchange, with [terms and conditions](https://exnovin.io/terms/). 

> This contract is based on Article 10 of the Civil Code, and subject to all laws and regulations of the Islamic Republic of Iran in general and the Electronic Commerce Law approved in 2003 and the relevant regulations and the Computer Crimes Law in particular,

Depositing BTC is made available once email verification is completed. Like most centralized exchanges there are various kinds of user tiers that depend on the amount of verification the user complies with. This can include:

- Mobile number verification
- Bank account verification
- National card verification
- Confirmation of fixed number and address

We chatted with their on site support to verify. This is their reply:

> جهت تضمین معاملات انجام شده، کلید خصوصی کیف پول ها در اختیار صرافی هست<br>
To guarantee the transactions, the private key of the wallets is in the possession of the exchange

This is a **custodial** app, therefore it is **not verifiable.**
