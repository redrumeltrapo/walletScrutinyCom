---
wsId: Burency
title: "Burency Global – Insured Crypto Exchange & Wallet"
altTitle: 
authors:
- danny
users: 10000
appId: com.burency.app
released: 2021-03-21
updated: 2021-05-07
version: "1.0.2"
stars: 4.4
ratings: 230
reviews: 130
size: 22M
website: https://www.burency.com/
repository: 
issue: 
icon: com.burency.app.png
bugbounty: 
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: BurencyOfficial
providerLinkedIn: burencyofficial
providerFacebook: BurencyOfficial
providerReddit: Burency

redirect_from:

---


## App Description

Describes itself as a regulated cryptocurrency exchange

## The Site

### Terms and Conditions

Section 11 focuses on account termination,

> You agree that Burency shall have **the right to immediately suspend your account** (and any accounts beneﬁcially owned by related entities or aﬃliates), freeze or lock the funds in all such accounts, and suspend your access to Burency for any reason including if it suspects any such accounts to be in violation of these Terms, our Privacy Policy, or any applicable laws and regulations.

## The App

We downloaded the app, but was unable to find any feature that allows the export of private keys.

## Verdict

Any app that has the power to suspend, freeze or lock accounts is **custodial** and **not verifiable.**

