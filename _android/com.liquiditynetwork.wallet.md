---
wsId: 
title: "Liquidity Wallet - A Significantly Better Account"
altTitle: 
authors:

users: 10000
appId: com.liquiditynetwork.wallet
released: 2018-06-07
updated: 2020-07-16
version: "1.27.2"
stars: 4.1
ratings: 56
reviews: 27
size: 8.1M
website: 
repository: 
issue: 
icon: com.liquiditynetwork.wallet.png
bugbounty: 
verdict: stale
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


