---
wsId: 
title: "Yes Wallet"
altTitle: 
authors:
- leo
users: 5000
appId: com.yes.yeswallet
released: 2019-04-24
updated: 2019-04-24
version: "1.0"
stars: 2.8
ratings: 45
reviews: 31
size: 1.5M
website: http://www.yeswallet.io
repository: 
issue: 
icon: com.yes.yeswallet.png
bugbounty: 
verdict: defunct
date: 2021-06-18
signer: 
reviewArchive:
- date: 2020-12-14
  version: "1.0"
  appHash: 
  gitRevision: 612e60ecd2013c802012d1c553a2ff8b56004226
  verdict: custodial

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.yes.yeswallet/
---


This app ~~is~~was **very likely a scam** judging by the reviews and the website being
down and the last update being almost two years old.

For what we know, we know little but the coins are probably under the control of
the provider, so the app is also **not verifiable**.
