---
wsId: 
title: "HitAPI"
altTitle: 
authors:

users: 10000
appId: net.benoitbasset.hitapi
released: 2018-02-07
updated: 2019-05-07
version: "1.3.0"
stars: 4.0
ratings: 143
reviews: 60
size: 4.7M
website: 
repository: 
issue: 
icon: net.benoitbasset.hitapi.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


