---
wsId: 
title: "LiteForex: investing, trading"
altTitle: 
authors:
- danny
users: 100000
appId: com.ittrendex.liteforex
released: 2017-10-23
updated: 2021-01-29
version: "1.4.8"
stars: 4.0
ratings: 897
reviews: 415
size: 4.2M
website: https://www.liteforex.com/
repository: 
issue: 
icon: com.ittrendex.liteforex.png
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: litefinanceeng
providerLinkedIn: litefinance-official
providerFacebook: LiteFinancebroker
providerReddit: 

redirect_from:

---


> LiteForex: Investments and Trading is a high-tech Forex app developed by LiteForex specialists that has the most user-friendly interface for online trading and investing on financial markets. 

The FAQ had some information on withdrawing funds, although there was no clear tutorials.

There are different levels of verification required for different types of withdrawals.

Due to a lack of more information, we can assume this app is **custodial** and as such **not verifiable.**
