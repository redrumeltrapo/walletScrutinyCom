---
wsId: PilotTrading
title: "Pilot Trading"
altTitle: 
authors:
- danny
users: 5000
appId: com.pilotmobiletrading
released: 2019-06-09
updated: 2021-09-13
version: "6.02.325"
stars: 3.3
ratings: 62
reviews: 36
size: 29M
website: https://pilottrading.co/
repository: 
issue: 
icon: com.pilotmobiletrading.png
bugbounty: 
verdict: nowallet
date: 2021-11-08
signer: 
reviewArchive:


providerTwitter: tradewithpilot
providerLinkedIn: 
providerFacebook: tradewithpilot
providerReddit: 

redirect_from:

---


## App Description

> Pilot **simplifies active trading,** providing you with the most relevant trading opportunities powered by artificial intelligence and Real Time Sentiment, in an intuitive and easy to use mobile experience. With Pilot, you can paper trade, see live signals and also trade live by connecting to your existing brokerage account.

Pilot says that it is a trading app while at the same time providing real time signals for traders.

## The Site

It's vague on whether you can actually hold bitcoin assets here or not.

We find a page on cryptocurrencies.

> The list below includes all of the currently supported cryptocurrency exchanges on Pilot for real time alerts **(in-app trading coming soon)**. 

## The App

We tried the app. Users have two options in creating an account. There is a Demo option and a Live option.

We could not find a way to access any cryptocurrency wallet or trade crypto.

## Verdict

At the moment, this app cannot trade crypto assets. **It cannot function as a bitcoin wallet or exchange platform.**
