---
wsId: currencycominvesting
title: "Currency.com: Investing"
altTitle: 
authors:
- danny
users: 100000
appId: com.currency.exchange.investsmart
released: 2020-12-10
updated: 2021-07-20
version: "1.14.0"
stars: 4.3
ratings: 204
reviews: 74
size: Varies with device
website: https://currency.com/
repository: 
issue: 
icon: com.currency.exchange.investsmart.png
bugbounty: 
verdict: defunct
date: 2021-10-06
signer: 
reviewArchive:
- date: 2021-09-11
  version: "1.14.0"
  appHash: 
  gitRevision: 390ad12adfd0448e851c0112bc5cc9c2a11698b4
  verdict: custodial

providerTwitter: currencycom
providerLinkedIn: 
providerFacebook: currencycom
providerReddit: currencycom

redirect_from:

---


**Update 2021-09-29**: This app is not on the Store anymore.

It is necessary to distinguish this specific app from another related app from the same company. "Currency.com Investing" is not the same app as {% include walletLink.html wallet='android/com.currency.exchange.prod2' verdict='true' %}. Although, they link to each other via Google Play and in their respective domains, we'll treat them separately to avoid confusion. 

The app on Google Play references currency.com but has expcapital.com as its domain, when clicking on View Website.

We downloaded the app and after registering, we chose bitcoin as a currency and the app made a wallet address where it is possible to deposit after some account verification. 

Currency.com's [Deposits/Withdrawals Page](https://currency.com/deposits-withdrawals)

This is a **custodial** service and thus **not verifiable**.

