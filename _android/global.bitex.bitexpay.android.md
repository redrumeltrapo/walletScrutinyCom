---
wsId: 
title: "BitexPay"
altTitle: 
authors:

users: 500
appId: global.bitex.bitexpay.android
released: 2020-09-10
updated: 2020-11-15
version: "1.1.0"
stars: 4.1
ratings: 13
reviews: 7
size: 7.3M
website: 
repository: 
issue: 
icon: global.bitex.bitexpay.android.png
bugbounty: 
verdict: fewusers
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


