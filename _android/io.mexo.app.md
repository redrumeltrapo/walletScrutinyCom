---
wsId: Mexo
title: "Mexo.io - - Bitcoin, Cripto & Más"
altTitle: 
authors:
- danny
users: 10000
appId: io.mexo.app
released: 2020-08-30
updated: 2021-10-28
version: "2.0.0"
stars: 3.8
ratings: 345
reviews: 280
size: 49M
website: http://mexo.io/
repository: 
issue: 
icon: io.mexo.app.png
bugbounty: 
verdict: custodial
date: 2021-10-04
signer: 
reviewArchive:


providerTwitter: mexo_io
providerLinkedIn: mexoio
providerFacebook: mexo.io
providerReddit: 

redirect_from:

---


### App Description
The app description appears to be entirely in Spanish. Quotes below are taken from Google Translate.

From the app description:

> Listed among the top 30 exchanges on Coinmaketcap. Mexo is the most complete and reliable cryptocurrency exchange in Latin America, with more than 70 cryptocurrency assets and more than 60 trading pairs, including the DeFi and NFT sections. The Mexo app provides comprehensive information and tools for users to trade on the go.

Mexo has the interface of an exchange and states that it is among the top 30 on Coinmarketcap.

In Google Play, a lot of reviewers report delays in the verification process. As of the date of this writing, the overall Google Play rating is just 1.9.

> [Firoz Kureshi](https://play.google.com/store/apps/details?id=io.mexo.app&reviewId=gp%3AAOqpTOHfsOALOjnZzu3w992TM9z-S9h9_sUmopfbv-pAlD5vJJZUbziGl3bOVFZnehO5pZlX8dQ_3BgavlWh90Y)<br>
  ★★☆☆☆ September 12, 2021 <br>
  Verification process takes so much time , from 4 days still my verification is in progress, not completed .
       
### The Site
Mexo has [a blog post](https://blog.mexo.io/how-to-buy-bitcoin/) directly stating that it is a custodial platform.

> Most cryptocurrency exchanges, __including Mexo__, allow you to securely custody Bitcoin on their platform after purchase.  If you are not familiar with how to store your own private key or have zero experience in Bitcoin wallet, then it could be easier for you to store your asset on a trustable exchange.

### The App
We downloaded this app to view the features. It is possible to send and receive crypto using a BTC address or QR code. Users may trade bitcoin from the markets. 

There is no option to export private keys as it is a custodial service.

### Verdict
This app is **custodial** therefore **not verifiable.**


