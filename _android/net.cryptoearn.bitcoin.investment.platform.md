---
wsId: 
title: "Bitcoin Investment Platform"
altTitle: 
authors:

users: 10
appId: net.cryptoearn.bitcoin.investment.platform
released: 2021-06-29
updated: 2021-06-29
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: net.cryptoearn.bitcoin.investment.platform.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
