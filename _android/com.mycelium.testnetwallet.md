---
wsId: 
title: "Mycelium Testnet Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.mycelium.testnetwallet
released: 2013-10-04
updated: 2021-09-23
version: "3.11.0.9-TESTNET"
stars: 3.7
ratings: 139
reviews: 73
size: 25M
website: 
repository: 
issue: 
icon: com.mycelium.testnetwallet.png
bugbounty: 
verdict: nobtc
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is the testnet version of {% include walletLink.html wallet='android/com.mycelium.wallet' verdict='true' %}