---
wsId: btcc
title: "BTCC - Trade Bitcoin & Crypto Futures"
altTitle: 
authors:
- danny
users: 100000
appId: com.btcc.hy
released: 2020-08-04
updated: 2021-11-04
version: "5.6.3"
stars: 4.9
ratings: 10983
reviews: 143
size: 49M
website: https://www.btcc.com/
repository: 
issue: 
icon: com.btcc.hy.png
bugbounty: 
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: YourBTCC
providerLinkedIn: yourbtcc
providerFacebook: yourbtcc
providerReddit: YourBTCC

redirect_from:

---


In its Google Play description:

>The BTCC app allows you to register, deposit and trade within 30 seconds. It further allows you to enjoy trading cryptocurrency futures anytime, anywhere.
In addition to the major currencies such as BTC, ETH, BCH, LTC, ADA, EOS, XRP,
LINK, DOT, UNI, DASH, DOGE, FIL, XLM are also available. Leverage is as much as 150 times your total capital.

Registration was quick and we were able to access the "Deposit" function with the minimum requirements. True enough, a wallet address was generated but with no indication of where the private key might be stored. As this is a centralized exchange, we can assume that it is custodial.

Its [Terms and Conditions](https://www.btcc.com/detail/142638.html) require AML, KYC, CTF requirements as well as several forms of ID verification. 

>Each user must apply for registering a BTCC account before using BTCC services. When registering for a BTCC account, you must provide your true name, email and password, and accept these Terms, privacy policy and other BTCC platform rules. BTCC may, at its sole discretion, refuse to open a BTCC account for you.

Interestingly, this exchange also trades in [cryptocurrency CFDs](https://www.btcc.com/blog/trade-cryptocurrency-with-cfds/). Under normal circumstances, we label exchanges with this function as not having the ability to send or receive bitcoins or other cryptocurrencies. However, since we tested the platform and found that it *can* send and receive cryptocurrencies, we deem this to be a **custodial** service and thus **not verifiable**.