---
wsId: 
title: "Vex Block Wallet"
altTitle: 
authors:

users: 5000
appId: co.vexblock.android
released: 2019-09-17
updated: 2019-10-15
version: "1.0.3"
stars: 3.6
ratings: 189
reviews: 173
size: 20M
website: 
repository: 
issue: 
icon: co.vexblock.android.png
bugbounty: 
verdict: obsolete
date: 2021-10-04
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


