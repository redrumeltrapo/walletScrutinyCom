---
wsId: 
title: "OTPPAY - Crypto Exchange & Merchant Payments"
altTitle: 
authors:

users: 1000
appId: com.idbtec.otppaycrypto
released: 2018-08-13
updated: 2020-04-01
version: "1.21"
stars: 4.6
ratings: 91
reviews: 58
size: 6.4M
website: 
repository: 
issue: 
icon: com.idbtec.otppaycrypto.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


