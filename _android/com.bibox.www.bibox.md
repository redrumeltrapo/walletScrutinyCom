---
wsId: Bibox
title: "Bibox:Crypto Exchange,Bitcoin"
altTitle: 
authors:
- danny
users: 50000
appId: com.bibox.www.bibox
released: 2019-03-24
updated: 2021-11-04
version: "4.8.5"
stars: 5.0
ratings: 1251
reviews: 571
size: 47M
website: https://www.bibox.com
repository: 
issue: 
icon: com.bibox.www.bibox.png
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: Bibox365
providerLinkedIn: biboxexchange
providerFacebook: Bibox2017
providerReddit: Bibox

redirect_from:

---


From its Google Play description:

> Bibox, AI-enhanced digital asset exchange, is one of the world's best cryptocurrency exchanges now. Bibox provides token exchange, contracts, bonds and margin trading with high liquidity and market depth.

Our initial assessment indicates that this is a centralized exchange.

We downloaded the app and found that it is possible to send and receive BTC. However, you are not provided with the keys to the wallet. 

We emailed [Bibox support](support@bibox.zendesk.com) to further validate our findings. 

Until such time that new information arises, and with no mention of self-custody, it's presumable that this app is **custodial** and **not verifiable.**

We will update the verdict once they have replied.