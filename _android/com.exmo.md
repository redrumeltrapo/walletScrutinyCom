---
wsId: exmo
title: "EXMO: Buy & Sell Bitcoin (BTC) on Crypto Exchange"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.exmo
released: 2019-11-12
updated: 2021-11-03
version: "3.0.1"
stars: 4.1
ratings: 2372
reviews: 1609
size: 73M
website: https://exmo.com
repository: 
issue: 
icon: com.exmo.png
bugbounty: 
verdict: custodial
date: 2021-04-21
signer: 
reviewArchive:


providerTwitter: Exmo_com
providerLinkedIn: 
providerFacebook: exmo.market
providerReddit: 

redirect_from:

---


The Exmo [support FAQ](https://info.exmo.com/en/faq/) states under "Where are my EXMO funds kept?"

> Users cryptocurrency funds are stored on the exchange’s crypto wallets: cold and hot vaults.

this leads us to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
