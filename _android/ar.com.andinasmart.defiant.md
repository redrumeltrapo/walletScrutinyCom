---
wsId: 
title: "Defiant"
altTitle: 
authors:

users: 1000
appId: ar.com.andinasmart.defiant
released: 2019-11-19
updated: 2021-10-26
version: "1.2.7"
stars: 4.5
ratings: 58
reviews: 35
size: 27M
website: 
repository: 
issue: 
icon: ar.com.andinasmart.defiant.png
bugbounty: 
verdict: wip
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


