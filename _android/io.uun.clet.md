---
wsId: 
title: "Clet - Crypto Portal"
altTitle: 
authors:

users: 50000
appId: io.uun.clet
released: 2018-07-20
updated: 2019-11-11
version: "1.17.1"
stars: 2.5
ratings: 732
reviews: 360
size: 19M
website: 
repository: 
issue: 
icon: io.uun.clet.png
bugbounty: 
verdict: obsolete
date: 2021-11-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


