---
wsId: Bitget
title: "Bitget ：Crypto Trading & BitCoin Contract Platfom"
altTitle: 
authors:
- danny
users: 50000
appId: com.bitget.exchange
released: 2020-04-03
updated: 2021-11-07
version: "1.2.18"
stars: 4.9
ratings: 1212
reviews: 1098
size: 44M
website: https://www.bitget.com/
repository: 
issue: 
icon: com.bitget.exchange.png
bugbounty: 
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: bitgetglobal
providerLinkedIn: bitget
providerFacebook: BitgetGlobal
providerReddit: 

redirect_from:

---


**Update 2021-09-12**: This app appears to be back on Play Store

**Update 2021-08-05**: This app is not on Play Store anymore.

Given Bitget refers to itself in the title as a "Crypto Trading & BitCoin Contract Platfom" it is presumably custodial.

In Bitget's website, under the "Security" feature:
> Supports Dedicated/Multi-signature for Cold/Hot Wallet

Also in the Play store description:

> Protecting the safety of your assets is our top priority. The wallet we use has multiple physical isolations and supports dedicated personal multiple signatures.

This is *also* very usually an indicator that this product is **custodial** thus **not verifiable.** 

