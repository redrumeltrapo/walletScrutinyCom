---
wsId: 
title: "Moon Faucet - BTC, LTC, XDG, BCH"
altTitle: 
authors:

users: 10000
appId: g3shanappz.moonfaucet
released: 2020-06-29
updated: 2020-06-29
version: "9.8"
stars: 4.0
ratings: 616
reviews: 295
size: 8.5M
website: 
repository: 
issue: 
icon: g3shanappz.moonfaucet.jpg
bugbounty: 
verdict: defunct
date: 2021-09-16
signer: 
reviewArchive:
- date: 2021-08-17
  version: "9.8"
  appHash: 
  gitRevision: 426b03e10033c114da5f157279a73c4d07399626
  verdict: stale
  

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-08**: This app is not available anymore.
