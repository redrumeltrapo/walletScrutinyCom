---
wsId: 
title: "Chivo Wallet"
altTitle: "(Fake) Chivo Wallet"
authors:
- leo
users: 500
appId: com.chivowalletapp.co
released: 2021-08-12
updated: 2021-08-13
version: "1.0"
stars: 1.6
ratings: 64
reviews: 46
size: 3.9M
website: 
repository: 
issue: 
icon: com.chivowalletapp.co.png
bugbounty: 
verdict: defunct
date: 2021-08-21
signer: 
reviewArchive:
- date: 2021-08-20
  version: "1.0"
  appHash: 
  gitRevision: 009f04e77df6a800d039513746016ec961541d38
  verdict: fake

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-21**: This app is no more.

This app pretends to be the hugely anticipated
{% include walletLink.html wallet='android/com.chivo.wallet' verdict='true' %}
[by the government of El Salvador](https://www.youtube.com/watch?v=E77xEF-E2hs)
but with no government website linking there and the app not claiming to have a
website and the developer email being "just" some gmail address, we can assume
this is a fake wallet.

We first heard about this app from [this tweet](https://twitter.com/ClaudyCordova).
