---
wsId: 
title: "BitBoxApp"
altTitle: 
authors:

users: 5000
appId: ch.shiftcrypto.bitboxapp
released: 2020-07-13
updated: 2021-09-07
version: "android-4.29.1"
stars: 4.7
ratings: 73
reviews: 34
size: 35M
website: https://shiftcrypto.ch/app
repository: https://github.com/digitalbitbox/bitbox-wallet-app
issue: 
icon: ch.shiftcrypto.bitboxapp.png
bugbounty: 
verdict: nowallet
date: 2021-01-23
signer: 
reviewArchive:


providerTwitter: ShiftCryptoHQ
providerLinkedIn: shift-crypto
providerFacebook: Shiftcrypto
providerReddit: 

redirect_from:

---


The description of this app reads:

> A BitBox02 hardware wallet is required.

so we assume that this app does not manage private keys or send transactions if
not approved via the hardware wallet. It itself is **not a wallet**.
