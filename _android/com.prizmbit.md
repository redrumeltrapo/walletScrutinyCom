---
wsId: prizmbit
title: "Prizmbit wallet, p2p cryptocurrency exchange"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.prizmbit
released: 2019-06-28
updated: 2021-09-16
version: "1.5"
stars: 3.8
ratings: 555
reviews: 269
size: 16M
website: https://prizmbit.com/
repository: 
issue: 
icon: com.prizmbit.png
bugbounty: 
verdict: custodial
date: 2021-05-01
signer: 
reviewArchive:


providerTwitter: prizmbit
providerLinkedIn: 
providerFacebook: prizmbit
providerReddit: 

redirect_from:

---


There is no statement regarding how private keys are managed in the play store description or on the [providers website](https://prizmbit.com/) or FAQ.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.
