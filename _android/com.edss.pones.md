---
wsId: 
title: "MX exchange - buying & sell Bitcoin"
altTitle: 
authors:
- danny
users: 10000
appId: com.edss.pones
released: 2021-04-29
updated: 2021-04-29
version: "1.0"
stars: 3.7
ratings: 33
reviews: 13
size: 3.3M
website: 
repository: 
issue: 
icon: com.edss.pones.png
bugbounty: 
verdict: fake
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


⚠️ **Warning**: There are many instances which give us pause when it comes to this app. Please see below. Proceed with utmost scrutiny, due diligence and caution when examining this app. 

## Notes

- This app's logo and name greatly resemble that of {% include walletLink.html wallet='android/com.mexcpro.client' verdict='true' %}
- The app does not have a website listed on its Google Play page.

## App Description

From the description in Google Play:

> MX exchange is a secure exchange built by senior Silicon Valley technology, a convenient bitcoin exchange where you can safely buy and sell bitcoins

This app claimed to be an exchange.

## The App

We tried the app **but it only contains the prices of various cryptocurrencies**. Clicking on one card, for example Bitcoin, just brings up an information background about its creation. The other two tabs are Interpretation and Related. We explored them both. We were not able to find send or receive functions or even an exchange, which is quite odd.

## Verdict

The lack of information and the sparse functionalities with no ability to send, receive, or even exchange means that this app does not have a bitcoin wallet. This is contradictory to its name and claims in the description. As the app has not been honest with it's actual features, we conclude that it is a **fake exchange.**
