---
wsId: 
title: "Zero Mobile Wallet"
altTitle: 
authors:

users: 1000
appId: insight.zero.communitywallet
released: 2018-07-09
updated: 2019-01-01
version: "1.0.1"
stars: 4.8
ratings: 55
reviews: 34
size: 11M
website: 
repository: 
issue: 
icon: insight.zero.communitywallet.png
bugbounty: 
verdict: obsolete
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


