---
wsId: 
title: "Tronz Wallet"
altTitle: 
authors:

users: 1000
appId: com.walletapp.walletapp
released: 2020-08-04
updated: 2021-01-28
version: "1.0.14"
stars: 4.2
ratings: 54
reviews: 46
size: 28M
website: 
repository: 
issue: 
icon: com.walletapp.walletapp.png
bugbounty: 
verdict: defunct
date: 2021-06-18
signer: 
reviewArchive:
- date: 2021-04-27
  version: "1.0.14"
  appHash: 
  gitRevision: 612e60ecd2013c802012d1c553a2ff8b56004226
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


