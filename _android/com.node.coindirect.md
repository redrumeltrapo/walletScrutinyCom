---
wsId: coindirect
title: "Coindirect - Buy & Sell Bitcoin Instantly"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.node.coindirect
released: 2018-10-10
updated: 2021-07-07
version: "1.3.0"
stars: 2.6
ratings: 523
reviews: 425
size: 30M
website: https://www.coindirect.com
repository: 
issue: 
icon: com.node.coindirect.png
bugbounty: 
verdict: custodial
date: 2021-04-26
signer: 
reviewArchive:


providerTwitter: coindirectcom
providerLinkedIn: coindirect
providerFacebook: coindirectcom
providerReddit: 

redirect_from:

---


It is concerning to see a wallet with a rating of less than 2 in the Google Play
Store with close to 500 total ratings.
The website really states nothing about private key management. Just some reference to cold storage of bitcoins.

> The majority of our users’ Bitcoins are kept securely in cold storage. This is equivalent to a digital bank vault.

this leads us to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

