---
wsId: abccExchange
title: "ABCC Exchange"
altTitle: 
authors:
- danny
users: 100000
appId: com.abcc.exchange
released: 2018-08-23
updated: 2021-11-02
version: "1.9.4"
stars: 4.2
ratings: 1537
reviews: 1270
size: 15M
website: https://abcc.com/
repository: 
issue: 
icon: com.abcc.exchange.png
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: ABCCExOfficial
providerLinkedIn: 
providerFacebook: ABCC-Exchange-558472047871367
providerReddit: 

redirect_from:

---


Also known as "Alphabit Consulting Pte", it is self-described as an 'Unregulated Platform' in its [Terms](https://abcc.com/en/documents/terms). 

Sections 3.7 and 3.8 of its User Agreements page, procedures regarding the [Deposit/Withdrawal of digital assets](https://abcc.com/en/documents/agreement) indicate that this is a custodial exchange. Withdrawal may take up to 3 to 30 days depending on the site's discretion and AML policy.

This is evidently a **custodial** service that is **not verifiable**

