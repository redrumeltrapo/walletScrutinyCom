---
wsId: BTCcomPool
title: "BTC.com Pool"
altTitle: 
authors:
- danny
users: 10000
appId: com.btcpool.app.android
released: 2020-02-25
updated: 2021-10-27
version: "1.1.5"
stars: 4.2
ratings: 169
reviews: 64
size: 28M
website: http://btc.com
repository: 
issue: 
icon: com.btcpool.app.android.png
bugbounty: 
verdict: nowallet
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: btccom_official
providerLinkedIn: btc.com
providerFacebook: btccom
providerReddit: 

redirect_from:

---


## App Description

This is meant to be a tool for managing mining pools. The wallet is a seperate app.

> BTC Pool is coming! BTC pool is a whole new choice for miners! With much more stable architecture, much better user experience, much lower fees and much stronger service, you will never find that mining could be in this way!

## The Site

Their site [links](https://wallet.btc.com/#/setup/register) to this separate wallet app: {% include walletLink.html wallet='android/com.blocktrail.mywallet' verdict='true' %}

## Verdict

This specific app is meant as a tool for managing and/or monitoring mining pools. It is **not a bitcoin wallet.**
