---
wsId: 
title: "SparkPoint Crypto Wallet"
altTitle: 
authors:
- leo
- emanuel
users: 10000
appId: com.sparkpoint
released: 2019-07-20
updated: 2021-11-03
version: "7.2.2"
stars: 4.6
ratings: 594
reviews: 505
size: 13M
website: https://sparkpoint.io/
repository: 
issue: 
icon: com.sparkpoint.png
bugbounty: 
verdict: nosource
date: 2021-03-06
signer: 
reviewArchive:


providerTwitter: sparkpointio
providerLinkedIn: sparkpointio
providerFacebook: sparkpointio
providerReddit: SparkPoint

redirect_from:

---


This app sounds at first as if it was not for Bitcoin but self-custodial:

> In this initial release of SparkPoint, we are first enabling SparkPoint
  Wallet, a 100% non-custodial wallet, so you can store your ETH and SRK tokens
  on your Android devices.

but further down we can also find Bitcoin:

> - Send and receive Bitcoin (BTC)

**The reviews are brutal!!** Read the scam accusations and consider that
positive ratings might be bought! This amount of accusations is not normal!

That said, we can't find any source code and conclude the app is **not verifiable**.
