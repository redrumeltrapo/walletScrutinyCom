---
wsId: CoinList
title: "CoinList"
altTitle: 
authors:
- danny
users: 500000
appId: com.coinlist.trade
released: 2020-08-10
updated: 2021-05-19
version: "2.0.3"
stars: 2.8
ratings: 3080
reviews: 2041
size: 5.6M
website: https://coinlist.co/
repository: 
issue: 
icon: com.coinlist.trade.png
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: coinlist
providerLinkedIn: 
providerFacebook: CoinListOfficial
providerReddit: 

redirect_from:

---


## App Description

Google Play lists its features: 

> - Buy and sell cryptocurrencies
> - Store, deposit and send crypto
> - Earn yield on Proof-of-Stake assets
> - Convert bitcoin into wrapped bitcoin

Proof that it is custodial:

> We use two-factor authentication (2FA) for every account and most transactions, and we’re backed by **top custodians like BitGo and Gemini Custody** so your funds are safe. Best of all, crypto storage is free. We don’t charge any custody or wallet fees when using the CoinList app.

## Verdict

App is **custodial** and thus **not verifiable**. 
