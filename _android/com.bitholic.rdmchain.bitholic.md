---
wsId: 
title: "Bitholic - Global Digital Asset Exchange"
altTitle: 
authors:
- leo
users: 10000
appId: com.bitholic.rdmchain.bitholic
released: 2019-01-20
updated: 2021-08-02
version: "1.1.1"
stars: 4.1
ratings: 163
reviews: 103
size: 2.2M
website: https://www.bithumbsg.com
repository: 
issue: 
icon: com.bitholic.rdmchain.bitholic.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.bitholic.rdmchain.bitholic/
  - /posts/com.bitholic.rdmchain.bitholic/
---


This app is an interface for an exchange. Your coins are stored on their
infrastructure. As a custodial service it is **not verifiable**.
