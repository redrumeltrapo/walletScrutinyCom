---
wsId: 
title: "Neo Bitcoin Wallet"
altTitle: 
authors:

users: 1000
appId: com.neobitcoin.wallet
released: 2020-07-03
updated: 2020-07-03
version: "1.0.0"
stars: 1.6
ratings: 129
reviews: 111
size: 48M
website: 
repository: 
issue: 
icon: com.neobitcoin.wallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2020-12-14
  version: "1.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nobtc

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.neobitcoin.wallet/
---


Apart from not being a Bitcoin wallet (only Neo Bitcoin), this is also the
wallet app with the lowest rating on Google Play (1.4 stars) that we have ever
come across.
