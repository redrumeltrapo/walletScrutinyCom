---
wsId: coholdmobile
title: "HOLD — Buy Bitcoin & Crypto"
altTitle: 
authors:
- leo
users: 10000
appId: co.hold.mobile
released: 2018-09-27
updated: 2021-07-06
version: "3.13.3"
stars: 4.1
ratings: 174
reviews: 102
size: Varies with device
website: https://hold.io
repository: 
issue: 
icon: co.hold.mobile.png
bugbounty: 
verdict: custodial
date: 2021-03-10
signer: 
reviewArchive:


providerTwitter: HoldHQ
providerLinkedIn: holdhq
providerFacebook: HoldHQ
providerReddit: 

redirect_from:

---


> SAFETY FIRST<br>
  Regulated and licensed in the EU. Your money is securely held by banks within
  the European Union and your crypto protected by the world-renowned custodian
  BitGo.

This app is a custodial offering and thus **not verifiable**.
