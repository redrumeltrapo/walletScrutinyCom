---
wsId: 
title: "Blockchain India"
altTitle: "(Fake) Blockchain India"
authors:
- emanuel
- leo
users: 5000
appId: com.crypto.blockchain
released: 2021-06-22
updated: 2021-08-17
version: "1.1.0"
stars: 3.6
ratings: 29
reviews: 13
size: 8.0M
website: 
repository: 
issue: 
icon: com.crypto.blockchain.jpg
bugbounty: 
verdict: defunct
date: 2021-10-15
signer: 
reviewArchive:
- date: 2021-08-08
  version: "1.1.0"
  appHash: 
  gitRevision: 8601f531a9b3352939739447796fad666f17ccd4
  verdict: fake
  
providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

**Update 2021-10-07**: This app is not on Play Store anymore.

This looks like a fake {% include walletLink.html wallet='android/piuk.blockchain.android' verdict='true' %}.