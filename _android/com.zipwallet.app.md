---
wsId: zipwalletpay
title: "Zipwallet- Buy Bitcoin with Paypal & Credit Card"
altTitle: 
authors:
- danny
users: 50000
appId: com.zipwallet.app
released: 2018-01-30
updated: 2021-11-09
version: "9.9.7.9"
stars: 4.4
ratings: 615
reviews: 383
size: 10M
website: https://zipwalletpay.com/
repository: 
issue: 
icon: com.zipwallet.app.png
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


Its homepage description reads:

> Simple exchange platform with over 300 payment providers around the world.<br>
Universal solutions for fund exchange between payments providers. 

We downloaded the app and was able to locate the BTC wallet. However, the private keys nor the mnemonic seed phrases were both not readily apparent. It seems to be a fiat/voucher/gift card balance to bitcoin exchange and vice versa. 

When we tried to share our BTC address from the wallet, no QR code was generated and the BTC address would only appear visually once it has already been shared to the receiving party. 

A huge chunk of its short [User Agreement](https://zipwalletpay.com/agreement) is focused on prohibited entities including businesses.

As there is a capability to send and receive Bitcoins with an entity that has the power to  that , we determine this app to be **custodial** and thus, **not verifiable**. 

