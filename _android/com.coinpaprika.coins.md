---
wsId: coinsonepaprika
title: "COINS: One App For Crypto by Coinpaprika"
altTitle: 
authors:
- danny
users: 100000
appId: com.coinpaprika.coins
released: 2020-06-15
updated: 2021-09-10
version: "1.14.2"
stars: 4.3
ratings: 1243
reviews: 640
size: 32M
website: https://coins.coinpaprika.com/
repository: 
issue: 
icon: com.coinpaprika.coins.png
bugbounty: 
verdict: nosource
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: CoinsOneApp
providerLinkedIn: 
providerFacebook: CoinsOneApp
providerReddit: 

redirect_from:

---


From the Google Play app description: 

> With COINS, you can Discover, Store, Invest, Send & Receive over 2000 cryptocurrencies. All in one app. 

Furthermore:

> Our non-custodial solution lets you store over different 1500 cryptocurrencies inside your phone. By using advanced biometrics, we make sure that your private keys never leave your device. Nobody has access to your funds, but you!

We downloaded the app and proceeded to create a BTC wallet. The app gives you the option to use either biometric data or a pin before the wallet is created. It then prompts the user to backup the wallet via secret phrase. You then input your biometric data again, and the app shows 12 words. Then, the 12 are re-entered to be verified. 

Furtheremore, no KYC is required.

We reached out via twitter and they confirmed that their app was non-custodial. Their source code is based on [Trust Wallet](https://github.com/trustwallet/wallet-core)

This app is **non-custodial**, so the next step is to try to find the source code for the wallet. 

We conversed with them via [twitter](https://twitter.com/dannybuntu/status/1434825692944818193).

>@coinpaprika - 33m<br>
non-custodial<br><br>
Daniel Andrei R. Garcia<br>
@dannybuntu - 23m<br>
Yeah, doing backup of seed phrase now. Your app is sleek, very well designed. Is the source code available to the general public?<br><br>
@coinpaprika Replying to @dannybuntu<br>
It's based on Trust Core - open source so kind of ;)<br>
https://github.com/trustwallet/wallet-core

Absent source code for **this specific app**, it is **not verifiable**.

