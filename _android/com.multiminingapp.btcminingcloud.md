---
wsId: 
title: "BTC Mining Cloud"
altTitle: 
authors:
- danny
- leo
users: 10000
appId: com.multiminingapp.btcminingcloud
released: 2021-03-25
updated: 2021-03-31
version: "2.1"
stars: 4.1
ratings: 3255
reviews: 1238
size: 4.9M
website: 
repository: 
issue: 
icon: com.multiminingapp.btcminingcloud.png
bugbounty: 
verdict: defunct
date: 2021-10-30
signer: 
reviewArchive:
- date: 2021-09-23
  version: "2.1"
  appHash: 
  gitRevision: 70bdd6a4573c03410f6563e850ff522f756f5fdc
  verdict: nowallet


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-19**: This app is no more.

This app advertises itself as a "cloud mining service" so it is likely not a standard BTC wallet. 

> Bitcoin Mining Cloud is a cloud mining service for a cryptocurrency (Cloud Mining), in which you can buy a plane and start mining to earn crypto coins. BTC Mining Cloud allows everyone to earn cryptocurrencies like Bitcoin.

There is a "wallet" that allegedly will show your satoshi balance, but there is no mention of private keys nor is there a wallet address.

The provider's name is listed as "[Video Player & Video Downloader & Tools](https://play.google.com/store/apps/details?id=com.multiminingapp.btcminingcloud)" 

They do not appear to have an official website or even social media accounts. There is no reference to any bitcoin mining rigs or facilities. There is no way of proving that the balance they claim users have "mined" is actually true satoshis, or that any money is actually stored in this app.

We do **not classify this as a wallet.**
