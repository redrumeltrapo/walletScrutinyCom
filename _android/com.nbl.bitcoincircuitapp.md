---
wsId: 
title: "Bitcoin Circuit App - New Bitcoin Trading System"
altTitle: 
authors:

users: 1000
appId: com.nbl.bitcoincircuitapp
released: 
updated: 2021-02-27
version: "1.0.0"
stars: 3.4
ratings: 28
reviews: 25
size: 19M
website: 
repository: 
issue: 
icon: com.nbl.bitcoincircuitapp.png
bugbounty: 
verdict: defunct
date: 2021-08-20
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.1"
  appHash: 
  gitRevision: 94a906f24c1e25b517f8270944b2565285a1074c
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-12**: This app is not on the Play Store anymore
