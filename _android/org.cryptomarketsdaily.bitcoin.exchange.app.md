---
wsId: 
title: "Bitcoin Exchange App"
altTitle: 
authors:

users: 1
appId: org.cryptomarketsdaily.bitcoin.exchange.app
released: 2021-06-17
updated: 2021-06-17
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 3.5M
website: 
repository: 
issue: 
icon: org.cryptomarketsdaily.bitcoin.exchange.app.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
