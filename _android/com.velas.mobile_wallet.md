---
wsId: VelasWallet
title: "Velas Wallet"
altTitle: 
authors:
- danny
users: 10000
appId: com.velas.mobile_wallet
released: 2020-11-17
updated: 2021-10-25
version: "2.0.9"
stars: 4.7
ratings: 486
reviews: 426
size: 38M
website: https://velas.com/
repository: https://github.com/velas/mobile-wallet
issue: 
icon: com.velas.mobile_wallet.png
bugbounty: 
verdict: wip
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

> Velas Wallet is a secure, easy-to-use and completely free application to manage your cryptocurrency. With Velas Wallet you can not only store digital currencies, but also actively use them; pay bills, make purchases, and pay for other services by using a QR code.
>
> With this release, the Velas Wallet now has staking functionalities and token holders can earn rewards for securing and maintaining the Velas Network.
>
> **Decentralization and Anonymity**
> Velas Wallet is a fully decentralized application. We don't store any of your data, nor do we require any verification for basic services. The Velas team has no access to your funds, as your Wallet’s mnemonic phrase is only stored by the user themselves.
> 
> Convenience & Easy to use:
> - Multi-language support.
> - Multi-currencies: VLX, BTC, ETH, SYX, USDT & LTC
> - Available for all countries - no geo restrictions.
> - Activity log: view your transaction history.
> - Fingerprint authentication.

## The Site

Wallets are available for: 

- Google Play
- Mac OS 
- Linux
- Windows
- Web Wallet

## The App

We downloaded the app and assigned a password. 
The seed phrase is composed of 24 words.

## Verdict

This app is self-custodial. We are trying to find out if it is reproducible [here](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/362).


