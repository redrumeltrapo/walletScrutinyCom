---
wsId: 
title: "Bitcoin & Crypto payments - BitBay Terminal (POS)"
altTitle: 
authors:

users: 1000
appId: com.bitbay.pay.bitcoin.pos.terminal
released: 2019-03-18
updated: 2020-03-06
version: "1.6.6"
stars: 0.0
ratings: 
reviews: 
size: 5.9M
website: 
repository: 
issue: 
icon: com.bitbay.pay.bitcoin.pos.terminal.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


