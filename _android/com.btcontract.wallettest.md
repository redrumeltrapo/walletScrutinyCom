---
wsId: 
title: "Simple Bitcoin Wallet TESTNET"
altTitle: 
authors:

users: 100
appId: com.btcontract.wallettest
released: 2021-06-28
updated: 2021-07-30
version: "2.0"
stars: 0.0
ratings: 
reviews: 
size: 18M
website: 
repository: 
issue: 
icon: com.btcontract.wallettest.png
bugbounty: 
verdict: nobtc
date: 2021-08-09
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is for testnet only.