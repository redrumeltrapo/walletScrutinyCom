---
wsId: 
title: "IQFinex Wallet"
altTitle: 
authors:

users: 500
appId: com.iqfinex
released: 2018-09-10
updated: 2019-01-24
version: "1.0.4"
stars: 4.8
ratings: 270
reviews: 261
size: 6.7M
website: 
repository: 
issue: 
icon: com.iqfinex.png
bugbounty: 
verdict: defunct
date: 2021-10-22
signer: 
reviewArchive:

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-13**: This app is no more.

