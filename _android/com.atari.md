---
wsId: 
title: "Atari Smart Wallet"
altTitle: 
authors:

users: 1000
appId: com.atari
released: 2021-07-11
updated: 2021-09-09
version: "2.07.02"
stars: 3.5
ratings: 79
reviews: 58
size: 53M
website: 
repository: 
issue: 
icon: com.atari.png
bugbounty: 
verdict: wip
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


