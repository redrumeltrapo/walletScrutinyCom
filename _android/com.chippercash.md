---
wsId: 
title: "Chipper Cash"
altTitle: 
authors:
- kiwilamb
- danny
users: 1000000
appId: com.chippercash
released: 2018-07-07
updated: 2021-10-29
version: "1.9.16"
stars: 4.3
ratings: 50431
reviews: 30572
size: 97M
website: https://chippercash.com/
repository: 
issue: 
icon: com.chippercash.png
bugbounty: 
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: chippercashapp
providerLinkedIn: 
providerFacebook: Chippercashapp
providerReddit: 

redirect_from:

---


Stated in their sites [support article](https://support.chippercash.com/en/articles/4750740-how-to-buy-sell-cryptocurrency-on-chipper-cash) 
"Currently it's not possible to send to or receive Bitcoin or Ethereum from external wallets"

Conclusion is that Chipper is a custodial wallet as funds are held by Chipper on behalf of the user.

**Edit:** 

> We're excited to introduce the Crypto RECEIVE feature. This will enable you receive Bitcoin/Ethereum/USDC from another Crypto(ETH/BTC/USDC) address.

As of this date it seems that it's now possible to send and receive from external addresses.
