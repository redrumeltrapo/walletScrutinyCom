---
wsId: 
title: "Bian：Bitcoin Wallet-Virtual currency"
altTitle: "(Fake) Bian：Bitcoin Wallet-Virtual currency"
authors:
- emanuel
- leo
users: 50
appId: com.xmzys.binance73
released: 2021-07-03
updated: 2021-07-03
version: "2.0"
stars: 0.0
ratings: 
reviews: 
size: 3.0M
website: 
repository: 
issue: 
icon: com.xmzys.binance73.png
bugbounty: 
verdict: defunct
date: 2021-10-22
signer: 
reviewArchive:
- date: 2021-07-24
  version: "2.0"
  appHash: 
  gitRevision: f9f046037c44e67715b35a4a2fbf64ab6b2244ac
  verdict: fake
  

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-14**: This app is no more available.

This app "{{ page.title }}" clearly tries to imitate
{% include walletLink.html wallet='android/com.binance.us' verdict='true' %} or
{% include walletLink.html wallet='android/com.binance.dev' verdict='true' %}.
