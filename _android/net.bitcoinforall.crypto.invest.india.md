---
wsId: 
title: "Crypto Invest India"
altTitle: 
authors:

users: 1
appId: net.bitcoinforall.crypto.invest.india
released: 2021-06-17
updated: 2021-06-17
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 3.4M
website: 
repository: 
issue: 
icon: net.bitcoinforall.crypto.invest.india.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
