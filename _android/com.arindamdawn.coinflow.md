---
wsId: 
title: "Coinflow - Exchange Bitcoin and Altcoins instantly"
altTitle: 
authors:
- leo
users: 100
appId: com.arindamdawn.coinflow
released: 2017-07-26
updated: 2017-07-27
version: "1.0.1"
stars: 0.0
ratings: 
reviews: 
size: 1.6M
website: 
repository: 
issue: 
icon: com.arindamdawn.coinflow.png
bugbounty: 
verdict: defunct
date: 2021-10-30
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-21**: This app is no more.
