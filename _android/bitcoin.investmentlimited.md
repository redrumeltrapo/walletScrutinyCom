---
wsId: 
title: "Bitcoin Investment"
altTitle: 
authors:

users: 5000
appId: bitcoin.investmentlimited
released: 2019-01-02
updated: 2021-08-04
version: "9.0"
stars: 4.1
ratings: 75
reviews: 46
size: 10M
website: 
repository: 
issue: 
icon: bitcoin.investmentlimited.png
bugbounty: 
verdict: wip
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- Emanuel thinks this is probably a scam. See https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/314 -->
