---
wsId: TradeSanta
title: "TradeSanta: Crypto Trading Bot for Binance, Huobi"
altTitle: 
authors:
- danny
users: 10000
appId: com.tradesanta
released: 2019-05-20
updated: 2021-10-21
version: "2.4.12"
stars: 3.8
ratings: 658
reviews: 464
size: 12M
website: https://tradesanta.com/en
repository: 
issue: 
icon: com.tradesanta.png
bugbounty: 
verdict: nowallet
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description and Verdict
TradeSanta isn't a cryptocurrency exchange or wallet, but claims to provide trading bots that automate trading strategies on crypto exchanges, including:

> ➡️ Binance exchange, Binance US, Binance futures trading, HitBTC, Okex, Huobi and UpBit

It also has the following instructions for an easy setup:

> - **Connect to a supported exchange via API**
> - Choose a long/short template or create a trading bot from scratch
> - Select a pair
> - Fine-tune crypto bot’s parameters

TradeSanta clearly is **not a bitcoin wallet.**
