---
wsId: FreeWalletIO
title: "FreeWallet"
altTitle: 
authors:
- leo
users: 10000
appId: io.freewallet.mobile
released: 2016-09-01
updated: 2019-03-17
version: "0.1.9"
stars: 4.0
ratings: 69
reviews: 34
size: 6.7M
website: https://freewallet.io
repository: https://github.com/jdogresorg/freewallet-mobile
issue: https://github.com/jdogresorg/freewallet-mobile/issues/34
icon: io.freewallet.mobile.jpg
bugbounty: 
verdict: obsolete
date: 2021-10-01
signer: 
reviewArchive:
- date: 2020-12-05
  version: "0.1.9"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: ftbfs

providerTwitter: freewallet
providerLinkedIn: 
providerFacebook: freewallet.io
providerReddit: 

redirect_from:
  - /io.freewallet.mobile/
---


The provider of this Freewallet reached out to us to stress that freewallet.io
was not the same as freewallet.org which is spamming Google Play with many
wallets and we have reviewed three of those here, too:

* [Bitcoin Wallet. Buy & Exchange BTC coin－Freewallet](/btc.org.freewallet.app/)
  is custodial with 500k downloads.
* [Bitcoin & Crypto Blockchain Wallet: Freewallet](/mw.org.freewallet.app/) is
  custodial, with 100k downloads.
* [Lite HD Wallet – Your Coin Base](/org.freewallet.lite.android/) is probably
  also custodial, with only 500 downloads so far.

He says that his wallet is non-custodial and open source and indeed we see those
same claims on Google Play.

On the website we read:

> **Open Source**<br>
  FreeWallet Mobile is open-source, and available for anyone to fork or review, so you know that it works exactly the way that it is supposed to.

and indeed we find [a repository](https://github.com/jdogresorg/freewallet-mobile).
It had no changes for two years and also Google Play does not report any changes
since March 2019 but this looks like open source.

Unfortunately there is no build instructions so we cannot reproduce the app and
it remains **not verifiable**.
