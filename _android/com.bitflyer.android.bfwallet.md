---
wsId: bitFlyer
title: "bitFlyer: Crypto Exchange to Buy & Trade Bitcoin"
altTitle: 
authors:
- danny
users: 500000
appId: com.bitflyer.android.bfwallet
released: 
updated: 2021-11-01
version: "6.4.0"
stars: 4.0
ratings: 3975
reviews: 1332
size: 17M
website: https://bitflyer.com/
repository: 
issue: 
icon: com.bitflyer.android.bfwallet.png
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: bitFlyer
providerLinkedIn: 
providerFacebook: bitflyer
providerReddit: 

redirect_from:

---


> We ensure the safety of your funds with state-of-the-art security features including cold wallets, Multisig, 2FA, Installation of web application firewall (WAF), encryption of customer information, asset segregation and more.

bitFlyer is **custodial** and **not verifiable.**

As an additional note, some users seemed to have problems with verification.

> [Sabari Nathan](https://play.google.com/store/apps/details?id=com.bitflyer.android.bfwallet&reviewId=gp%3AAOqpTOENgSfeAmSBEhWgih3JVV2d1hrztZJcZno8NBByWFLFpGOhiCobomj-P5225_rUUuCNjHSXcS84Dryj)<br>
  ★☆☆☆☆ 9 April 2021<br>
 I cannot verify my bank details, I have submitted my all the bank details clearly but they closed my account without giving the proper reason. But I am still trying to create, And also I cannot connect the Custom care number. I have tried many times but still cannot.

> [MIHA](https://play.google.com/store/apps/details?id=com.bitflyer.android.bfwallet&reviewId=gp%3AAOqpTOFNSD8G9xwD2h08Lg_vozj5PMBAl0qPccZR504AqzOQNeKYiR5mOpCIWeW5SbmG16p6Vz9MhnnXy4dp)
<br>★☆☆☆☆ 8 January 2021<br>
I gave 1 star only because there is no 0 star option. One word, meh. Verification is slow and difficult, the flexibility of the app and platform needs a lot of work. Why use this when you got siperior alternatives. (this is not a question)
