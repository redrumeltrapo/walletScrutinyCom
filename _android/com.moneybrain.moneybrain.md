---
wsId: 
title: "Moneybrain - Peer to Peer Digital Banking"
altTitle: 
authors:

users: 1000
appId: com.moneybrain.moneybrain
released: 2019-10-17
updated: 2021-11-01
version: "1.2.14"
stars: 3.8
ratings: 9
reviews: 5
size: 9.4M
website: 
repository: 
issue: 
icon: com.moneybrain.moneybrain.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


