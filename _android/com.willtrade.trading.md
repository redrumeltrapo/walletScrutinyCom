---
wsId: 
title: "Will Trade - Crypto Forex Gold"
altTitle: 
authors:
- danny
users: 10000
appId: com.willtrade.trading
released: 2021-08-19
updated: 2021-09-29
version: "1.0.1"
stars: 4.9
ratings: 1203
reviews: 779
size: 13M
website: 
repository: 
issue: 
icon: com.willtrade.trading.jpg
bugbounty:
verdict: custodial
date: 2021-11-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

> Trade global popular varieties with one account: forex, gold, bitcoin, stock indices, energy and more, Be a trader now and start making money.
> Popular forex currency pairs : EUR/USD, XAU/USD, GBP/USD;
> Crypto: Bitcoin, Ethereum, Dogecoin, Ripple, Litecoin, and others;
> The main stock indices: NDX, HSI;

**Notes:**

- The app does not have a website listed on its Google Play page.
- We were not able to find social media accounts for the app

## The App

You can bind your bank account. Deposit and withdraw Bitcoin. Initially we thought this app was like those dime-a-dozen forex websites. We were surprised to find multiple crypto wallets including USDT, BTC, ETH and others.

## Verdict

The app has multiple cryptocurrency wallets which have the ability to send and receive. However, these do not have backup or restore options. This service is **custodial** and the app **cannot be verified**.
