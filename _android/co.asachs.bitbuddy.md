---
wsId: 
title: "BitBuddy –Buy & Sell Bitcoin"
altTitle: 
authors:

users: 1000
appId: co.asachs.bitbuddy
released: 2019-09-19
updated: 2020-09-28
version: "2.0.1"
stars: 3.9
ratings: 56
reviews: 38
size: 14M
website: 
repository: 
issue: 
icon: co.asachs.bitbuddy.png
bugbounty: 
verdict: stale
date: 2021-09-24
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


