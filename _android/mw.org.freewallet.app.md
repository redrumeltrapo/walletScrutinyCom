---
wsId: multifw
title: "BTC, ETH, SHIB, Altcoin Wallet"
altTitle: 
authors:
- leo
users: 500000
appId: mw.org.freewallet.app
released: 2017-08-10
updated: 2021-09-16
version: "1.16.5"
stars: 4.5
ratings: 12628
reviews: 8876
size: 13M
website: https://freewallet.org
repository: 
issue: 
icon: mw.org.freewallet.app.png
bugbounty: 
verdict: custodial
date: 2019-12-22
signer: 
reviewArchive:


providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: 

redirect_from:

---


According to the description

> In addition, the majority of cryptocurrency assets on the platform are stored
  in an offline vault. Your coins will be kept in cold storage with state of the
  art security protecting them.

This is a custodial app.

Our verdict: **not verifiable**.
