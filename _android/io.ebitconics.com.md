---
wsId: 
title: "eBitcoinics Inc - Simplified gateway to blockchain"
altTitle: 
authors:

users: 1000
appId: io.ebitconics.com
released: 2021-01-18
updated: 2021-01-18
version: "0.0.2"
stars: 4.4
ratings: 18
reviews: 15
size: 4.1M
website: 
repository: 
issue: 
icon: io.ebitconics.com.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


