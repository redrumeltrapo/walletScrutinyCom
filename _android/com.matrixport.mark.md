---
wsId: matrixport
title: "Matrixport: Get More From Your Crypto"
altTitle: 
authors:
- danny
users: 50000
appId: com.matrixport.mark
released: 2019-10-23
updated: 2021-10-15
version: "1.1.7"
stars: 4.1
ratings: 680
reviews: 446
size: 106M
website: https://www.matrixport.com/
repository: 
issue: 
icon: com.matrixport.mark.png
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: realMatrixport
providerLinkedIn: matrixport
providerFacebook: matrixport
providerReddit: Matrixport

redirect_from:

---


From its site description:

> Matrixport delivers a comprehensive suite of innovative and easy-to-use crypto investment products and financial services tailored for all levels of expertise. We empower people to take control of their personal finances, one satisfied customer at a time.

It offers interests, investing, loans and trading services.

Like most all-in-one crypto financial services, it is managed by a company and requires KYC verification. 

> Diverse kinds of crypto wallets are supported and we provide industry-level security. Assets are stored using multi-sig mechanism and bank-grade vaults to ensure your account security.

[This is the tutorial](https://support.matrixport.com/hc/en-us/articles/360048468273-How-to-withdraw-) for withdrawing cryptocurrencies.

> When you make the withdrawals, we’ll verify your identity. You would be requested to key in 1) the verification code we send to your phone or email, or the Google Authentication code, and 2) the Fund Password. Additionally, if you are trying to transfer the assets to a new address, we will also request the last 6 characters of your ID number.

This cryptocurrency finance app is **custodial** and thus, **not verifiable.**


