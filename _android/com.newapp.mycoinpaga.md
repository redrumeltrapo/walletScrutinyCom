---
wsId: 
title: "CoinPaga - Spend Bitcoin & Sell Giftcard"
altTitle: 
authors:
- leo
users: 100
appId: com.newapp.mycoinpaga
released: 2020-11-29
updated: 2020-12-07
version: "1.11"
stars: 4.0
ratings: 8
reviews: 5
size: 5.3M
website: 
repository: 
issue: 
icon: com.newapp.mycoinpaga.png
bugbounty: 
verdict: defunct
date: 2021-08-13
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.11"
  appHash: 
  gitRevision: 8846f7c2efdc1cf24d876fec2622625a77fe31a5
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-05**: This app is not anymore.
