---
wsId: stex
title: "Stex.com - Crypto exchange"
altTitle: 
authors:
- danny
users: 100000
appId: com.stocksexchange.android
released: 2018-05-15
updated: 2021-10-28
version: "2.1.0"
stars: 4.3
ratings: 1673
reviews: 604
size: 9.8M
website: https://stex.com
repository: 
issue: 
icon: com.stocksexchange.android.png
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


It has a Crypto Exchange interface and refers to itself as such. One of its general features advertise wallet service. This sounds custodial.

>  [To make the deposit find the required currency,](https://help.stex.com/en/articles/1657710-how-do-i-deposit-crypto-currencies) click on "deposit" , and generate address of your wallet by clicking on 'show me the deposit address'.

It's possible to send/[receive](https://help.stex.com/en/articles/1657719-how-do-i-withdraw-crypto-currencies) in this product.

This app being **custodial,** it's also **not verifiable.**