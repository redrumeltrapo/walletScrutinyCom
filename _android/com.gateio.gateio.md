---
wsId: gate.io
title: "gate.io - Buy BTC,ETH,SHIB"
altTitle: 
authors:
- danny
users: 1000000
appId: com.gateio.gateio
released: 2017-11-03
updated: 2021-11-08
version: "3.0.4"
stars: 3.0
ratings: 8483
reviews: 5988
size: 187M
website: https://gate.io/
repository: 
issue: 
icon: com.gateio.gateio.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: gate_io
providerLinkedIn: 
providerFacebook: gateioglobal
providerReddit: GateioExchange

redirect_from:

---


> Gate.io is a reliable and secure crypto exchange for trading and investing bitcoin(...)

It's an exchange, so probably custodial.

Adding to that, Gate.io claims to use

> cold storage of the majority of users’ digital assets

We mark this wallet as **custodial** and therefore **not verifiable.**