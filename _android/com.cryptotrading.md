---
wsId: 
title: "Crypto Trading"
altTitle: 
authors:

users: 100
appId: com.cryptotrading
released: 2021-03-13
updated: 2021-07-08
version: "1.0.4"
stars: 0.0
ratings: 
reviews: 
size: 14M
website: 
repository: 
issue: 
icon: com.cryptotrading.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
