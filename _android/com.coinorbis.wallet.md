---
wsId: 
title: "Coinorbis Wallet - Multi Cryptocurrency Wallet"
altTitle: 
authors:

users: 500
appId: com.coinorbis.wallet
released: 2018-11-27
updated: 2019-01-25
version: "1.2.1"
stars: 4.5
ratings: 8
reviews: 3
size: 9.0M
website: 
repository: 
issue: 
icon: com.coinorbis.wallet.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


