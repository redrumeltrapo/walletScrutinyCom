---
wsId: 
title: "Ja­xx Lib­erty: Crypto Wallet"
altTitle: "(Fake) Jaxx Lib­erty: Crypto Wallet"
authors:
- leo
users: 500
appId: jax.wallet.liberty
released: 2021-08-30
updated: 2021-08-31
version: "12.003"
stars: 0.0
ratings: 
reviews: 
size: 9.3M
website: 
repository: 
issue: 
icon: jax.wallet.liberty.png
bugbounty: 
verdict: defunct
date: 2021-10-22
signer: 
reviewArchive:
- date: 2021-10-05
  version: "12.003"
  appHash: 
  gitRevision: f9f046037c44e67715b35a4a2fbf64ab6b2244ac
  verdict: fake
  

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-14**: This app is no more available.

Funny enough this app is not called "Jaxx". Try searching for "Jaxx" here or on
Google Play. It won't show up. It has some invisible symbols between the "Ja"
and the "xx". Anyway, it's still a fake of
{% include walletLink.html wallet='android/com.liberty.jaxx' verdict='true' %}.
