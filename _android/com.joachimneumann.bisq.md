---
wsId: 
title: "Bisq Notifications"
altTitle: 
authors:

users: 10000
appId: com.joachimneumann.bisq
released: 2018-09-06
updated: 2018-12-04
version: "1.1.0"
stars: 3.9
ratings: 64
reviews: 29
size: 3.6M
website: 
repository: 
issue: 
icon: com.joachimneumann.bisq.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


