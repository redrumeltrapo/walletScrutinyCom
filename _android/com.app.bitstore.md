---
wsId: 
title: "Bitstore.tech - crypto shopping platform"
altTitle: 
authors:

users: 500
appId: com.app.bitstore
released: 2019-03-19
updated: 2019-07-24
version: "1.0.11"
stars: 4.1
ratings: 17
reviews: 7
size: 21M
website: 
repository: 
issue: 
icon: com.app.bitstore.png
bugbounty: 
verdict: obsolete
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


