---
wsId: 
title: "Bitcoin Trading App Free"
altTitle: 
authors:

users: 5
appId: net.cryptee.bitcoin.trading.app.free
released: 2021-06-19
updated: 2021-06-19
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: net.cryptee.bitcoin.trading.app.free.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
