---
wsId: 
title: "Genesis Block - digital banking powered by crypto"
altTitle: 
authors:

users: 1000
appId: com.genesisblock.production
released: 2020-05-06
updated: 2021-11-05
version: "0.1.64"
stars: 3.7
ratings: 49
reviews: 29
size: Varies with device
website: 
repository: 
issue: 
icon: com.genesisblock.production.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


