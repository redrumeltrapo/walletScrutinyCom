---
wsId: 
title: "Invest On Crypto"
altTitle: 
authors:

users: 0
appId: org.thecrypto.apps.invest.on.crypto
released: 2021-06-22
updated: 2021-06-22
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.4M
website: 
repository: 
issue: 
icon: org.thecrypto.apps.invest.on.crypto.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
