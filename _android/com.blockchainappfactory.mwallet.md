---
wsId: 
title: "M Wallet"
altTitle: 
authors:

users: 500
appId: com.blockchainappfactory.mwallet
released: 2020-03-08
updated: 2020-03-09
version: "1.0"
stars: 3.4
ratings: 5
reviews: 
size: 5.2M
website: 
repository: 
issue: 
icon: com.blockchainappfactory.mwallet.png
bugbounty: 
verdict: stale
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


