---
wsId: 
title: "Fake Bitcoin Investor"
altTitle: 
authors:

users: 50
appId: com.cryptoidlemine.fake.bitcoin.investor
released: 2021-07-20
updated: 2021-07-20
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: com.cryptoidlemine.fake.bitcoin.investor.png
bugbounty: 
verdict: defunct
date: 2021-09-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-09**: This app is not on Google Play anymore.

