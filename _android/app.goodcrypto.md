---
wsId: 
title: "Good Crypto: one trading app - 30 crypto exchanges"
altTitle: 
authors:
- leo
users: 100000
appId: app.goodcrypto
released: 2019-05-20
updated: 2021-10-20
version: "1.8.2"
stars: 4.5
ratings: 869
reviews: 344
size: 20M
website: https://goodcrypto.app
repository: 
issue: 
icon: app.goodcrypto.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-10
signer: 
reviewArchive:


providerTwitter: GoodCryptoApp
providerLinkedIn: goodcrypto
providerFacebook: GoodCryptoApp
providerReddit: GoodCrypto

redirect_from:
  - /app.goodcrypto/
---


This app allows you to connect to accounts on trading platforms and does not
work as a wallet as such. We assume that you cannot receive Bitcoins to and send
them from this app.
