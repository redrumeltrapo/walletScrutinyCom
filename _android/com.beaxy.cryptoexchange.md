---
wsId: Beaxy
title: "Beaxy Exchange"
altTitle: 
authors:
- danny
users: 10000
appId: com.beaxy.cryptoexchange
released: 2020-02-07
updated: 2021-06-15
version: "2.9"
stars: 3.9
ratings: 158
reviews: 72
size: 44M
website: https://www.beaxy.com/
repository: 
issue: 
icon: com.beaxy.cryptoexchange.png
bugbounty: 
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: BeaxyExchange
providerLinkedIn: 
providerFacebook: beaxyexchange
providerReddit: BeaxyExchange

redirect_from:

---


## App Description

Cryptocurrency pairs along with fiat trading are included in this self-described as a registered US exchange. It is also a Money Services Business that's registered with FinCEN. Funds on the platform are held by Curv Institutional Custody. FDIC insured. 

## The Site

**Terms and Conditions**

> We reserve the right to cancel and/or suspend your Beaxy Account(s) and/or block transactions or freeze funds immediately and without notice if we determine, in our sole discretion, that your Account is associated with a Prohibited Use and/or a Prohibited Business.

## Verdict

Verdict is **custodial** and thus **not verifiable**

