---
wsId: DeltaTrading
title: "Delta Trading – FX, Shares, Indices, Crypto, CFDs"
altTitle: 
authors:
- danny
users: 50000
appId: delta.pk.mobile
released: 2013-03-29
updated: 2021-05-17
version: "5.8.7"
stars: 4.0
ratings: 407
reviews: 103
size: 1.2M
website: https://www.deltastock.com/
repository: 
issue: 
icon: delta.pk.mobile.png
bugbounty: 
verdict: nosendreceive
date: 2021-10-13
signer: 
reviewArchive:


providerTwitter: deltastock
providerLinkedIn: deltastock-ad
providerFacebook: Deltastock
providerReddit: 

redirect_from:

---


## App Description

> _"...trade CFDs (contracts for difference) on over 1000 financial instruments: forex, shares, indices, precious metals, commodity futures, popular cryptocurrencies and ETFs."_

## The Site

More information on trading cryptocurrency CFDs via DeltaStock's [DeltaTrading](https://www.deltastock.com/english/instruments/cfd-cryptocurrencies.asp)

[**Deposit and Withdraw Options**](https://www.deltastock.com/english/my-account/deposit-withdraw.asp)

They do not provide bitcoin or other cryptocurrency deposit and withdrawal options. These are the only options available:

- Bank Wire Transfer
- Credit and Debit Card Deposits
- ePay Deposits

## Verdict

Financial platforms that trade in cryptocurrency CFDs more often than not, **cannot send or receive bitcoins**.

