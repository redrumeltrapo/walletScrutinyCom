---
wsId: Nominex
title: "Nominex: Cryptocurrency trading without commission"
altTitle: 
authors:
- danny
users: 10000
appId: com.nominex.app
released: 2021-06-01
updated: 2021-10-12
version: "1.3.4"
stars: 4.0
ratings: 135
reviews: 89
size: 58M
website: https://nominex.io/
repository: 
issue: 
icon: com.nominex.app.png
bugbounty: 
verdict: custodial
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: NominexExchange
providerLinkedIn: nominex
providerFacebook: Nominex
providerReddit: 

redirect_from:

---


## App Description

The cryptocurrency exchange allows the _"trading of Bitcoin, Ethereum, NMX, Link, Tezos, Cardano, BNB and many other cryptocurrencies WITHOUT any fees."_

There is no need for KYC for withdrawals and deposits of less than 3 BTC per day.

Has support for Visa and Mastercard 

> **Secure wallets**<br>
Your funds are stored in the wallets of the largest exchange — Binance.

## The Site

[**Terms of Use**](https://nominex.io/docs/nominex-terms.pdf)

> Nominex maintains a stance of cooperation with law enforcement authorities globally and will not hesitate to seize, freeze, terminate the account and funds of Users which are flagged out or investigated by legal mandate.

## Verdict

The previous quote is very telling of this app's custodial nature. Maintaining a stance of "cooperation with law enforcement authorities, and attesting that they would **not hesitate** to seize, freeze, terminate the account and funds of Users", is highly indicative of a **custodial** service. This makes the app **not verifiable**.


