---
wsId: geminiwallet
title: "Gemini: Buy Bitcoin & Crypto"
altTitle: 
authors:
- leo
users: 1000000
appId: com.gemini.android.app
released: 2018-12-10
updated: 2021-11-03
version: "21.1102.0"
stars: 4.4
ratings: 26960
reviews: 5605
size: Varies with device
website: https://gemini.com
repository: 
issue: 
icon: com.gemini.android.app.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: gemini
providerLinkedIn: geminitrust
providerFacebook: GeminiTrust
providerReddit: 

redirect_from:
  - /com.gemini.android.app/
  - /posts/com.gemini.android.app/
---


This provider being an exchange, together with the lack of clear words of who
gets to hold the private keys leads us to believe this app is only an interface
to the Gemini exchange account and thus custodial and thus **not verifiable**.
