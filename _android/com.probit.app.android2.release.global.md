---
wsId: 
title: "ProBit Global: Buy & Sell Bitcoin. Crypto Exchange"
altTitle: 
authors:
- leo
users: 500000
appId: com.probit.app.android2.release.global
released: 2019-06-19
updated: 2021-11-04
version: "1.36"
stars: 3.2
ratings: 5596
reviews: 3391
size: 19M
website: https://www.probit.com
repository: 
issue: 
icon: com.probit.app.android2.release.global.png
bugbounty: 
verdict: custodial
date: 2020-11-28
signer: 
reviewArchive:


providerTwitter: ProBit_Exchange
providerLinkedIn: probit-exchange
providerFacebook: probitexchange
providerReddit: 

redirect_from:
  - /com.probit.app.android2.release.global/
---


Probit appears to also and mainly be an exchange and as we can't find claims to
the contrary, we assume this app is a custodial offering and thus **not verifiable**.
