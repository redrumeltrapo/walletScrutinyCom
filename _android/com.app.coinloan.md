---
wsId: CoinLoan
title: "CoinLoan: crypto wallet, trading, loans & deposits"
altTitle: 
authors:
- danny
users: 10000
appId: com.app.coinloan
released: 2019-11-28
updated: 2021-09-23
version: "Varies with device"
stars: 4.3
ratings: 245
reviews: 119
size: Varies with device
website: https://coinloan.io/
repository: 
issue: 
icon: com.app.coinloan.png
bugbounty: 
verdict: custodial
date: 2021-10-04
signer: 
reviewArchive:


providerTwitter: coin_loan
providerLinkedIn: coinloan
providerFacebook: coinloan.project
providerReddit: coinloan

redirect_from:

---


### App Description

> CoinLoan is your mobile e-currency wallet that allows trading, taking loans secured by e-currency, as well as exchanging, buying, and selling.

### The Site

> [Get a Loan Backed by Your Bitcoin](https://coinloan.io/crypto-backed-loans/bitcoin-loan/)

The [Terms and Conditions](https://coinloan.io/terms-and-conditions/) state in Section 5.18 that

> Please note that Your funds’ withdrawals and deposits to any of Your Accounts/wallets **outside the Platform may take up to seven (7) business days** in some cases. 

Furthermore in Section 7.2, 

> In case we detect that the information submitted by You is false/not up to date/wrong, we may, at our sole discretion, stop the service rendering on the Platform and/or **freeze Your funds on Your Account** on the Platform.

### The App

The app was not available for download in our locale. 

### Verdict

The ability to freeze accounts, and claims to be regulatory compliant, indicate with a strong degree of certainty that this is a **custodial** service and thus **not verifiable.**

