---
wsId: 
title: "Folgory Merchant"
altTitle: 
authors:

users: 1000
appId: com.app.folgory
released: 2019-08-14
updated: 2019-08-14
version: "1.5"
stars: 2.6
ratings: 14
reviews: 11
size: 8.4M
website: 
repository: 
issue: 
icon: com.app.folgory.png
bugbounty: 
verdict: obsolete
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


