---
wsId: rainfinancial
title: "Rain: Buy & Sell Bitcoin"
altTitle: 
authors:
- danny
users: 100000
appId: com.rainmanagement.rain
released: 2018-11-02
updated: 2021-11-03
version: "2.4.3"
stars: 3.0
ratings: 3259
reviews: 1725
size: 159M
website: https://www.rain.bh/
repository: 
issue: 
icon: com.rainmanagement.rain.png
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: rainfinancial
providerLinkedIn: rainfinancial
providerFacebook: rainfinancial
providerReddit: 

redirect_from:

---


### Google Play

The app is described as offering services that allow users to buy and sell Bitcoin using fiat from the Middle East. It goes on further:

> Rain is the first Middle Eastern cryptocurrency brokerage licensed by the Central Bank of Bahrain (CBB). We currently operate in Bahrain, Saudi Arabia, United Arab Emirates, Kuwait, and Oman. Rain allows you to securely buy, store and sell cryptocurrencies like Bitcoin, Ethereum, Litecoin. You can use your bank account or card to purchase cryptocurrency instantly through Rain.

### The Site

It describes itself as a [licensed custodian](https://www.rain.bh/)

### Verdict

Despite our inability to successfully use and register the app, it is clear that this is a **custodial** service and therefore, **not verifiable**

