---
wsId: brasilBitcoin
title: "Brasil Bitcoin"
altTitle: 
authors:
- danny
users: 100000
appId: br.com.brasilbitcoin
released: 2021-02-17
updated: 2021-10-27
version: "2.5.15"
stars: 3.0
ratings: 2114
reviews: 1457
size: 43M
website: https://brasilbitcoin.com.br
repository: 
issue: 
icon: br.com.brasilbitcoin.png
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: brbtcoficial
providerLinkedIn: 
providerFacebook: brbtcoficial
providerReddit: 

redirect_from:

---


This custodial service offers users the ability to buy, store, send and receive cryptocurrencies using an account. 

It is possible to purchase cryptocurrencies on the site using a debit card. 

KYC is required. 

In the [terms page](https://brasilbitcoin.com.br/terms), Brasil Bitcoin notes that:

> BRASIL BITCOIN SERVIÇOS DIGITAIS LTDA reserves the right to refuse any registration request and to suspend or cancel a previously accepted Registration in cases 

Verdict is **custodial** and **not verifiable**