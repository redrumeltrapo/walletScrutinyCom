---
wsId: 
title: "ECOINEX - 암호화폐(비트코인,이더리움) 거래소"
altTitle: 
authors:

users: 500
appId: cme.ecoinexV1.app
released: 2018-12-19
updated: 2018-12-19
version: "1.01"
stars: 0.0
ratings: 
reviews: 
size: 6.7M
website: 
repository: 
issue: 
icon: cme.ecoinexV1.app.png
bugbounty: 
verdict: defunct
date: 2021-10-22
signer: 
reviewArchive:

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-13**: This app is no more.

