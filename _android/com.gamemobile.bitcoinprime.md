---
wsId: 
title: "Bitcoin Prime - Robotics Cryptocurrency (2021)"
altTitle: 
authors:

users: 1000
appId: com.gamemobile.bitcoinprime
released: 2021-06-25
updated: 2021-06-25
version: "1.0.0"
stars: 0.0
ratings: 
reviews: 
size: 5.5M
website: 
repository: 
issue: 
icon: com.gamemobile.bitcoinprime.png
bugbounty: 
verdict: defunct
date: 2021-09-23
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-14**: This app is not on the Play Store anymore.
