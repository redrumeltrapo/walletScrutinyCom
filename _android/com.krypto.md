---
wsId: 
title: "Krypto: Bitcoin Investment App"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.krypto
released: 2020-04-29
updated: 2021-11-09
version: "8.5"
stars: 4.1
ratings: 3425
reviews: 1251
size: 28M
website: https://letskrypto.com
repository: 
issue: 
icon: com.krypto.png
bugbounty: 
verdict: custodial
date: 2021-04-25
signer: 
reviewArchive:


providerTwitter: letskrypto
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


The Krypto wallet has no statements on [their website](https://letskrypto.com) regarding the management of private keys.
this leads us to conclude the wallet funds are likely under the control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
