---
wsId: 
title: "BitStash Wallet - Multi Cryptocurrency Wallet"
altTitle: 
authors:

users: 100
appId: app.odapplications.bitstashwallet
released: 2019-11-10
updated: 2019-11-24
version: "0.0.3"
stars: 0.0
ratings: 
reviews: 
size: 15M
website: 
repository: 
issue: 
icon: app.odapplications.bitstashwallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


