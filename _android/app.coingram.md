---
wsId: 
title: "Coingram | بیت کوین , ارز دیجیتال | کوینگرام"
altTitle: 
authors:
- danny
users: 100000
appId: app.coingram
released: 2019-07-02
updated: 2021-09-20
version: "3.0.1"
stars: 4.7
ratings: 43666
reviews: 24515
size: 24M
website: 
repository: 
issue: 
icon: app.coingram.png
bugbounty: 
verdict: nowallet
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


Note that as of 2021-08-23 this app does not seem to have an English translation.

> Coingram offers an overview of financial markets particularly focus on the digital currencies, ideal for staying up to date with the latest news, tutorials and analysis from the experts and various popular sources.

Nothing about Coingram suggests that you can send or receive bitcoins. It does not even refer to itself as a wallet.

> Using the Coingram **portfolio tracker**, you can instantly check the profits and losses of your assets and have access to all transactions and information of each digital currency.

However, it does have a portfolio tracker.

Until further update, this app will be classified as **not a wallet.**
