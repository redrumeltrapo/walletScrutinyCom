---
wsId: 
title: "Aries: Investment & Trading, Commission-free"
altTitle: 
authors:

users: 1000
appId: com.trading.aries
released: 2021-06-23
updated: 2021-10-04
version: "2.01.3"
stars: 4.2
ratings: 13
reviews: 9
size: 9.4M
website: 
repository: 
issue: 
icon: com.trading.aries.png
bugbounty: 
verdict: wip
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


