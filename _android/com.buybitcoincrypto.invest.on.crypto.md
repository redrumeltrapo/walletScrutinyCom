---
wsId: 
title: "Invest On Crypto"
altTitle: 
authors:

users: 0
appId: com.buybitcoincrypto.invest.on.crypto
released: 2021-06-24
updated: 2021-06-24
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: com.buybitcoincrypto.invest.on.crypto.png
bugbounty: 
verdict: defunct
date: 2021-08-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-13**: This app is not available anymore
