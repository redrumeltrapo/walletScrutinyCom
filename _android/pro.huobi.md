---
wsId: huobi
title: "Huobi Global: Buy BTC, NFTs& Meta"
altTitle: 
authors:
- leo
users: 1000000
appId: pro.huobi
released: 2017-11-01
updated: 2021-10-27
version: "6.5.6"
stars: 4.3
ratings: 16571
reviews: 5306
size: 98M
website: https://www.huobi.com/en-us
repository: 
issue: 
icon: pro.huobi.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: HuobiGlobal
providerLinkedIn: 
providerFacebook: huobiglobalofficial
providerReddit: 

redirect_from:
  - /pro.huobi/
  - /posts/pro.huobi/
---


Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
