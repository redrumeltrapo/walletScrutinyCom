---
wsId: 
title: "업비트 - 가장 신뢰받는 디지털 자산(비트코인, 이더리움, 비트코인캐시) 거래소"
altTitle: 
authors:
- danny
users: 5000000
appId: com.dunamu.exchange
released: 2017-10-23
updated: 2021-11-09
version: "1.15.2p1"
stars: 4.2
ratings: 28167
reviews: 10411
size: 66M
website: https://upbit.com
repository: 
issue: 
icon: com.dunamu.exchange.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: upbitglobal
providerLinkedIn: upbit-official
providerFacebook: upbit.exchange
providerReddit: 

redirect_from:

---


There are two versions of this app: one for Korea and one that's global. 

Upbit uses ID verification and KYC therefore we can assume that it is **custodial** and **not verifiable**. You cannot deposit or withdraw BTC as well as other altcoins without finishing their level 2 KYC. They also claim to have multi-sig and cold wallets.
