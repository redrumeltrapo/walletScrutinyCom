---
wsId: 
title: "BoltX: Ethereum & Crypto Wallet"
altTitle: 
authors:
- kiwilamb
users: 5000
appId: com.bolt.pegasus
released: 2019-08-26
updated: 2021-11-08
version: "1.20.2"
stars: 4.5
ratings: 363
reviews: 296
size: 46M
website: https://bolt.global/
repository: 
issue: 
icon: com.bolt.pegasus.png
bugbounty: 
verdict: custodial
date: 2021-04-15
signer: 
reviewArchive:


providerTwitter: bolt_global
providerLinkedIn: bolt-global
providerFacebook: Global.Bolt
providerReddit: 

redirect_from:

---


Seems like a secondary project called "Pegasus" of Bolt Global where you can earn BOLT tokens into the Bolt-X wallet from using the Bolt+ interactive media companion app.

*(Besides that, we couldn't find any source code or even a claim of it being non-custodial.)*
