---
wsId: 
title: "Cloud 2.0"
altTitle: 
authors:

users: 100000
appId: com.cloudwallet.android
released: 2019-05-11
updated: 2020-01-22
version: "2.0.16"
stars: 1.5
ratings: 3977
reviews: 2311
size: 31M
website: 
repository: 
issue: 
icon: com.cloudwallet.android.png
bugbounty: 
verdict: stale
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


