---
wsId: 
title: "BTC Wallet - Bitcoin Exchange"
altTitle: 
authors:
- leo
users: 1000000
appId: btc.org.freewallet.app
released: 2016-06-13
updated: 2021-09-20
version: "2.6.8"
stars: 4.6
ratings: 6639
reviews: 3441
size: 8.3M
website: https://freewallet.org
repository: 
issue: 
icon: btc.org.freewallet.app.png
bugbounty: 
verdict: custodial
date: 2020-10-12
signer: 
reviewArchive:


providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: Freewallet_org

redirect_from:
  - /btc.org.freewallet.app/
  - /posts/btc.org.freewallet.app/
---


According to their description on Google Play, this is a custodial app:

> The Freewallet team keeps most of our customers’ coins in offline cold storage
to ensure the safety of your funds.

Our verdict: This app is **not verifiable**.
