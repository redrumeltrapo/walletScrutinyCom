---
wsId: 
title: "Alpha Wallet. Bitcoin, Ethereum, Litecoin"
altTitle: 
authors:

users: 1000
appId: com.alpha.wallet
released: 
updated: 2018-05-31
version: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: 
repository: 
issue: 
icon: com.alpha.wallet.png
bugbounty: 
verdict: defunct
date: 2021-10-15
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

**Update 2021-10-07**: This app is not on Play Store anymore.

