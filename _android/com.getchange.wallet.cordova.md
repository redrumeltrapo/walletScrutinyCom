---
wsId: getchange
title: "Change: Simple Investment App"
altTitle: 
authors:
- leo
users: 100000
appId: com.getchange.wallet.cordova
released: 2018-06-07
updated: 2021-10-08
version: "10.31.0"
stars: 4.3
ratings: 2534
reviews: 1124
size: 32M
website: https://getchange.com
repository: 
issue: 
icon: com.getchange.wallet.cordova.jpg
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: changefinance
providerLinkedIn: changeinvest
providerFacebook: changeinvest
providerReddit: 

redirect_from:
  - /com.getchange.wallet.cordova/
  - /posts/com.getchange.wallet.cordova/
---


On their Google Play description we find

> • Secure: Funds are protected in multi-signature, cold-storage cryptocurrency
  wallets

which means it is a custodial service and thus **not verifiable**.
