---
wsId: BlockTrade
title: "Blocktrade - Digital assets exchange"
altTitle: 
authors:
- danny
users: 10000
appId: com.blocktrade.android
released: 2018-10-28
updated: 2021-10-22
version: "1.3.4-77-g0476"
stars: 4.4
ratings: 146
reviews: 66
size: 11M
website: https://blocktrade.com/
repository: 
issue: 
icon: com.blocktrade.android.jpg
bugbounty: 
verdict: custodial
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: Blocktradecom
providerLinkedIn: blocktradecom
providerFacebook: Blocktradecom
providerReddit: 

redirect_from:

---


## App Description

This app provides cryptocurrency trading services.

## The Site

### [Terms of Use](https://blocktrade.com/legal/terms-of-use/)

**Private Keys**

> **Section 6.3**. The Crypto-assets of the User are in **custody** of the Operator at the level of a general account opened in the Operator’s name with a Crypto-asset custody wallet provider. In accordance thereto, the Operator holds the public and private keys giving access to the relevant account at the level of the Crypto-asset wallet provider.

**Termination**

> **Section 13.2.** User consents that Companies may, by giving notice, in its sole discretion terminate Users access to the Account and use of Services including without limitation:
>
> - terminate the Services,
> - prohibit access to certain Services,
> - delay or remove certain Services.

## The App

The app can only be accessed once a lengthy questionnaire is filled. Examples of questions include: 

> - Will you trade with your own funds?
> - Has someone advised you make specific trades or transaction on blocktrade.com?
> - Are you associated in any business or personally with a politically exposed individual?

## Verdict

KYC, AML statutes, plus an extensive questionnaire and the outright detailing of the custodial arrangements of the service, make this **custodial** making the app **not possible to verify**.

