---
wsId: blockfolio
title: "FTX (formerly Blockfolio) - Buy Bitcoin Now"
altTitle: 
authors:
- leo
users: 1000000
appId: com.blockfolio.blockfolio
released: 2015-10-01
updated: 2021-10-31
version: "4.2.1"
stars: 4.1
ratings: 157877
reviews: 48335
size: 63M
website: https://www.blockfolio.com
repository: 
issue: 
icon: com.blockfolio.blockfolio.png
bugbounty: 
verdict: custodial
date: 2021-08-09
signer: 
reviewArchive:
- date: 2019-11-10
  version: "4.0.1"
  appHash: 
  gitRevision: a7a616c5d7474cbbd0fdaa0387b8ce4cc578e960
  verdict: nowallet

providerTwitter: Blockfolio
providerLinkedIn: 
providerFacebook: Blockfolio
providerReddit: 

redirect_from:
  - /blockfolio/
  - /com.blockfolio.blockfolio/
  - /posts/2019/11/blockfolio/
  - /posts/com.blockfolio.blockfolio/
---


**Update 2021-08-09**: By now this app clearly sounds like an exchange that lets
you buy, hold, send and receive BTC but as an exchange it's certainly custodial
and thus **not verifiable**. There are a total of 6 related apps that all appear to belong to the same "FTX":

* {% include walletLink.html wallet='android/com.blockfolio.blockfolio' %}
* {% include walletLink.html wallet='iphone/com.blockfolio.blockfolio' %}
* {% include walletLink.html wallet='android/com.ftxmobile.ftx' %}
* {% include walletLink.html wallet='iphone/org.reactjs.native.example.FTXMobile.FTX' %}
* {% include walletLink.html wallet='android/com.ftxmobile.ftxus' %}
* {% include walletLink.html wallet='iphone/com.ftx.FTXMobile.FTXUS' %}

# Old Analysis

This is not a wallet.
