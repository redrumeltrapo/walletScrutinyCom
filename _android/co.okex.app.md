---
wsId: OKEx
title: "اوکی اکسچنج، خرید بیت کوین و ارز دیجیتال"
altTitle: 
authors:
- leo
users: 100000
appId: co.okex.app
released: 2019-09-11
updated: 2021-10-10
version: "6.0.2"
stars: 4.1
ratings: 5774
reviews: 2836
size: 9.2M
website: https://ok-ex.co
repository: 
issue: 
icon: co.okex.app.png
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:
- date: 2020-11-16
  version: ""
  appHash: 
  gitRevision: bcb5dbfd724ca531c1965cce7ef0d38f023e4c0c
  verdict: custodial

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /co.okex.app/
---


**Update:** This app appears to have disappeared from Google Play or maybe only
from English Google Play, as it apparently was in Arab, only? If we should
re-add it, please create an issue on our GitLab.

> The okex app is a digital currency trading platform

as such, this is probably a custodial offering.

As the website is broken, we can't find any contrary claims and conclude, this
app is **not verifiable**.
