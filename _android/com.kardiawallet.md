---
wsId: kardiawallet
title: "KardiaChain Wallet"
altTitle: 
authors:
- danny
users: 50000
appId: com.kardiawallet
released: 2021-01-13
updated: 2021-11-05
version: "2.3.39"
stars: 4.5
ratings: 815
reviews: 404
size: 56M
website: https://kardiachain.io/
repository: 
issue: 
icon: com.kardiawallet.jpg
bugbounty: 
verdict: nobtc
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: KardiaChain
providerLinkedIn: 
providerFacebook: KardiaChainFoundation
providerReddit: 

redirect_from:

---


The KardiaChain wallet is specifically for use with the KAI token. 

From its [documentation](https://docs.kardiachain.io/docs/)

>KardiaChain is an accessible blockchain for millions and is a decentralized public blockchain which provides the dual Master node to facilitate inter-chain operations among both existing and upcoming blockchain platforms

We downloaded the app and verified that it **does not have a bitcoin wallet**. 

