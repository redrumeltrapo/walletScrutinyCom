---
wsId: 
title: "IQFinex Exchange"
altTitle: 
authors:

users: 100
appId: com.iqfinex_exchange
released: 2019-05-16
updated: 2019-10-30
version: "1.0.4"
stars: 1.7
ratings: 6
reviews: 3
size: 6.0M
website: 
repository: 
issue: 
icon: com.iqfinex_exchange.png
bugbounty: 
verdict: defunct
date: 2021-10-22
signer: 
reviewArchive:

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-13**: This app is no more.

