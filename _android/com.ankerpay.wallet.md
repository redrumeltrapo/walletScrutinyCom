---
wsId: 
title: "AnkerPay: Blockchain Crypto Wallet – BTC, ETH, LTC"
altTitle: 
authors:
- leo
users: 10000
appId: com.ankerpay.wallet
released: 2019-09-17
updated: 2021-10-11
version: "v1.0.12.4"
stars: 4.0
ratings: 107
reviews: 89
size: 4.9M
website: https://ankerid.com/mobile-wallet
repository: 
issue: 
icon: com.ankerpay.wallet.png
bugbounty: 
verdict: nosource
date: 2020-06-20
signer: 
reviewArchive:


providerTwitter: AnkerPay
providerLinkedIn: 
providerFacebook: AnkerPlatform
providerReddit: 

redirect_from:
  - /com.ankerpay.wallet/
  - /posts/com.ankerpay.wallet/
---


This app promises to be non-custodial:

> AnkerSwitch is an alternative cryptocurrency exchange where users have more
  control. Swap any 2 currencies seamlessly while still controlling the assets
  within your wallet. This allows for complete control and safety since your
  crypto will never be left on an exchange.

Unfortunately neither the description on Google Play nor their website has a
link to source code which leads to our verdict **not verifiable**.
