---
wsId: bitay
title: "Bitay - Easy Bitcoin Exchange"
altTitle: 
authors:
- danny
users: 100000
appId: com.app.bitay
released: 2019-04-15
updated: 2020-11-09
version: "2.1.1"
stars: 4.5
ratings: 5715
reviews: 3934
size: 3.6M
website: https://www.bitay.com
repository: 
issue: 
icon: com.app.bitay.png
bugbounty: 
verdict: stale
date: 2021-11-07
signer: 
reviewArchive:
- date: 2021-08-27
  version: "2.1.1"
  appHash: 
  gitRevision: a5f6ad88ff8926faf6f2ce111aff123860ea1e50
  verdict: custodial

providerTwitter: BitayTurkiye
providerLinkedIn: 
providerFacebook: bitayturkiye
providerReddit: 

redirect_from:

---


Site Description:

> Turkey's Most Trusted Crypto Currency Exchange

Bitay's [Security Policy](https://www.bitay.com/en/security-policy) discusses how its wallets are secured:

> the technology from our trading platform to wallets has been developed within the company in a 100% secure manner. No 3rd party software has been used in case of a security risk. 98% of your platform's crypto-currencies are stored privately in cold wallets that are not connected to the internet.

Bitay also requires [identity verification](https://www.bitay.com/en/how/how-to-do-identity-verification)

The [Support Center > Deposit and Withdraw > Withdraw page](https://www.bitay.com/en/support/deposit-withdraw/withdraw-cryptocurrency-from-bitay) is blank.

However, it has a [status page](https://www.bitay.com/en/status) for the availability of cryptocurrency Withdrawals/Deposits and fees. 

These point to a **custodial** service with an exchange.
