---
wsId: 
title: "CryptoMarket"
altTitle: 
authors:

users: 50000
appId: com.cryptomkt.app
released: 2017-08-03
updated: 2020-07-20
version: "3.2.1"
stars: 2.9
ratings: 460
reviews: 323
size: 11M
website: 
repository: 
issue: 
icon: com.cryptomkt.app.png
bugbounty: 
verdict: defunct
date: 2021-09-19
signer: 
reviewArchive:
- date: 2021-08-17
  version: "3.2.1"
  appHash: 
  gitRevision: 44349336ff99dc0f303d6e8cf752be5bb05d3447
  verdict: stale
  
providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-11**: This app is not on the Play Store anymore.
