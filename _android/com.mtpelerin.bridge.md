---
wsId: 
title: "Bridge Wallet, the Swiss app for Bitcoin & Crypto"
altTitle: 
authors:
- leo
users: 10000
appId: com.mtpelerin.bridge
released: 2020-04-07
updated: 2021-10-28
version: "1.18"
stars: 4.3
ratings: 116
reviews: 63
size: 167M
website: https://www.mtpelerin.com/bridge-wallet
repository: 
issue: 
icon: com.mtpelerin.bridge.png
bugbounty: 
verdict: nosource
date: 2020-12-27
signer: 
reviewArchive:


providerTwitter: mtpelerin
providerLinkedIn: mt-pelerin
providerFacebook: mtpelerin
providerReddit: MtPelerin

redirect_from:
  - /com.mtpelerin.bridge/
---


On Google Play they claim

> **YOU ARE IN CONTROL**<br>
  Bridge Wallet is a decentralized, non-custodial wallet. It means that you are
  in full control of your wallet, its content and seed phrase.

But while the provider [has a GitHub presence](https://github.com/MtPelerin),
there is no claim about public source and neither do we find any wallet
repository on their GitHub.

As a closed source wallet, this is **not verifiable**.
