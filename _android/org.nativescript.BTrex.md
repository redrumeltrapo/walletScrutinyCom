---
wsId: 
title: "Multi Exchange Crypto Trading Terminal"
altTitle: 
authors:
- leo
users: 10
appId: org.nativescript.BTrex
released: 2018-04-26
updated: 2018-06-03
version: "1.0.6.2"
stars: 0.0
ratings: 
reviews: 
size: 21M
website: 
repository: 
issue: 
icon: org.nativescript.BTrex.png
bugbounty: 
verdict: defunct
date: 2021-10-30
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-21**: This app is no more.
