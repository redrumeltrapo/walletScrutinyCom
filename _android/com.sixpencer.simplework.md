---
wsId: dfox
title: "Dfox-Crypto Wallet and DeFi Portfolio"
altTitle: 
authors:
- leo
users: 10000
appId: com.sixpencer.simplework
released: 2020-10-09
updated: 2021-06-23
version: "1.4.3"
stars: 4.2
ratings: 189
reviews: 93
size: 21M
website: https://dfox.cc
repository: 
issue: 
icon: com.sixpencer.simplework.jpg
bugbounty: 
verdict: nowallet
date: 2021-07-29
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app appears not to get access to spend your Bitcoins:

> Dfox is a chain-agnostic crypto portfolio tracker.
