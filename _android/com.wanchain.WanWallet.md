---
wsId: WanWallet
title: "WanWallet"
altTitle: 
authors:
- danny
users: 10000
appId: com.wanchain.WanWallet
released: 2019-08-18
updated: 2021-04-25
version: "5.0"
stars: 3.8
ratings: 113
reviews: 71
size: 65M
website: https://www.wanchain.org/
repository: https://github.com/wanchain
issue: 
icon: com.wanchain.WanWallet.png
bugbounty: 
verdict: wip
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: wanchain_org
providerLinkedIn: 
providerFacebook: wanchainfoundation
providerReddit: wanchain

redirect_from:

---


## App Description
WanWallet was developed by Wanchain Foundation. It says that it supports Wanchain Galaxy Consensus Staking, which they also call "PoS Staking."

> You can manage your Wanchain assets and stake your Wancoins to receive staking rewards using Wanchain Wallet.

> Wanchain Wallet is a non-custodial wallet, which means you own the keys and you are in control. Wanchain Foundation will never ask you for your HD seed phrase under any circumstances.

## The App
We tried the app. We are immediately greeted with the option to create a wallet or import one.

We chose to create a wallet and were asked to enter a password. Then, we are provided with a 12-word seed phrase, and told to record it in a safe place, preferably offline. The app noting that:

> the Wanchain team will never ask for your seed phrase.


Wanchain has a Github account with repositories for apps and SDKs. Their [**repository for the desktop wallet**](https://github.com/wanchain/wan-wallet-desktop) is publicly available source code with build instructions.

At the moment, we will [wait for the source-code to be tested](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/354) and proved reproducible before making a verdict.
