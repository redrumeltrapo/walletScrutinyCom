---
wsId: 
title: "Bitocto: Jual Beli Bitcoin Indonesia"
altTitle: 
authors:
- danny
users: 10000
appId: com.bitocto
released: 2019-07-18
updated: 2021-10-21
version: "3.12"
stars: 4.3
ratings: 1498
reviews: 1210
size: 5.3M
website: https://bitocto.com/
repository: 
issue: 
icon: com.bitocto.png
bugbounty: 
verdict: custodial
date: 2021-09-23
signer: 
reviewArchive:


providerTwitter: bitoctoexchange
providerLinkedIn: bitocto
providerFacebook: Bitocto
providerReddit: 

redirect_from:

---


This Indonesian cryptocurrency exchange allows users to buy, sell and invest in Bitcoin and other cryptocurrencies. 

As with most custodial services, its [Terms and Conditions](https://bitocto.com/en/terms-of-use/) are good indicators for its custodial nature:

> You must verify your identity to withdraw & deposit funds (Digital Asset or Fiat).<br><br>
Bitocto does not guarantee Bitcoin, other digital assets or bank transfer disbursement times and will not be liable for any delays.<br><br>
Orders, withdrawals and deposits are subject to limits which may be changed over time.<br><br>
Bitocto may suspend your account at any time for illegal activities or misconduct.<br><br>

### Private Keys

>Bitocto securely stores all Supported Digital Asset private keys associated with any Bitocto account. You accept and agree that Bitocto shall retain full ownership and control of the Private Keys associated with your Bitocto account and that you shall have no control of, access to, or the ability to use, such Private Keys.

Evidently, this exchange is **custodial** and therefore **not verifiable**.

