---
title: "Sats App by Casa"
altTitle: 

users: 1000
appId: com.satsapp
released: 
updated: 2020-04-13
version: "1.3.1"
stars: 3.1
ratings: 14
reviews: 8
size: 29M
website: https://keys.casa
repository: 
issue: 
icon: com.satsapp.png
bugbounty: 
verdict: defunct
date: 2020-07-31
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.satsapp/
  - /posts/com.satsapp/
---


This app was removed from Google Play.
