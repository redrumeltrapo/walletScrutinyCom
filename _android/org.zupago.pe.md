---
wsId: 
title: "ZuPago"
altTitle: 
authors:
- kiwilamb
users: 100
appId: org.zupago.pe
released: 
updated: 2018-12-08
version: "Varies with device"
stars: 5.0
ratings: 5
reviews: 2
size: Varies with device
website: https://zupago.pe/
repository: 
issue: 
icon: org.zupago.pe.png
bugbounty: 
verdict: defunct
date: 2021-04-15
signer: 
reviewArchive:
- date: 2021-04-15
  version: 
  appHash: 
  gitRevision: 2adf93055d5b552806d8a041f41c1e8e4a7c5fd6
  verdict: fewusers


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


