---
wsId: 
title: "VisionWallet — Crypto & Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 1000
appId: com.visionwallet.app
released: 2020-09-07
updated: 2020-11-05
version: "1.3.2"
stars: 3.9
ratings: 77
reviews: 71
size: 4.9M
website: https://visionwallet.com/en
repository: 
issue: 
icon: com.visionwallet.app.png
bugbounty: 
verdict: stale
date: 2021-11-01
signer: 
reviewArchive:
- date: 2020-12-01
  version: "1.3.2"
  appHash: 
  gitRevision: 09529b3429431f06b68b1e2e04d5ec285067c626
  verdict: custodial

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.visionwallet.app/
---


> Passive funds accumulation (interest on the account balance)

A feature like this makes no sense in a non-custodial app and as there are no
actual claims about this app being non-custodial, we file it as custodial and
thus **not verifiable**.
