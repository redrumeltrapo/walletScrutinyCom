---
wsId: Bitwells
title: "Bitwells - Easy Bitcoin/Crypto Futures Trading"
altTitle: 
authors:
- danny
users: 5000
appId: com.bitwells.android
released: 2021-05-17
updated: 2021-06-02
version: "1.0.5"
stars: 4.6
ratings: 1303
reviews: 1298
size: 11M
website: https://www.bitwells.com/
repository: 
issue: 
icon: com.bitwells.android.png
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: bitwells
providerLinkedIn: 
providerFacebook: Bitwells
providerReddit: 

redirect_from:

---


## App Description

Bitwells describes itself as a:

> Financial derivatives trading platform focusing on the Bitcoin market, providing futures leveraged trading of mainstream digital currencies like Bitcoin, Ethereum, Litecoin, Ripple, etc. 

The app claims that it does not require KYC

## The Site

### User Agreement

> **Section 11. Termination & Remedies for Breach of these Terms by You**
>
> a) Bitwells reserves the right to seek all remedies available at law and in equity for violations of these Terms, including without limitation, **the right to restrict, suspend or terminate your account or deny you access to the Website without notice**; and
> b) _Bitwells shall be entitled to disclose your user identity and personal details if required or requested by a court of law_, governmental agency or any other law enforcement authority in such circumstances as Bitwells in its sole discretion considers reasonably necessary or appropriate.

## The App

We downloaded the app, and it was possible to deposit and withdraw Bitcoin. 

Security measures include:

- Password
- Google Authenticator
- Touch ID

Seed phrases, mnemonics or backup of private keys are not available

## Verdict

As a futures trading platform and even without KYC, this app is centralized with the app developers. As stated in their user-agreement, Bitwells even has the power to inform authorities of their users' identities. With no seed-phrase, private keys backup or any features resembling those, it is safe to say that this service is **custodial**. The **app cannot be verified.**
