---
wsId: 
title: "Trademonk Cryptocurrency Exchange"
altTitle: 
authors:

users: 10000
appId: com.ionicframework.myapp474083
released: 2017-06-16
updated: 2018-10-22
version: "2.0.13"
stars: 2.9
ratings: 278
reviews: 174
size: 8.6M
website: 
repository: 
issue: 
icon: com.ionicframework.myapp474083.jpg
bugbounty: 
verdict: defunct
date: 2021-10-15
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-07**: This app is not on Play Store anymore.
