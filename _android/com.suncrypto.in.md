---
wsId: 
title: "Sun Crypto: Bitcoin, Crypto Trading Exchange India"
altTitle: 
authors:

users: 100000
appId: com.suncrypto.in
released: 2021-06-28
updated: 2021-10-08
version: "1.5"
stars: 3.9
ratings: 2122
reviews: 1331
size: 12M
website: https://suncrypto.in/
repository: 
issue: 
icon: com.suncrypto.in.png
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: suncryptoin
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


We downloaded the app. 

As a cryptocurrency exchange, our initial analysis points to this as a custodial service. 

According to its [Terms](https://suncrypto.in/terms) page:

> After registration of your Client Asset Account, as per the required process You will be eligible to purchase, send, receive or store crypto assets supported by the SunCrypto Platform. You may be required to maintain a minimum balance of funds in your wallet before you initiate certain transactions. **We reserve the right to refuse to execute any order and/or transaction initiated by you, if the same is in contravention to the AML Policy or to comply with directions of appropriate enforcement authorities.**

KYC verification is required.

Under "Client Funds" on the same page:

> By accepting these Terms of Use, and to the extent not prohibited by applicable law, You hereby grant to SunCrypto the authority to open and establish a ‘client account’ and **You agree to grant SunCrypto the right to control the Client’s assets including crypto assets and client fiat**.

The transaction fees are detailed on this [page](https://suncrypto.in/trading-deposit)

This concludes our verdict describing this app as a **custodial** service and thus **cannot be verified.**
