---
wsId: ProtonWallet
title: "Proton Wallet"
altTitle: 
authors:
- danny
users: 10000
appId: com.metallicus.protonwallet
released: 2021-02-22
updated: 2021-10-21
version: "1.5.20"
stars: 4.7
ratings: 526
reviews: 318
size: 46M
website: https://www.protonchain.com/
repository: https://github.com/ProtonProtocol/
issue: 
icon: com.metallicus.protonwallet.png
bugbounty: 
verdict: nosource
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: protonxpr
providerLinkedIn: 
providerFacebook: protonxpr
providerReddit: ProtonChain

redirect_from:

---


## App Description

The app describes that the private keys are stored on the user's device. It allows staking and features support for multiple cryptocurrencies.

## The Site

### Terms and Conditions

The [Terms](https://www.protonchain.com/terms/) has a provision on termination:

> In the event of termination concerning your license to use Proton Wallet, your obligations under this Agreement will still continue. **Your access to the funds in your Wallet after termination will depend on your access to your backup of your Wallet address and private key.**

## The App

Upon app installation, registration and verification, the first thing to do was to create a backup for the private key. The private key was provided. We tried to import it into Electrum, and was successful. 

## Verdict

We checked the repository of the [Proton on Github](https://github.com/ProtonProtocol/) but was unable to find the source code for the Android App. Our verdict details a self-custodial app with **no source code.**

