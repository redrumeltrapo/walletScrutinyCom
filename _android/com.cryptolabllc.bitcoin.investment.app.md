---
wsId: 
title: "Bitcoin Investment App"
altTitle: 
authors:

users: 1
appId: com.cryptolabllc.bitcoin.investment.app
released: 2021-06-18
updated: 2021-06-18
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 3.4M
website: 
repository: 
issue: 
icon: com.cryptolabllc.bitcoin.investment.app.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
