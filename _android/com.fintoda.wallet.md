---
wsId: 
title: "Fintoda Wallet"
altTitle: 
authors:

users: 10
appId: com.fintoda.wallet
released: 
updated: 2021-08-02
version: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: 
repository: 
issue: 
icon: com.fintoda.wallet.png
bugbounty: 
verdict: defunct
date: 2021-09-04
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-26**: This app is no more.
