---
wsId: BitPreco
title: "Comprar Bitcoin, Ethereum e criptomoedas: BitPreço"
altTitle: 
authors:
- danny
users: 10000
appId: com.bitpreco.bitprecoAppAndroid
released: 2021-02-21
updated: 2021-08-29
version: "1.8.16"
stars: 4.2
ratings: 555
reviews: 379
size: 63M
website: https://bitpreco.com/
repository: 
issue: 
icon: com.bitpreco.bitprecoAppAndroid.png
bugbounty: 
verdict: custodial
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: BitPreco
providerLinkedIn: bitpreco
providerFacebook: BitPreco
providerReddit: 

redirect_from:

---


## App Description

This app looks like a Brazilian cryptocurrency exchange. 

Self-described as a Cryptocurrency marketplace, it claims to provide services to Latin Americans and has claimed to partner with Kraken, Binance, Braziliex, Coinext, Foxbit, Bitcambio, Bitblue, Bitfines, Bitcointrade, Novadax, FTX, etc. 

It allows the buying and selling of Bitcoins. 

## The Site

It is clear that this service is custodial based on [Section 12](https://bitpreco.com/termos) of their Terms and Conditions stating:

> In case of suspected fraud or any other illegal activity, BitPreço may, in addition to resorting to applicable legal measures, retain any funds or Cryptocurrencies stored in the Wallet and/or User Account or in any other way in the custody of BitPreço, until the conclusion of the respective investigations and legal determinations.

The jurisdiction that will apply is the Federative Republic of Brazil.

[**Section 6.1](https://bitpreco.com/termos) Cryptocurrency Sales Order**

> In order to be able to issue a Sales Order, the User must have Cryptocurrencies stored in his Wallet. Cryptocurrencies can be stored in the Wallet upon deposit at the address provided by the Platform.

## Verdict

The platform provides the wallets for the user which is hosted on their service. This service is **custodial** making the app **not verifiable.**
