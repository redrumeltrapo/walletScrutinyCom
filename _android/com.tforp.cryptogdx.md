---
wsId: BitcoinTrends
title: "Bitcoin Trends: Cryptocurrency trading signals"
altTitle: 
authors:
- danny
users: 100000
appId: com.tforp.cryptogdx
released: 2018-02-04
updated: 2021-05-28
version: "2.3"
stars: 4.5
ratings: 3093
reviews: 1366
size: 9.2M
website: https://trading4pro.com/
repository: 
issue: 
icon: com.tforp.cryptogdx.png
bugbounty: 
verdict: nowallet
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

The app provides cryptocurrency market trends from Coinbase Pro. It then processes these information into trading signals with indicators for "Strong Buy" or "Strong Sell". 

The app uses the following technical analysis: EMA, Stochastic and RSI. It also provides trading volume analysis including data derived from buy/sell ratio plus "Cryptocurrency Heatmaps".

## Verdict

The app **does not have any cryptocurrency or bitcoin wallet**.
