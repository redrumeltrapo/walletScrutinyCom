---
wsId: 
title: "Bitcoin Testnet Wallet for COINiD"
altTitle: 
authors:

users: 1000
appId: org.coinid.wallet.tbtc
released: 2019-02-06
updated: 2021-02-20
version: "1.8.0"
stars: 3.0
ratings: 6
reviews: 3
size: 34M
website: 
repository: 
issue: 
icon: org.coinid.wallet.tbtc.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


