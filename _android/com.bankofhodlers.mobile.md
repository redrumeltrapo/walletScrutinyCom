---
wsId: Vauld
title: "Vauld -  Earn, Borrow & Trade With Crypto"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.bankofhodlers.mobile
released: 2020-04-30
updated: 2021-10-21
version: "2.4.0"
stars: 4.3
ratings: 2706
reviews: 1015
size: 29M
website: https://www.vauld.com/
repository: 
issue: 
icon: com.bankofhodlers.mobile.jpg
bugbounty: 
verdict: custodial
date: 2021-05-08
signer: 
reviewArchive:


providerTwitter: Vauld_
providerLinkedIn: vauld
providerFacebook: VauldOfficial
providerReddit: BankofHodlers

redirect_from:

---


The Vauld website Help Center had an article "Security at Vauld" which covers a number of security risk.<br>
A statement of the management of the users "funds" makes it pretty clear the wallets private keys are in control of the provider.

> Our funds are managed through a multi signature system with the signatories being our co-founders.

Our verdict: This wallet is custodial and therefore **not verifiable**.

