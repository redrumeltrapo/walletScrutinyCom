---
wsId: LeadWallet
title: "Lead Wallet – Swap, Multisend BTC, BNB, ETH, LEAD"
altTitle: 
authors:
- danny
users: 5000
appId: com.leadWallet
released: 2021-07-29
updated: 2021-10-27
version: "1.0.8"
stars: 4.7
ratings: 1527
reviews: 1307
size: 38M
website: https://leadwallet.io/
repository: 
issue: 
icon: com.leadWallet.png
bugbounty: 
verdict: nosource
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: leadwallet
providerLinkedIn: leadwallet
providerFacebook: 
providerReddit: LeadWallet

redirect_from:

---


This app is not to be confused with its BETA version, {% include walletLink.html wallet='android/com.leadwallet.io' verdict='true' %} 

As seen on the [wallet page](https://leadwallet.io/wallet), the Beta version will be **discontinued.**

> The beta version of Lead Wallet App will be discontinued shortly, kindly download our mainnet app...

## App Description

> Crypto, DeFi and NFT Access Wallet for Bitcoin, Litecoin, Ethereum, Tether, Tron, BNB.

## The Site

### [The Terms of Service (PDF)](https://leadwallet.io/pdf/Terms%20of%20Service.pdf)

> With our services, you can generate and save the private master key that only corresponds to the wallet and must be used in conjunction with the wallet to authorize you to access the wallet. We do not store any data in our server, and all encrypted backup of certain information about the wallet is stored on the user side. You are solely responsible for the security of your private primary key and any backup mnemonic security associated with your wallet.

## The App

We downloaded the app. It offers two choices: create a wallet or import a wallet. Wallet creation entails the provision of the 12-seed words. There is a BTC wallet that can send and receive. 

## Verdict

The Github repository has been provided for this app. However, there is no appID, Android build instructions, or any Android app source code. Unless there is any further clarification, we assume that this app is closed source.
