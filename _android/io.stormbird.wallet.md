---
wsId: AlphaWallet
title: "AlphaWallet"
altTitle: 
authors:
- danny
users: 10000
appId: io.stormbird.wallet
released: 2018-05-19
updated: 2021-11-01
version: "3.40.5"
stars: 4.2
ratings: 189
reviews: 78
size: 35M
website: https://alphawallet.com/
repository: https://github.com/alphawallet
issue: 
icon: io.stormbird.wallet.png
bugbounty: 
verdict: nobtc
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: Alpha_wallet
providerLinkedIn: 
providerFacebook: 
providerReddit: AlphaWallet

redirect_from:

---


## App Description

This app is an Ethereum wallet.

> AlphaWallet is an easy to use and secure **Ethereum wallet with native ERC20, ERC721 and ERC875 support.** AlphaWallet supports all Ethereum based networks, Ethereum, xDai, Ethereum Classic, Artis, POA, Ropsten, Goerli, Kovan, Rinkeby and Sokol.

## Verdict

It is **not a bitcoin wallet.**
