---
wsId: xChangeBG
title: "xChange.bg - Buy Bitcoin I Sell Bitcoin"
altTitle:
authors:
- danny
users: 1000
appId: bg.xchange
released: 2020-12-22
updated: 2021-10-15
version: "1.8.6"
stars: 4.9
ratings: 201
reviews: 63
size: 19M
website: https://xchange.bg/
repository:
issue:
icon: bg.xchange.png
bugbounty:
verdict: nowallet
date: 2021-11-10
signer:
reviewArchive:


providerTwitter: xchangebg
providerLinkedIn:
providerFacebook: xchangebg
providerReddit:

redirect_from:

---


## App Description

>  Buy, sell, and exchange Bitcoin and the most popular altcoins and stablecoins

## The App

The app only requires ID verification if you are converting from or to fiat currency. It is similar to:

{% include walletLink.html wallet='android/com.changelly.app' verdict='true' %}

The user has to input a receiving address and send coins to xchangebg's own address for conversions or swaps to happen. No funds are stored on the app.

## Verdict

This exchange is **not a wallet**.
