---
wsId: 
title: "Coincheck Bitcoin Wallet"
altTitle: 
authors:
- danny
users: 1000000
appId: jp.coincheck.android
released: 2015-04-23
updated: 2021-11-08
version: "4.2.5"
stars: 3.9
ratings: 9818
reviews: 2967
size: 67M
website: http://coincheck.com/
repository: 
issue: 
icon: jp.coincheck.android.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: coincheckjp
providerLinkedIn: 
providerFacebook: coincheck
providerReddit: 

redirect_from:

---


> Anyone can easily send Bitcoins just by scanning QR code! You can also convert address for receiving Bitcoins to QR code as well. Coincheck wallet will enable everyone to exchange money without using cash or credit card.

Sounds like a BTC wallet.

> Coincheck wallet supports 2-step authentication and PIN code lock.

Sounds like a custodial product. Reviews for the app also mention ID card verification.

[API is available](https://coincheck.com/documents/exchange/api) but unfortunately there's no source code. This app is **not verifiable.**

