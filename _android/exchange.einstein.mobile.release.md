---
wsId: 
title: "Einstein Convert - Safe and Secure Bitcoin Wallet"
altTitle: 
authors:

users: 50000
appId: exchange.einstein.mobile.release
released: 2019-02-01
updated: 2019-10-17
version: "1.2.2"
stars: 1.5
ratings: 949
reviews: 768
size: 32M
website: 
repository: 
issue: 
icon: exchange.einstein.mobile.release.png
bugbounty: 
verdict: obsolete
date: 2021-10-09
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


