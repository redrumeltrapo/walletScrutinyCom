---
wsId: 
title: "Bitcoin Invest App"
altTitle: 
authors:

users: 1
appId: net.cryptonews.bitcoin.invest.app
released: 2021-06-23
updated: 2021-06-23
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: net.cryptonews.bitcoin.invest.app.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
