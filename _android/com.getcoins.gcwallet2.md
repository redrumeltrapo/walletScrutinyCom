---
wsId: 
title: "GetCoins Wallet"
altTitle: 
authors:

users: 1000
appId: com.getcoins.gcwallet2
released: 2019-03-21
updated: 2020-05-13
version: "3.6.0"
stars: 2.8
ratings: 15
reviews: 11
size: 18M
website: 
repository: 
issue: 
icon: com.getcoins.gcwallet2.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


