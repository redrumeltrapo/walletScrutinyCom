---
wsId:
title: "CryptoTrader"
altTitle:
authors:
- danny
users: 5000
appId: com.cryptotrader.app
released: 2020-08-04
updated: 2021-02-24
version: "1.2.2"
stars: 4.1
ratings: 39
reviews: 17
size: 61M
website:
repository:
issue:
icon: com.cryptotrader.app.png
bugbounty:
verdict: nowallet
date: 2021-11-10
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---


The app does not have a listed website or social media accounts. It is similar to the following:

> - {% include walletLink.html wallet='android/net.benoitbasset.ograpi' verdict='true' %}<br>
> - {% include walletLink.html wallet='android/net.oblade.krakapi' verdict='true' %}<br>

The app merely features cryptocurrency trading pair charts and provisions for entering the API keys for various exchanges: Huobi, Binance, Kucoin and HitBTC.

This app does **not have a bitcoin wallet.**
