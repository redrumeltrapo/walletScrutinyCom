---
wsId: 
title: "DiDimessage-Make New Friends & Play Games"
altTitle: 
authors:
- danny
users: 100000
appId: com.didi.message
released: 2020-04-30
updated: 2021-11-09
version: "1.11"
stars: 4.1
ratings: 1039
reviews: 414
size: 96M
website: https://didimessage.com/
repository: 
issue: 
icon: com.didi.message.png
bugbounty: 
verdict: custodial
date: 2021-11-02
signer: 
reviewArchive:


providerTwitter: DiDimessage
providerLinkedIn: 
providerFacebook: DiDi-Message-105506995020030
providerReddit: 

redirect_from:

---


## App Description

> DiDi the world's best instant messenger that provides real protection for correspondence. In our messenger you can encrypt the sent and received message.

There wasn't that much information on the app description concerning the wallet. DiDi is mainly a messaging app that's similar to discord. 


## The Site

On the homepage, DiDi has more information about the wallet, saying it has:

> More flexible digital currency storage, flexible storage solution of iccbank custody and decentralized wallet，Multi-signature storage of funds, safer blockchain capital flow and settlement

 
## The App

We installed the app on a Samsung A11 phone and was able to locate the bitcoin wallet. The user terms of agreement can only be accessed by installing the app. There is the option to send and receive. Upon choosing to receive BTC, you are provided with a QR code and BTC address.


## The Verdict

There was no apparent method how to access the private keys and we weren't able to locate the source code for the app. Since it has a lot of functionalities for commercial purposes, we assume that it is a **custodial** app and hence, **not verifiable.**

