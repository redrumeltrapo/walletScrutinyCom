---
wsId: 
title: "INLOCK: Crypto Savings Account"
altTitle: 
authors:

users: 1000
appId: io.inlock.mobileapp
released: 2020-08-04
updated: 2021-11-04
version: "1.5.3"
stars: 4.7
ratings: 84
reviews: 37
size: 46M
website: 
repository: 
issue: 
icon: io.inlock.mobileapp.png
bugbounty: 
verdict: wip
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


