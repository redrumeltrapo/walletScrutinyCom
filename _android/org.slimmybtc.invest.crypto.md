---
wsId: 
title: "Invest Crypto"
altTitle: 
authors:

users: 1
appId: org.slimmybtc.invest.crypto
released: 2021-07-18
updated: 2021-07-18
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.4M
website: 
repository: 
issue: 
icon: org.slimmybtc.invest.crypto.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
