---
wsId: 
title: "Smart chain"
altTitle: 
authors:
- danny
users: 5000
appId: smartchain.aiqt
released: 2021-06-21
updated: 2021-06-21
version: "9.8"
stars: 4.0
ratings: 81
reviews: 33
size: 10M
website: https://smartchainapp.blogspot.com/
repository: 
issue: 
icon: smartchain.aiqt.png
bugbounty: 
verdict: nowallet
date: 2021-11-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

The full description is:

> Cypto, Bitcoin, NFTs, Market its all here. (link to binance/referer)

One of the negative reviews:

> [Hamid karimzadeh ghazijahani](https://play.google.com/store/apps/details?id=smartchain.aiqt&reviewId=gp%3AAOqpTOGD1yMWrsKgYPCjsRa1c6UYafdDefQd8nqZWKc1UoncD8Rg30jpiHo0r0YhjRvxKP6srqYcYt9GxjPqKQ)<br>
  ★☆☆☆☆ August 19, 2021 <br>
      It's too slow and just a shortcut to website

Even the blogspot site linked in the description is completely empty.

## The App

This app just links to Binance. It's a referrer.

## Verdict

This is **not a wallet.**
