---
wsId: 
title: "Moonshine"
altTitle: 
authors:
- leo
users: 100
appId: com.kisswallet
released: 2020-02-16
updated: 2020-05-30
version: "0.3.6"
stars: 0.0
ratings: 
reviews: 
size: Varies with device
website: https://moonshinewallet.com
repository: https://github.com/coreyphillips/moonshine
issue: 
icon: com.kisswallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: coreylphillips
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.kisswallet/
  - /moonshine/
  - /posts/com.kisswallet/
---


This page was created by a script from the **appId** "com.kisswallet" and public
information found
[here](https://play.google.com/store/apps/details?id=com.kisswallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.
