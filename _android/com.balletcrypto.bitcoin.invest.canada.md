---
wsId: 
title: "Bitcoin Invest Canada"
altTitle: 
authors:

users: 0
appId: com.balletcrypto.bitcoin.invest.canada
released: 2021-06-20
updated: 2021-06-20
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: com.balletcrypto.bitcoin.invest.canada.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
