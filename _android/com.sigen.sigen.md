---
wsId: SIGEN.pro
title: "SIGEN.pro"
altTitle: 
authors:
- danny
users: 100000
appId: com.sigen.sigen
released: 2020-04-18
updated: 2021-11-08
version: "2.9.0"
stars: 4.6
ratings: 22266
reviews: 10501
size: 40M
website: https://sigen.pro/
repository: 
issue: 
icon: com.sigen.sigen.png
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: SIGENpro
providerLinkedIn: 
providerFacebook: sigen.pro
providerReddit: 

redirect_from:

---


> SIGEN.pro is not only a multifunctional trading platform, but also your convenient crypto wallet, as well as a service for mining cryptocurrency.

So we know that this exchange is a BTC 'wallet'.

On the official website SIGEN claims that it

> Is a platform for trading cryptocurrency for any fiat currency in the world. Trade on an exchange and earn money on fluctuations of exchange rates, save cryptocurrency, and exchange it for fiat money or other coins rapidly and with no intermediaries.

It's rather vague whether there are options to trade the BTC or if it must be converted back to fiat.

> [SIGEN has a cold-hot wallet policy](https://sigen.pro/help/faq?section=360013157091&articles=360030644311). The bulk of our clients' funds are in the "cold" wallet. The hot wallet only stores funds that are necessary to support withdrawal operations.

Since the clients' funds are in the custody of a third-party, this product is definitely **custodial** and **not verifiable**. 
