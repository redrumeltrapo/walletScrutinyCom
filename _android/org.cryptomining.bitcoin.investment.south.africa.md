---
wsId: 
title: "Bitcoin Investment South Africa"
altTitle: 
authors:

users: 10
appId: org.cryptomining.bitcoin.investment.south.africa
released: 2021-07-21
updated: 2021-07-21
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.4M
website: 
repository: 
issue: 
icon: org.cryptomining.bitcoin.investment.south.africa.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
