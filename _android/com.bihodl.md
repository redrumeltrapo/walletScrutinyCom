---
wsId: 
title: "BiHODL"
altTitle: 
authors:

users: 100
appId: com.bihodl
released: 2019-09-08
updated: 2019-10-31
version: "1.8.0"
stars: 1.0
ratings: 5
reviews: 5
size: 12M
website: 
repository: 
issue: 
icon: com.bihodl.png
bugbounty: 
verdict: obsolete
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


