---
wsId: 
title: "SovereignWallet, Sovereign Wallet, MUI, MUI Token"
altTitle: 
authors:

users: 1000
appId: exchange.sovereignwallet.mui
released: 2018-06-25
updated: 2021-03-10
version: "1.18.0"
stars: 4.2
ratings: 74
reviews: 33
size: 43M
website: 
repository: 
issue: 
icon: exchange.sovereignwallet.mui.png
bugbounty: 
verdict: wip
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


