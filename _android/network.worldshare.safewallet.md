---
wsId: 
title: "Safe Wallet"
altTitle: 
authors:

users: 500
appId: network.worldshare.safewallet
released: 2020-02-11
updated: 2020-02-12
version: "1.0.0"
stars: 3.0
ratings: 6
reviews: 3
size: 1.5M
website: 
repository: 
issue: 
icon: network.worldshare.safewallet.png
bugbounty: 
verdict: stale
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


