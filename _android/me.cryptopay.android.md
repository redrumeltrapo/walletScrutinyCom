---
wsId: cryptopay
title: "Cryptopay:Bitcoin wallet&card"
altTitle: 
authors:

users: 50000
appId: me.cryptopay.android
released: 2017-11-09
updated: 2021-10-20
version: "1.31"
stars: 4.5
ratings: 910
reviews: 478
size: 20M
website: https://cryptopay.me
repository: 
issue: 
icon: me.cryptopay.android.png
bugbounty: 
verdict: custodial
date: 2021-03-10
signer: 
reviewArchive:


providerTwitter: cryptopay
providerLinkedIn: cryptopay
providerFacebook: cryptopayme
providerReddit: 

redirect_from:

---


In the description the only sentence hinting at custodianship is:

> Use our secure multisig wallet to receive, store and transfer BTC, LTC, XRP,
  ETH to your friends.

but there is nothing more to be found and as "multisig wallet" could refer to
anything, we can't say with certainty that this wallet even tries to imply
being self-custodial and therefore consider it **not verifiable**.
