---
wsId: 
title: "ProfitTrading for BitMEX - Trade much faster"
altTitle: 
authors:
- danny
users: 50000
appId: com.profittrading.forbitmex
released: 2019-06-11
updated: 2021-11-04
version: "5.2.2"
stars: 4.0
ratings: 319
reviews: 142
size: 23M
website: https://profittradingapp.com/
repository: 
issue: 
icon: com.profittrading.forbitmex.png
bugbounty: 
verdict: custodial
date: 2021-09-20
signer: 
reviewArchive:


providerTwitter: ProfitTrading_
providerLinkedIn: 
providerFacebook: profittradingapp
providerReddit: 

redirect_from:

---


The provider [ProfitTradingApp](https://play.google.com/store/apps/dev?id=6470884744111312194) features a list of apps:

* {% include walletLink.html wallet='android/com.profittrading.forbinance' verdict='true' %}
* {% include walletLink.html wallet='android/com.profittrading.forkucoin' verdict='true' %}
* {% include walletLink.html wallet='android/com.profittrading.forftx' verdict='true' %}
* {% include walletLink.html wallet='android/com.profittrading.forokex' verdict='true' %}
* {% include walletLink.html wallet='android/com.profittrading.forbybit' verdict='true' %}
* {% include walletLink.html wallet='android/com.profittrading.forbinanceus' verdict='true' %}
* {% include walletLink.html wallet='android/com.profittrading.forhuobi' verdict='true' %}
* {% include walletLink.html wallet='android/com.profittrading.forhitbtc' verdict='true' %}
* {% include walletLink.html wallet='android/com.profittrading.forcoinbase' verdict='true' %}
* {% include walletLink.html wallet='android/com.profittrading.forkraken' verdict='true' %}
* {% include walletLink.html wallet='android/com.profittrading.forbittrex' verdict='true' %}

each of which act as an interface to the respectively mentioned exchange.

While the provider doesn't custody your coins, neither do you. This app can trade on your behalf via its bots and you can also withdraw and deposit. This means you can use it as a wallet but not only are your coins in custody of the respective exchange, this app probably can also empty all the accounts of all its users at once which is why we consider it itself custodial and thus **not verifiable**.

