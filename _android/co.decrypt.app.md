---
wsId: Decrypt
title: "Decrypt - Bitcoin & crypto news"
altTitle: 
authors:
- leo
- danny
users: 100000
appId: co.decrypt.app
released: 2020-05-06
updated: 2021-07-13
version: "3.0"
stars: 4.2
ratings: 2140
reviews: 1033
size: 31M
website: https://decrypt.co/
repository: 
issue: 
icon: co.decrypt.app.png
bugbounty: 
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive:


providerTwitter: decryptmedia
providerLinkedIn: 
providerFacebook: decryptmedia
providerReddit: 

redirect_from:
  - /co.decrypt.app/
---


This app only provides news about Bitcoin but no wallet itself.
