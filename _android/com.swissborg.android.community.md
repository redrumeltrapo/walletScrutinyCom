---
wsId: cryptochallenge
title: "Crypto Challenge: Bitcoin Price Prediction Game"
altTitle: 
authors:
- danny
users: 100000
appId: com.swissborg.android.community
released: 2019-03-27
updated: 2021-09-29
version: "2.1.0"
stars: 3.7
ratings: 6506
reviews: 3437
size: 19M
website: https://swissborg.com/
repository: 
issue: 
icon: com.swissborg.android.community.png
bugbounty: 
verdict: nowallet
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: cchallengesborg
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


Formerly known as the Swissborg Community App, this app does not have a wallet. It is a Bitcoin price prediction game with a leaderboard for points. Correct guesses for Bitcoin's price movement merits points for the user. You have to download {% include walletLink.html wallet='android/com.swissborg.android' verdict='true' %} in order to receive the rewards. 

[Reward withdrawal FAQ](https://help.swissborg.com/hc/en-gb/articles/360020296033-When-and-how-can-I-withdraw-my-rewards-)

> At the end of each Monthly series, rewards are distributed through the SwissBorg  App, where you can turn them into your own crypto investment.<br>
First, download and install the SwissBorg app on the same mobile device as the Crypto Challenge app.<br>
In the Crypto Challenge app, on the Rewards Tab, tap on the claim button, and you will be redirected to the SwissBorg app where your reward will be waiting for you.<br> 

This app has **no wallet**.

