---
wsId: 
title: "CryptoExchange - Simply buy cryptos with your Card"
altTitle: 
authors:

users: 500
appId: com.aaatrade.CryptoExchange
released: 2018-09-20
updated: 2021-03-19
version: "1.2.5"
stars: 4.5
ratings: 26
reviews: 15
size: 18M
website: 
repository: 
issue: 
icon: com.aaatrade.CryptoExchange.png
bugbounty: 
verdict: fewusers
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


