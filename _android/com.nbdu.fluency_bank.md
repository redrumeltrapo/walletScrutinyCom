---
wsId: 
title: "Fluency - The Global Cryptobank of the Future."
altTitle: 
authors:

users: 1000
appId: com.nbdu.fluency_bank
released: 2020-02-27
updated: 2020-04-29
version: "1.0.0"
stars: 4.4
ratings: 408
reviews: 106
size: 14M
website: 
repository: 
issue: 
icon: com.nbdu.fluency_bank.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


