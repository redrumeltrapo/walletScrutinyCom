---
wsId: voyager
title: "Voyager: Crypto Made Simple"
altTitle: 
authors:
- leo
users: 1000000
appId: com.investvoyager
released: 
updated: 2021-10-28
version: "3.0.2"
stars: 4.6
ratings: 13410
reviews: 5157
size: 51M
website: https://www.investvoyager.com/
repository: 
issue: 
icon: com.investvoyager.png
bugbounty: 
verdict: custodial
date: 2021-01-02
signer: 
reviewArchive:


providerTwitter: investvoyager
providerLinkedIn: investvoyager
providerFacebook: InvestVoyager
providerReddit: Invest_Voyager

redirect_from:
  - /com.investvoyager/
---


On their website we read:

> **Advanced Security**<br>
  Offline storage, advanced fraud protection, and government-regulated processes
  keep your assets secure and your personal information safe.

which means this is a custodial offering and therefore **not verifiable**.
