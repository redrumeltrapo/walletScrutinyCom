---
wsId: koinbazar
title: "Koinbazar: Bitcoin & Cryptocurrency Exchange App"
altTitle: 
authors:
- danny
users: 100000
appId: com.application.koinbazar
released: 2021-05-17
updated: 2021-10-21
version: "1.12"
stars: 3.3
ratings: 2692
reviews: 1217
size: 12M
website: https://www.koinbazar.com/
repository: 
issue: 
icon: com.application.koinbazar.png
bugbounty: 
verdict: custodial
date: 2021-10-04
signer: 
reviewArchive:


providerTwitter: koinbazar
providerLinkedIn: koinbazar
providerFacebook: koinbazar
providerReddit: 

redirect_from:

---


### App Description

INR (Indian Rupee) deposit and withdrawal as well as trading cryptocurrencies such as BTC, ETH, XRP and more are available.

### The Site

Like most custodial services, sign up is available on the app as well as the website. AML and KYC provisions are also in place.

#### Terms and Conditions

Section 9 of the [Terms and Conditions](https://www.koinbazar.com/terms-conditions) confirms that this is a custodial service.

>YOU ACKNOWLEDGE THAT OUR DECISION TO TAKE CERTAIN ACTIONS, INCLUDING LIMITING ACCESS TO, SUSPENDING, OR **CLOSING YOUR ACCOUNT OR WALLET**, MAY BE BASED ON CONFIDENTIAL CRITERIA THAT ARE ESSENTIAL TO OUR RISK MANAGEMENT AND SECURITY PROTOCOLS. YOU AGREE THAT KOIN BAZAR IS UNDER NO OBLIGATION TO DISCLOSE THE DETAILS OF ITS RISK MANAGEMENT AND SECURITY PROCEDURES TO YOU.

### The App

We tried the app. Once installed we registered and were able to successfully join the platform. The 'Wallet' sub menu allows Deposit/Withdrawal of INR, BTC and many other cryptocurrencies.

### Verdict

By most indications, this centralized cryptocurrency exchange is a **custodial** service and **not verifiable**


