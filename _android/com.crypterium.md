---
wsId: crypterium
title: "Crypterium | Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 100000
appId: com.crypterium
released: 2018-01-21
updated: 2021-11-02
version: "2.6.60.15"
stars: 4.3
ratings: 8368
reviews: 4292
size: 60M
website: https://crypterium.com
repository: 
issue: 
icon: com.crypterium.png
bugbounty: 
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: crypterium
providerLinkedIn: 
providerFacebook: crypterium.org
providerReddit: 

redirect_from:
  - /com.crypterium/
---


Judging by what we can find on the [wallet site](https://wallet.crypterium.com/):

> **Store**<br>
  keep your currencies<br>
  safe & fully insured

this is a custodial app as a self-custody wallet cannot ever have funds insured.

As a custodial app it is **not verifiable**.
