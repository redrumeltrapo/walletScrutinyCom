---
wsId: StockMaster
title: "Stock Master: Investing Stocks Markets Portfolios"
altTitle: 
authors:
- danny
users: 100000
appId: com.astontek.stock
released: 2020-03-16
updated: 2021-10-21
version: "6.07"
stars: 4.5
ratings: 2195
reviews: 825
size: 37M
website: http://www.astontek.com/
repository: 
issue: 
icon: com.astontek.stock.png
bugbounty: 
verdict: nowallet
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

View the Stock Market, Market News, Futures, Forex, Commodities, the US Treasury, Stock Options, Upcoming IPOs, Market Movers, Trending Stocks, Social Discussion and Crypto.

The app allows for an extensive analysis of markets with a full suite of tools for the trader.

## The App

We downloaded the app and found a Crypto section, but it only listed price changes and did not have trading nor any wallet features.

## Verdict

This app is **not a cryptocurrency or bitcoin wallet**.

