---
wsId: 
title: "Cent - The crypto wallet for DeFi"
altTitle: 
authors:

users: 1000
appId: com.atlas.mobile.wallet
released: 2020-08-07
updated: 2021-10-27
version: "1.0.44"
stars: 4.8
ratings: 22
reviews: 12
size: 14M
website: 
repository: 
issue: 
icon: com.atlas.mobile.wallet.png
bugbounty: 
verdict: wip
date: 2021-06-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.atlas.mobile.wallet/
---


