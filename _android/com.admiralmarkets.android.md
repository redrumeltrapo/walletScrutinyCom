---
wsId: AdmiralMarkets
title: "Admirals"
altTitle: 
authors:
- danny
users: 50000
appId: com.admiralmarkets.android
released: 2020-10-07
updated: 2021-10-28
version: "1.4.5"
stars: 3.8
ratings: 288
reviews: 158
size: 14M
website: https://admiralmarkets.com/
repository: 
issue: 
icon: com.admiralmarkets.android.png
bugbounty: 
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: AdmiralsGlobal
providerLinkedIn: -admiral-markets-group
providerFacebook: AdmiralsGlobal
providerReddit: 

redirect_from:

---


## Description

> trade CFDs on Forex, ETFs, Stocks, Indices, Metals, and Bonds

## The Site

In some jurisdictions, the site displays a [Digital Currency CFDs Trading](https://admiralmarkets.com/products/digital-currencies-cfds) page. It offers these features:

> - Competitive Leverages Rates (Up to 1:5 for Professional Clients)
- 24/7 Trading
- Go long or short on any digital currency CFD (no actual digital assets are required)

A few of their featured trading pairs include:

> - BTCUSD
- ETHUSD
- XRPUSD

**Note** Certain jurisdictions may show this message instead when trying to access their Cryptocurrency CFDs page

> Unfortunately, this section is not available for the current selected regulator. In order to view this page, please return back for the previous regulator

[Deposits](https://admiralmarkets.com/start-trading/deposits-and-withdrawals) can be made through these:

- Bank Transfer
- Klarna
- Visa and Mastercard
- Paypal
- Skrill
- Neteller
- Safety Pay
- Przelewy
- iDEAL

Withdrawals are only available in:

- Bank Transfer
- Paypal
- Skrill
- Neteller
- iBank&BankLink

## Verdict

Since this app trades in CFDs, and only accepts traditional modes for deposit/withdrawal, it **cannot send and receive bitcoins**.
