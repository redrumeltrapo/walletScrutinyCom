---
wsId: 
title: "BBC Token : Big Blockchain Community"
altTitle: 
authors:

users: 10000
appId: com.bbctoken
released: 2019-07-20
updated: 2020-09-16
version: "1.0.54"
stars: 2.8
ratings: 363
reviews: 258
size: 22M
website: 
repository: 
issue: 
icon: com.bbctoken.png
bugbounty: 
verdict: stale
date: 2021-09-12
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


