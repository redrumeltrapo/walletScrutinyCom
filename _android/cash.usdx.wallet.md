---
wsId: usdxwallet
title: "USDX Wallet - blockchain wallet with stable crypto"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: cash.usdx.wallet
released: 2018-03-22
updated: 2020-11-25
version: "1.34.2"
stars: 4.1
ratings: 5475
reviews: 4682
size: 37M
website: https://usdx.cash
repository: 
issue: 
icon: cash.usdx.wallet.jpg
bugbounty: 
verdict: nobtc
date: 2021-04-30
signer: 
reviewArchive:


providerTwitter: USDXwallet
providerLinkedIn: 
providerFacebook: USDXwallet
providerReddit: USDXwallet

redirect_from:

---


There is no evidence that this wallet supports BTC, the only 2 token supported from the website are USDX and LHT (Lighthouse token). <br>
The wallet may allow you to send it bitcoins to receive one of these tokens but there does not seem to be any bitcoins stored for the user.

Our verdict: This wallet does not support BTC.

