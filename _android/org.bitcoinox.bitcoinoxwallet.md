---
wsId: 
title: "Bitcoin OX Wallet — Exchange Wallet for Crypto"
altTitle: 
authors:
- leo
users: 1000
appId: org.bitcoinox.bitcoinoxwallet
released: 2019-02-04
updated: 2020-12-10
version: "2.3.5"
stars: 4.4
ratings: 29
reviews: 18
size: Varies with device
website: https://bitcoinox.com
repository: 
issue: 
icon: org.bitcoinox.bitcoinoxwallet.png
bugbounty: 
verdict: nosource
date: 2020-09-27
signer: 
reviewArchive:


providerTwitter: bitcoin_ox
providerLinkedIn: 
providerFacebook: bitcoinoxwallet
providerReddit: 

redirect_from:
  - /org.bitcoinox.bitcoinoxwallet/
  - /posts/org.bitcoinox.bitcoinoxwallet/
---


> Safety
> 
> • Due to Bitcoin OX, only you are in control of your Private Keys and manage
>   your Digital Assets
> 
> • No copies on our or public servers
> 
> • Compatible with BIP39 mnemonic code for generating deterministic keys

sounds like a non-custodial wallet but as there is no public source code linked
on their Google Play description or their website. This wallet is **not verifiable**.
