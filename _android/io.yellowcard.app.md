---
wsId: yellowcard
title: "Yellow Card: Buy and Sell Bitcoin"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: io.yellowcard.app
released: 2019-05-14
updated: 2021-10-21
version: "3.4.2"
stars: 3.6
ratings: 2082
reviews: 1582
size: 1.4M
website: https://yellowcard.io/
repository: 
issue: 
icon: io.yellowcard.app.png
bugbounty: 
verdict: custodial
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: yellowcard_app
providerLinkedIn: yellowcardapp
providerFacebook: yellowcardapp
providerReddit: 

redirect_from:

---


**Update 2021-07-18**: Currently the app is not on the App Store.

The Yellow Card wallet has no statements on [their website](https://yellowcard.io/) or in their FAQ regarding the management of private keys.
This leads us to conclude the wallet funds are likely under the control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

