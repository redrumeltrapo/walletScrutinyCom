---
wsId: 
title: "CM Robot - Autotrader Bitcoin App"
altTitle: 
authors:

users: 1000
appId: com.cryptomaniac.cmrobot
released: 2019-08-21
updated: 2019-09-11
version: "2.2.1"
stars: 2.8
ratings: 24
reviews: 19
size: 7.9M
website: 
repository: 
issue: 
icon: com.cryptomaniac.cmrobot.png
bugbounty: 
verdict: obsolete
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


