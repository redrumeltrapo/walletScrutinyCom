---
wsId: tabtrader
title: "TabTrader Buy Bitcoin and Ethereum on exchanges"
altTitle: 
authors:
- leo
- kiwilamb
users: 1000000
appId: com.tabtrader.android
released: 2014-08-14
updated: 2021-08-25
version: "4.9.7"
stars: 4.5
ratings: 55211
reviews: 23557
size: 14M
website: http://www.tab-trader.com
repository: 
issue: 
icon: com.tabtrader.android.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-10
signer: 
reviewArchive:


providerTwitter: tabtraderpro
providerLinkedIn: tabtrader
providerFacebook: tabtrader
providerReddit: 

redirect_from:

---


This app appears to not function as a wallet. At least we could not see any
documentation about depositing or withdrawing through the app, which makes the
verdict **not a wallet** but the app still has massive potential for abuse
if the provider front-runs the trades of the users from the insight they gain or
even worse, they could trigger lucrative-to-front-run trades the user never
intended to make.
