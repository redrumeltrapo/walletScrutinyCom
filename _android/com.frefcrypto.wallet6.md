---
wsId: 
title: "Crypto.com Wallet Bitcoin & Ethereum"
altTitle: "(Fake) Crypto.com Wallet Bitcoin & Ethereum"
authors:
- leo
users: 100
appId: com.frefcrypto.wallet6
released: 2021-09-10
updated: 2021-09-10
version: "1"
stars: 0.0
ratings: 
reviews: 
size: 9.3M
website: 
repository: 
issue: 
icon: com.frefcrypto.wallet6.png
bugbounty: 
verdict: defunct
date: 2021-11-10
signer: 
reviewArchive:
- date: 2021-10-24
  version: "1"
  appHash: 
  gitRevision: 1af5b9bcf87d45fe695ccccbca30a4a9d303a0f1
  verdict: fake


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-28**: This app is no more.

This app pretends to be by name and logo
{% include walletLink.html wallet='android/com.crypto.exchange' verdict='true' %}.
