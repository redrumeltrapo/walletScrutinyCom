---
wsId: 
title: "Xapo Bank"
altTitle: 
authors:

users: 10000
appId: com.xapo.bank
released: 2021-05-27
updated: 2021-11-01
version: "7.9.0"
stars: 1.7
ratings: 375
reviews: 257
size: 68M
website: https://www.xapo.com
repository: 
issue: 
icon: com.xapo.bank.png
bugbounty: 
verdict: custodial
date: 2021-08-06
signer: 
reviewArchive:


providerTwitter: xapo
providerLinkedIn: xapo
providerFacebook: xapoapp
providerReddit: 

redirect_from:

---


**Update 2021-08-06**: Xapo
[confirmed on Twitter](https://twitter.com/xapo/status/1423632786112516097)
that there is a transition happening from xapo to xapo bank:

> Hi, the Xapo Bank app is already available on Google Play, but we are
  currently only admitting existing users by invite. The old app was been
  deprecated on July 31st, but this has been amply communicated to our users.
  Thank you! The Xapo Team

This app was launched in May, yet on "their" website the app is announced as
"coming soon".

By its name alone we assume it is custodial and thus **not verifiable**.
