---
wsId: 
title: "Bitcoin Chivo -  Chivo Bitcoin"
altTitle: "(Fake) Bitcoin Chivo -  Chivo Bitcoin"
authors:

users: 100
appId: com.rvinc.bitminner
released: 2021-09-28
updated: 2021-09-28
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 7.9M
website: 
repository: 
issue: 
icon: com.rvinc.bitminner.jpg
bugbounty: 
verdict: defunct
date: 2021-11-10
signer: 
reviewArchive:
- date: 2021-10-18
  version: "1.0"
  appHash: 
  gitRevision: 1af5b9bcf87d45fe695ccccbca30a4a9d303a0f1
  verdict: fake
  

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-28**: This app is no more.

This scammer couldn't decide if he wanted to fool the users of
{% include walletLink.html wallet='android/com.chivo.wallet' verdict='true' %}
or the users of
{% include walletLink.html wallet='android/com.breadwallet' verdict='true' %}.

They just used the name of the former (sort of) and screenshots of the latter.
