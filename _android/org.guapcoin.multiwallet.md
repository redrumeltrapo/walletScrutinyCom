---
wsId: 
title: "Official Guap Coin & Bitcoin Wallet"
altTitle: 
authors:

users: 1000
appId: org.guapcoin.multiwallet
released: 2020-02-27
updated: 2020-02-27
version: "1.0.0"
stars: 4.2
ratings: 41
reviews: 25
size: 26M
website: 
repository: 
issue: 
icon: org.guapcoin.multiwallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


