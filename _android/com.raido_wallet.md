---
wsId: Raido
title: "Multifunctional crypto wallet - Raido"
altTitle: 
authors:
- danny
users: 10000
appId: com.raido_wallet
released: 2020-07-27
updated: 2021-10-15
version: "2.4.0"
stars: 4.1
ratings: 45
reviews: 34
size: 48M
website: https://raidofinance.eu
repository: 
issue: 
icon: com.raido_wallet.png
bugbounty: 
verdict: custodial
date: 2021-08-02
signer: 
reviewArchive:


providerTwitter: financialraido
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

> The Raido Finance crypto wallet app allows you to efficiently buy and sell cryptocurrency from anywhere in the world via your mobile device. This includes all forms of bitcoin exchange, as well as other currencies (Ethereum, EURS, USDT)

> **Security funds**
>
> With regards to our customers’ funds, we proudly implement the highest levels of protection, thanks to both the cold storage of assets and the use of our personal crypto Node. Our crypto exchange platform implements multiple layers of protection for both personal and commercial customer data in all cryptocurrency trading. 

These are the hallmarks of a custodial service.

## The Site

> - KYC is needed in order to receive cryptocurrency deposits.
> - ID Verification is mandatory

## Verdict

All indications point to **custodial** service. This makes the app **not verifiable**

