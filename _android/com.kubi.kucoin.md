---
wsId: kucoin
title: "KuCoin: Bitcoin, Crypto Trade"
altTitle: 
authors:
- leo
users: 1000000
appId: com.kubi.kucoin
released: 2018-05-03
updated: 2021-11-03
version: "3.43.1"
stars: 3.7
ratings: 16279
reviews: 7614
size: 63M
website: 
repository: 
issue: 
icon: com.kubi.kucoin.png
bugbounty: 
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive:


providerTwitter: KuCoinCom
providerLinkedIn: kucoin
providerFacebook: KuCoinOfficial
providerReddit: kucoin

redirect_from:

---


> KuCoin is the most popular bitcoin exchange that you can buy and sell bitcoin
  securely.

This app is the interface to an exchange. Exchanges are all custodial which
makes the app **not verifiable**.
