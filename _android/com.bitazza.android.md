---
wsId: bitazza
title: "Bitazza: Crypto Exchange"
altTitle: 
authors:
- danny
users: 100000
appId: com.bitazza.android
released: 2020-09-29
updated: 2021-10-30
version: "1.9.3"
stars: 4.3
ratings: 3138
reviews: 701
size: 42M
website: https://bitazza.com
repository: 
issue: 
icon: com.bitazza.android.jpg
bugbounty: 
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: bitazzaofficial
providerLinkedIn: bitazza
providerFacebook: bitazza
providerReddit: 

redirect_from:

---


Bitazza is a cryptocurrency exchange and wallet provider. 

Upon sign up, KYC is needed prior to accessing most features including the BTC wallet. The BTC address is not shown if you do not verify your identity. 

In Bitazza's [Terms Page](https://bitazza.com/tof.html), the user "appoints" and "authorizes" Bitazza to be its lawful agent and broker, possessing, transacting user's "Tokens". Bitazza defines "Tokens" as cryptocurrency (in general).

We reached out to Bitazza support and this is their reply:

> Bitazza company keeps the fund and assets by hot wallet and cold wallet.

> Our wallet are custodial online crypto wallets, all customer's funds deposited with Bitazza are secured as they are stored in hot and cold storage.

> For the private key, we can't provide for the customer.

Verdict is **custodial**



