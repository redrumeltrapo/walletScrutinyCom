---
wsId: Pintu
title: "Pintu: Buy/Sell Digital Assets with Rupiah (IDR)"
altTitle: 
authors:
- danny
users: 1000000
appId: com.valar.pintu
released: 2020-01-25
updated: 2021-10-18
version: "3.6.2"
stars: 3.4
ratings: 18598
reviews: 11991
size: Varies with device
website: https://pintu.co.id/
repository: 
issue: 
icon: com.valar.pintu.png
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: pintuid
providerLinkedIn: 
providerFacebook: pintucrypto
providerReddit: 

redirect_from:

---


Found in the [FAQ:](https://pintu.co.id/en/faq/private-keys)
> **Do I hold a Private Key?**<br>
  Pintu is a custodial crypto exchange, which means Pintu acts as a custodian/keeper of the users’ private keys.  Therefore, Pintu doesn’t share private keys with its users.

Pintu is **custodial** and **not verifiable.**

