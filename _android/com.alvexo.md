---
wsId: alvexo
title: "Alvexo: Online CFD Trading App on Forex & Stocks"
altTitle: 
authors:
- danny
users: 50000
appId: com.alvexo
released: 2018-07-13
updated: 2021-08-03
version: "3.0.46"
stars: 4.0
ratings: 687
reviews: 343
size: 17M
website: https://www.alvexo.com/
repository: 
issue: 
icon: com.alvexo.png
bugbounty: 
verdict: nosendreceive
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: Alvexo_Trade
providerLinkedIn: alvexo
providerFacebook: 
providerReddit: 

redirect_from:

---


As this is a CFD, stocks, indices, commodities and forex trading app, we can assume that this app does not allow "real" bitcoin to be sent or received. 

CFDs or Contract for Differences only allows for speculation on an asset without actually holding the asset. 

This is proven in Section 46.1 of its [Terms and Conditions](https://www.alvexo.com/documents/hsn/footer/hsn_capital_group_terms_and_conditions_31.03.2020.docx.pdf?ver=0.1631263857)

> The  Company  may,  at  its  sole  discretion  offer  CFDs  on  cryptocurrencies for  trading  on  its  Online Trading  Facility,  from  time  to  time.  

The verdict for this app, is that it **can't send or receive bitcoins**.

