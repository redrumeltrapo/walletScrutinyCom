---
wsId: 
title: "HomiEx Crypto Exchange"
altTitle: 
authors:

users: 100
appId: be.homiex.broker.android
released: 2021-08-03
updated: 2021-08-04
version: "4.1.2"
stars: 0.0
ratings: 
reviews: 
size: 21M
website: 
repository: 
issue: 
icon: be.homiex.broker.android.png
bugbounty: 
verdict: defunct
date: 2021-10-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-14**: This app is no more available.

