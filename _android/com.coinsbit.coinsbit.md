---
wsId: 
title: "Coinsbit - Cryptocurrency Exchange: BTC, ETH, USDT"
altTitle: 
authors:

users: 100000
appId: com.coinsbit.coinsbit
released: 2019-10-28
updated: 2019-11-27
version: "1.0"
stars: 2.3
ratings: 1663
reviews: 1194
size: 16M
website: 
repository: 
issue: 
icon: com.coinsbit.coinsbit.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


