---
wsId: ethosUW
title: "Ethos Universal Wallet"
altTitle: 
authors:

users: 50000
appId: io.ethos.universalwallet
released: 2018-07-10
updated: 2019-12-05
version: "2.0.5"
stars: 2.9
ratings: 1521
reviews: 800
size: 38M
website: https://www.ethos.io
repository: 
issue: 
icon: io.ethos.universalwallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-03-05
  version: "2.0.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nosource

providerTwitter: ethos_io
providerLinkedIn: 
providerFacebook: ethosplatform
providerReddit: ethos_io

redirect_from:

---


They claim:

> STATE-OF-THE-ART SECURITY - No more managing multiple private keys and
  wallets. With the Universal Wallet, you generate a single Ethos SmartKey for
  each wallet instance you create, and it takes care of the rest – providing
  automated maximum security management of all of your digital assets. You
  remain in complete control at all times. Lose your phone? Simply restore your
  cryptocurrency wallet and regain control of your funds using your Ethos
  SmartKey. Self-Custody at its finest!

so they claim self-custody but using some [brand name] SmartKey which definitely
does not sound like a standard other wallets would support. What if they go out of
business? **Judging by the most recent ratings, that's exactly what happened
already** but the comments also sound like the SmartKey is just BIP39 24 words
mnemonic, so a broadly used standard after all.

We can't find any source code and thus consider the app **not verifiable**.
