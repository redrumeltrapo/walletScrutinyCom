---
wsId: 
title: "Bitcoin Investment Apps"
altTitle: 
authors:

users: 1
appId: net.cryptoswingalert.bitcoin.investment.apps
released: 2021-07-17
updated: 2021-07-17
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: net.cryptoswingalert.bitcoin.investment.apps.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
