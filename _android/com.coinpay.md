---
wsId: coinpayapp
title: "Coinpay: Buy & Send Bitcoin Fast Cryptocurrency 😇"
altTitle: 
authors:
- leo
users: 5000
appId: com.coinpay
released: 2020-05-26
updated: 2020-08-23
version: "1.0.7"
stars: 3.3
ratings: 25
reviews: 18
size: 5.4M
website: https://www.coinpayapp.com
repository: 
issue: 
icon: com.coinpay.png
bugbounty: 
verdict: stale
date: 2021-08-19
signer: 
reviewArchive:
- date: 2020-12-14
  version: "1.0.7"
  appHash: 
  gitRevision: 79e92f0e1174136cdf05180253c87cacb589f002
  verdict: nosource

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.coinpay/
---


> We are a non-custodial wallet, so users can send Bitcoin and other
  cryptocurrencies globally with direct access to the blockchain.

... so this is (claiming to be) a non-custodial Bitcoin wallet. Can we verify
this?

The answer is "no". There is no source code linked on their website or Google
Play description. This app is closed source and thus **not verifiable**.
