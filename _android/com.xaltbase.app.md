---
wsId: 100xaltbase
title: "100xAltbase: Buy Crypto Altcoins, Altcoin Exchange"
altTitle: 
authors:
- danny
users: 10000
appId: com.xaltbase.app
released: 2021-05-31
updated: 2021-08-18
version: "3.0.2"
stars: 3.2
ratings: 3590
reviews: 2600
size: 17M
website: https://www.100xcoin.io/
repository: 
issue: 
icon: com.xaltbase.app.png
bugbounty: 
verdict: nobtc
date: 2021-09-30
signer: 
reviewArchive:


providerTwitter: 100XCoin_
providerLinkedIn: 100xCoin
providerFacebook: 100xcoinFB
providerReddit: 

redirect_from:

---


This DeFi trading app is described in its Google Play:

> Download this defi crypto app now for FREE and buy Alt Coins, Meme Coins & everything in-between. Invest in cryptocurrency with the exclusive app for new crypto coins - 100xAltbase.

We downloaded the app and proceeded to create a wallet. Creating a wallet was done through a 12 word mnemonic. After that, BNB and 100xcoin were readily available. As its name implies, it is focused on alt coins. We searched for a BTC wallet, and there was none. 

To verify we went on their Telegram support channel and asked. They confirmed this and suggested that it would be a "future feature".

This DeFi app, has **no bitcoin wallet**. 
