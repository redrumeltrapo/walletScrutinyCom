---
wsId: 
title: "Skrill - Pay and spend money online"
altTitle: 
authors:
- danny
users: 5000000
appId: com.moneybookers.skrillpayments
released: 2013-10-29
updated: 2021-11-01
version: "3.71.0-2021101416"
stars: 3.8
ratings: 128128
reviews: 65291
size: 58M
website: https://www.skrill.com/
repository: 
issue: 
icon: com.moneybookers.skrillpayments.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-09
signer: 
reviewArchive:


providerTwitter: PlugIntoPaysafe
providerLinkedIn: Paysafe Group
providerFacebook: PlugIntoPaysafe
providerReddit: 

redirect_from:

---


>The app empowers you to make online payments, send money to a friend or relative, and exchange cryptocurrencies.

From their terms of use:

>5.2.2 you will not have a personal cryptocurrency wallet with the Cryptocurrency Exchange

>The Supported Cryptocurrencies (and the fiat currency used to buy/sell them) are held in custody by a third party.

Based on this information, the app is custodial therefore not verifiable.
