---
wsId: 
title: "StiB P2P - Bitcoin Ethereum Decentralized Exchange"
altTitle: 
authors:

users: 5000
appId: com.StiBLabs.StiBp2p
released: 2019-10-07
updated: 2020-03-11
version: "1.0.5"
stars: 4.8
ratings: 453
reviews: 451
size: 8.7M
website: 
repository: 
issue: 
icon: com.StiBLabs.StiBp2p.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


