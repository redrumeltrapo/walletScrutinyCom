---
wsId: 
title: "Bitcoin Invest Uk"
altTitle: 
authors:

users: 0
appId: com.balletcrypto.bitcoin.invest.uk
released: 2021-06-20
updated: 2021-06-20
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: com.balletcrypto.bitcoin.invest.uk.jpg
bugbounty: 
verdict: defunct
date: 2021-09-23
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-13**: This app is not on the Play Store anymore.

<!-- Emanuel thinks this is probably a scam. See https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/314 -->
