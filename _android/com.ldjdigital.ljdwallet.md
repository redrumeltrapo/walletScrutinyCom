---
wsId: 
title: "LDJ Wallet, your preferred multi-currency wallet."
altTitle: 
authors:

users: 50
appId: com.ldjdigital.ljdwallet
released: 2020-02-29
updated: 2020-09-03
version: "1.18"
stars: 0.0
ratings: 
reviews: 
size: 5.5M
website: 
repository: 
issue: 
icon: com.ldjdigital.ljdwallet.png
bugbounty: 
verdict: stale
date: 2021-08-29
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


