---
wsId: 
title: "Crypto Invest App"
altTitle: 
authors:

users: 0
appId: org.bitcoinup.crypto.invest.app
released: 2021-06-17
updated: 2021-06-17
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 3.5M
website: 
repository: 
issue: 
icon: org.bitcoinup.crypto.invest.app.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
