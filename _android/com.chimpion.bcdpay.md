---
wsId: 
title: "BCD Pay"
altTitle: 
authors:

users: 5000
appId: com.chimpion.bcdpay
released: 2019-05-21
updated: 2020-03-16
version: "7.1.63"
stars: 4.7
ratings: 182
reviews: 152
size: 28M
website: 
repository: 
issue: 
icon: com.chimpion.bcdpay.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


