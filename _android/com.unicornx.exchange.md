---
wsId: 
title: "UnicornX Exchange - Digital Assets Trading"
altTitle: 
authors:

users: 1000
appId: com.unicornx.exchange
released: 2020-05-13
updated: 2020-07-28
version: "1.4.2"
stars: 4.8
ratings: 70
reviews: 54
size: 95M
website: 
repository: 
issue: 
icon: com.unicornx.exchange.png
bugbounty: 
verdict: stale
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


