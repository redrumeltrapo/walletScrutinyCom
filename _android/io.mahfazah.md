---
wsId: 
title: "Mahfazah | محفظة بيتكوين"
altTitle: 
authors:

users: 1000
appId: io.mahfazah
released: 2018-09-19
updated: 2018-11-22
version: "1.2.6"
stars: 4.4
ratings: 32
reviews: 15
size: 20M
website: 
repository: 
issue: 
icon: io.mahfazah.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


