---
wsId: ascendex
title: "AscendEX: Bitcoin Exchange/Crypto Trading Platform"
altTitle: 
authors:
- leo
users: 100000
appId: com.ascendex.exchange
released: 2021-04-28
updated: 2021-10-31
version: "2.6.8"
stars: 4.8
ratings: 3491
reviews: 1067
size: 47M
website: https://ascendex.com
repository: 
issue: 
icon: com.ascendex.exchange.png
bugbounty: 
verdict: custodial
date: 2021-05-12
signer: 
reviewArchive:


providerTwitter: AscendEX_Global
providerLinkedIn: 
providerFacebook: AscendEXOfficial
providerReddit: AscendEX_Official

redirect_from:

---


The AscendEx mobile app claims on the website help section to manage bitcoins...

> You can withdraw your digital assets to external platforms or wallets via
  their address. Copy the address from the external platform or wallet, and
  paste it into the withdrawal address field on AscendEX to complete the
  withdrawal. 

however there is no evidence of the wallet being non-custodial. This leads us to
conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.
