---
wsId: 
title: "Coins Wallet for bitcoin and litecoin"
altTitle: 
authors:
- leo
users: 10000
appId: com.newcurrency.coinstrunk.v1
released: 2020-10-15
updated: 2021-07-16
version: "1.10"
stars: 4.1
ratings: 554
reviews: 454
size: 6.1M
website: https://buxtank.com/
repository: 
issue: 
icon: com.newcurrency.coinstrunk.v1.png
bugbounty: 
verdict: custodial
date: 2021-07-29
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> This wallet helps people to create bitcoin or litecoin transactions

ok, so it's a wallet for BTC.

> with 0 (zero) blockchain fee. Yes, you send cash absolutely free.

that sounds suspicious as zero fee usually only works when no on-chain transactions
happen.

In their [FAQ](https://buxtank.com/faqs) they state:

> **I forgot my password, I cannot log into my account. What should I do?**<br>
  Open the Account Information Reminder web page and type your login name. Then you will be asked a security question; if the answer if correct you will obtain access your account.

So here they are talking about an account which is a contradiction to a wallet.

In summary the provider makes no clear statements about the app being self-custodial
and bits here and there indicate it is custodial and thus **not verifiable**.
