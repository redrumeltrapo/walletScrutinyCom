---
wsId: 
title: "Kraken Futures: Bitcoin & Crypto Futures Trading"
altTitle: 
authors:
- leo
- danny
users: 10000
appId: com.krakenfutures
released: 2019-10-07
updated: 2021-03-24
version: "5.24.0"
stars: 1.1
ratings: 412
reviews: 277
size: 12M
website: https://futures.kraken.com
repository: 
issue: 
icon: com.krakenfutures.png
bugbounty: 
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.krakenfutures/
  - /posts/com.krakenfutures/
---


This is the interface for an exchange and nothing in the description hints at
non-custodial parts to it.

It also appears [you can only transfer funds to and from your Kraken account.](https://support.kraken.com/hc/en-us/articles/360022627692-Transferring-funds-to-and-from-the-Holding-Wallet#:~:text=Navigate%20to%20%E2%80%9CWallets%E2%80%9D%20on%20the,that%20the%20transfer%20was%20requested.)