---
wsId: STASISStablecoinWallet
title: "STASIS Stablecoin Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.stasis.stasiswallet
released: 2018-06-13
updated: 2021-09-20
version: "1.9.24"
stars: 4.4
ratings: 163
reviews: 97
size: 18M
website: https://stasis.net/wallet
repository: https://github.com/stasisnet
issue: 
icon: com.stasis.stasiswallet.png
bugbounty: 
verdict: custodial
date: 2020-05-03
signer: 
reviewArchive:


providerTwitter: stasisnet
providerLinkedIn: stasisnet
providerFacebook: stasisnet
providerReddit: 

redirect_from:
  - /com.stasis.stasiswallet/
  - /posts/com.stasis.stasiswallet/
---


On Google Play and their website there is no mention of being non-custodial and
certainly there is no source code available. Until we hear opposing claims
we consider it a custodial app and therefore **not verifiable**.
