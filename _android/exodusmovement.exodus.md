---
wsId: ExodusCryptoBitcoinWallet
title: "Exodus: Crypto Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 1000000
appId: exodusmovement.exodus
released: 2019-06-06
updated: 2021-11-04
version: "21.11.4"
stars: 4.6
ratings: 73266
reviews: 22545
size: 45M
website: https://www.exodus.io/mobile
repository: https://github.com/exodusmovement
issue: 
icon: exodusmovement.exodus.png
bugbounty: 
verdict: nosource
date: 2020-10-12
signer: 
reviewArchive:


providerTwitter: exodus_io
providerLinkedIn: 
providerFacebook: exodus.io
providerReddit: 

redirect_from:

---


This app certainly sounds like it is non-custodial:

> Don’t give control of your private keys to centralized wallets and exchanges
that can suffer from hacks and lose your funds. Exodus encrypts your private
keys and transaction data on your device so that no one can access your
cryptocurrency but you. You can also Enable Face or Touch ID to conveniently
secure your wallet without having to type your passcode.

Can we find the source code though? ...

No word about a repository on the description but the website links to
[this GitHub](https://github.com/ExodusMovement) but there is no repository name
suggesting there to be an Android wallet and as none of the company's
repositories contains the appId or at least [GitHub can't find it](https://github.com/search?q=org%3AExodusMovement+%22exodusmovement.exodus%22&type=Code),
we conclude that this app is closed source.

**Update**: We asked them about the source code but
[they confirmed](https://twitter.com/exodus_io/status/1208416689672663046):
Exodus [is and stays closed source](https://support.exodus.io/article/89-is-exodus-open-source).

Our verdict: This app is **not verifiable**.
