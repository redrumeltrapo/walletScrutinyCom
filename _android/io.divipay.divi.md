---
wsId: DiviWallet
title: "Divi Wallet: Crypto & Staking"
altTitle: 
authors:
- danny
users: 5000
appId: io.divipay.divi
released: 2021-04-28
updated: 2021-10-22
version: "1.12"
stars: 4.5
ratings: 118
reviews: 80
size: 49M
website: https://diviproject.org/
repository: https://github.com/DiviProject
issue: https://github.com/DiviProject/Divi-Desktop-Public/issues/214
icon: io.divipay.divi.png
bugbounty: 
verdict: nosource
date: 2021-11-04
signer: 
reviewArchive:


providerTwitter: diviproject
providerLinkedIn: 
providerFacebook: diviproject
providerReddit: DiviProject

redirect_from:

---


## App Description

Rewards, nodes, staking payments, lottery and others are the main features of this wallet

> **We never have access to your private keys** or backup phrases, meaning you are fully in control of your digital assets. Our crypto wallet integrates the latest biometric security and 2FA authentication

## The App

We downloaded the app and two options were initially available: Create a new Wallet and 'I already have a Wallet'.

We are then asked to assign a 4-digit pin code. Afterwards, we are asked to verify our phone number, this can be skipped. Email verification can also be skipped. We also skipped profile creation.

Afterwards, it would seem that profile creation was mandatory after all. Skipping brings us to profile details. We filled it up. After filling up some more details, we are then presented with the 12-word backup phrase. We then confirmed it.

## Verdict

This is evidently a **self-custodial** wallet.

While [there is a repository](https://github.com/DiviProject/Divi-Desktop-Public) for the Desktop App, we cannot find source for the Google Play app. We have created [an issue in Github.](https://github.com/DiviProject/Divi-Desktop-Public/issues/214) At the moment, however, this app has **no source available.**
