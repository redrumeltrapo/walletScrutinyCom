---
wsId: 
title: "Bottrex - Cryptocurrency trading bot"
altTitle: 
authors:
- danny
users: 10000
appId: io.bottrex.starter
released: 2019-05-20
updated: 2021-10-08
version: "0.4.5"
stars: 3.7
ratings: 86
reviews: 47
size: 8.8M
website: https://bottrex.net/en
repository: 
issue: 
icon: io.bottrex.starter.png
bugbounty: 
verdict: nowallet
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: bottrexoficial
providerLinkedIn: 
providerFacebook: bottrex
providerReddit: 

redirect_from:

---


## App Description

Bottrex is not an exchange or wallet, it is a trading bot. It connects to exchanges via API.

> Bottrex is a trading robot that trades Bitcoin and other cryptocurrencies **on the world's largest exchanges.**

The app description even lists exchanges that this bot functions on:

> Automate your trading on the main market exchanges: Binance, Bittrex, Huobi, HitBTC, Kucoin, Bitfinex, OKEX, BitcoinTrade, NovaDAX and FTX.

Here's a [tutorial](https://support.bottrex.net/en/articles/61/connect-your-bot-to-huobi) on how to connect Bottrex with huobi.

This is **not a wallet.**
