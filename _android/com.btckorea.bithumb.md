---
wsId: bithumbko
title: "Bithumb"
altTitle: 
authors:
- leo
users: 1000000
appId: com.btckorea.bithumb
released: 2017-09-26
updated: 2021-10-26
version: "2.3.0"
stars: 3.5
ratings: 22167
reviews: 9343
size: 40M
website: https://www.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumb.png
bugbounty: 
verdict: custodial
date: 2021-02-19
signer: 
reviewArchive:


providerTwitter: BithumbOfficial
providerLinkedIn: 
providerFacebook: bithumb
providerReddit: 

redirect_from:

---


This app is an interface to an exchange and to our knowledge only features
custodial accounts and therefore is **not verifiable**.
