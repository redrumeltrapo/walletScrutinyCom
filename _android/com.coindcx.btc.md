---
wsId: CoinDCXPro
title: "CoinDCX:Bitcoin Investment App"
altTitle: 
authors:
- danny
users: 5000000
appId: com.coindcx.btc
released: 2020-12-09
updated: 2021-11-04
version: "2.4.005"
stars: 4.1
ratings: 140166
reviews: 36756
size: 68M
website: https://coindcx.com/
repository: 
issue: 
icon: com.coindcx.btc.png
bugbounty: https://coindcx.com/bug-bounty
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: coindcx
providerLinkedIn: coindcx
providerFacebook: CoinDCX
providerReddit: 

redirect_from:

---


[CoinDCX **Pro**](https://walletscrutiny.com/android/com.coindcx) and CoinDCX **Go** are from the same providers. Go is a

> Bitcoin & Crypto Investment app

while Pro is a trading app.

> Our cryptocurrency wallets are extremely secure and the funds are ensured with BitGo. 98% of CoinDCX's funds are held in geographically distributed multi-signature cold wallets.

According to this claim from the Google Play description, this means that funds are under the custody of a third party. The product is **custodial** and therefore **not verifiable**.
