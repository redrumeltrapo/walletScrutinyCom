---
wsId: 
title: "GENE Wallet (Bitcoin, Ether, GENE, ERC-20)"
altTitle: 
authors:

users: 1000
appId: com.gene.wallet
released: 2018-03-06
updated: 2019-02-08
version: "1.4.1"
stars: 4.1
ratings: 95
reviews: 60
size: 24M
website: 
repository: 
issue: 
icon: com.gene.wallet.png
bugbounty: 
verdict: obsolete
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


