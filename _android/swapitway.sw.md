---
wsId: 
title: "Swapitway : Sell Trade or Swap Gift Card & Bitcoin"
altTitle: 
authors:

users: 500
appId: swapitway.sw
released: 2021-02-10
updated: 2021-02-21
version: "2.5"
stars: 3.7
ratings: 15
reviews: 10
size: 8.9M
website: 
repository: 
issue: 
icon: swapitway.sw.jpg
bugbounty: 
verdict: defunct
date: 2021-09-06
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-28**: This app is no more.
