---
wsId: 
title: "Alterdice"
altTitle: 
authors:

users: 1000
appId: com.alterdice.android
released: 2018-09-24
updated: 2018-11-22
version: "1.3.7"
stars: 2.1
ratings: 19
reviews: 15
size: 1.4M
website: 
repository: 
issue: 
icon: com.alterdice.android.png
bugbounty: 
verdict: obsolete
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


