---
wsId: 
title: "WalletConnect : best Crypto Bitcoin Wallet"
altTitle: 
authors:

users: 1000
appId: com.walletconnect.walletair
released: 2021-08-23
updated: 2021-08-23
version: "1.0"
stars: 2.4
ratings: 90
reviews: 75
size: 19M
website: 
repository: 
issue: 
icon: com.walletconnect.walletair.png
bugbounty: 
verdict: defunct
date: 2021-09-16
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-08**: This app is not available anymore.

