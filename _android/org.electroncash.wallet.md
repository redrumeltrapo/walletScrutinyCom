---
wsId: 
title: "Electron Cash wallet for Bitcoin Cash"
altTitle: 
authors:

users: 10000
appId: org.electroncash.wallet
released: 2018-12-08
updated: 2021-10-03
version: "4.2.5-5"
stars: 4.1
ratings: 172
reviews: 81
size: 36M
website: https://electroncash.org
repository: 
issue: 
icon: org.electroncash.wallet.png
bugbounty: 
verdict: nobtc
date: 2019-12-20
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /org.electroncash.wallet/
  - /posts/org.electroncash.wallet/
---


