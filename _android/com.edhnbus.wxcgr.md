---
wsId: 
title: "Exodus Bitcoin & Ethereum- Crypto Exchange"
altTitle: "(Fake) Exodus Bitcoin & Ethereum- Crypto Exchange"
authors:
- emanuel
- leo
users: 100
appId: com.edhnbus.wxcgr
released: 2021-07-21
updated: 2021-07-21
version: "0.92"
stars: 0.0
ratings: 
reviews: 
size: 10M
website: 
repository: 
issue: 
icon: com.edhnbus.wxcgr.png
bugbounty: 
verdict: defunct
date: 2021-08-08
signer: 
reviewArchive:
- date: 2021-08-02
  version: "0.92"
  appHash: 
  gitRevision: 52858b8264017c91d1a62d3ae4fcd1fc9946be18
  verdict: fake

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


By name and logo this tries to fool users into believing it's
{% include walletLink.html wallet='android/exodusmovement.exodus' verdict='true' %}.
