---
wsId: 
title: "Murex Trivium (Bitcoin Lightning wallet)"
altTitle: 
authors:
- kiwilamb
users: 50
appId: com.murextrivium.walletapp
released: 2018-09-22
updated: 2018-09-23
version: "1.2"
stars: 0.0
ratings: 
reviews: 
size: 6.0M
website: http://murexbitcoinsolutions.com/
repository: 
issue: 
icon: com.murextrivium.walletapp.png
bugbounty: 
verdict: defunct
date: 2021-08-09
signer: 
reviewArchive:
- date: 2021-04-15
  version: "1.2"
  appHash: 
  gitRevision: e2e703e641028d5466e8e09931b9c89d19790759
  verdict: fewusers

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


A Bitcoin Lightning wallet.

This is a fork of {% include walletLink.html wallet='android/com.btcontract.wallettest' verdict='true' %} but with its website down, it looks unmaintained.
