---
wsId: 
title: "Algorand Wallet"
altTitle: 
authors:

users: 100000
appId: com.algorand.android
released: 2019-06-07
updated: 2021-11-05
version: "4.10.8"
stars: 4.7
ratings: 8617
reviews: 2073
size: 28M
website: 
repository: 
issue: 
icon: com.algorand.android.png
bugbounty: 
verdict: nobtc
date: 2020-12-06
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.algorand.android/
---


