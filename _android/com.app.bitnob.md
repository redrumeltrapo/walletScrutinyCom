---
wsId: Bitnob
title: "Bitnob"
altTitle: 
authors:
- danny
users: 10000
appId: com.app.bitnob
released: 2020-05-25
updated: 2021-11-01
version: "1.0.69"
stars: 4.1
ratings: 232
reviews: 139
size: 33M
website: https://bitnob.com/
repository: 
issue: 
icon: com.app.bitnob.png
bugbounty: 
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: Bitnob_official
providerLinkedIn: bitnob
providerFacebook: bitnob
providerReddit: 

redirect_from:

---


## App Description

The app allows users to buy, save and invest in Bitcoin. Its focus is in savings, investment via Dollar Cost Averaging. 

## The Site

The [**Terms and Conditions**](https://bitnob.com/terms-of-services/) specify:

>This license shall automatically terminate if you violate any of these restrictions and may be terminated by Bitnob Technologies at any time. Upon terminating your viewing of these materials or upon the termination of this license, **you must destroy any downloaded materials in your possession whether in electronic or printed format.**

## The App

We downloaded, installed and registered on the service. After verification, we were able to locate the BTC wallet with the ability to send and receive. It is possible to receive BTC via Lightning or Onchain. There are no overt ways to export or backup the private keys or mnemonics.

## Verdict

This app is primarily a method to save or invest in Bitcoin. Thus, its interface and service is geared towards Bitcoin users who would like to use a **custodial** service. It is **not verifiable**.

