---
wsId: eidoo
title: "Eidoo: Bitcoin and Ethereum Wallet and Exchange"
altTitle: 
authors:
- leo
users: 100000
appId: io.eidoo.wallet.prodnet
released: 2017-09-25
updated: 2021-11-08
version: "3.6.3"
stars: 3.1
ratings: 1621
reviews: 806
size: 13M
website: https://eidoo.io
repository: 
issue: 
icon: io.eidoo.wallet.prodnet.png
bugbounty: 
verdict: nosource
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: eidoo_io
providerLinkedIn: eidoo
providerFacebook: eidoocrypto
providerReddit: eidooapp

redirect_from:
  - /io.eidoo.wallet.prodnet/
  - /posts/io.eidoo.wallet.prodnet/
---


The app's description:

> As a non-custodial wallet, funds will always remain in your full control, with
  effortless wallet backup and recovery options.

Unfortunately we can't find any source code. No such link on their website and
neither can we find anything relevant on GitHub which leads to the verdict:
**not verifiable**.
