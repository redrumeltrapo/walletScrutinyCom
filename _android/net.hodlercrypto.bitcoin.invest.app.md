---
wsId: 
title: "Bitcoin Invest"
altTitle: 
authors:

users: 1
appId: net.hodlercrypto.bitcoin.invest.app
released: 2021-06-18
updated: 2021-06-18
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 3.4M
website: 
repository: 
issue: 
icon: net.hodlercrypto.bitcoin.invest.app.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
