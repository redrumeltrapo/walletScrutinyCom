---
wsId: 
title: "Forex indicator signals and crypto currency (Free)"
altTitle: 
authors:
- danny
users: 100000
appId: st.xpairssignals
released: 2017-10-07
updated: 2021-10-03
version: "4.6"
stars: 4.2
ratings: 869
reviews: 501
size: 4.6M
website: 
repository: 
issue: 
icon: st.xpairssignals.png
bugbounty: 
verdict: nowallet
date: 2021-10-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description
From Google Play's description:

> Forex signals are providing 2000+ Different currency pairs from Forex and 4000+ from digital cryptocurrency. Our forex trading experts check each market trade opportunities and hands over to you with easy to watch trading signals.

It's an app that is meant to provide forex trading signals. It does not sound like a bitcoin wallet.


## The App and Verdict
Downloading the app, we see forex signals and no way to receive or send bitcoin. This confirms that the app is **not a wallet.**

