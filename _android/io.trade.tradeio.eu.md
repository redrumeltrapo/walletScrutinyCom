---
wsId: 
title: "trade.io - Smarter crypto trading"
altTitle: 
authors:

users: 5000
appId: io.trade.tradeio.eu
released: 2019-07-22
updated: 2019-09-20
version: "1.0.0"
stars: 2.3
ratings: 27
reviews: 17
size: 3.6M
website: 
repository: 
issue: 
icon: io.trade.tradeio.eu.png
bugbounty: 
verdict: obsolete
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


