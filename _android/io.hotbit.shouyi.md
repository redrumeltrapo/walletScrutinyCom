---
wsId: hotbit
title: "Hotbit"
altTitle: 
authors:
- danny
users: 1000000
appId: io.hotbit.shouyi
released: 2019-09-19
updated: 2021-11-08
version: "1.3.40"
stars: 4.2
ratings: 54187
reviews: 20508
size: 11M
website: https://www.hotbit.io
repository: 
issue: 
icon: io.hotbit.shouyi.png
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: Hotbit_news
providerLinkedIn: hotbitexchange
providerFacebook: hotbitexchange
providerReddit: 

redirect_from:

---


Hotbit describes itself as

> a professional cryptocurrency trading platform

these usually tend to be custodial.

It laters mentions the use of hot and cold wallets, and both are usually traits of custodial offerings. We mark this as **not verifiable.**
