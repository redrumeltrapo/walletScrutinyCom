---
wsId: incognito
title: "Incognito - Anonymous Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 10000
appId: com.incognito.wallet
released: 2019-08-12
updated: 2021-09-26
version: "4.9.0"
stars: 4.1
ratings: 318
reviews: 180
size: 47M
website: https://incognito.org
repository: https://github.com/incognitochain/incognito-wallet
issue: https://github.com/incognitochain/incognito-wallet/issues/1422
icon: com.incognito.wallet.png
bugbounty: 
verdict: nonverifiable
date: 2020-12-07
signer: 
reviewArchive:


providerTwitter: incognitochain
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.incognito.wallet/
---


After many many reviews of custodial wallets, this one makes a promising claim:

> Don’t leave yourself exposed. Go Incognito. It’s non-custodial, decentralized,
  and completely open-source.

And indeed there is [a repository](https://github.com/incognitochain/incognito-wallet)
with 3647 commits by 16 contributors with recent commits. That looks solid!

Unfortunately the project appears to not share the needed `.env` file for
reproducible builds or otherwise build instructions for that purpose.

We reached out to them in
[this issue](https://github.com/incognitochain/incognito-wallet/issues/1422)
and hope to get feedback, soon but for now the app is **not verifiable**.
