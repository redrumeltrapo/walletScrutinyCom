---
wsId: UBFXGlobal
title: "UBFX Global"
altTitle: 
authors:
- danny
users: 10000
appId: com.ubankfx.forex
released: 2017-05-17
updated: 2021-10-20
version: "v7.15-5092-7e966d495"
stars: 4.8
ratings: 1467
reviews: 869
size: 32M
website: https://www.ubfx.co.uk/
repository: 
issue: 
icon: com.ubankfx.forex.png
bugbounty: 
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description
From Google Play:

> UBFX is a foreign exchange trading platform designed for starter investors and professional traders.

Also in the description, it mentions the use of CFDs.

> Cryptocurrency CFDs are provided - BTCUSD, BCHUSD, LTCUSD, ETHUSD and RPLUSD.

## The App
We tried the app and could find no wallet or option for the user to manage bitcoins. As this platform utilizes CFD trading, users do not actually own the assets.

## Verdict
You **cannot send or receive bitcoins** on this app.
