---
wsId: 
title: "Bitfairex: Digital Assets Marketplace"
altTitle: 
authors:

users: 100
appId: com.bitfairex
released: 2020-12-27
updated: 2021-03-25
version: "1.2.3 Release"
stars: 4.7
ratings: 32
reviews: 27
size: 5.3M
website: 
repository: 
issue: 
icon: com.bitfairex.png
bugbounty: 
verdict: fewusers
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


