---
wsId: Talken
title: "Talken -   Multi-chain NFT Wallet & Marketplace"
altTitle: 
authors:
- kiwilamb
- danny
users: 10000
appId: io.talken.wallet
released: 2019-07-31
updated: 2021-10-15
version: "1.0.34"
stars: 4.7
ratings: 5859
reviews: 4647
size: 11M
website: https://talken.io/
repository: 
issue: 
icon: io.talken.wallet.png
bugbounty: 
verdict: nobtc
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: Talken_
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**UPDATE 2021-09-01**: This "wallet" is meant for NFTs and not Bitcoin.

With this statement below from the providers [Play store description](https://play.google.com/store/apps/details?id=io.talken.wallet), it is clear that the user is not in control of the wallets private keys.

> Easy and secure wallet
> Easy wallet service without managing private keys and mnemonics.

Our Verdict: This "wallet" is custodial and therefore **not verifiable**

