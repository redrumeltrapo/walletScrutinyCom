---
wsId: 
title: "BitcoinCore for Android"
altTitle: 
authors:

users: 1000
appId: org.lndroid.bitcoincore
released: 2020-07-02
updated: 2020-07-08
version: "0.6"
stars: 4.1
ratings: 13
reviews: 4
size: 7.5M
website: 
repository: 
issue: 
icon: org.lndroid.bitcoincore.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/189 -->
