---
wsId: AxiaInvestments
title: "Axia Investments"
altTitle: 
authors:
- danny
users: 50000
appId: com.pandats.axia
released: 2020-11-16
updated: 2021-09-05
version: "1.0.43"
stars: 3.2
ratings: 656
reviews: 552
size: 22M
website: https://axiainvestments.com/
repository: 
issue: 
icon: com.pandats.axia.png
bugbounty: 
verdict: nosendreceive
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

> Axia Investments is a registered brand of the most secure and trusted trading online broker in the MENA region. Axia Investments, through its client-centric strategy, empowers clients’ investment decisions by providing a unique practical educational program backed by original resourceful materials.

## The Site

It allows the trade of: forex, CFDs (including cryptocurrencies), commodities, indices and stocks

## Verdict

Like most forex trading apps, this service only allows the trading of cryptocurrencies via CFDs. The means of funding the user account is also constrained to traditional fiat means. This app, therefore **cannot send and receive bitcoins** in the sense of a Bitcoin wallet.