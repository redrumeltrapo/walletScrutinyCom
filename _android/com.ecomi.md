---
wsId: 
title: "ECOMI Secure Wallet"
altTitle: 
authors:

users: 10000
appId: com.ecomi
released: 2018-08-15
updated: 2020-03-05
version: "2.9.2"
stars: 3.7
ratings: 123
reviews: 60
size: 33M
website: 
repository: 
issue: 
icon: com.ecomi.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


