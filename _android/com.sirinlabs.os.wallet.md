---
wsId: 
title: "FINNEY Wallet"
altTitle: 
authors:

users: 1000
appId: com.sirinlabs.os.wallet
released: 2019-01-13
updated: 2020-09-15
version: "1.0.9.5489"
stars: 4.0
ratings: 23
reviews: 19
size: 25M
website: 
repository: 
issue: 
icon: com.sirinlabs.os.wallet.png
bugbounty: 
verdict: stale
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


