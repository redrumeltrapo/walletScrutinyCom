---
wsId: cointiger
title: "CoinTiger-Crypto Exchange"
altTitle: 
authors:
- danny
users: 100000
appId: com.cointiger.ex
released: 2020-07-23
updated: 2021-10-22
version: "5.0.40.0"
stars: 4.7
ratings: 11335
reviews: 982
size: 39M
website: https://www.cointiger.com/
repository: 
issue: 
icon: com.cointiger.ex.png
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: CoinTigerEX
providerLinkedIn: 
providerFacebook: CoinTigerEX
providerReddit: CoinTigerExchange

redirect_from:

---


CoinTiger Exchange [Terms of Service](https://www.cointiger.com/en-us/#/footer/service)

> About CoinTiger<br>
  As an important part of the CoinTiger Ecosystem, CoinTiger mainly serves as a global online platform for Digital Assets trading, and provides Users with a trading platform, financing services, technical services and other Digital Assets-related services. As further detailed in Article 3 below, Users must register and open an account with CoinTiger, and deposit Digital Assets into their account prior to trading. Users may, subject to the restrictions set forth in these Terms, apply for the withdrawal of Digital Assets.

CoinTiger ZenDesk Page [How to Withdraw Cryptocurrencies](https://cointiger.zendesk.com/hc/en-us/articles/360009805554-How-to-withdraw)

CoinTiger [Disclaimer Page](https://cointiger.zendesk.com/hc/en-us/articles/360009807774)

> The company shall not be liable for any accidents arising out of transactions that are outside of the trading rules stipulated in these terms and conditions and shall not be liable for any dispute arising out of the negligence of the seller or buyer. The damage or loss caused by the company's affiliates shall be in accordance with the terms of the affiliates, and the dispute between the affiliates and the customers shall be settled in principle.

Cointiger is developed by SINGAPORE TAI-E CYBER-TECH PTE. LTD.

This app is a cryptocurrency exchanged developed by a company based in Singapore. It's cryptocurrency wallets are **custodial**.

