---
wsId: Fiboda
title: "Fiboda - Copy trading Platform"
altTitle: 
authors:
- danny
users: 5000
appId: com.fiboda.app
released: 2021-01-14
updated: 2021-07-08
version: "1.0.5"
stars: 4.7
ratings: 308
reviews: 31
size: 44M
website: https://www.fiboda.com/
repository: 
issue: 
icon: com.fiboda.app.png
bugbounty: 
verdict: nosendreceive
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

> FIBODA is a leading multi-broker, Social, and Copy-trading Platform. Invest by copy trading with Top and curated Traders in Forex, Indices, Commodities, **Stock & Cryptos CFDs!**

FIBODA supports the buying and selling of CFDs, which means that users don't actually own the assets.

## The Site

From the Terms of Service:

> Fiboda offers solely the technical means that enable you to adopt trading strategies of other users of the Platform, based on the assumption that you have consciously chosen to conduct **CFD and/or FX transactions with a Broker of your choice.**

There's little to read in this site concerning cryptocurrencies.

## Verdict

Due to this platform being created for traders who conduct CFD transactions, users do not own the digital assets. This app **does not allow users to send or receive** bitcoins.
