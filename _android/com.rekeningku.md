---
wsId: rekeningku
title: "Rekeningku.com: Jual Beli Bitcoin, SHIB dan Crypto"
altTitle: 
authors:
- danny
users: 100000
appId: com.rekeningku
released: 2018-02-21
updated: 2021-11-09
version: "2.1.62"
stars: 4.7
ratings: 18955
reviews: 6667
size: 32M
website: https://www.rekeningku.com
repository: 
issue: 
icon: com.rekeningku.png
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: Rekeningkucom
providerLinkedIn: rekeningku-com
providerFacebook: Rekeningkucom
providerReddit: 

redirect_from:

---


Rekeningku offers the Digital Asset Transaction services that allow its users to buy or sell Bitcoin and other cryptocurrencies. It  is managed by PT. My Account Dotcom Indonesia.

It uses cold storage for its wallet security.

Help page on how to [withdraw digital assets](https://help.rekeningku.com/cara-melakukan-withdraw-aset-digital-coin-dan-token/)

Help page on how to [deposit digital assets](https://help.rekeningku.com/cara-deposit-aset-digital-anda/)

Help page on [how to order sell/buy digital assets](https://help.rekeningku.com/cara-melakukan-order-jual-beli-aset-digital/)

Help page [Terms and Conditions](https://help.rekeningku.com/syarat-dan-ketentuan-rekeningku-com/)

> In the event of a suspicious transaction and/or transaction exceeding the transaction volume limit set by Accountku.com against the member for any reason, Accountku.com reserves the right at any time to delay crediting funds to the member's Accountku.com account and/or to block the wallet. member until the investigation process is completed within the period specified by Accountku.com.

KYC, and account access restrictions based on its Terms and Conditions indicates that this is a custodial service.


