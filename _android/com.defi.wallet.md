---
wsId: cryptoComDefi
title: "Crypto.com l DeFi Wallet"
altTitle: 
authors:
- leo
users: 1000000
appId: com.defi.wallet
released: 2020-05-11
updated: 2021-11-04
version: "1.20.0"
stars: 4.0
ratings: 8824
reviews: 2832
size: 54M
website: https://crypto.com/en/defi/
repository: 
issue: 
icon: com.defi.wallet.png
bugbounty: 
verdict: nosource
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:
  - /com.defi.wallet/
  - /posts/com.defi.wallet/
---


This app's description is promising:

> Decentralized:
> - Gain full control of your crypto and private keys [...]

On their website though we cannot find any links to source code.

Searching their `appId` on GitHub,
[yields nothing](https://github.com/search?q=%22com.defi.wallet%22) neither.

This brings us to the verdict: **not verifiable**.

The provider has a family of three apps that we triple-checked to be linked from
their website:

* {% include walletLink.html wallet='android/com.crypto.exchange' verdict='true' %}
* {% include walletLink.html wallet='android/co.mona.android' verdict='true' %}
* {% include walletLink.html wallet='android/com.defi.wallet' verdict='true' %}
