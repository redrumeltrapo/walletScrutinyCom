---
wsId: 
title: "ELLIPAL: Crypto Bitcoin Wallet"
altTitle: 
authors:

users: 10000
appId: com.ellipal.wallet
released: 2018-07-02
updated: 2021-10-13
version: "3.2.0"
stars: 4.3
ratings: 718
reviews: 408
size: 27M
website: https://www.ellipal.com
repository: 
issue: 
icon: com.ellipal.wallet.png
bugbounty: 
verdict: nosource
date: 2021-03-05
signer: 
reviewArchive:


providerTwitter: ellipalwallet
providerLinkedIn: 
providerFacebook: ellipalclub
providerReddit: ELLIPAL_Official

redirect_from:

---


This app comes with the claim:

> Secure HD wallet for cryptocurrencies. Store, transact, and trade Bitcoin and
  Crypto: BTC ETH LTC DGB BSV BAT OMG XRP XVG & 1000+ more.

and absent more explicit claims, we have to guess that HD means "hierarchically
deterministic", a standard for self-custodial wallets.

As we can't find a source code repository on their website or
[their company GitHub account](https://github.com/ELLIPAL?tab=repositories&type=source),
we assume the app is closed source and thus **not verifiable**.
