---
wsId: 
title: "Galaxy Wallet"
altTitle: 
authors:

users: 1000
appId: com.application.galaxywallet
released: 2019-12-09
updated: 2020-05-19
version: "1.2"
stars: 3.2
ratings: 21
reviews: 14
size: 5.9M
website: 
repository: 
issue: 
icon: com.application.galaxywallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


