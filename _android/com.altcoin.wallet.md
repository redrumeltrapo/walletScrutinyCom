---
wsId: 
title: "Altcoin Wallet. Your coins protected"
altTitle: 
authors:

users: 10000
appId: com.altcoin.wallet
released: 2018-03-19
updated: 2020-01-21
version: "v1.6"
stars: 3.0
ratings: 101
reviews: 83
size: 3.7M
website: 
repository: 
issue: 
icon: com.altcoin.wallet.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


