---
wsId: 
title: "KaiserWallet2 BLE - Cold wallet, Hardware wallet"
altTitle: 
authors:

users: 100
appId: io.kaiser.kaiserwallet2.ble_reader
released: 2018-12-04
updated: 2020-02-28
version: "2.9.10"
stars: 0.0
ratings: 
reviews: 
size: 2.8M
website: 
repository: 
issue: 
icon: io.kaiser.kaiserwallet2.ble_reader.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


