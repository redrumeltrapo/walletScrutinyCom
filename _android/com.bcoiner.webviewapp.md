---
wsId: 
title: "Bcoiner - Free Bitcoin Wallet"
altTitle: 
authors:

users: 100000
appId: com.bcoiner.webviewapp
released: 2014-12-01
updated: 2018-01-05
version: "1.3.2"
stars: 4.3
ratings: 2468
reviews: 1251
size: 2.2M
website: https://bcoiner.com
repository: 
issue: 
icon: com.bcoiner.webviewapp.png
bugbounty: 
verdict: defunct
date: 2021-06-23
signer: 
reviewArchive:
- date: 2021-06-17
  version: "1.3.2"
  appHash: 
  gitRevision: 43f90b53d67bc38cec05b316d39730d071039d3a
  verdict: wip
- date: 2021-01-15
  version: "1.3.2"
  appHash: 
  gitRevision: 43f90b53d67bc38cec05b316d39730d071039d3a
  verdict: defunct
- date: 2019-11-12
  version: "1.3.2"
  appHash: 
  gitRevision: 372c9c03c6422faed457f1a9975d7cab8f13d01f
  verdict: nosource

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /bcoiner/
  - /com.bcoiner.webviewapp/
  - /posts/com.bcoiner.webviewapp/
---


**Update 2021-06-24**: This app is back on Google Play but given the app is the
one app we got most support requests about, from people that believe it's a
scam and given their latest comments remain scam accusations, we return to the
defunct verdict, assuming it doesn't work as intended. If you think that is a
mistake, please open an issue in our GitLab.

**Update 2021-01-15**: This app is not on Google Play anymore

Bcoiner - Free Bitcoin Wallet
does not share clear information it looks custodial and absent source code it is
definitely **not verifiable**.

Other observations
------------------

* The ratings are ... suspicious. Blocks of 5 star and 1 star claiming 5 star
  are fake and scam accusations.
