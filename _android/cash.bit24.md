---
wsId: 
title: "خرید و فروش بیت کوین تتر و ارز دیجیتال : بیت 24"
altTitle: 
authors:
- danny
users: 10000
appId: cash.bit24
released: 2021-05-30
updated: 2021-09-14
version: "4.0.4"
stars: 4.5
ratings: 1314
reviews: 412
size: 9.5M
website: https://bit24.cash/
repository: 
issue: 
icon: cash.bit24.png
bugbounty: 
verdict: nowallet
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## Google Play Description

Buy and sell bitcoins as well as other cryptocurrencies. (Iranian)

## Site

The web platform and the app allows bitcoin trades via their platform. It is Iranian in origin and does not have an iOS app. This platform does not provide any form of bitcoin wallet for the user. It facilitates the buying and selling, through bank transfers and then proceeds to send the cryptocurrencies to the user.

FAQ

>  یت 24 در حال حاضر سرویس کیف پول ارائه نمی‌دهد اما اگر شما به دنبال یک کیف پول امن، سریع و آسان هستید که تنوع گسترده‌ای از ارزهای دیجیتال را پشتیبانی کند، کیف پول کوینومی  یکی از گزینه‌هایی است که با آن می‌توانید ارزهای دیجیتال را بر روی تلفن همراه خود ذخیره کنید.

We translated the first part via Google translate:

> Bit24 does not currently offer wallet service

### Funding 

> به طور کلی تمامی خریدها از طریق درگاه بانکی و قابل پرداخت با تمامی کارت های بانکی صورت می‌پذیرد اما در شرایط خاص و برای مبالغ بالای "یکصد میلیون تومان" امکان واریز به "حساب بانکی" میسر خواهد بود.

Translated:

> In general, all purchases are made through a bank portal and can be paid with all bank cards, but in special circumstances and for amounts higher than "one hundred million tomans", it will be possible to deposit to a "bank account".

## Verdict

This app **does not provide a bitcoin wallet** for the user. 
