---
wsId: whitebit
title: "WhiteBIT – buy & sell bitcoin. Crypto wallet"
altTitle: 
authors:
- danny
users: 100000
appId: com.whitebit.android
released: 2019-06-07
updated: 2021-10-23
version: "2.1.17"
stars: 4.3
ratings: 1851
reviews: 969
size: 13M
website: https://whitebit.com
repository: 
issue: 
icon: com.whitebit.android.png
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: whitebit
providerLinkedIn: whitebit-cryptocurrency-exchange
providerFacebook: whitebit
providerReddit: WhiteBitExchange

redirect_from:

---


This centralized cryptocurrency exchange also has a decentralized exchange. 

The centralized exchange allows for [deposit/withdrawal of cryptocurrencies](https://whitebit.com/faq#22) up to 2 BTC in value without KYC. Above that value, KYC would be needed.

Proof that this is a custodial service can be seen in White Bit's [terms](https://whitebit.com/terms)

> The Platform may set some restrictions on the Withdrawal the Funds and/or on the Transactions

Cold storage for the securing user funds is employed and this further indicates a **custodial** offering. 