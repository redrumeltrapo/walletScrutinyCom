---
wsId: coinbasepro
title: "Coinbase Pro: Trade Bitcoin, Ethereum, SHIB"
altTitle: 
authors:
- leo
users: 1000000
appId: com.coinbase.pro
released: 2020-01-06
updated: 2021-11-09
version: "1.0.82"
stars: 4.0
ratings: 19593
reviews: 5623
size: 56M
website: https://pro.coinbase.com
repository: 
issue: 
icon: com.coinbase.pro.jpg
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: CoinbasePro
providerLinkedIn: 
providerFacebook: coinbase
providerReddit: 

redirect_from:
  - /com.coinbase.pro/
  - /posts/com.coinbase.pro/
---


This is the interface for a trading platform aka exchange. The funds are stored
with the provider. As a custodial service it is **not verifiable**.
