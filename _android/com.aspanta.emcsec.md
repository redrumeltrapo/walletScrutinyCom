---
wsId: 
title: "Emercoin Secure Wallet"
altTitle: 
authors:

users: 1000
appId: com.aspanta.emcsec
released: 2017-11-28
updated: 2018-12-25
version: "1.2.1"
stars: 4.3
ratings: 28
reviews: 8
size: 8.9M
website: 
repository: 
issue: 
icon: com.aspanta.emcsec.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


