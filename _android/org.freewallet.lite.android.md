---
wsId: 
title: "Lite HD Wallet – Your Coin Base"
altTitle: 
authors:
- leo
users: 500
appId: org.freewallet.lite.android
released: 
updated: 2019-03-01
version: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: https://freewallet.org
repository: 
issue: 
icon: org.freewallet.lite.android.png
bugbounty: 
verdict: defunct
date: 2021-05-25
signer: 
reviewArchive:
- date: 2019-11-29
  version: ""
  appHash: 
  gitRevision: 0d5d72cd32532e6c3974d872a76997f7044c5906
  verdict: fewusers


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
---


**Update 2021-05-25**: This app is not on the Play Store anymore. The app's
website links to {% include walletLink.html wallet='android/mw.org.freewallet.app' %} now.

**Update 2019-11-29**: This wallet shares no source code but was categorized as
too small, which makes it irrelevant for listings.

In their description they claim

> Keys are always kept in your phone and never sent outside your device.

so it's a non-custodial wallet.

Neither on Google Play nor on their website could we find a link to the source
code.

[Searching their application ID on GitHub](https://github.com/search?q="org.freewallet.lite.android")
was without results, too.

This wallet is **not verifiable**.
