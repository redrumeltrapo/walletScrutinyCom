---
wsId: BitWallet
title: "BitWallet - Buy & Sell Bitcoin"
altTitle: 
authors:
- leo
users: 10000
appId: com.Android.Inc.bitwallet
released: 2019-07-22
updated: 2021-07-31
version: "1.4.17"
stars: 4.5
ratings: 943
reviews: 783
size: 26M
website: https://www.bitwallet.org
repository: 
issue: 
icon: com.Android.Inc.bitwallet.png
bugbounty: 
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive:


providerTwitter: bitwalletinc
providerLinkedIn: 
providerFacebook: BitWalletInc
providerReddit: 

redirect_from:
  - /com.Android.Inc.bitwallet/
  - /posts/com.Android.Inc.bitwallet/
---


This appears to be primarily an exchange and as there are no claims of you being
in sole control of your funds, we have to assume it is a custodial service and
therefore **not verifiable**.
