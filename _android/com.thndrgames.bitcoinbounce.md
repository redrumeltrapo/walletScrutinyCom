---
wsId: BitcoinBounce
title: "Bitcoin Bounce - Earn Bitcoin"
altTitle: 
authors:
- danny
users: 100000
appId: com.thndrgames.bitcoinbounce
released: 2020-03-20
updated: 2021-09-20
version: "1.1.29"
stars: 3.5
ratings: 1795
reviews: 707
size: 59M
website: https://thndr.games/
repository: 
issue: 
icon: com.thndrgames.bitcoinbounce.png
bugbounty: 
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive:


providerTwitter: thndrgames
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


Bitcoin Bounce is a lightning network powered game.

Players can win satoshis by earning tickets. The tickets are raffled and the winner receives the Satoshis. Creating an account saves the user's progress. Players who earn Satoshis can then withdraw their Bitcoins in another Bitcoin lightning wallet.

This app **cannot receive Bitcoins.** It cannot hold any of your money, so it **does not really function as a wallet.**
