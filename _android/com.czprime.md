---
wsId: CoinZoom
title: "CoinZoom Pro"
altTitle: 
authors:
- kiwilamb
users: 100000
appId: com.czprime
released: 2020-06-11
updated: 2021-10-29
version: "1.4.16"
stars: 4.3
ratings: 2141
reviews: 5
size: 29M
website: https://www.coinzoom.com
repository: 
issue: 
icon: com.czprime.png
bugbounty: 
verdict: custodial
date: 2021-04-20
signer: 
reviewArchive:


providerTwitter: GetCoinZoom
providerLinkedIn: coinzoomhq
providerFacebook: CoinZoom
providerReddit: 

redirect_from:

---


The CoinZoom [support FAQ](https://www.coinzoom.com/support/) states the wallet is custodial... 
Found under "Where can I find the private keys for my wallet?"

> As CoinZoom is a hosted wallet, it's not feasible to provide the private keys to individual wallet addresses; doing so would prevent us from taking advantage of our secure cold-storage technology to protect your funds.

this leads us to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
