---
wsId: ambercrypto
title: "Amber App - Swap & Earn Crypto"
altTitle: 
authors:
- danny
users: 100000
appId: io.ambergroup.amber
released: 2020-09-21
updated: 2021-10-24
version: "1.7.6"
stars: 3.9
ratings: 475
reviews: 228
size: 86M
website: https://www.ambergroup.io
repository: 
issue: 
icon: io.ambergroup.amber.png
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: ambergroup_io
providerLinkedIn: amberbtc
providerFacebook: ambergroup.io
providerReddit: 

redirect_from:

---


The app is a crypto investment app that allows users to: 

- Have attractive yield on major crypto assets
- Swap assets at competitive, real-time pricing
- Spot/Margin trade crypto with ease

We downloaded the app and registered with the service. Using the app requires identity verification from the very beginning. It is possible to [deposit/withdraw bitcoin](https://support.ambergroup.io/hc/en-us/articles/900000933006-How-to-make-a-withdrawal-)

As part of its [Terms](https://www.ambergroup.io/terms)

> We may store Digital Currency you deposit for use of the Services in a “hot wallet”, “cold wallet” or other storage methods at our sole discretion. We shall record your ownership of your Digital Currency in our records.

Furtheremore, 

> Amber maintains full custody of the funds and User data/information which may be turned over to governmental authorities in the event of Account suspension/closure arising from fraud investigations, violation of law investigations or violation of these Terms.

This app is **custodial** and therefore **not verifiable** 