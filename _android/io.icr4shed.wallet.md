---
wsId: 
title: "MultiWallet: MultiChain Crypto Wallet"
altTitle: 
authors:

users: 1000
appId: io.icr4shed.wallet
released: 2021-09-10
updated: 2021-10-19
version: "3.4"
stars: 4.6
ratings: 53
reviews: 17
size: 34M
website: 
repository: 
issue: 
icon: io.icr4shed.wallet.png
bugbounty: 
verdict: wip
date: 2021-10-30
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


