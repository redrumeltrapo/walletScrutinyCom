---
wsId: 
title: "Roostoo: Mock Crypto Trading"
altTitle: 
authors:

users: 10000
appId: com.roostoo.roostoo
released: 2019-07-15
updated: 2020-07-09
version: "2.0.0"
stars: 4.2
ratings: 155
reviews: 76
size: 9.8M
website: 
repository: 
issue: 
icon: com.roostoo.roostoo.png
bugbounty: 
verdict: stale
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


