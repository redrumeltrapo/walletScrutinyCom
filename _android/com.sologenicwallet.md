---
wsId: 
title: "SOLO DEX & Wallet"
altTitle: 
authors:

users: 10000
appId: com.sologenicwallet
released: 2020-02-04
updated: 2021-11-04
version: "2.0.12"
stars: 4.6
ratings: 428
reviews: 98
size: 48M
website: 
repository: 
issue: 
icon: com.sologenicwallet.png
bugbounty: 
verdict: nobtc
date: 2020-06-20
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.sologenicwallet/
  - /posts/com.sologenicwallet/
---


This wallet does not support BTC.
