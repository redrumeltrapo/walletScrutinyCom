---
wsId: bitcointradingcapital
title: "Bitcoin Trading - Capital.com"
altTitle: 
authors:
- danny
users: 100000
appId: com.kapital.trade.crypto
released: 2021-01-20
updated: 2021-10-26
version: "1.34.3"
stars: 4.5
ratings: 929
reviews: 312
size: 67M
website: https://expcapital.com
repository: 
issue: 
icon: com.kapital.trade.crypto.png
bugbounty: 
verdict: nosendreceive
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


The app doesn't have a web equivalent and the primary domain associated with the app points to the services company that created the software. 

Its self-description reads as follows: 

> Trade CFDs on Bitcoin, Ethereum, Ripple, Dogecoin, Litecoin, Shib and other major crypto pairs with 0% commission and tight spreads.

A Contract for Differences [CFD](https://www.investopedia.com/articles/stocks/09/trade-a-cfd.asp) is a financial instrument that allows traders to speculate on an asset without actually owning the asset.

Not much information can be gleaned from the web domain so most of the information is derived from the Google Play description. 

> Capital.com is here to provide an engaging trading experience on our user-friendly platform that is designed to make trading more intuitive. Gain exposure to the crypto markets with derivatives and become a Bitcoin trader *without needing to hold any Bitcoin*.

The presence of CFDs indicate an inability to send and receive bitcoins.


Additional observations: We wonder how this app relates to Capital.com, as that site redirects to {% include walletLink.html wallet='android/com.capital.trading.prod2' verdict='false' %} and not this app.
