---
wsId: 
title: "Fenix Wallet"
altTitle: 
authors:

users: 100
appId: com.fenixcoin.wallet
released: 2018-04-09
updated: 2018-04-09
version: "v1.0.1"
stars: 0.0
ratings: 
reviews: 
size: 3.9M
website: 
repository: 
issue: 
icon: com.fenixcoin.wallet.png
bugbounty: 
verdict: defunct
date: 2021-07-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app, released in 2018 is not on the Play Store anymore. As it had just one
version ever, it's probably not coming back.
