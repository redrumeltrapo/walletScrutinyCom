---
wsId: Zipmex
title: "Zipmex: Sell buy Bitcoin, Ethereum, Cryptocurrency"
altTitle: 
authors:
- danny
users: 500000
appId: com.zipmex
released: 2020-03-30
updated: 2021-11-05
version: "21.11.1.1424724013"
stars: 4.3
ratings: 10831
reviews: 5516
size: 130M
website: https://zipmex.com/th/en/
repository: 
issue: 
icon: com.zipmex.png
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: ZipmexTH
providerLinkedIn: 
providerFacebook: ZipmexThailand
providerReddit: 

redirect_from:

---


Zipmex is a "digital assets exchange" and claims that all assets are safely and securely stored with BitGo.

With this quote from the Play Store:

> Zipmex is one of only a few exchanges to be licensed by Thailand’s Securities and Exchange Commission, approved by Indonesia’s commodity futures trading regulator BAPPEBTI and is also registered with the Australian Transaction Reports and Analysis Centre (AUSTRAC).

Zipmex is strongly hinting towards a custodial nature.

In the official website, Zipmex has an article on private/public keys:

> Remembering public and private keys is always a hassle for any individual. However, if you have an account with Zipmex, you will not have to worry about forgetting your public key or losing your private key.<br>
Public keys are fixed on the Zipmex wallet, so users can just login to their Zipmex accounts and go over to the wallet page to check out their public keys. Users do not need to unlock their wallet using a private key on Zipmex. Once you have logged in to your account, you’ll be able to access to your wallet which reduce the risk of losing or misremembering private keys. <br>
Zipmex’s wallet is secured by BitGo, which is essentially the ‘gold standard’ of the digital assets storage. BitGo  provides institutional-grade security, high compliance, and liquidity for your digital currency—$100 (USD) million theft insurance too.

In the case of this product, the provider is in charge of the keys. This app is **custodial** and thus **not verifiable.**