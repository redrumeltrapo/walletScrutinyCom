---
wsId: 
title: "Bitcoin Invest India"
altTitle: 
authors:

users: 1
appId: org.cryptotreasures.bitcoin.invest.india
released: 2021-06-24
updated: 2021-06-24
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.4M
website: 
repository: 
issue: 
icon: org.cryptotreasures.bitcoin.invest.india.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
