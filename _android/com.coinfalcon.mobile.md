---
wsId: coinfalcon
title: "CoinFalcon – Buy & sell Bitcoin"
altTitle: 
authors:
- leo
users: 10000
appId: com.coinfalcon.mobile
released: 2019-07-10
updated: 2021-02-08
version: "2.1.13"
stars: 3.0
ratings: 100
reviews: 68
size: 64M
website: https://coinfalcon.com
repository: 
issue: 
icon: com.coinfalcon.mobile.png
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: coinfalcon
providerLinkedIn: 
providerFacebook: CoinFalcon
providerReddit: CoinFalcon

redirect_from:

---


In the description we read:

> **State-of-the-Art Security**<br>
  CoinFalcon stores 98% of digital funds in an offline, secure wallet, while the
  rest is protected by high-grade security systems. We are committed to the
  highest safety standards both here on the app and our web platform.

which is clearly about a custodial offering.
