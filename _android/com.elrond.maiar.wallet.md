---
wsId: maiar
title: "Maiar: Crypto & eGold Wallet - Buy, Earn & Pay"
altTitle: 
authors:
- danny
users: 500000
appId: com.elrond.maiar.wallet
released: 2021-01-15
updated: 2021-11-08
version: "1.3.77"
stars: 4.7
ratings: 4458
reviews: 1900
size: 68M
website: https://maiar.com/
repository: 
issue: 
icon: com.elrond.maiar.wallet.png
bugbounty: 
verdict: nosource
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: getMaiar
providerLinkedIn: getmaiar
providerFacebook: getMaiar
providerReddit: 

redirect_from:

---


> Maiar is a digital wallet and global payments app that is reimagining the way we interact with money, allowing you to exchange and securely store crypto on your mobile phone.

> Create a wallet in seconds. No username, no password, no recovery phrase to backup. Just use your phone number.


It's rather unclear if this means there's *no recovery phrase provided at all* or if it's simply optional.

> **Control your crypto. Be your own bank.** At Maiar we help you protect your crypto with the highest level of security via cryptography and privacy features.

> We're approaching security through a process called progressive security. When having nothing to lose, you start light, and gradually receive security suggestions proportional to the assets you store. The more you use the app, the more sophisticated security features are triggered, to help you stay safe and secure.

This seemed to be a relevant piece of information regarding security.

Maiar.com has an FAQ on the homepage.

> [**Is Maiar a custodial or non-custodial wallet?**](https://maiar.com/)

> The Maiar wallet is completely decentralised and non-custodial. We don't store or have access to the user's funds at any time. The user owns the private key and has full control over their funds.

> **How secure is Maiar?**

> Maiar protects your assets and data using cutting edge encryption and verification techniques to ensure they're securely stored. In the event where your phone is lost or stolen, your private key (recovery phrase) can be safely used to recover your funds.

In this case, Maiar is **non-custodial.** 

As there's no accessible source code, this is **not verifiable.**
