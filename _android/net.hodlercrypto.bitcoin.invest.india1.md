---
wsId: 
title: "Bitcoin Invest India"
altTitle: 
authors:

users: 1
appId: net.hodlercrypto.bitcoin.invest.india1
released: 2021-06-18
updated: 2021-06-18
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 3.3M
website: 
repository: 
issue: 
icon: net.hodlercrypto.bitcoin.invest.india1.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- Emanuel thinks this is probably a scam. See https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/314 -->
**Update 2021-09-22**: This app is not on the Play Store anymore.
