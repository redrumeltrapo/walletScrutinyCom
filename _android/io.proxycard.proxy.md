---
wsId: 
title: "Proxy | Secure Cryptocurrency Storage & Chat"
altTitle: 
authors:

users: 1000
appId: io.proxycard.proxy
released: 2018-01-31
updated: 2018-05-03
version: "1.0.89"
stars: 4.6
ratings: 69
reviews: 38
size: 41M
website: 
repository: 
issue: 
icon: io.proxycard.proxy.png
bugbounty: 
verdict: obsolete
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


