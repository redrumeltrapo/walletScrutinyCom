---
wsId: crypterApp
title: "Crypto App - Widgets, Alerts, News, Bitcoin Prices"
altTitle: 
authors:
- danny
users: 1000000
appId: com.crypter.cryptocyrrency
released: 2017-09-09
updated: 2021-11-04
version: "2.6.8"
stars: 4.7
ratings: 73199
reviews: 20732
size: 16M
website: https://thecrypto.app/
repository: 
issue: 
icon: com.crypter.cryptocyrrency.png
bugbounty: 
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive:


providerTwitter: TrustSwap
providerLinkedIn: TrustSwap
providerFacebook: TrustSwap
providerReddit: 

redirect_from:

---


From their official website: 

> The Crypto App Team would like to announce that The Crypto App Wallet will be discontinued on August 20th, 2021. All other services within The Crypto App remain operational.

The rest of this app's features, which include:

> Live crypto price alerts, price tracking, crypto coin news updates, live crypto conversion, crypto portfolio tracker

will still remain after the wallet's discontinuation. This app will still function, but **not as a wallet**
