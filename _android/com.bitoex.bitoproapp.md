---
wsId: bitopro
title: "BitoPro Cryptocurrency Exchange-BTC,ETH,TWD"
altTitle: 
authors:
- danny
users: 50000
appId: com.bitoex.bitoproapp
released: 2018-07-30
updated: 2021-02-02
version: "4.0.0"
stars: 2.3
ratings: 587
reviews: 308
size: 32M
website: https://www.bitopro.com/
repository: 
issue: 
icon: com.bitoex.bitoproapp.png
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: BitoEX_Official
providerLinkedIn: 
providerFacebook: bitopro.bito
providerReddit: 

redirect_from:

---


Most centralized cryptocurrency exchanges do not provide the private keys to the wallets on the app or web platform. 

This is confirmed in this bitopro zendesk article, ["Is there a private key?"](https://bitopro.zendesk.com/hc/zh-tw/articles/360001215612-Is-there-a-private-key-)

> BitoPro is a website exchange platform that does not provide private keys to users.

It also has [identity verification](https://bitopro.zendesk.com/hc/zh-tw/articles/360015033812-How-to-purchase-Is-KYC-required-)

>To prevent fraud, tokens can only be purchased through the BitoPro Exchange.<br><br>
Users who would like to purchase need to register BitoPro and complete Level 1 verification.

Verdict is **custodial** and thus **not verifiable**.