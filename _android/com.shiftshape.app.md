---
wsId: 
title: "Portis - Multi Blockchain Wallet"
altTitle: 
authors:
- leo
users: 1000
appId: com.shiftshape.app
released: 2021-08-04
updated: 2021-08-04
version: "1.0.0"
stars: 4.0
ratings: 7
reviews: 3
size: 5.6M
website: 
repository: 
issue: 
icon: com.shiftshape.app.png
bugbounty: 
verdict: defunct
date: 2021-08-31
signer: 
reviewArchive:
- date: 2021-08-21
  version: "1.0.0"
  appHash: 
  gitRevision: bc6b1b52f31f708150d15a4d62c2a054df9c78f7
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-23**: This app is no more on Play Store. Some time ago
we marked as {% include verdictBadge.html verdict="defunct" type='short' %} a
very similar app: {% include walletLink.html wallet='android/com.shiftshape.wallet' verdict='true' %}.

