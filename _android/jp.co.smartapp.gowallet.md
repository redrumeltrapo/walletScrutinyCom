---
wsId: gowallet
title: "GO ! WALLET -  Ethereum Crypto Wallet & DApp"
altTitle: 
authors:
- danny
users: 10000
appId: jp.co.smartapp.gowallet
released: 2019-06-16
updated: 2020-10-07
version: "1.2.5"
stars: 4.3
ratings: 796
reviews: 431
size: 162M
website: https://www.go-wallet.app/
repository: 
issue: 
icon: jp.co.smartapp.gowallet.png
bugbounty: 
verdict: stale
date: 2021-10-03
signer: 
reviewArchive:
- date: 2021-09-23
  version: "1.2.5"
  appHash: 
  gitRevision: 3131b4551e61e557d8683fe5221303935a160c7f
  verdict: nobtc

providerTwitter: gowallet_app_
providerLinkedIn: 
providerFacebook: gowalletappli
providerReddit: 

redirect_from:

---


This is currently an Ethereum and DApp focused app. 

They, however, indicate that they may support Bitcoin (BTC) in the future according to their Google Play description:

> ※ We can exchange only for ETH (ethereum) now, but will be able to exchange for Bitcoin (BTC) or NFT (erc721) soon in the near future.

Our verdict for this app is that it is **not a bitcoin wallet**.