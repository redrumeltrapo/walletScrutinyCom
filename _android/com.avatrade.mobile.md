---
wsId: 
title: "Avatrade: Forex & CFD Trading"
altTitle: 
authors:
- danny
users: 1000000
appId: com.avatrade.mobile
released: 2017-05-23
updated: 2021-11-03
version: "95.7.0"
stars: 4.7
ratings: 7019
reviews: 5034
size: 19M
website: 
repository: 
issue: 
icon: com.avatrade.mobile.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: AvaTrade
providerLinkedIn: AvaTrade
providerFacebook: AvaTrade
providerReddit: 

redirect_from:

---


>AvaTradeGO enables users to trade, monitor their accounts, engage in social trading and follow top traders from all over the globe.

It's a trading platform and most likely custodial.

From [their website:](https://www.avatrade.com/forex/cryptocurrencies/bitcoin)

> As a CFD brokerage firm, we do not provide crypto wallets to store your Bitcoin. We simply provide access to intuitive platforms for you to trade real-time price changes of Bitcoin.

It clearly is not a wallet. Sounds like a no send/receive. 
