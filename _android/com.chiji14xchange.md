---
wsId: RocketByChiji
title: "Rocket by Chiji14xchange - buy and sell crypto"
altTitle: 
authors:
- danny
users: 10000
appId: com.chiji14xchange
released: 2019-04-22
updated: 2021-05-22
version: "3.0.5"
stars: 3.9
ratings: 1050
reviews: 923
size: 30M
website: https://chiji14xchange.com/
repository: 
issue: 
icon: com.chiji14xchange.png
bugbounty: 
verdict: custodial
date: 2021-09-23
signer: 
reviewArchive:


providerTwitter: myrocketapp
providerLinkedIn: 
providerFacebook: myrocketapp
providerReddit: 

redirect_from:

---


There is not that much information in the Play Store description:

> Welcome to Chiji14xchange App, this App enables you to transact and exchange bitcoins and gift cards with adequate security features that guarantees safety for clients at all times.

Searching the website, [we find that KYC verification is in use.](https://intercom.help/chiji14xchange/en/articles/4885360-what-is-kyc)

> In order to meet regulatory standards and to aid in crime prevention and detection, Chiji14Xchange has taken steps to ensure proper customer identification through its Customer Due Diligence and Know Your Customer (KYC) procedure.

It is unknown where the keys are stored.

We find that this exchange uses Peer to Peer trading.

> P2P trading system allows you to connect with any interested buyer/seller of crypto asset. Which means you can trade Bitcoin/USDT with other traders interested in buying or selling crypto assets.

Here is their word on Rocket's involvement:

> Rocket P2P platform serves as the gateway for the trade by providing a platform for buyers and sellers to broadcast their offers, **and at the same time, serves as escrow services of online digital asset to ensure the safety and timely delivery of digital asset during trade execution.** 

Customers can add their bank account and use fiat to buy BTC from the listings of other users. The platform can also be used to pay bills.

From the [terms & conditions](https://chiji14xchange.com/terms):

> Chiji14xchange securely stores all Digital Currency private keys in our control in a combination of online and offline storage. 

This confirms that the provider is in control of the keys, making this app **custodial.** Thus, it is **not verifiable.**