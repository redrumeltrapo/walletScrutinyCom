---
wsId: jubiter
title: "JuBiter Wallet 2.0 - Secure Hardware Crypto Wallet"
altTitle: 
authors:
- leo
users: 100
appId: com.jubiter.app
released: 2020-11-19
updated: 2021-09-02
version: "2.5.4"
stars: 2.8
ratings: 5
reviews: 3
size: 40M
website: 
repository: 
issue: 
icon: com.jubiter.app.png
bugbounty: 
verdict: defunct
date: 2021-09-16
signer: 
reviewArchive:
- date: 2021-09-08
  version: "2.5.4"
  appHash: 
  gitRevision: cfaa2364ba00347a969aeadef14b8ee616166450
  verdict: nosource
  

providerTwitter: JuBiterWallet
providerLinkedIn: jubiter-wallet
providerFacebook: JuBiterWallet
providerReddit: 

redirect_from:

---


**Update 2021-09-06**: This app is not on Play Store anymore.

{{ page.title }} is the companion app to
{% include walletLink.html wallet='hardware/jubiterblade' verdict='true' %} but
also a "software wallet":

> JuBiter wallet 2.0 is not only the official companion to JuBiter Blade - the
  cryptocurrency hardware wallet produced by JuBiter, but also a feature-rich
  cryptocurrency software wallet.

This means you can use it as a wallet without the hardware wallet.

> Security:
> 
> - Local private key generation and storage with encryption of user’s PIN.

This means they claim it's self-custodial but absent public source code it is
**not verifiable**.
