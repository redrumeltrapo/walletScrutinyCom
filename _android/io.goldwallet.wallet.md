---
wsId: 
title: "GoldWallet - Bitcoin Vault Wallet"
altTitle: 
authors:

users: 50000
appId: io.goldwallet.wallet
released: 2020-02-18
updated: 2021-08-24
version: "Varies with device"
stars: 3.7
ratings: 489
reviews: 279
size: Varies with device
website: https://bitcoinvault.global
repository: 
issue: 
icon: io.goldwallet.wallet.png
bugbounty: 
verdict: nobtc
date: 2020-12-14
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /io.goldwallet.wallet/
---


This app appears to not be a vault for Bitcoin but something for Bitcoin Vault.
