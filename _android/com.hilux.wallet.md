---
wsId: 
title: "Hilux Multiwallet"
altTitle: 
authors:

users: 500
appId: com.hilux.wallet
released: 2019-03-31
updated: 2019-12-04
version: "v2.70"
stars: 4.8
ratings: 54
reviews: 48
size: 5.7M
website: 
repository: 
issue: 
icon: com.hilux.wallet.png
bugbounty: 
verdict: stale
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


