---
wsId: 
title: "Bitcoin POS"
altTitle: 
authors:
- kiwilamb
users: 5000
appId: com.coingate.pos
released: 2015-02-19
updated: 2016-08-18
version: "2.0.0"
stars: 3.7
ratings: 24
reviews: 9
size: 8.1M
website: https://coingate.com/
repository: 
issue: 
icon: com.coingate.pos.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-15
  version: "2.0.0"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nowallet

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is a POS solution aimed at Merchants receiving crypo currencies, the POS mobile app is listed on [their website](https://coingate.com/pos), however no mention of any source repository for it.
This mobile solution looks to work via an Api and can not send crypto currencies, hence it is not a wallet.
