---
wsId: 
title: "Rise Wallet"
altTitle: 
authors:

users: 1000
appId: com.risewallet.riseapp
released: 2019-03-08
updated: 2021-11-05
version: "1.5.0"
stars: 4.4
ratings: 38
reviews: 23
size: 7.3M
website: 
repository: 
issue: 
icon: com.risewallet.riseapp.png
bugbounty: 
verdict: wip
date: 2021-08-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


