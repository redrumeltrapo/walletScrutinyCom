---
wsId: 
title: "Crypto Market Game"
altTitle: 
authors:
- danny
users: 10000
appId: ru.borik.cryptomarket.android
released: 2017-12-18
updated: 2021-04-01
version: "1.62"
stars: 4.1
ratings: 127
reviews: 57
size: 7.5M
website: 
repository: 
issue: 
icon: ru.borik.cryptomarket.android.png
bugbounty: 
verdict: nowallet
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

This app is a humorous cryptocurrency trading game. It does not have a website listed on its Google Play app page. 

> The simplified mechanism of buying and selling game crypto currency will allow you to understand how to properly trade on the exchange. In which currencies to invest money and what to look for. Choose Bitcoin, Ethereum, Waves, Bitshares or any other currency. Assess the risks of each of them, and start playing.

## The App

We downloaded the app to see if we can put "real" cryptocurrency on it. Sadly no. 

## Verdict

This app **does not have a bitcoin wallet.**

