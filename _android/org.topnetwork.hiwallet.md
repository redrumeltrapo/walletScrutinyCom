---
wsId: 
title: "Bitcoin Wallet. A Secure Crypto Wallet - HiWallet"
altTitle: 
authors:

users: 5000
appId: org.topnetwork.hiwallet
released: 2020-04-08
updated: 2021-08-13
version: "V2.3.2"
stars: 3.6
ratings: 48
reviews: 28
size: 34M
website: 
repository: 
issue: 
icon: org.topnetwork.hiwallet.jpg
bugbounty: 
verdict: defunct
date: 2021-11-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-30**: This app is no more.

