---
wsId: lobstrco
title: "LOBSTR Stellar Lumens Wallet. Buy XLM Trade Crypto"
altTitle: 
authors:
- danny
users: 100000
appId: com.lobstr.client
released: 2015-04-27
updated: 2021-11-01
version: "7.6.1"
stars: 4.7
ratings: 7877
reviews: 3642
size: 30M
website: https://lobstr.co
repository: 
issue: 
icon: com.lobstr.client.jpg
bugbounty: 
verdict: nobtc
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: Lobstrco
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


As the description states, lobstr is a stellar lumens wallet. 

However, looking deeper you'll find that you can purchase other assets including bitcoin. You can also add that to the "Assets". 

You need to deposit 5 XLM in order to add the BTC asset.

However, lobstr presumably only holds btc tokens and not actual bitcoin. Hence, the **nobtc** verdict.

Zendesk support page "[Buying Crypto with LOBSTR wallet](https://lobstr.zendesk.com/hc/en-us/articles/360014741460-Buying-crypto-with-LOBSTR-wallet)"

