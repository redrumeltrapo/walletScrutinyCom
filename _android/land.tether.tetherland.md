---
wsId: 
title: "تترلند | خرید تتر و فروش تتر | Tether Land"
altTitle: 
authors:
- danny
users: 50000
appId: land.tether.tetherland
released: 2019-12-09
updated: 2021-10-19
version: "4.3"
stars: 4.6
ratings: 1094
reviews: 391
size: 29M
website: https://tetherland.net
repository: 
issue: 
icon: land.tether.tetherland.png
bugbounty: 
verdict: nobtc
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: tetherland
providerLinkedIn: tetherland
providerFacebook: tetherland
providerReddit: 

redirect_from:

---


We downloaded the app, but have difficulty with the registration because it's in Persian.

Translated from its Google Play description:

> Tether Land Exchange is one of the Biggest, fastest and most popular cryptocurrency exchange platforms in Iran.The platform focuses on security, robustness, and execution speed - attracting enthusiasts and professional traders alike.Tether Land exchange provides access to USDT/RIAL currency pairs,

Described in its [Terms and Conditions](https://tetherland.net/terms) page:

> Tetrelland is subject to the laws of the Islamic Republic of Iran and is a platform for the exchange of digital assets of Tetra with Rials, and no exchange of currency, whether buying or selling dollars or other paper currencies, takes place in this platfor

Due to the language barrier, we decided to contact support via chat.

> Daniel: آیا امکان داشتن کیف پول بیت کوین در این پلتفرم وجود دارد؟<br>
Is it possible to have a bitcoin wallet on this platform?

> Wida Kushki: خیر فقط خرید و فروش تتر پشتیبانی میشه بزرگوار<br>
"No, only the sale and purchase of Tetr is supported,"

This is **not a Bitcoin wallet**.