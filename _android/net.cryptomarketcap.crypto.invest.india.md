---
wsId: 
title: "Crypto Invest India"
altTitle: 
authors:

users: 0
appId: net.cryptomarketcap.crypto.invest.india
released: 2021-06-22
updated: 2021-06-22
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: net.cryptomarketcap.crypto.invest.india.jpg
bugbounty: 
verdict: defunct
date: 2021-08-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-13**: This app is not available anymore
