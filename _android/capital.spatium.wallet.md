---
wsId: 
title: "Spatium Keyless Bitcoin Wallet"
altTitle: 
authors:

users: 1000
appId: capital.spatium.wallet
released: 2018-08-31
updated: 2021-10-21
version: "3.0.44"
stars: 3.9
ratings: 29
reviews: 13
size: 55M
website: 
repository: 
issue: 
icon: capital.spatium.wallet.png
bugbounty: 
verdict: wip
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


