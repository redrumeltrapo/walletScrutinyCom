---
wsId: 
title: "Bitrah"
altTitle: 
authors:

users: 100
appId: com.bitrah.pos
released: 2020-07-21
updated: 2020-11-02
version: "2.0.0"
stars: 4.4
ratings: 8
reviews: 4
size: Varies with device
website: 
repository: 
issue: 
icon: com.bitrah.pos.png
bugbounty: 
verdict: stale
date: 2021-10-29
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


