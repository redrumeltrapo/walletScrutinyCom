---
wsId: 
title: "QuickX Touch"
altTitle: 
authors:
- leo
users: 500
appId: kr.co.keypair.quickxtouch
released: 2018-10-09
updated: 2019-07-17
version: "1.0.0.57"
stars: 3.5
ratings: 15
reviews: 10
size: 10M
website: https://www.quickx.io
repository: 
issue: 
icon: kr.co.keypair.quickxtouch.jpg
bugbounty: 
verdict: obsolete
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: quickxprotocol
providerLinkedIn: quickx
providerFacebook: quickxprotocol
providerReddit: QuickX

redirect_from:

---


<!-- nosource -->
As far as we can see, this is the same as
{% include walletLink.html wallet='android/kr.co.keypair.keywalletTouch' %} and thus is **not verifiable**.
