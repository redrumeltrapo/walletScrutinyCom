---
wsId: 
title: "Invest In Bitcoin"
altTitle: 
authors:

users: 1
appId: com.cryptolabllc.invest.in.bitcoin
released: 2021-06-18
updated: 2021-06-18
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 3.3M
website: 
repository: 
issue: 
icon: com.cryptolabllc.invest.in.bitcoin.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
