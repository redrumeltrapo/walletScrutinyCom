---
wsId: ATokenWallet
title: "ATokenWallet"
altTitle: 
authors:
- kiwilamb
users: 1000000
appId: wallet.gem.com.atoken
released: 2020-03-01
updated: 2021-09-30
version: "4.1.9"
stars: 4.0
ratings: 4008
reviews: 2458
size: 55M
website: https://www.atoken.com
repository: 
issue: 
icon: wallet.gem.com.atoken.png
bugbounty: 
verdict: nosource
date: 2021-04-16
signer: 
reviewArchive:


providerTwitter: ATokenOfficial
providerLinkedIn: 
providerFacebook: ATokenOfficial
providerReddit: 

redirect_from:

---


Found on their support website...

> The AToken Wallet server does not save any private keys, mnemonics, and
  passwords for users, so mnemonics or lost private keys cannot be retrieved
  from the AToken wallet.<br>
  Please make sure that all users make backups and keep them properly. Do not
  share them with anyone.

The claim on their website is that the wallet is non-custodial, but without source code, this is **not verifiable**.
