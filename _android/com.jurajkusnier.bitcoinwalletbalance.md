---
wsId: 
title: "Wealth Check - Bitcoin Wallet Balance and History"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.jurajkusnier.bitcoinwalletbalance
released: 2017-12-29
updated: 2020-06-01
version: "2.4"
stars: 4.0
ratings: 234
reviews: 125
size: 5.0M
website: https://jurajkusnier.com/
repository: https://github.com/jurajkusnier/bitcoin-balance-check/
issue: 
icon: com.jurajkusnier.bitcoinwalletbalance.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-05-02
  version: "2.4"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nowallet

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is only for retrival of bitcoin address balance and transaction information.

Our verdict: This is **not a wallet**.

