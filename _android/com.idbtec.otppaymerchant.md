---
wsId: 
title: "OTPPAY - Merchant Payments"
altTitle: 
authors:

users: 1000
appId: com.idbtec.otppaymerchant
released: 2018-12-05
updated: 2019-06-18
version: "1.4"
stars: 4.4
ratings: 13
reviews: 8
size: 3.9M
website: 
repository: 
issue: 
icon: com.idbtec.otppaymerchant.png
bugbounty: 
verdict: obsolete
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


