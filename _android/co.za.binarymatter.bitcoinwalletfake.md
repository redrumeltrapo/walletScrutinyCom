---
wsId: 
title: "Fake Bitcoin Wallet"
altTitle: 
authors:
- danny
users: 10000
appId: co.za.binarymatter.bitcoinwalletfake
released: 2021-03-21
updated: 2021-04-16
version: "1.3"
stars: 3.4
ratings: 44
reviews: 28
size: 5.6M
website: 
repository: 
issue: 
icon: co.za.binarymatter.bitcoinwalletfake.png
bugbounty: 
verdict: nowallet
date: 2021-04-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


## App Description

> Prank Bitcoin Wallet to fool your friends and family.
>
> Receive as many bitcoins as you want.
>
> Send them out at any time.
>
> Save a history of all transactions.
>
> Total value of wallet displayed on top.

## Verdict

As the name implies, this app does **not have a bitcoin wallet.**
