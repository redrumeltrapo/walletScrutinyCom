---
wsId: 
title: "MEXC Exchange - Buy & Sell Bitcoin, ETH and DeFi"
altTitle: 
authors:
- danny
users: 1000000
appId: com.mexcpro.client
released: 2020-07-23
updated: 2021-11-05
version: "3.1.8"
stars: 4.6
ratings: 7110
reviews: 3913
size: 55M
website: https://www.mexc.com/
repository: 
issue: 
icon: com.mexcpro.client.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: MEXC_Global
providerLinkedIn: mexcglobal
providerFacebook: mexcglobal
providerReddit: MXCexchange

redirect_from:

---


> As a world’s leading digital asset trading platform, MEXC Exchange is committed to providing users safer, smarter and more convenient digital-asset trading and management services.

So it's an exchange. There's no word on self-custody or wallets though, but these exchanges usually have 'wallets' where users can trade BTC.

Due to this being a trading platform, we can assume the wallets are **custodial** therefore **not verifiable.**

Their website wasn't linked, but clicking the privacy policy will redirect you to the MEXC homepage.
