---
wsId: 
title: "Bgogo - Digital Asset Exchange"
altTitle: 
authors:

users: 1000
appId: com.bgg.bgg_exchange
released: 2018-10-29
updated: 2020-09-09
version: "1.6.7"
stars: 3.9
ratings: 35
reviews: 23
size: 15M
website: 
repository: 
issue: 
icon: com.bgg.bgg_exchange.png
bugbounty: 
verdict: stale
date: 2021-09-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


