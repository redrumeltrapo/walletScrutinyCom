---
wsId: 
title: "Crypto Invest"
altTitle: 
authors:

users: 0
appId: com.magnumwallet.crypto.invest
released: 2021-07-18
updated: 2021-07-18
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.3M
website: 
repository: 
issue: 
icon: com.magnumwallet.crypto.invest.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
