---
wsId: 
title: "PlusBit Wallet"
altTitle: 
authors:

users: 500
appId: com.plusbit
released: 2020-04-07
updated: 2020-07-12
version: "1.0"
stars: 4.9
ratings: 96
reviews: 90
size: 24M
website: 
repository: 
issue: 
icon: com.plusbit.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


