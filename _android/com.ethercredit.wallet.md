---
wsId: 
title: "ERC Wallet"
altTitle: 
authors:

users: 100
appId: com.ethercredit.wallet
released: 2020-09-23
updated: 2020-09-23
version: "1.0"
stars: 3.8
ratings: 5
reviews: 3
size: 47M
website: 
repository: 
issue: 
icon: com.ethercredit.wallet.png
bugbounty: 
verdict: stale
date: 2021-09-19
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


