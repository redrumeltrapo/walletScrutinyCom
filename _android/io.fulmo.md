---
wsId: 
title: "Fulmo: Bitcoin & Altcoins Wallet"
altTitle: 
authors:

users: 50
appId: io.fulmo
released: 2019-01-09
updated: 2019-06-11
version: "1.0.0"
stars: 4.3
ratings: 11
reviews: 7
size: 9.0M
website: 
repository: 
issue: 
icon: io.fulmo.png
bugbounty: 
verdict: defunct
date: 2021-11-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-26**: This app is no more.

