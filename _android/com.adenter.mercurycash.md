---
wsId: mercurycash
title: "Mercury Cash"
altTitle: 
authors:
- leo
users: 50000
appId: com.adenter.mercurycash
released: 2017-07-28
updated: 2021-09-22
version: "4.3.3"
stars: 3.7
ratings: 223
reviews: 150
size: 80M
website: http://mercury.cash
repository: 
issue: 
icon: com.adenter.mercurycash.png
bugbounty: 
verdict: custodial
date: 2020-08-06
signer: 
reviewArchive:


providerTwitter: mercurycash
providerLinkedIn: 
providerFacebook: mercurycash
providerReddit: 

redirect_from:
  - /com.adenter.mercurycash/
---


This app makes no claims about self-custody so we have to assume it is a
custodial product and thus **not verifiable**.
