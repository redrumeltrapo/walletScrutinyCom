---
wsId: 
title: "Simple Crypto Widget"
altTitle: 
authors:

users: 100000
appId: com.brentpanther.bitcoinwidget
released: 2013-03-11
updated: 2021-11-06
version: "8.1.5"
stars: 4.0
ratings: 1989
reviews: 812
size: 4.4M
website: 
repository: 
issue: 
icon: com.brentpanther.bitcoinwidget.png
bugbounty: 
verdict: nowallet
date: 2021-03-05
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> Note: This is only a widget. You must add the widget to your launcher, it will
  not appear in your apps list.
