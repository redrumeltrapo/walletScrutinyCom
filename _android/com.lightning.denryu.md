---
wsId: 
title: "Denryu Wallet"
altTitle: 
authors:
- kiwilamb
users: 500
appId: com.lightning.denryu
released: 2018-12-14
updated: 2018-12-26
version: "0.1"
stars: 2.5
ratings: 17
reviews: 12
size: 6.2M
website: https://denryu.hashhub.tokyo/
repository: 
issue: 
icon: com.lightning.denryu.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is a lightning wallet.

