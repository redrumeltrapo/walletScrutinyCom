---
wsId: ExanteTrading
title: "EXANTE Trading"
altTitle: 
authors:
- danny
users: 10000
appId: eu.exante.client
released: 2014-04-25
updated: 2021-11-05
version: "4.15.3"
stars: 4.0
ratings: 207
reviews: 117
size: 22M
website: https://exante.eu/
repository: 
issue: 
icon: eu.exante.client.png
bugbounty: 
verdict: nowallet
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: EXANTE_EU
providerLinkedIn: exante-ltd
providerFacebook: exante.global
providerReddit: 

redirect_from:

---


## App Description

Exante offers:

> Trading stocks, options, futures, forex, bonds, cryptocurrency and hedge funds from a single account
> As a fully licensed and regulated European broker, EXANTE offers online trading on over 50+ markets including NYSE, NASDAQ, CBOE, MOEX, Euronext Group. Dynamic trading tools, extensive IT infrastructure with over 750 servers make EXANTE a powerful industry leader.

It claims to be [the only broker](https://exante.eu/lp/only-one/) that offers CBOE, CME, Bitcoin and Altcoins from the same trading account.

## Deposit

[Deposits](https://exante.eu/markets/) can only be made using bank transfer with the following currencies: GBP, USD, JPY, EUR, CHF, CZK, RUB, SEK, CAD, HKD, MXN, PLN, NOK, SGD or AUD. A minimum of €10,000 is required and €50,000 for corporate clients. 

Withdrawals are processed within 3-5 banking days.

## Verdict

This service **does not have a bitcoin or any cryptocurrency wallet**.

