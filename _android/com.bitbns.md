---
wsId: 
title: "Bitbns: Bitcoin, Crypto Trading Exchange India"
altTitle: 
authors:
- danny
users: 500000
appId: com.bitbns
released: 2019-09-21
updated: 2021-10-20
version: "4.5.0"
stars: 3.5
ratings: 12509
reviews: 6343
size: 38M
website: https://bitbns.com
repository: 
issue: 
icon: com.bitbns.png
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: bitbns
providerLinkedIn: bitbnsinc
providerFacebook: bitbns
providerReddit: Bitbns

redirect_from:

---


From their linkedin page:

> Bitbns is the fastest and simplest way to trade cryptocurrencies with one of the best three-way Peer-to-Peer system for purchasing cryptocurrency with FIAT in India 

This "Peer to Peer" [cryptocurrency exchange requires KYC](https://bitbns.com/trade/#/verify/international)

Freskdesk Support Article "How Safe is BitBNS"

> It is extremely safe to trade cryptocurrencies on Bitbns. Bitbns is built with the latest technology architecture and grade-A security feature, which make sure that all your data, cryptocurrencies, INR volumes, and wallets are secure. Rest assured, they are not accessible to unauthorized entities.

Support Tutorial on how to [withdraw Cryptocurrencies from BitBNS](https://bitbns.freshdesk.com/support/solutions/articles/35000045142-how-to-withdraw-cryptocurrencies-from-bitbns-)

Support Tutorial on how to [deposit Cryptocurrencies to BitBNS](https://bitbns.freshdesk.com/support/solutions/articles/35000045132-how-to-deposit-cryptocurrencies-on-bitbns-)
