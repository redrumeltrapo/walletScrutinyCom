---
wsId: 
title: "Exodus Bitcoin Manage, and Exchange cryptocurrency"
altTitle: "(Fake) Exodus Bitcoin Manage, and Exchange cryptocurrency"
authors:
- leo
users: 10
appId: crypto.walls.exoduks.comy
released: 2021-10-03
updated: 2021-10-04
version: "19.28"
stars: 0.0
ratings: 
reviews: 
size: 11M
website: 
repository: 
issue: 
icon: crypto.walls.exoduks.comy.png
bugbounty: 
verdict: defunct
date: 2021-10-30
signer: 
reviewArchive:
- date: 2021-10-18
  version: "19.28"
  appHash: 
  gitRevision: 70bdd6a4573c03410f6563e850ff522f756f5fdc
  verdict: fake


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-23**: This app is no more.

Apparently this is a BTC wallet:

> Exodus allows you to secure, manage, and exchange your favorite
  cryptocurrencies like Bitcoin, Ethereum, Ripple, and more from a beautiful,
  easy to use wallet that puts you in control of your wealth.

There are several other products using the name "Exodus", so this might be a
fake of
{% include walletLink.html wallet='android/exodusmovement.exodus' verdict='true' %}?

No website is provided and the developer uses the email
`contact@exuduxcrypto.com`, which is a weird derivation of the "Exodus" brand.

For the lack of further information we side with caution and give this a
**fake** verdict.
