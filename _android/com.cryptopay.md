---
wsId: 
title: "Crypto Pay"
altTitle: 
authors:
- kiwilamb
users: 1000
appId: com.cryptopay
released: 2018-12-17
updated: 2019-02-14
version: "1.5"
stars: 4.8
ratings: 5
reviews: 1
size: 9.1M
website: https://shamlatech.com/
repository: 
issue: 
icon: com.cryptopay.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-04-15
  version: "1.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nosource

providerTwitter: shamlatech
providerLinkedIn: shamlatech
providerFacebook: shamlatechsolutions
providerReddit: 

redirect_from:

---


This wallet claims to be non-custodial but we cannot find any source code on their [official Website page](https://shamlatech.com/).

Our verdict: This app is **not verifiable**.

Tried to make contact to discover source code repository...
- Website chat - no response
- Telegram - cannot find the posted user/group.
- Whatsapp - wanted to make contact via a phone number which i declined. 
