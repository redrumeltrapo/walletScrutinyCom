---
wsId: 
title: "excoino | اکسکوینو"
altTitle: 
authors:
- danny
users: 500000
appId: com.excoino.excoino
released: 2018-09-27
updated: 2021-11-09
version: "8.1.1"
stars: 4.1
ratings: 7860
reviews: 3327
size: 16M
website: https://www.excoino.net/
repository: 
issue: 
icon: com.excoino.excoino.png
bugbounty: 
verdict: custodial
date: 2021-08-21
signer: 
reviewArchive:


providerTwitter: excoino
providerLinkedIn: excoino-com
providerFacebook: 
providerReddit: 

redirect_from:

---


The app appears to be an Iranian exchange and there's no English translation.

However, users in the comments claim that they have problems with ID verification.

Due to lack of evidence proving otherwise, we assume this is a **custodial exchange** and therefore **not verifiable.**

