---
wsId: 
title: "Conio Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 50000
appId: com.conio.wallet
released: 
updated: 2021-11-03
version: "3.4.0"
stars: 3.6
ratings: 626
reviews: 414
size: 71M
website: https://www.conio.com/en
repository: 
issue: 
icon: com.conio.wallet.png
bugbounty: 
verdict: custodial
date: 2020-04-09
signer: 
reviewArchive:


providerTwitter: conio
providerLinkedIn: conio
providerFacebook: ConioHQ
providerReddit: 

redirect_from:
  - /com.conio.wallet/
  - /posts/com.conio.wallet/
---


This app has wonderful security claims on Google Play:

> *5. STAY CAREFREE* With Conio you can recover your Bitcoins even if you forget
> all your credentials! Just create your profile and upload a selfie when you
> start.

Unfortunately this means that the provider does control the keys which makes it
a custodial app. Our verdict: **not verifiable**.
