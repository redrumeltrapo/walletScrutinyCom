---
wsId: 
title: "MBC Wallet - MicroBitcoin Wallet"
altTitle: 
authors:

users: 5000
appId: com.microbitcoin
released: 2018-10-30
updated: 2020-01-23
version: "2.0.1"
stars: 3.7
ratings: 68
reviews: 43
size: 9.9M
website: https://microbitcoin.org
repository: 
issue: 
icon: com.microbitcoin.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2019-12-28
  version: "2.0.1"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: nobtc

providerTwitter: MicroBitcoinOrg
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.microbitcoin/
  - /posts/com.microbitcoin/
---


