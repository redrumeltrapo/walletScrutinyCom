---
wsId: webull
title: "Webull: Investing & Trading"
altTitle: 
authors:
- danny
- leo
users: 10000000
appId: org.dayup.stocks
released: 
updated: 2021-11-09
version: "7.3.0.62"
stars: 4.4
ratings: 151935
reviews: 45566
size: 54M
website: https://www.webull.com/
repository: 
issue: 
icon: org.dayup.stocks.png
bugbounty: 
verdict: nosendreceive
date: 2021-08-10
signer: 
reviewArchive:


providerTwitter: WebullGlobal
providerLinkedIn: webullfinancialllc
providerFacebook: 
providerReddit: 

redirect_from:

---


The app allows you to trade BTC but it is only an exchange and not a wallet.

From their website:
> We provide our customers with access to cryptocurrency trading through Apex Crypto. Apex Crypto is not a registered broker-dealer or FINRA member and your cryptocurrency holdings are not FDIC or SIPC insured.

> You can buy and sell cryptocurrency on Webull. However, we do not support transferring crypto into or out of your Webull account at this time.

