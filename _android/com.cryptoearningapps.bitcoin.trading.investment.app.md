---
wsId: 
title: "Bitcoin Trading Investment App"
altTitle: 
authors:

users: 5
appId: com.cryptoearningapps.bitcoin.trading.investment.app
released: 2021-06-29
updated: 2021-06-29
version: "1.0"
stars: 0.0
ratings: 
reviews: 
size: 2.2M
website: 
repository: 
issue: 
icon: com.cryptoearningapps.bitcoin.trading.investment.app.png
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-09-22**: This app is not on the Play Store anymore.
