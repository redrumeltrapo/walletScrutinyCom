---
wsId: Mixin
title: "Mixin - Crypto Wallet & Private Messenger"
altTitle: 
authors:
- danny
users: 10000
appId: one.mixin.messenger
released: 2018-05-24
updated: 2021-11-09
version: "0.33.2"
stars: 4.5
ratings: 1225
reviews: 952
size: 27M
website: https://mixinmessenger.zendesk.com/
repository: https://github.com/MixinNetwork
issue: https://github.com/MixinNetwork/android-app/issues/2490
icon: one.mixin.messenger.png
bugbounty: 
verdict: wip
date: 2021-09-23
signer: 
reviewArchive:


providerTwitter: MixinMessenger
providerLinkedIn: 
providerFacebook: MixinNetwork
providerReddit: mixin

redirect_from:

---


From the Play Store description:

> Mixin Messenger is an open-source cryptocurrency wallet and signal protocol messenger, which supports almost all popular cryptocurrencies.

It supports Bitcoin, Ethereum, EOS, Monero and thousands of other cryptocurrencies.

Also relevant:

> Mixin Messenger is built on Mixin Network, it's a PoS second layer solution for other blockchains. Mixin Network is a distributed second layer ledger, so you own your crypto assets. Because of this second layer, it's normal that you can't check your BTC address balance on a Bitcoin blockchain explorer.

More information about how its wallet functions can be found on this Zendesk article ["Guides for Wallet"](https://mixinmessenger.zendesk.com/hc/en-us/sections/360002664251-Guides-for-Wallet)

We emailed the Mixin development team on September 16, 2021, to verify whether they have custody of the private keys. We await their response.

The [repository](https://github.com/MixinNetwork) is linked in the description.

It appears that this wallet is **non-custodial** and that source-code is available. 

An issue has been made on Gitlab with the corresponding [results](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/344). 

Issue has been raised in MixinNetwork's [Github Issues page](https://github.com/MixinNetwork/android-app/issues/2490). 

This app is still a **work in progress**.
