---
wsId: 
title: "Hibana Wallet"
altTitle: 
authors:
- kiwilamb
users: 50
appId: app.hibana.hibana
released: 2018-12-11
updated: 2018-12-11
version: "0.1"
stars: 0.0
ratings: 
reviews: 
size: 6.5M
website: 
repository: 
issue: 
icon: app.hibana.hibana.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is a lightning wallet.
