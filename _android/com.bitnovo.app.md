---
wsId: bitnovo
title: "Bitnovo - Crypto Wallet"
altTitle: 
authors:
- leo
users: 50000
appId: com.bitnovo.app
released: 2017-05-18
updated: 2021-10-29
version: "2.8.7"
stars: 3.0
ratings: 357
reviews: 237
size: 34M
website: http://www.bitnovo.com
repository: 
issue: 
icon: com.bitnovo.app.png
bugbounty: 
verdict: custodial
date: 2021-05-30
signer: 
reviewArchive:


providerTwitter: bitnovo
providerLinkedIn: 
providerFacebook: BitcoinBitnovo
providerReddit: 

redirect_from:
  - /com.bitnovo.app/
---


On the App Store, of 4 ratings, 3 were 1 star. With an average of 2.5 stars the
app is slightly higher rated on the Play Store. **Caution is advised!**

The provider clearly controls the funds and thus this "wallet" is **not
verifiable**.
