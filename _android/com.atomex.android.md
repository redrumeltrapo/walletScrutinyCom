---
wsId: atomex
title: "Atomex - Crypto Wallet & Atomic swap DEX"
altTitle: 
authors:
- leo
users: 5000
appId: com.atomex.android
released: 2020-12-06
updated: 2021-11-09
version: "1.12.1"
stars: 4.3
ratings: 56
reviews: 43
size: 61M
website: https://atomex.me
repository: https://github.com/atomex-me/atomex.mobile
issue: https://github.com/atomex-me/atomex.mobile/issues/24
icon: com.atomex.android.png
bugbounty: 
verdict: nonverifiable
date: 2021-04-11
signer: 
reviewArchive:


providerTwitter: atomex_official
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> Atomex — is a non-custodial crypto wallet

and it also supports Bitcoin and ...

> Atomex is an open-source project by the Baking Bad team, which is known in the
  Tezos community as one of the most active ecosystem contributors.

and indeed their linked GitHub account has a repository that looks promising:
[atomex-me/atomex.mobile](https://github.com/atomex-me/atomex.mobile).

Unfortunately at this point I have to give up as this is the first project we
review that was built in Visual Studio using C#. As there are no build
instructions I can only hope for
[help from the provider](https://github.com/atomex-me/atomex.mobile/issues/24)
and conclude for now that the app is **not verifiable**.
