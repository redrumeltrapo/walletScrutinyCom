---
wsId: 
title: "RWallet"
altTitle: 
authors:

users: 1000
appId: com.rsk.rwallet.reactnative
released: 
updated: 2020-10-29
version: "1.3.3"
stars: 3.8
ratings: 20
reviews: 15
size: 28M
website: 
repository: 
issue: 
icon: com.rsk.rwallet.reactnative.png
bugbounty: 
verdict: defunct
date: 2021-04-13
signer: 
reviewArchive:
- date: 2021-04-13
  version: 
  appHash: 
  gitRevision: 2adf93055d5b552806d8a041f41c1e8e4a7c5fd6
  verdict: fewusers


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


Apparently this app was re-launched under a different applicationId.
