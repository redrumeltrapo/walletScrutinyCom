---
wsId: 
title: "MoonX - Crypto Trading Platform"
altTitle: 
authors:

users: 500
appId: moonx.exchange.moonx
released: 2019-08-09
updated: 2020-07-16
version: "2.0"
stars: 4.9
ratings: 13
reviews: 13
size: 9.6M
website: 
repository: 
issue: 
icon: moonx.exchange.moonx.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


