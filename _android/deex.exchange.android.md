---
wsId: 
title: "Deex Exchange"
altTitle: 
authors:

users: 1000
appId: deex.exchange.android
released: 2019-11-29
updated: 2020-04-21
version: "0.3.9"
stars: 4.2
ratings: 248
reviews: 200
size: 5.7M
website: 
repository: 
issue: 
icon: deex.exchange.android.png
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


