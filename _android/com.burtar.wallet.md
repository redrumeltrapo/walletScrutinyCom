---
wsId:
title: "Burtar wallet: Buy, sell & swap bitcoin & crypto"
altTitle:
authors:
- danny
users: 1000
appId: com.burtar.wallet
released: 2021-07-29
updated: 2021-07-30
version: "1.0"
stars: 4.4
ratings: 776
reviews: 725
size: 6.1M
website: https://burtar.com/
repository:
issue:
icon: com.burtar.wallet.png
bugbounty:
verdict: custodial
date: 2021-11-10
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---


**Warning:** The reviews of this app are filled with scam accusations, with some users explaining that the app requires them to give 5* ratings first. We recommend to exercise caution with this app.

## App Description

> - Burtar is the most robust and yet easiest platform for trading crypto
> - Buy crypto in 5mins with your bank card
> - 0% trading fees
> - Withdraw fiat straight to your local bank in minutes
> - No Strict KYC Policies

### Google Play Critical Reviews

> [Nduka Miracle](https://play.google.com/store/apps/details?id=com.burtar.wallet&reviewId=gp%3AAOqpTOFNeKbFBlmdknPTz5o7lIYwX-FAyFSFk62V-thIfYy3hhmWjCuDK50bNDe3Es83Hz5y-SNv8Sq9q0sI42U)<br>
  ★☆☆☆☆ October 22, 2021 <br>
       This app is a total scam oooo. I deposited#2500 and it didn't reflect on my account and I contacted their customer service and they told me it wasn't successful. I had to transfer another#5000. I tried logging in the next day but I was told that my email doesn't exist. And I had to restart the registration. I had to contact my first bank for resolution and I was made to know that the transaction was successful with transaction details proof sent to my email.

> [Wilson Reginald](https://play.google.com/store/apps/details?id=com.burtar.wallet&reviewId=gp%3AAOqpTOGZmvjPtxcgBBrWK4dIMgmy9nlS_GwJ7A0HGnE2tZ7hznQTV2DbbCNyXlZ9kQIBKc4aUdcHmhOEwc-wCZY)<br>
  ★☆☆☆☆ August 29, 2021 <br>
       I opened and account with you guys and i couldn't get my otp and i complained on the live caht i was ask to drop my email and i did two hours later i got and email that my accout have been assesed by someone and when i try to chnage my password they said email not found and i funded the account. You guys are scam

## The Site

The site's functionalities seem to be limited. Most of the links such as the one for "Terms and Conditions" do not work.

## The App

We downloaded the app however it seems to want registrants only from the United States. We were able to access that when we put United States in our country field. There is a send Bitcoin function, but is temporarily unavailable. There was no reason why. When we clicked on Receive bitcoin, a QR code appeared with a BTC address. We were not able to locate Backup function, nor mnemonics.

## Verdict

We have reason to believe that the app is **custodial** since a lot of functions for the app can be controlled by the developer. The users also would not have access to the private key or make a backup using the seed phrases. This makes the app **not verifiable**.
