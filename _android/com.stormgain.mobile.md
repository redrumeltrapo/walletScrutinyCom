---
wsId: stormgain
title: "StormGain: Bitcoin Wallet App"
altTitle: 
authors:
- leo
users: 1000000
appId: com.stormgain.mobile
released: 2019-07-08
updated: 2021-10-26
version: "1.20.0"
stars: 4.4
ratings: 77056
reviews: 38628
size: 37M
website: https://stormgain.com
repository: 
issue: 
icon: com.stormgain.mobile.png
bugbounty: 
verdict: custodial
date: 2021-03-10
signer: 
reviewArchive:


providerTwitter: StormGain_com
providerLinkedIn: 
providerFacebook: StormGain.official
providerReddit: 

redirect_from:

---


This app's description mainly focuses on trading and interest earning, features
usually associated with custodial offerings. Also their little paragraph on
security:

> With Industry-Leading Security Protocols, Two-Factor Authentication and Cold
  Wallet Storage, StormGain is a secure environment for Crypto Trading.

sounds more like a custodial offering with the "Cold Wallet Storage" in there.
We conclude the app is **not verifiable**.
