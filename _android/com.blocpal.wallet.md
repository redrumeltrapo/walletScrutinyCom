---
wsId: 
title: "BlocPal Wallet"
altTitle: 
authors:

users: 1000
appId: com.blocpal.wallet
released: 2018-10-30
updated: 2021-01-20
version: "1.1.7"
stars: 4.0
ratings: 60
reviews: 38
size: 57M
website: 
repository: 
issue: 
icon: com.blocpal.wallet.png
bugbounty: 
verdict: wip
date: 2021-08-08
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


