---
wsId: aryainvest
title: "ARYA Invest smart, live better"
altTitle: 
authors:
- danny
users: 100000
appId: com.techx.arya
released: 2019-09-10
updated: 2021-10-25
version: "2.10.3"
stars: 4.2
ratings: 278
reviews: 115
size: 63M
website: https://arya.xy
repository: 
issue: 
icon: com.techx.arya.png
bugbounty: 
verdict: nowallet
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: TheAryaApp
providerLinkedIn: thearyaapp
providerFacebook: WeloveArya
providerReddit: 

redirect_from:

---


From its Google Play description:

> A unique social App, empowering everyone, to invest smartly.

We downloaded the app and found it to be an investment social network, where users follow experts who give them updates about what investments to make. 

Nowhere in the site did we find any mention of the ability to deposit or withdraw bitcoin or any other cryptocurrency.

This is **not a wallet**
