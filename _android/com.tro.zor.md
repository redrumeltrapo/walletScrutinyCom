---
wsId: 
title: "Mobile Wallet Manager"
altTitle: 
authors:

users: 0
appId: com.tro.zor
released: 2021-09-23
updated: 2021-09-23
version: "1.0.4"
stars: 0.0
ratings: 
reviews: 
size: 859k
website: 
repository: 
issue: 
icon: com.tro.zor.png
bugbounty: 
verdict: defunct
date: 2021-10-15
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-10-07**: This app is not on Play Store anymore.
