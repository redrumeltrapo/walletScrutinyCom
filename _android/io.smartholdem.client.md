---
wsId: 
title: "Smart Secure Wallet - SmartHoldem STH Coin"
altTitle: 
authors:

users: 100
appId: io.smartholdem.client
released: 2020-03-01
updated: 2020-10-15
version: "1.0.15"
stars: 4.7
ratings: 15
reviews: 12
size: 7.3M
website: 
repository: 
issue: 
icon: io.smartholdem.client.png
bugbounty: 
verdict: stale
date: 2021-10-11
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


