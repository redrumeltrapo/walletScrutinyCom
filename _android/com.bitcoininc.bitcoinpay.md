---
wsId: 
title: "Bitcoin Pay"
altTitle: 
authors:

users: 10000
appId: com.bitcoininc.bitcoinpay
released: 2016-05-13
updated: 2017-07-06
version: "1.0.1"
stars: 3.5
ratings: 113
reviews: 54
size: 14M
website: 
repository: 
issue: 
icon: com.bitcoininc.bitcoinpay.png
bugbounty: 
verdict: obsolete
date: 2021-08-17
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.bitcoininc.bitcoinpay/
  - /posts/com.bitcoininc.bitcoinpay/
---


This app refers to [Bitcoin.org](http://bitcoin.org/) as it's website, which is
highly suspicious for anybody knowing how the bitcoin.org team "ticks".

The last update of this wallet was 2017.

The latest user comments all report lost funds.

The provider answered to the highest rated such one-star comment:

> login to same account using the AIRBITZ wallet, Bitcoin Pay is the receiving
  point of sale app, it doesn't allow sending of funds, only accepting, this
  allows any waiter, point of sale or employee to accept BTC for the business
  without having access to those funds

implying a common provider with [AIRBITZ](/airbitz/) in a way asking to enter
Airbitz credentials in this wallet???

At the risk of being paranoid this smells too much like a scam with zero
accountability to not call it **defunct**.
