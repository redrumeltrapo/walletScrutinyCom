---
wsId: 
title: "Coin-DC: Bitcoin, cryptocurrency investment app"
altTitle: 
authors:

users: 100
appId: latoken.kucoin.trustapp.zebpay.coinmarketcap.coindcx
released: 2021-06-01
updated: 2021-06-02
version: "1.1"
stars: 0.0
ratings: 
reviews: 
size: 13M
website: 
repository: 
issue: 
icon: latoken.kucoin.trustapp.zebpay.coinmarketcap.coindcx.png
bugbounty: 
verdict: defunct
date: 2021-09-02
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-25**: This app is no more.
