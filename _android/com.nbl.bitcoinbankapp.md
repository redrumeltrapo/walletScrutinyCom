---
wsId: 
title: "Bitcoin Bank App: Profit from Crypto"
altTitle: 
authors:

users: 1000
appId: com.nbl.bitcoinbankapp
released: 
updated: 2021-02-23
version: "1.0.1"
stars: 3.6
ratings: 19
reviews: 14
size: 17M
website: 
repository: 
issue: 
icon: com.nbl.bitcoinbankapp.png
bugbounty: 
verdict: defunct
date: 2021-08-20
signer: 
reviewArchive:
- date: 2021-08-01
  version: "1.0.1"
  appHash: 
  gitRevision: 94a906f24c1e25b517f8270944b2565285a1074c
  verdict: wip

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-08-12**: This app is not on the Play Store anymore
