---
title: "Littleshell"
appId: littleshell
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: https://www.amazon.ca/Littleshell-Cryptocurrency-Hardware-Wallet-ERC20-tokens/dp/B07KPT79RN
company: 
companywebsite: 
country: 
price: 
repository: 
issue:
icon: littleshell.png
bugbounty:
verdict: defunct
date: 2021-07-10
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

Very little can be found about this hardware device, the only reference to this wallet can be a purchase link on [Amazom](https://www.amazon.ca/Littleshell-Cryptocurrency-Hardware-Wallet-ERC20-tokens/dp/B07KPT79RN)

This listing has the wallet first available on Nov. 21 2018, currently it is unavailable. I would conclude this is defunct.