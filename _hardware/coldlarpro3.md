---
title: "ColdLar Pro 3"
appId: coldlarpro3
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [123, 62.5, 8.6]
weight: 163
website: https://www.coldlar.com/productDetails/10062
shop: https://www.coldlar.com/productDetails/10062
company: ColdLar Information Technology Co.
companywebsite: https://www.coldlar.com/
country: CH
price: 620USD
repository: 
issue:
icon: coldlarpro3.png
bugbounty:
verdict: wip
date: 2021-07-10
signer:
reviewArchive:


providerTwitter: Coldlar
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

