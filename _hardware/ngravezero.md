---
title: "NGRAVE ZERO"
appId: ngravezero
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [72, 125, 14]
weight: 180
website: https://www.ngrave.io/products/zero
shop: https://shop.ngrave.io/
company: NGRAVE
companywebsite: https://www.ngrave.io/
country: BE
price: 377USD
repository: https://github.com/ngraveio
issue:
icon: ngravezero.png
bugbounty:
verdict: unreleased
date: 2021-07-29
signer:
reviewArchive:


providerTwitter: ngrave_official
providerLinkedIn: ngrave-io
providerFacebook: ngrave.io
providerReddit: 
---

This wallet is on pre-order and hence has not been released yet.