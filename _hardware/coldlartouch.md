---
title: "ColdLar Touch"
appId: coldlartouch
authors:
- kiwilamb
- felipe
released: 
discontinued: # date
updated:
version:
dimensions: [54, 86, 0.86]
weight: 6
website: https://www.coldlar.com/productDetails/10065
shop: https://www.coldlar.com/productDetails/10065
company: ColdLar Information Technology Co.
companywebsite: https://www.coldlar.com/
country: CH
price: 28.8USD
repository: 
issue:
icon: coldlartouch.png
bugbounty:
verdict: noita
date: 2021-07-11
signer:
reviewArchive:


providerTwitter: Coldlar
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

Without a screen and whitout a button, this device cannot provide basic security of hardware wallets.