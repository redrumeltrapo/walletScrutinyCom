---
title: "BitSafe"
appId: bitsafe
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: 
companywebsite:
country: 
price: 
repository: 
issue:
icon: bitsafe.png
bugbounty:
verdict: defunct # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-01
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

This hardware wallet looks to be an early experimental design and no real production was produced.
There is the [initial thread](https://bitcointalk.org/index.php?topic=152517.0) on its creation in early 2013 and is now considered defunct.