---
title: "OPOLO Cosmos"
appId: opolocosmos
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [89.2,66.4,9.5]
weight: 
website: https://www.opolo.io/
shop: https://secure.opolo.shop/
company: OPOLO Limited
companywebsite: https://www.opolo.io
country: LU
price: 199EUR
repository: 
issue:
icon: opolocosmos.png
bugbounty:
verdict: unreleased 
date: 2021-07-29
signer:
reviewArchive:


providerTwitter: OpoloWallet
providerLinkedIn: opolo-ltd
providerFacebook: opolo.wallet.3
providerReddit: 
---

This wallet is on pre-order and hence has not been released yet.
