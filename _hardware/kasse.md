---
title: "KASSE HK-1000"
appId: kasse
authors:
- kiwilamb
- leo
released: 
discontinued: # date
updated: 2020-04-16
version: 2.11.0
dimensions: [60.4, 22.4, 8.2]
weight: 11
website: https://kasseusa.com/kasse-hk-1000-cryptocurrency-hardware-wallet/
shop: https://kasseusa.com/kasse-hk-1000-cryptocurrency-hardware-wallet/
company: KASSE USA (HyundaiPay)
companywebsite: https://kasseusa.com
country: US
price: 42.95USD
repository: 
issue:
icon: kasse.png
bugbounty:
verdict: stale
date: 2021-09-01
signer:
reviewArchive:


providerTwitter: KasseUSA
providerLinkedIn: 
providerFacebook: KasseUSA
providerReddit: 
---


{{ page.title }} has screen and buttons and a secure element with a true
random number generator but there appears to be no public source code, so their
claims are **not verifiable**.

Also the [latest software update](https://kasseusa.com/software/) is from
2020-04-16 which makes the product **stale** by our definition.

The product *only working with Windows* would be yet another down-side.
