---
title: "Tangem"
appId: tangem
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 85, 0.6]
weight: 3
website: https://shop.tangem.com/collections/crypto-hardware-wallet
shop: https://shop.tangem.com/products/tangem-card-pack
company: Tangem
companywebsite: https://tangem.com
country: CH
price: 39.99USD
repository: https://github.com/tangem
issue:
icon: tangem.png
bugbounty:
verdict: noita # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-08
signer:
reviewArchive:


providerTwitter: tangem
providerLinkedIn: tangem
providerFacebook: 
providerReddit: 
---


This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.