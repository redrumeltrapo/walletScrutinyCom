---
title: "Keevo Model 1"
appId: keevo
authors:
- kiwilamb
released: 2020-09-31 # Q3 2020 according to provider
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://www.keevowallet.com/products/keevo-model-1
shop: https://www.keevowallet.com/products/keevo-model-1
company: BitKey Technogies
companywebsite: https://www.keevowallet.com
country: US
price: 299USD
repository: 
issue:
icon: keevo.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: keevowallet
providerLinkedIn: 
providerFacebook: keevowallet
providerReddit: 
---

