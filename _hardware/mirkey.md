---
title: "MIRkey"
appId: mirkey
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [19, 45, 10]
weight: 7
website: https://ellipticsecure.com/products/mirkey_overview.html
shop: https://ellipticsecure.com/order.html
company: Elliptic Secure
companywebsite: https://ellipticsecure.com
country: US
price: 49USD
repository: https://github.com/ellipticSecure
issue:
icon: mirkey.png
bugbounty:
verdict: wip
date: 2021-07-21
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: ellipticsecure
providerFacebook: 
providerReddit: 
---

