---
title: "OmniStaker"
appId: omnistaker
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://omnistaker.com
shop: https://omnistaker.com/shop/
company: OmniSolutions
companywebsite: https://omnistaker.com
country: 
price: 
repository: 
issue:
icon: omnistaker.png
bugbounty:
verdict: nobtc
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

This hardware device is for staking coins and does not support bitcoins.
