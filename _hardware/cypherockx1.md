---
title: "Cypherock X1"
appId: cypherockx1
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 5
website: https://www.cypherock.com/
shop: https://shop.cypherock.com/
company: Cypherock
companywebsite: https://cypherock.com/
country: IN
price: 99USD
repository: 
issue:
icon: cypherockx1.png
bugbounty:
verdict: wip 
date: 2021-07-26
signer:
reviewArchive:


providerTwitter: CypherockWallet
providerLinkedIn: cypherockwallet
providerFacebook: cypherock
providerReddit: 
---

