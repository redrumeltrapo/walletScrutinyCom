---
title: "ColdLar Smart"
appId: coldlarsmart
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://www.coldlar.com/Smart
shop: 
company: ColdLar Information Technology Co.
companywebsite: https://www.coldlar.com/
country: CH
price: 
repository: 
issue:
icon: coldlarsmart.png
bugbounty:
verdict: wip
date: 2021-07-10
signer:
reviewArchive:


providerTwitter: Coldlar
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

