---
title: "OneKey - Limited Edition"
appId: onekey
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://onekey.so/en-US/hardware
shop: https://onekey.so/en-US/hardware
company: Bixin
companywebsite: https://onekey.so/
country: CN
price: 499CNY
repository: https://github.com/OneKeyHQ/OneKey-Wallet
issue:
icon: onekey.png
bugbounty:
verdict: wip
date: 2021-08-03
signer:
reviewArchive:


providerTwitter: OneKeyHQ
providerLinkedIn: 
providerFacebook: 
providerReddit: OneKeyHQ
---

