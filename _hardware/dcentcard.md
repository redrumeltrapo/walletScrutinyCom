---
title: "D'CENT Card Wallet"
appId: dcentcard
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 86, 0.86]
weight: 6
website: https://dcentwallet.com/products/CardWallet
shop: https://dcentwallet.com/Shop
company: IOTrust
companywebsite: https://iotrust.kr/
country: KP
price: 23USD
repository: 
issue:
icon: dcentcard.png
bugbounty:
verdict: nobtc
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: DCENTwallets
providerLinkedIn: 
providerFacebook: DcentWalletGlobal
providerReddit: 
---

This device does not support Bitcoin.
