---
title: "Prokey Optimum"
appId: prokeyoptimum
authors:
- kiwilamb
released: 2020-01-01
discontinued: # date
updated:
version:
dimensions: [41, 50, 9.4]
weight: 16.7
website: https://prokey.io/prokey-optimum
shop: https://prokey.io/prokey-optimum
company: Prokey
companywebsite: https://prokey.io/
country: MY
price: 59USD
repository: https://github.com/prokey-io
issue:
icon: prokeyoptimum.png
bugbounty:
verdict: wip 
date: 2021-07-26
signer:
reviewArchive:


providerTwitter: tryProkey
providerLinkedIn: 
providerFacebook: prokey.io
providerReddit: 
---

