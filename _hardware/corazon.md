---
title: "Corazon"
appId: corazon
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://gray.inc/collections/corazon-wallet
shop: https://gray.inc/products/corazon-titanium
company: GRAY®
companywebsite: https://gray.inc/
country: SG
price: 599USD
repository: 
issue:
icon: corazon.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: graysingapore
providerLinkedIn: gray-tech-international-private-limited
providerFacebook: graysingapore
providerReddit: 
---

This wallet is a Trezor Model T firmware with fancy casing options.
