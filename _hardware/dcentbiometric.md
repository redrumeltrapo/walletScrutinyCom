---
title: "D'CENT Biometric Wallet"
appId: dcentbiometric
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [78.2, 43.2, 10.8]
weight: 36
website: https://dcentwallet.com/products/BiometricWallet
shop: https://dcentwallet.com/Shop/detail/b15125cd52814be19a3f0edf54c8bc17
company: IOTrust
companywebsite: https://iotrust.kr/
country: KP
price: 119USD
repository: https://github.com/DcentWallet
issue:
icon: dcentbiometric.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: DCENTwallets
providerLinkedIn: 
providerFacebook: DcentWalletGlobal
providerReddit: 
---

