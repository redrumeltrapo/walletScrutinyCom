---
title: "HyperMate G"
appId: hypermateg
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [38, 64, 2.3]
weight: 
website: https://hyperpay.me/hypermatepro/g
shop: https://shop.hyperpay.tech/products/crypto-hardware-wallet-hypermate-g
company: HyperPay
companywebsite: https://hyperpay.tech/
country: HK
price: 129USD
repository: https://github.com/hyperpayorg/hardwallet
issue:
icon: hypermateg.png
bugbounty:
verdict: wip
date: 2021-08-03
signer:
reviewArchive:


providerTwitter: HyperPay_tech
providerLinkedIn: 
providerFacebook: hyperpayofficial
providerReddit: 
---

