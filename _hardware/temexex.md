---
title: "Temexe X"
appId: temexex
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [35, 61, 8]
weight: 30
website: https://www.amazon.com/-/es/Temexe-Cryptocurrency-Bluetooth-Encryption-recuperaciA13%B3n/dp/B07GJJ7RRS/ref=cm_cr_arp_d_bdcrb_top?ie=UTF8
shop: https://www.amazon.com/-/es/Temexe-Cryptocurrency-Bluetooth-Encryption-recuperaciA13%B3n/dp/B07GJJ7RRS/ref=cm_cr_arp_d_bdcrb_top?ie=UTF8
company: Temexe X
companywebsite: https://temexe.com/
country: US
price: 
repository: 
issue:
icon: temexex.png
bugbounty:
verdict: defunct # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-01
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: temexetech
providerReddit: 
---

The [provider's website](https://temexe.com/) is inaccessible and we considered it defunct.