---
title: "BITHD Watch 1"
appId: bithdwatch1
authors:
- kiwilamb
released: 2017-01-01
discontinued: # date
updated:
version:
dimensions: [42, 37, 14]
weight: 
website: https://bithd.com/BITHD-watch-1.html
shop: https://bithd.com/BITHD-watch-1.html
company: BitHD
companywebsite: https://bithd.com
country: CH
price: 
repository: https://github.com/bithd
issue:
icon: bithdwatch1.png
bugbounty:
verdict: wip
date: 2021-07-16
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

