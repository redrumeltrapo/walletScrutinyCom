---
title: "ELLIPAL TITAN"
appId: ellipaltitan
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://www.ellipal.com/pages/coldwallet
shop: https://www.ellipal.com/products/ellipal-titan
company: ELLIPAL Limited
companywebsite: https://www.ellipal.com
country: HK
price: 139USD
repository: https://github.com/ellipal
issue:
icon: ellipaltitan.png
bugbounty:
verdict: wip
date: 2021-07-10
signer:
reviewArchive:


providerTwitter: ellipalwallet
providerLinkedIn: 
providerFacebook: ellipalclub
providerReddit: 
---

