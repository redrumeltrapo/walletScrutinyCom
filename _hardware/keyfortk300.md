---
title: "KeyFort K300"
appId: keyfortk300
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: KeyFort
companywebsite: https://www.keyfort.io/
country: CH
price: 
repository: 
issue:
icon: keyfortk300.png
bugbounty:
verdict: defunct
date: 2021-07-16
signer:
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

This hardware wallet looks to be defunct, the provider’s [main site is not accessible](https://www.keyfort.io).
