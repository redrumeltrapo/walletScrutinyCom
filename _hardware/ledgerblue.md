---
title: "Ledger Blue"
appId: ledgerblue
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: 
shop: 
company: Ledger
companywebsite: https://www.ledger.com
country: FR
price: 
repository: https://github.com/LedgerHQ
issue:
icon: ledgerblue.png
bugbounty:
verdict: wip # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-08
signer:
reviewArchive:


providerTwitter: Ledger
providerLinkedIn: ledgerhq
providerFacebook: Ledger
providerReddit: 
---

This hardware device still seems to be supported via the Ledger website, however it is not currently being promoted from the websites product range. This hardware device may soon be discontinued.
