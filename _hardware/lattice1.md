---
title: "Lattice1"
appId: lattice1
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://gridplus.io/products/grid-lattice1
shop: https://gridplus.io/products/grid-lattice1
company: Grid Plus
companywebsite: https://gridplus.io
country: US
price: 349USD
repository: https://github.com/GridPlus/lattice-firmware-history
issue:
icon: lattice1.png
bugbounty:
verdict: wip
date: 2021-07-23
signer:
reviewArchive:


providerTwitter: gridplus
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

Lattice devices can work in conjunction with the providers safecards, safecards store private keys offline from the lattice device.
Safecards themselves could be considered wallets, but they are unable to sign transactions without the lattice device.

The lattice device should be reviewed in conjuction with the safecard, the safecard by itself is of no use without the lattice device.
