---
title: "Skywallet"
appId: skywallet
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions:
weight: 
website: https://www.skycoin.com/skywallet/
shop: https://store.skycoin.com/products/skywallet
company: Skycoin
companywebsite: https://www.skycoin.com/
country: 
price: 39USD
repository: https://github.com/skycoinproject
issue:
icon: skywallet.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: skycoinproject
providerLinkedIn: skycoin
providerFacebook: SkycoinOfficial
providerReddit: 
---

