---
title: "SafePal S1"
appId: safepals1
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: 
weight: 
website: https://shop.safepal.io/products/safepal-hardware-wallet-s1-bitcoin-wallet
shop: https://shop.safepal.io/products/safepal-hardware-wallet-s1-bitcoin-wallet
company: SafePal
companywebsite: https://www.safepal.io
country: CH
price: 49.99USD
repository: 
issue:
icon: safepals1.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: iSafePal
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

