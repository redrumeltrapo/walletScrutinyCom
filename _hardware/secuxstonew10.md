---
title: "SecuX STONE W10"
appId: secuxstonew10
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [59, 89, 13]
weight: 
website: https://shop.secuxtech.com/products/w10-hardware-wallet-for-computer/
shop: https://shop.secuxtech.com/products/w10-hardware-wallet-for-computer/
company: SecuX Technology Inc.
companywebsite: https://secuxtech.com
country: TW
price: 69USD
repository: 
issue:
icon: secuxstonew10.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: SecuXwallet
providerLinkedIn: secuxtech
providerFacebook: secuxtech
providerReddit: 
---

