---
title: "SecuX STONE W20"
appId: secuxstonew20
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [59, 89, 13]
weight: 
website: https://shop.secuxtech.com/products/w20-hardware-wallet-for-computer-mobile-user/
shop: https://shop.secuxtech.com/products/w20-hardware-wallet-for-computer-mobile-user/
company: SecuX Technology Inc.
companywebsite: https://secuxtech.com
country: TW
price: 119USD
repository: 
issue:
icon: secuxstonew20.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: SecuXwallet
providerLinkedIn: secuxtech
providerFacebook: secuxtech
providerReddit: 
---

