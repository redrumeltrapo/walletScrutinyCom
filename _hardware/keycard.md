---
title: "KeyCard"
appId: keycard
authors:
- kiwilamb
released: 
discontinued: # date
updated:
version:
dimensions: [54, 85, 1]
weight: 10
website: https://keycard.tech
shop: https://get.keycard.tech/
company: KeyCard
companywebsite: https://keycard.tech
country: 
price: 24.9EUR
repository: https://github.com/status-im/status-keycard
issue:
icon: keycard.png
bugbounty:
verdict: noita # wip noita nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-08-08
signer:
reviewArchive:


providerTwitter: Keycard_
providerLinkedIn: 
providerFacebook: 
providerReddit: 
---

This hardware device lacks a screen or a button, this device cannot provide basic security of hardware wallets.