---
title: "BitLox Ultimate"
appId: bitloxultimate
authors:
- kiwilamb
released: 2015-01-01
discontinued: # date
updated:
version:
dimensions: [54, 85, 4]
weight: 25
website: https://www.bitlox.com/products/bitlox-ultimate
shop: https://www.bitlox.com/products/bitlox-ultimate
company: BitLox
companywebsite: https://bitlox.com
country: HK
price: 148USD
repository: https://github.com/bitlox
issue:
icon: bitloxultimate.png
bugbounty:
verdict: wip
date: 2021-07-15
signer:
reviewArchive:


providerTwitter: bitlox
providerLinkedIn: 
providerFacebook: BitLoxWallet
providerReddit: 
---

