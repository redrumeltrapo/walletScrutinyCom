---
title: "Secure Wallet"
appId: securewallet
authors:
- kiwilamb
released: 2018-04-01
discontinued: # date
updated:
version:
dimensions: [54, 85, 0.84]
weight: 
website: https://securewallet.shop/
shop: https://securewallet.shop/products/secure-wallet
company: ECOMI Technologies PTE
companywebsite: https://www.ecomi.com/
country: SG
price: 199USD
repository: 
issue:
icon: securewallet.png
bugbounty:
verdict: wip
date: 2021-07-07
signer:
reviewArchive:


providerTwitter: ecomi_?s
providerLinkedIn: ecomi-technology
providerFacebook: ecomi.ecosystem
providerReddit: 
---

