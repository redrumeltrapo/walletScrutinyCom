---
wsId: CoinbaseWallet
title: "Coinbase Wallet"
altTitle: 
authors:
- leo
appId: org.toshi.distribution
appCountry: 
idd: 1278383455
released: 2017-09-27
updated: 2021-11-02
version: "25.1"
stars: 4.60776
reviews: 68871
size: 139952128
website: https://wallet.coinbase.com
repository: 
issue: 
icon: org.toshi.distribution.jpg
bugbounty: 
verdict: nosource
date: 2021-01-04
signer: 
reviewArchive:


providerTwitter: CoinbaseWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This is the iPhone version of the Android
{% include walletLink.html wallet='android/org.toshi' %}.

Just like the Android version, this wallet is **not verifiable**.
