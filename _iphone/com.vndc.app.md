---
wsId: VNDCPro
title: "ONUS: Invest BTC, ETH, DOGE"
altTitle: 
authors:
- danny
appId: com.vndc.app
appCountry: us
idd: 1498452975
released: 2020-03-09
updated: 2021-11-05
version: "2.0.2"
stars: 4.71429
reviews: 406
size: 186630144
website: https://goonus.io/en
repository: 
issue: 
icon: com.vndc.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: vncd_official
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
