---
wsId: WanWallet
title: "WanWallet"
altTitle: 
authors:
- danny
appId: org.wanchain.WanWallet
appCountry: us
idd: 1477039507
released: 2019-08-22
updated: 2021-08-05
version: "5.1"
stars: 3.90476
reviews: 21
size: 23052288
website: https://www.wanchain.org
repository: https://github.com/wanchain
issue: 
icon: org.wanchain.WanWallet.jpg
bugbounty: 
verdict: wip
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: wanchain_org
providerLinkedIn: 
providerFacebook: wanchainfoundation
providerReddit: wanchain

redirect_from:

---

{% include copyFromAndroid.html %}
