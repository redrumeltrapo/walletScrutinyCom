---
wsId: bingbon
title: "Bingbon"
altTitle: 
authors:
- kiwilamb
- leo
appId: pro.bingbon.finance
appCountry: de
idd: 1500217666
released: 2020-02-25
updated: 2021-11-08
version: "2.42.0"
stars: 5
reviews: 2
size: 96167936
website: https://bingbon.com
repository: 
issue: 
icon: pro.bingbon.finance.jpg
bugbounty: 
verdict: custodial
date: 2021-04-24
signer: 
reviewArchive:


providerTwitter: BingbonOfficial
providerLinkedIn: bingbon
providerFacebook: BingbonOfficial
providerReddit: Bingbon

redirect_from:

---

We cannot find any claims as to the custody of private keys found from Bingbon.
We must assume the wallet app is custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

