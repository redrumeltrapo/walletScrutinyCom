---
wsId: ChivoWallet
title: "Chivo Wallet"
altTitle: 
authors:
- danny
appId: com.chivo.wallet
appCountry: sv
idd: 1581515981
released: 2021-09-07
updated: 2021-09-07
version: "1.0"
stars: 2.59774
reviews: 3458
size: 70994944
website: https://chivowallet.com
repository: 
issue: 
icon: com.chivo.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: chivowallet
providerLinkedIn: 
providerFacebook: ChivoWalletSLV
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
