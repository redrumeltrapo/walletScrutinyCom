---
wsId: roqqu
title: "Roqqu - Buy and Sell Bitcoin"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.roqqu.ios
appCountry: 
idd: 1505370687
released: 2020-04-06
updated: 2021-04-23
version: "1.3.1"
stars: 2.7
reviews: 220
size: 45339648
website: https://roqqu.com
repository: 
issue: 
icon: com.roqqu.ios.jpg
bugbounty: 
verdict: defunct
date: 2021-04-29
signer: 
reviewArchive:
- date: 2021-04-19
  version: "1.3.1"
  appHash: 
  gitRevision: 2adf93055d5b552806d8a041f41c1e8e4a7c5fd6
  verdict: custodial


providerTwitter: roqqupay
providerLinkedIn: 
providerFacebook: roqqupay
providerReddit: 

redirect_from:

---

The app can't be found on the App Store anymore.