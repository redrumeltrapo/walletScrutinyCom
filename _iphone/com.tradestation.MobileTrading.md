---
wsId: TradeStation
title: "TradeStation - Trade & Invest"
altTitle: 
authors:
- danny
appId: com.tradestation.MobileTrading
appCountry: us
idd: 581548081
released: 2012-12-10
updated: 2021-11-02
version: "5.13.0"
stars: 4.57272
reviews: 15037
size: 32477184
website: http://www.tradestation.com/trading-technology/tradestation-mobile
repository: 
issue: 
icon: com.tradestation.MobileTrading.jpg
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: tradestation
providerLinkedIn: 
providerFacebook: TradeStation
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
