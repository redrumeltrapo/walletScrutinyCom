---
wsId: kleverexchange
title: "Klever Exchange"
altTitle: 
authors:
- danny
appId: io.klever.secure.exchange
appCountry: us
idd: 1553486059
released: 2021-09-25
updated: 2021-10-01
version: "1.0.3"
stars: 4.78788
reviews: 33
size: 87657472
website: https://klever.io
repository: 
issue: 
icon: io.klever.secure.exchange.jpg
bugbounty: 
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: klever_io
providerLinkedIn: klever-app
providerFacebook: klever.io
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
