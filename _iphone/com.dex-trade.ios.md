---
wsId: DexTrade
title: "Dex-Trade"
altTitle: 
authors:
- danny
appId: com.dex-trade.ios
appCountry: us
idd: 1496672790
released: 2020-01-28
updated: 2021-09-20
version: "2.1.5"
stars: 4.5
reviews: 2
size: 7494656
website: https://dex-trade.com/
repository: 
issue: 
icon: com.dex-trade.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: dextrade_
providerLinkedIn: dex-trade-exchange
providerFacebook: DexTradeExchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
