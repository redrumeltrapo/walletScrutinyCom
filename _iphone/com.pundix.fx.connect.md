---
wsId: functionX
title: "f(x)Wallet by Pundi X Labs"
altTitle: 
authors:
- danny
appId: com.pundix.fx.connect
appCountry: us
idd: 1504798360
released: 2021-03-25
updated: 2021-11-04
version: "1.9.1"
stars: 2.80952
reviews: 42
size: 105571328
website: https://pundix.com
repository: https://github.com/FunctionX/fx-wallet-android
issue: 
icon: com.pundix.fx.connect.jpg
bugbounty: 
verdict: nosource
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: FUNCTIONX_IO
providerLinkedIn: functionx
providerFacebook: FunctionX.io
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
