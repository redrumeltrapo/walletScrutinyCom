---
wsId: MetalPay
title: "Metal Pay"
altTitle: 
authors:
- danny
appId: com.metallicus.metalpay
appCountry: us
idd: 1345101178
released: 2018-09-14
updated: 2021-11-06
version: "2.7.11"
stars: 4.29054
reviews: 4027
size: 120137728
website: https://metalpay.com
repository: 
issue: 
icon: com.metallicus.metalpay.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: metalpaysme
providerLinkedIn: 
providerFacebook: metalpaysme
providerReddit: MetalPay

redirect_from:

---

{% include copyFromAndroid.html %}

