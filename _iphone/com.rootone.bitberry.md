---
wsId: bitberry
title: "Bitberry : Safe Wallet"
altTitle: 
authors:
- danny
appId: com.rootone.bitberry
appCountry: us
idd: 1411817291
released: 2018-10-09
updated: 2020-03-05
version: "1.3.3"
stars: 4.14286
reviews: 7
size: 118583296
website: http://bitberry.app
repository: 
issue: 
icon: com.rootone.bitberry.jpg
bugbounty: 
verdict: stale
date: 2021-09-12
signer: 
reviewArchive:
- date: 2021-09-11
  version: "1.3.3"
  appHash: 
  gitRevision: a56730247355e1acdeba52317b006511806100fb
  verdict: custodial

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
