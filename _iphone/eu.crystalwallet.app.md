---
wsId: CrystalWallet
title: "Crystal Wallet"
altTitle:
authors:
- danny
appId: eu.crystalwallet.app
appCountry: us
idd: 1501729731
released: 2020-03-13
updated: 2021-10-23
version: "1.78"
stars: 5
reviews: 6
size: 41720832
website: https://crystalwallet.eu/#about
repository:
issue:
icon: eu.crystalwallet.app.jpg
bugbounty:
verdict: nosource
date: 2021-11-10
signer:
reviewArchive:


providerTwitter: CrystalWalletEU
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
{% include copyFromAndroid.html %}
