---
wsId: coincloud
title: "Coin Cloud Wallet"
altTitle: 
authors:
- kiwilamb
appId: com.bitpay.coincloud
appCountry: 
idd: 1421460676
released: 2018-09-09
updated: 2021-08-26
version: "1.9.3"
stars: 4.08387
reviews: 155
size: 80644096
website: https://www.coin.cloud/app
repository: 
issue: 
icon: com.bitpay.coincloud.jpg
bugbounty: 
verdict: nosource
date: 2021-05-12
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

providerTwitter: CoinCloudATM
providerLinkedIn: 
providerFacebook: coincloudATM
providerReddit: 

redirect_from:

---

It is very clear that the provider is claiming that this wallet is non-custodial with this early statement found in the [app store description](https://apps.apple.com/app/id1421460676).

> Keep your bitcoin and other digital currency secure and under your own control with the non-custodial Coin Cloud Wallet app. No third-party custodial services or key management

With keys in control of the user, we need to find the source code in order to check reproducibility. However we are unable to locate a public source repository.

Our verdict: As there is no source code to be found anywhere, this wallet is at best a non-custodial closed source wallet and as such **not verifiable**.

