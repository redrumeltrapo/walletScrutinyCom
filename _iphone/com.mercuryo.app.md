---
wsId: Mercuryo
title: "Mercuryo Bitcoin Cryptowallet"
altTitle: 
authors:
- leo
appId: com.mercuryo.app
appCountry: 
idd: 1446533733
released: 2019-02-08
updated: 2021-11-02
version: "1.77"
stars: 4.72994
reviews: 648
size: 46427136
website: https://mercuryo.io/
repository: 
issue: 
icon: com.mercuryo.app.jpg
bugbounty: 
verdict: custodial
date: 2021-05-22
signer: 
reviewArchive:


providerTwitter: Mercuryo_io
providerLinkedIn: mercuryo-io
providerFacebook: mercuryo.io
providerReddit: mercuryo

redirect_from:

---

This app has a strong focus on cashing in and out with linked cards and low
exchange fees but no word on who holds the keys. At least not on the App Store.
On their website we find:

> Your private key is safely stored and fully restorable thanks to customer
  verification. Cryptocurrency is stored in safe offline wallets.

which is the definition of a custodial app. This wallet is **not verifiable**.
