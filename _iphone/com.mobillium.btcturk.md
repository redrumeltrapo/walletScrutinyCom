---
wsId: BtcTurk
title: "BtcTurk - Bitcoin Al/Sat"
altTitle: 
authors:
- danny
appId: com.mobillium.btcturk
appCountry: tr
idd: 1503482896
released: 2020-04-09
updated: 2021-10-30
version: "1.11.1"
stars: 4.6429
reviews: 21882
size: 164731904
website: https://www.btcturk.com
repository: 
issue: 
icon: com.mobillium.btcturk.jpg
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: btcturk
providerLinkedIn: btcturk
providerFacebook: btcturk
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
