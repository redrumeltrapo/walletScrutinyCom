---
wsId: bitcoinglobal
title: "Bitcoin Global"
altTitle: 
authors:
- danny
appId: com.global.bitcoin
appCountry: us
idd: 1536910503
released: 2020-10-26
updated: 2021-11-04
version: "2.10.0"
stars: 4.92232
reviews: 103
size: 25984000
website: https://bitcoin.global/
repository: 
issue: 
icon: com.global.bitcoin.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: BitcoinGlobalEx
providerLinkedIn: 
providerFacebook: BitcoinGlobalEx
providerReddit: BITCOIN_GLOBAL

redirect_from:

---

{% include copyFromAndroid.html %}
