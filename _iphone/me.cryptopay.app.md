---
wsId: cryptopay
title: "Cryptopay: Bitcoin-Brieftasche"
altTitle: 
authors:

appId: me.cryptopay.app
appCountry: de
idd: 1223340174
released: 2017-06-08
updated: 2021-10-25
version: "1.38"
stars: 4.55036
reviews: 278
size: 67243008
website: https://cryptopay.me/
repository: 
issue: 
icon: me.cryptopay.app.jpg
bugbounty: 
verdict: custodial
date: 2021-03-10
signer: 
reviewArchive:


providerTwitter: cryptopay
providerLinkedIn: cryptopay
providerFacebook: cryptopayme
providerReddit: 

redirect_from:

---

In the description the only sentence hinting at custodianship is:

> Use our secure multisig wallet to receive, store and transfer BTC, LTC, XRP,
  ETH to your friends.

but there is nothing more to be found and as "multisig wallet" could refer to
anything, we can't say with certainty that this wallet even tries to imply
being self-custodial and therefore consider it **not verifiable**.
