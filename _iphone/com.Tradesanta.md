---
wsId: TradeSanta
title: "TradeSanta: Crypto Trading Bot"
altTitle: 
authors:
- danny
appId: com.Tradesanta
appCountry: us
idd: 1457051269
released: 2019-05-18
updated: 2021-10-18
version: "2.5.81"
stars: 3.75758
reviews: 33
size: 67682304
website: https://tradesanta.com/en
repository: 
issue: 
icon: com.Tradesanta.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

