---
wsId: MOBOX
title: "Mobox Wallet"
altTitle: 
authors:
- danny
appId: com.mobox.wallet
appCountry: us
idd: 1545109501
released: 2021-04-29
updated: 2021-10-28
version: "1.9.1"
stars: 4
reviews: 16
size: 58362880
website: https://mobox.io
repository: 
issue: 
icon: com.mobox.wallet.jpg
bugbounty: 
verdict: nobtc
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: MOBOX_Official
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
