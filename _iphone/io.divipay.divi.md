---
wsId: DiviWallet
title: "Divi Wallet: Crypto & Staking"
altTitle: 
authors:
- danny
appId: io.divipay.divi
appCountry: gb
idd: 1516551223
released: 2021-10-08
updated: 2021-10-23
version: "1.12"
stars: 4.75
reviews: 8
size: 34355200
website: http://wallet.diviproject.org
repository: https://github.com/DiviProject
issue: https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/366
icon: io.divipay.divi.jpg
bugbounty: 
verdict: wip
date: 2021-11-04
signer: 
reviewArchive:


providerTwitter: diviproject
providerLinkedIn: 
providerFacebook: diviproject
providerReddit: DiviProject

redirect_from:

---

{% include copyFromAndroid.html %}
