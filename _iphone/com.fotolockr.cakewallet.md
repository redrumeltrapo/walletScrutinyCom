---
wsId: cake
title: "Cake Wallet"
altTitle: 
authors:
- leo
appId: com.fotolockr.cakewallet
appCountry: 
idd: 1334702542
released: 2018-01-19
updated: 2021-09-19
version: "4.2.7"
stars: 3.31818
reviews: 44
size: 79865856
website: http://cakewallet.com
repository: https://github.com/cake-tech/cake_wallet
issue: https://github.com/cake-tech/cake_wallet/issues/112
icon: com.fotolockr.cakewallet.jpg
bugbounty: 
verdict: nonverifiable
date: 2021-04-14
signer: 
reviewArchive:


providerTwitter: cakewallet
providerLinkedIn: 
providerFacebook: cakewallet
providerReddit: cakewallet

redirect_from:

---

**Update 2021-04-14**: They now do have a public issue tracker and
[emanuel](/authors/emanuel) tried to build with
[slightly more success](https://github.com/cake-tech/cake_wallet/issues/112)
but the verdict remains the same.

> Cake Wallet allows you to safely store, send receive and exchange your XMR /
  Monero and BTC / Bitcoin.

is an implicit claim of this being a non-custodial Bitcoin wallet but:

> -You control your own seed and keys

is more explicit about the non-custodial part.

On their website we read:

> **FEATURES**<br>
  ...<br>
  Open source

and indeed, there is [a source code repo](https://github.com/cake-tech/cake_wallet).

There is no claim about reproducibility or build instructions. As the app uses
[Flutter](https://flutter.dev/) and we have no experience with that, we have to
stop here. Usually at this point we open issues on the code repository but they
have no public issue tracker.
