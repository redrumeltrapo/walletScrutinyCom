---
wsId: Beaxy
title: "Beaxy Exchange"
altTitle: 
authors:
- danny
appId: com.beaxy.cryptoexchange
appCountry: us
idd: 1493015933
released: 2020-02-12
updated: 2021-06-08
version: "2.9"
stars: 4.54795
reviews: 73
size: 187341824
website: https://www.beaxy.com/
repository: 
issue: 
icon: com.beaxy.cryptoexchange.jpg
bugbounty: 
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: BeaxyExchange
providerLinkedIn: 
providerFacebook: beaxyexchange
providerReddit: BeaxyExchange

redirect_from:

---

{% include copyFromAndroid.html %}
