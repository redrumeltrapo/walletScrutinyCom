---
wsId: Nuri
title: "Nuri"
altTitle: 
authors:
- danny
appId: com.bitwala.app
appCountry: gd
idd: 1454003161
released: 2019-05-11
updated: 2021-11-05
version: "2.2.1"
stars: 
reviews: 
size: 100853760
website: https://www.nuri.com
repository: 
issue: 
icon: com.bitwala.app.jpg
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: nuribanking
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

From the site description:

> Nuri is the app to manage, save and grow your money. Invest in cryptocurrencies, create savings plans & earn up to 5% interest per year on bitcoin directly from a German bank account.

The app is complicated as it offers both custodial and non-custodial wallets. But then again, if a user has to pass ID verification and KYC, then even the non-custodial offering is in fact, **custodial**.

It identifies its non-custodial wallets as ["Vaults"](https://nuri.com/how-to/wallet/)

> The Bitcoin (BTC) VAult is available on both web and mobile devices and is a multi-signature non-custodial wallet. A multi-signature protocol, available for Bitcoin acts as a built-in additional security factor. The wallet is backed up with two seed phrases, which let you recover and access your bitcoin in case you lose access to your Nuri account. Under no circumstances, Nuri or anyone else can access or control your funds or transactions.

More information on the distinction between [Nuri Wallets vs Vaults](https://support.nuri.com/hc/en-gb/articles/360022033460-Wallets-Vaults-What-s-the-difference-)

Nuri also has a [risk and disclosure page](https://nuri.com/uploads/Nuri_Bitcoin_Interest_Account_Risk_Warning_EN_b93582385c.pdf)

> Investors cannot verify whether Celsius Network conducts business activities that will enable it to service the claims of investors from the Bitcoin Interest Account in the future.The business activities carried out by Celsius Network may result in further risks for Nuri investors.

[Identity Verification Criteria](https://support.nuri.com/hc/en-gb/articles/360021577139-What-are-the-verification-criteria-)

> Nuri offers the blockchain banking solution for European residents.

> Resident in the European Union, A valid passport or ID card that contains the required security features, minimum age 18 years,
a proof of address document (POA), in order for the account to be opened successfully, this data is verified during a video call with our partner IDnow.
