---
wsId: coholdmobile
title: "HOLD — Buy Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: co.hold.mobile
appCountry: de
idd: 1435187229
released: 2018-09-28
updated: 2021-08-05
version: "3.13.7"
stars: 3.94737
reviews: 19
size: 33504256
website: https://hold.io
repository: 
issue: 
icon: co.hold.mobile.jpg
bugbounty: 
verdict: custodial
date: 2021-03-11
signer: 
reviewArchive:


providerTwitter: HoldHQ
providerLinkedIn: holdhq
providerFacebook: HoldHQ
providerReddit: 

redirect_from:

---

> SAFETY FIRST<br>
  Regulated and licensed in the EU. Your money is securely held by banks within
  the European Union and your crypto protected by the world-renowned custodian
  BitGo.

This app is a custodial offering and thus **not verifiable**.

