---
wsId: crypterium
title: "Crypterium | Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.Crypterium.Crypterium
appCountry: 
idd: 1360632912
released: 2018-03-26
updated: 2021-11-02
version: "1.18.0"
stars: 4.38765
reviews: 988
size: 220540928
website: https://cards.crypterium.com/visa
repository: 
issue: 
icon: com.Crypterium.Crypterium.jpg
bugbounty: 
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: crypterium
providerLinkedIn: 
providerFacebook: crypterium.org
providerReddit: 

redirect_from:

---

Judging by what we can find on the [wallet site](https://wallet.crypterium.com/):

> **Store**<br>
  keep your currencies<br>
  safe & fully insured

this is a custodial app as a self-custody wallet cannot ever have funds insured.

As a custodial app it is **not verifiable**.
