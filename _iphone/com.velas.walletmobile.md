---
wsId: VelasWallet
title: "Velas Mobile Wallet"
altTitle: 
authors:
- danny
appId: com.velas.walletmobile
appCountry: us
idd: 1541032748
released: 2020-12-12
updated: 2021-10-23
version: "2.0.9"
stars: 2.4
reviews: 5
size: 33361920
website: https://velas.com
repository: https://github.com/velas/mobile-wallet
issue: 
icon: com.velas.walletmobile.jpg
bugbounty: 
verdict: wip
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: velasblockchain
providerLinkedIn: velas-ag
providerFacebook: velasblockchain
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
