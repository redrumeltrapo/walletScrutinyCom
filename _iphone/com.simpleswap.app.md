---
wsId: simpleswap
title: "Crypto Exchange - Buy & Sell"
altTitle: 
authors:
- danny
appId: com.simpleswap.app
appCountry: gb
idd: 1506038278
released: 2020-05-15
updated: 2021-11-03
version: "3.1.9"
stars: 4
reviews: 7
size: 45437952
website: https://simpleswap.io/mobile-app
repository: 
issue: 
icon: com.simpleswap.app.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: SimpleSwap_io
providerLinkedIn: 
providerFacebook: SimpleSwap.io
providerReddit: simpleswapexchange

redirect_from:

---

 {% include copyFromAndroid.html %}
