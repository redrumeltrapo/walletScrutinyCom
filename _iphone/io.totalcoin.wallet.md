---
wsId: Totalcoin
title: "Bitcoin Wallet App - Totalcoin"
altTitle: 
authors:
- leo
appId: io.totalcoin.wallet
appCountry: 
idd: 1392398906
released: 2018-07-05
updated: 2021-09-12
version: "4.0.1"
stars: 4.4898
reviews: 98
size: 41118720
website: http://totalcoin.io
repository: 
issue: 
icon: io.totalcoin.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: totalcoin.io
providerReddit: 

redirect_from:

---

On the wallet's website there is no claim about custodianship which makes us
assume it is a custodial product.

As such it is **not verifiable**.
