---
wsId: xapo
title: "Xapo"
altTitle: 
authors:
- leo
appId: com.iphone.XapoApp
appCountry: 
idd: 917692892
released: 2014-11-13
updated: 2021-07-20
version: "6.12.0"
stars: 3.88591
reviews: 149
size: 151875584
website: https://xapo.com
repository: 
issue: 
icon: com.iphone.XapoApp.jpg
bugbounty: 
verdict: defunct
date: 2021-08-06
signer: 
reviewArchive:
- date: 2021-05-24
  version: "6.12.0"
  appHash: 
  gitRevision: 8f9eb9d0591a6808b2ba47c5874c317a73f1ebd6
  verdict: custodial

providerTwitter: xapo
providerLinkedIn: 
providerFacebook: xapoapp
providerReddit: 

redirect_from:

---

**Update 2021-08-02**: Xapo discontinued this app to migrate its users to the
Apple version of {% include walletLink.html wallet='android/com.xapo.bank' verdict='true' %}.

Xapo describes itself as a wallet:

> We’re a digital wallet that allows you to easily and safely send, receive,
  store and spend any traditional currencies and bitcoin.

but under closer investigation, we can't find any Bitcoin wallet here. Naming
"traditional currencies" and "bitcoin" in the same security
claim sound like it has to be custodial.

Their [website on security](https://xapo.com/en/security) is not claiming
otherwise neither which makes us conclude: This app is **not verifiable**.
