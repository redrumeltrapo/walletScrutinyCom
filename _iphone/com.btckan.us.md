---
wsId: BitKan
title: "BitKan: Buy BTC,ETH,DOGE,SHIB"
altTitle: 
authors:
- danny
appId: com.btckan.us
appCountry: us
idd: 1004852205
released: 2015-06-24
updated: 2021-11-01
version: "8.0.67"
stars: 4.97692
reviews: 780
size: 162350080
website: https://bitkan.com/
repository: 
issue: 
icon: com.btckan.us.jpg
bugbounty: 
verdict: nosource
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: bitkanofficial
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
