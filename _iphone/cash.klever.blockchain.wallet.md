---
wsId: klever
title: "Klever: Secure Crypto Wallet"
altTitle: 
authors:
- leo
appId: cash.klever.blockchain.wallet
appCountry: 
idd: 1525584688
released: 2020-08-26
updated: 2021-11-04
version: "4.5.6"
stars: 4.3296
reviews: 446
size: 132254720
website: https://klever.io
repository: 
issue: 
icon: cash.klever.blockchain.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-05-22
signer: 
reviewArchive:


providerTwitter: klever_io
providerLinkedIn: 
providerFacebook: klever.io
providerReddit: 

redirect_from:

---

On their website we read:

> **Peer-to-Peer**<br>
  Klever is a decentralized p2p and self-custody wallet network. Your Keys, your
  crypto.

so they claim the app is self-custodial but we cannot find any source code which
makes the app **not verifiable**.
