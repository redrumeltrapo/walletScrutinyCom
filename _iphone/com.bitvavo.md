---
wsId: bitvavo
title: "Bitvavo | Buy Bitcoin & Crypto"
altTitle: 
authors:
- danny
appId: com.bitvavo
appCountry: be
idd: 1483903423
released: 2020-05-28
updated: 2021-09-08
version: "1.0.79"
stars: 3.61607
reviews: 112
size: 64846848
website: https://bitvavo.com
repository: 
issue: 
icon: com.bitvavo.jpg
bugbounty: 
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
