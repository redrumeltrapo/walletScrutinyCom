---
wsId: trendofx
title: "Analise Forex & Stock"
altTitle: 
authors:
- danny
appId: ios.m3.Trendo
appCountry: in
idd: 1530580389
released: 2020-09-29
updated: 2021-10-14
version: "2.5.47"
stars: 
reviews: 
size: 80217088
website: 
repository: 
issue: 
icon: ios.m3.Trendo.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
