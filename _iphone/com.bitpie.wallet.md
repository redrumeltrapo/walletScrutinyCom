---
wsId: bitpie
title: "Bitpie-Universal Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.bitpie.wallet
appCountry: 
idd: 1481314229
released: 2019-10-01
updated: 2021-10-28
version: "5.0.050"
stars: 3.51351
reviews: 37
size: 324062208
website: https://bitpie.com
repository: 
issue: 
icon: com.bitpie.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: BitpieWallet
providerLinkedIn: 
providerFacebook: BitpieOfficial
providerReddit: BitpieWallet

redirect_from:

---

 {% include copyFromAndroid.html %}
