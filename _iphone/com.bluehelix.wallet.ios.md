---
wsId: bhWallet
title: "Bluehelix Wallet"
altTitle: 
authors:
- danny
appId: com.bluehelix.wallet.ios
appCountry: us
idd: 1535791362
released: 2021-07-06
updated: 2021-10-23
version: "1.3.4"
stars: 5
reviews: 2
size: 30423040
website: https://www.bhexchain.com/
repository: 
issue: 
icon: com.bluehelix.wallet.ios.jpg
bugbounty: 
verdict: fewusers
date: 2021-11-08
signer: 
reviewArchive:


providerTwitter: BHEXOfficial
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
