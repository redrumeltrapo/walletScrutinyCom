---
wsId: Burency
title: "Burency"
altTitle: 
authors:
- danny
appId: com.burency.app
appCountry: us
idd: 1548673602
released: 2021-01-20
updated: 2021-03-23
version: "1.0.1"
stars: 4.625
reviews: 24
size: 74470400
website: https://burency.com/
repository: 
issue: 
icon: com.burency.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: BurencyOfficial
providerLinkedIn: burencyofficial
providerFacebook: BurencyOfficial
providerReddit: Burency

redirect_from:

---

{% include copyFromAndroid.html %}
