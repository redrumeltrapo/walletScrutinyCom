---
wsId: melisclientwallet
title: "Melis Wallet"
altTitle: 
authors:
- leo
appId: io.melis.clientwallet
appCountry: 
idd: 1176840794
released: 2017-06-07
updated: 2021-05-08
version: "1.6.21"
stars: 
reviews: 
size: 28587008
website: http://melis.io
repository: https://github.com/melis-wallet/melis-cm-client
issue: https://github.com/melis-wallet/melis-cm-client/issues/1
icon: io.melis.clientwallet.jpg
bugbounty: 
verdict: nonverifiable
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

On the App Store this provider makes no claims about the app being non-custodial
or source code being public but on their website we find:

> **The safest wallet**<br>
  With Melis you have the complete control of your bitcoins and private keys,
  you can define spending limits policies and make use of two or more factors
  authentication.

which is a clear claim but the multi factor authentication is a bit worrying as
that might rely on giving their server power to inhibit your transactions.

We also find:

> Melis is open source, published on [GitHub](https://github.com/melis-wallet).

but none of the repositories there has an obvious mobile wallet name. If the
source code is somewhere then this is probably an unfortunate way of getting the
verdict **not verifiable** due to lack of source.
