---
wsId: xwallet
title: "XWallet by Pundi X"
altTitle: 
authors:
- kiwilamb
appId: com.pundix.wallet
appCountry: 
idd: 1321754661
released: 2019-01-26
updated: 2020-12-29
version: "2.9.4"
stars: 3.9375
reviews: 80
size: 276264960
website: https://pundix.com
repository: 
issue: 
icon: com.pundix.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-04-29
signer: 
reviewArchive:


providerTwitter: PundiXLabs
providerLinkedIn: pundix
providerFacebook: pundixlabs
providerReddit: 

redirect_from:

---

Searching the Pundix [support FAQ](https://support.pundix.com/) we find an FAQ that answers the private key managment question.

> **Will I have a private key when setting up an account in XWallet app?**<br>
  No. Your email address and mobile number are required when setting up an XWallet app account.

The wallet does not provide the user access to the private keys.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.

