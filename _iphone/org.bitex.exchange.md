---
wsId: Bitex
title: "Bitex - Crypto Exchange"
altTitle: 
authors:
- danny
appId: org.bitex.exchange
appCountry: ae
idd: 1492803003
released: 2020-02-25
updated: 2021-11-01
version: "2.6"
stars: 2.33333
reviews: 15
size: 103431168
website: https://bitex.com
repository: 
issue: 
icon: org.bitex.exchange.jpg
bugbounty: 
verdict: custodial
date: 2021-11-04
signer: 
reviewArchive:


providerTwitter: bitexexch
providerLinkedIn: bitexworldwide
providerFacebook: Bitexofficial
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
