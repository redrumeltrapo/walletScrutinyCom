---
wsId: korbit
title: "korbit"
altTitle: 
authors:
- danny
appId: com.korbit.exchange
appCountry: us
idd: 1434511619
released: 2018-10-18
updated: 2021-11-04
version: "4.2.6"
stars: 2.85714
reviews: 14
size: 128753664
website: http://www.korbit.co.kr
repository: 
issue: 
icon: com.korbit.exchange.jpg
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
