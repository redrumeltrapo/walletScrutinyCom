---
wsId: eXchangily
title: "Exchangily DEX Wallet"
altTitle: 
authors:
- danny
appId: com.exchangily.app
appCountry: 
idd: 1503068552
released: 2020-03-23
updated: 2021-09-02
version: "2.0.27"
stars: 4.16667
reviews: 6
size: 49359872
website: https://exchangily.com
repository: https://github.com/blockchaingate/exchangily-mobile-app
issue: https://github.com/blockchaingate/exchangily-mobile-app/issues/1
icon: com.exchangily.app.jpg
bugbounty: 
verdict: ftbfs
date: 2021-06-25
signer: 
reviewArchive:


providerTwitter: ExchangilyC
providerLinkedIn: 
providerFacebook: Exchangily-439040053240813
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
