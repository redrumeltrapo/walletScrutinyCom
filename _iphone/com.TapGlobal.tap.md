---
wsId: tapngo
title: "Tap - Buy & Sell Bitcoin"
altTitle: 
authors:
- danny
appId: com.TapGlobal.tap
appCountry: gb
idd: 1492263993
released: 2019-12-20
updated: 2021-10-29
version: "2.2.4"
stars: 4.67753
reviews: 521
size: 182639616
website: https://www.tap.global/
repository: 
issue: 
icon: com.TapGlobal.tap.jpg
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
