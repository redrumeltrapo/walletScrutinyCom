---
wsId: bittrex
title: "Bittrex"
altTitle: 
authors:
- leo
appId: com.bittrex.trade
appCountry: 
idd: 1465314783
released: 2019-12-19
updated: 2021-10-14
version: "1.15.5"
stars: 4.65583
reviews: 1354
size: 82240512
website: https://global.bittrex.com
repository: 
issue: 
icon: com.bittrex.trade.jpg
bugbounty: 
verdict: custodial
date: 2021-02-09
signer: 
reviewArchive:


providerTwitter: BittrexGlobal
providerLinkedIn: 
providerFacebook: BittrexGlobal
providerReddit: 

redirect_from:

---

This app is an interface to a trading platform:

> The Bittrex Global mobile app allows you to take the premiere crypto trading
  platform with you wherever you go.

As such, it lets you access your account with them but not custody your own
coins and therefore is **not verifiable**.
