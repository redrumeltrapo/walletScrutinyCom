---
wsId: WealthsimpleTrade
title: "Wealthsimple Trade: Buy Stocks"
altTitle: 
authors:
- danny
appId: com.wealthsimple.trade
appCountry: ca
idd: 1403491709
released: 2019-02-26
updated: 2021-11-09
version: "2.7.0"
stars: 4.68429
reviews: 121847
size: 80834560
website: https://www.wealthsimple.com/en-ca/product/trade/
repository: 
issue: 
icon: com.wealthsimple.trade.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: Wealthsimple
providerLinkedIn: 
providerFacebook: wealthsimple
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
