---
wsId: Cryptology
title: "Cryptology: Digital Assets"
altTitle: 
authors:
- danny
appId: com.cryptology.ios
appCountry: gb
idd: 1313186415
released: 2018-03-23
updated: 2021-10-29
version: "3.0.1"
stars: 5
reviews: 3
size: 48886784
website: http://cryptology.com
repository: 
issue: 
icon: com.cryptology.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-10-08
signer: 
reviewArchive:


providerTwitter: Cryptologyexch
providerLinkedIn: 
providerFacebook: Cryptologyexch
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
