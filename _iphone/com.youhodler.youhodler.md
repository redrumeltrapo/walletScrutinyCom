---
wsId: YouHodler
title: "YouHodler - Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: com.youhodler.youhodler
appCountry: 
idd: 1469351696
released: 2019-07-25
updated: 2021-09-01
version: "2.20.0"
stars: 4.83402
reviews: 241
size: 38839296
website: https://www.youhodler.com/
repository: 
issue: 
icon: com.youhodler.youhodler.jpg
bugbounty: 
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: youhodler
providerLinkedIn: youhodler
providerFacebook: YouHodler
providerReddit: 

redirect_from:

---

This app is the interface to an exchange and might have a non-custodial part to
it but if so, it is not well advertised on their website and we assume it is
custodial and therefore **not verifiable**.
