---
wsId: 2cash
title: "2cash"
altTitle: 
authors:
- danny
appId: com.voicapps.app2cash-ios
appCountry: us
idd: 1530672224
released: 2020-12-01
updated: 2020-12-01
version: "1.2.4"
stars: 5
reviews: 1
size: 81442816
website: https://www.2cash.io
repository: 
issue: 
icon: com.voicapps.app2cash-ios.jpg
bugbounty: 
verdict: custodial
date: 2021-11-08
signer: 
reviewArchive:


providerTwitter: 2cashnetwork
providerLinkedIn: 2cash
providerFacebook: 2cashnetwork
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
