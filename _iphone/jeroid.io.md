---
wsId: Jeroid
title: "Jeroid"
altTitle: 
authors:
- danny
appId: jeroid.io
appCountry: us
idd: 1539278280
released: 2021-02-27
updated: 2021-10-14
version: "1.3.5"
stars: 2.79412
reviews: 68
size: 72709120
website: https://jeroid.ng/
repository: 
issue: 
icon: jeroid.io.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-11-04
signer: 
reviewArchive:


providerTwitter: jeroidng
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
