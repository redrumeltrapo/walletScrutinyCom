---
wsId: BBX
title: "BBX"
altTitle: 
authors:
- danny
appId: com.bbx.bbx
appCountry: us
idd: 1539393718
released: 2020-12-01
updated: 2021-05-27
version: "1.3.1"
stars: 
reviews: 
size: 41028608
website: https://bbx.com
repository: 
issue: 
icon: com.bbx.bbx.jpg
bugbounty: 
verdict: defunct
date: 2021-10-22
signer: 
reviewArchive:
- date: 2021-10-10
  version: "1.3.1"
  appHash: 
  gitRevision: f9f046037c44e67715b35a4a2fbf64ab6b2244ac
  verdict: custodial
  

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

**Update 2021-10-14**: This app is no more available.


{% include copyFromAndroid.html %}
