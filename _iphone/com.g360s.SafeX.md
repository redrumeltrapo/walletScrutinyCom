---
wsId: ecoinSafex
title: "SafeX"
altTitle: 
authors:
- danny
appId: com.g360s.SafeX
appCountry: us
idd: 1484347213
released: 2019-11-04
updated: 2020-12-14
version: "2.0.9"
stars: 5
reviews: 1
size: 64462848
website: https://ecoinofficial.org
repository: 
issue: 
icon: com.g360s.SafeX.jpg
bugbounty: 
verdict: nobtc
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: ecoinofficial
providerLinkedIn: 
providerFacebook: ecoinofficial
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
