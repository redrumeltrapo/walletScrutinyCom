---
wsId: coinfalcon
title: "CoinFalcon – Buy Bitcoin"
altTitle: 
authors:
- leo
appId: com.coinfalcon.mobile
appCountry: 
idd: 1396963260
released: 2018-10-05
updated: 2021-02-09
version: "2.1.13"
stars: 3.30769
reviews: 13
size: 52423680
website: https://coinfalcon.com
repository: 
issue: 
icon: com.coinfalcon.mobile.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: coinfalcon
providerLinkedIn: 
providerFacebook: CoinFalcon
providerReddit: CoinFalcon

redirect_from:

---

In the description we read:

> **State-of-the-Art Security**<br>
  CoinFalcon stores 98% of digital funds in an offline, secure wallet, while the
  rest is protected by high-grade security systems. We are committed to the
  highest safety standards both here on the app and our web platform.

which is clearly about a custodial offering.
