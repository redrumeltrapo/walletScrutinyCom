---
wsId: tokocrypto
title: "Tokocrypto"
altTitle: 
authors:
- danny
- leo
appId: com.binanceCloudtoko.binance
appCountry: ph
idd: 1538556690
released: 2020-12-01
updated: 2021-10-28
version: "1.3.0"
stars: 
reviews: 
size: 90774528
website: https://www.tokocrypto.com
repository: 
issue: 
icon: com.binanceCloudtoko.binance.jpg
bugbounty: 
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: tokocrypto
providerLinkedIn: toko
providerFacebook: TCDXOfficial
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
