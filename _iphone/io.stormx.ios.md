---
wsId: stormX
title: "StormX"
altTitle: 
authors:
- danny
appId: io.stormx.ios
appCountry: us
idd: 1420545397
released: 2018-12-12
updated: 2021-11-04
version: "4.12.3"
stars: 4.61306
reviews: 1132
size: 47662080
website: https://stormx.io/
repository: 
issue: 
icon: io.stormx.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-11-01
signer: 
reviewArchive:


providerTwitter: stormxio
providerLinkedIn: StormX
providerFacebook: stormxio
providerReddit: stormxio

redirect_from:

---

{% include copyFromAndroid.html %}
