---
wsId: BitPreco
title: "BitPreço Oficial"
altTitle: 
authors:
- danny
appId: com.bitpreco.bitprecoApp
appCountry: br
idd: 1545825554
released: 2021-03-18
updated: 2021-08-29
version: "1.8.16"
stars: 4.6178
reviews: 191
size: 75334656
website: https://bitpreco.com/
repository: 
issue: 
icon: com.bitpreco.bitprecoApp.jpg
bugbounty: 
verdict: custodial
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: BitPreco
providerLinkedIn: bitpreco
providerFacebook: BitPreco
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

