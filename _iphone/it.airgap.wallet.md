---
wsId: AirGapWallet
title: "AirGap Wallet"
altTitle: 
authors:
- danny
appId: it.airgap.wallet
appCountry: 
idd: 1420996542
released: 2018-08-24
updated: 2021-11-02
version: "3.12.0"
stars: 3.9
reviews: 10
size: 104578048
website: https://airgap.it/
repository: 
issue: 
icon: it.airgap.wallet.jpg
bugbounty: 
verdict: nowallet
date: 2021-03-07
signer: 
reviewArchive:


providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

redirect_from:

---

{% include copyFromAndroid.html %}
