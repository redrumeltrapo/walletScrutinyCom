---
wsId: incognito
title: "Incognito crypto wallet"
altTitle: 
authors:
- leo
appId: com.incognito.wallet
appCountry: 
idd: 1475631606
released: 2019-08-21
updated: 2021-10-01
version: "4.7.0"
stars: 3.87209
reviews: 86
size: 51854336
website: https://incognito.org
repository: https://github.com/incognitochain/incognito-wallet
issue: 
icon: com.incognito.wallet.jpg
bugbounty: 
verdict: nonverifiable
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: incognitochain
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
