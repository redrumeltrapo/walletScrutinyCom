---
wsId: ROInvesting
title: "ROInvesting: Online FX Trading"
altTitle: 
authors:
- danny
appId: com.roinvesting
appCountry: gb
idd: 1529122271
released: 2020-10-19
updated: 2021-08-23
version: "1.68.10"
stars: 1
reviews: 1
size: 32552960
website: https://www.roinvesting.com/
repository: 
issue: 
icon: com.roinvesting.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: Roinvesting
providerLinkedIn: roinvesting
providerFacebook: ROInvesting
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
