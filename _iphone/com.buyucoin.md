---
wsId: buyucoin
title: "BuyUcoin"
altTitle: 
authors:
- leo
appId: com.buyucoin
appCountry: 
idd: 1539456610
released: 2020-11-17
updated: 2021-08-15
version: "1.16"
stars: 2
reviews: 8
size: 47090688
website: https://www.buyucoin.com
repository: 
issue: 
icon: com.buyucoin.jpg
bugbounty: 
verdict: custodial
date: 2021-05-30
signer: 
reviewArchive:


providerTwitter: buyucoin
providerLinkedIn: buyucoin
providerFacebook: BuyUcoin
providerReddit: BuyUcoin

redirect_from:

---

On the App Store, 4 of 6 ratings are 1-star. On the Play Store the average
rating is 3.5 stars with many complaints in the reviews. Caution is
advised!

This app appears to be the broken interface for a broken exchange, judging by
the vast majority of user comments. It is certainly **not verifiable**.
