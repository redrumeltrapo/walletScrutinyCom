---
wsId: AladdinPro
title: "Aladdin Wallet"
altTitle: 
authors:
- danny
appId: com.abbc.wallet
appCountry: us
idd: 1475883958
released: 2019-08-16
updated: 2020-02-12
version: "1.3.3"
stars: 5
reviews: 1
size: 52381696
website: https://abbccoin.com/
repository: 
issue: 
icon: com.abbc.wallet.jpg
bugbounty: 
verdict: stale
date: 2021-10-09
signer: 
reviewArchive:
- date: 2021-09-15
  version: "1.3.3"
  appHash: 
  gitRevision: 9dfdd677cd0ab7e3f2f9fafa26671e03f636a19b
  verdict: custodial

providerTwitter: abbcfoundation
providerLinkedIn: abbcfoundation
providerFacebook: abbcfoundation
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
