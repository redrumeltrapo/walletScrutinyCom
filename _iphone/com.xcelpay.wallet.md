---
wsId: XcelPay
title: "XcelPay - Secure Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.xcelpay.wallet
appCountry: 
idd: 1461215417
released: 2019-05-26
updated: 2021-10-26
version: "2.51.12"
stars: 4.38095
reviews: 42
size: 56143872
website: http://xcelpay.io
repository: 
issue: 
icon: com.xcelpay.wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: XcelPayWallet
providerLinkedIn: in/xcelpaywallet
providerFacebook: xcelpay
providerReddit: 

redirect_from:

---

This wallet has no claim of being non-custodial in the app's description.

The one-star ratings over and over tell:

* there is a referral program, promising rewards
* the rewards are never reflected in the wallet
* funds cannot be sent to a different wallet
* SCAM

As a probably custodial app, it is **not verifiable**.
