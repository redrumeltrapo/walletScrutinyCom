---
wsId: OKEx
title: "OKEx: Trade Bitcoin, ETH, SHIB"
altTitle: 
authors:
- leo
appId: com.okex.OKExAppstoreFull
appCountry: 
idd: 1327268470
released: 2018-01-04
updated: 2021-11-08
version: "5.3.14"
stars: 4.93186
reviews: 16100
size: 352161792
website: https://www.okex.com
repository: 
issue: 
icon: com.okex.OKExAppstoreFull.jpg
bugbounty: 
verdict: custodial
date: 2020-12-23
signer: 
reviewArchive:


providerTwitter: OKEx
providerLinkedIn: 
providerFacebook: okexofficial
providerReddit: OKEx

redirect_from:

---

On their website we find:

> **Institutional-grade Security**<br>
  Cold wallet technology developed by the world's top security team adopts a
  multi-security-layer mechanism to safeguard your assets

"Cold wallet technology" means this is a custodial offering and therefore
**not verifiable**.
