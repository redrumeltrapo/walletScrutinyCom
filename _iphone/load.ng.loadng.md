---
wsId: loadNG
title: "LoadNG"
altTitle: 
authors:
- danny
appId: load.ng.loadng
appCountry: ng
idd: 1537865343
released: 2020-11-02
updated: 2021-10-16
version: "2.1"
stars: 4.11111
reviews: 9
size: 42493952
website: https://app.load.ng/
repository: 
issue: 
icon: load.ng.loadng.jpg
bugbounty: 
verdict: nowallet
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: loadngautomated
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
