---
wsId: FlareWallet
title: "Flare Wallet"
altTitle: 
authors:

appId: org.flarewallet.flare
appCountry: 
idd: 1496651406
released: 2020-02-11
updated: 2021-03-13
version: "1.4.0"
stars: 3.97143
reviews: 35
size: 24013824
website: https://flarewallet.io
repository: 
issue: 
icon: org.flarewallet.flare.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}

Old Review
---

**Update:** We did not get to review this app before it was removed from the App
Store.
