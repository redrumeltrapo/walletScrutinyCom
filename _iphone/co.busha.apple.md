---
wsId: busha
title: "Busha - Buy and Sell Bitcoins"
altTitle: 
authors:
- leo
appId: co.busha.apple
appCountry: 
idd: 1450373493
released: 2019-02-03
updated: 2021-06-03
version: "2.6.13"
stars: 3.96774
reviews: 62
size: 65311744
website: https://busha.co
repository: 
issue: 
icon: co.busha.apple.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: getbusha
providerLinkedIn: 
providerFacebook: getbusha
providerReddit: 

redirect_from:

---

The description

> Won’t you rather trade and store your crypto assets on a platform you can
  trust? Busha is a Nigerian based crypto exchange that offers you all these and
  more.

sounds like it's an app to access an account on a custodial platform.

On their website they are more explicit:

> **Safe & Secure**<br>
  Our 24/7 monitoring systems, cold storage and industry-standard multi-sig
  wallets ensure that your assets are the safest they can be.

which is a list of features only relevant in a custodial context.

Our verdict: **not verifiable**.
