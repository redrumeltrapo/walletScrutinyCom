---
wsId: SmartWallet
title: "SmartWallet"
altTitle: 
authors:
- danny
appId: br.com.coinbr.smartwallet
appCountry: us
idd: 1302270848
released: 2018-01-02
updated: 2021-11-02
version: "2.0.28"
stars: 5
reviews: 1
size: 12990464
website: https://stratum.hk/press
repository: 
issue: 
icon: br.com.coinbr.smartwallet.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: stratumhk
providerLinkedIn: stratumbr
providerFacebook: stratum.hk
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
