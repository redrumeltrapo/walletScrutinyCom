---
wsId: AirGapVault
title: "AirGap Vault - Secure Secrets"
altTitle: 
authors:
- leo
appId: it.airgap.vault
appCountry: 
idd: 1417126841
released: 2018-08-24
updated: 2021-11-02
version: "3.11.1"
stars: 4.33333
reviews: 6
size: 88687616
website: 
repository: https://github.com/airgap-it/airgap-vault
issue: 
icon: it.airgap.vault.jpg
bugbounty: 
verdict: nonverifiable
date: 2021-09-29
signer: 
reviewArchive:


providerTwitter: AirGap_it
providerLinkedIn: 
providerFacebook: 
providerReddit: AirGap

redirect_from:

---

This app for Android is reproducible but unfortunately due to limitations of the
iPhone platform, we so far were not able to reproduce any App Store app.
