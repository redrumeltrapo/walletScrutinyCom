---
wsId: kinesismoney
title: "Kinesis - Buy gold and silver"
altTitle: 
authors:
- danny
appId: com.kinesis.kinesis-app
appCountry: us
idd: 1490483608
released: 2020-02-28
updated: 2021-11-05
version: "1.3.9"
stars: 4.5
reviews: 30
size: 64452608
website: https://kinesis.money/
repository: https://github.com/KinesisNetwork/wallet-mobile
issue: 
icon: com.kinesis.kinesis-app.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: KinesisMonetary
providerLinkedIn: kinesismoney
providerFacebook: kinesismoney
providerReddit: Kinesis_money

redirect_from:

---

{% include copyFromAndroid.html %}
