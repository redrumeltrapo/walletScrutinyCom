---
wsId: bitcointrade
title: "BitcoinTrade - Criptomoedas"
altTitle: 
authors:
- danny
appId: com.root.BitcoinTrade
appCountry: br
idd: 1320032339
released: 2017-12-13
updated: 2021-11-04
version: "4.0.9"
stars: 3.69278
reviews: 651
size: 36858880
website: http://www.bitcointrade.com.br/
repository: 
issue: 
icon: com.root.BitcoinTrade.jpg
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: bitcointrade
providerFacebook: BitcointradeBR
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
