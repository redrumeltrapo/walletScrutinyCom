---
wsId: CryptoTabPro
title: "CryptoTab Browser Pro"
altTitle: 
authors:
- danny
appId: pro.cryptobrowser.ios
appCountry: us
idd: 1524974223
released: 2020-09-21
updated: 2020-09-24
version: "5.4.4"
stars: 2.75
reviews: 100
size: 121089024
website: https://cryptobrowser.site/
repository: 
issue: 
icon: pro.cryptobrowser.ios.jpg
bugbounty: 
verdict: stale
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
