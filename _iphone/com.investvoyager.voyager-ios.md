---
wsId: voyager
title: "Voyager: Crypto Made Simple"
altTitle: 
authors:
- leo
appId: com.investvoyager.voyager-ios
appCountry: 
idd: 1396178579
released: 2019-02-13
updated: 2021-11-09
version: "3.0.4"
stars: 4.74882
reviews: 87479
size: 71620608
website: https://www.investvoyager.com/
repository: 
issue: 
icon: com.investvoyager.voyager-ios.jpg
bugbounty: 
verdict: custodial
date: 2021-01-02
signer: 
reviewArchive:


providerTwitter: investvoyager
providerLinkedIn: investvoyager
providerFacebook: InvestVoyager
providerReddit: Invest_Voyager

redirect_from:

---

On their website we read:

> **Advanced Security**<br>
  Offline storage, advanced fraud protection, and government-regulated processes
  keep your assets secure and your personal information safe.

which means this is a custodial offering and therefore **not verifiable**.
