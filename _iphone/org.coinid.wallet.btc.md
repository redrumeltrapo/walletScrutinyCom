---
wsId: COINiDwallet
title: "Bitcoin Wallet for COINiD"
altTitle: 
authors:
- leo
- danny
appId: org.coinid.wallet.btc
appCountry: 
idd: 1370200585
released: 2018-10-10
updated: 2021-02-20
version: "1.8.0"
stars: 4.68889
reviews: 45
size: 18523136
website: https://coinid.org
repository: https://github.com/COINiD/COINiDWallet
issue: https://github.com/COINiD/COINiDWallet/issues/24
icon: org.coinid.wallet.btc.jpg
bugbounty: 
verdict: ftbfs
date: 2021-06-25
signer: 
reviewArchive:


providerTwitter: COINiDGroup
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
