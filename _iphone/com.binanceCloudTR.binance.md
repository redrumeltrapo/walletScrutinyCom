---
wsId: BinanceTR
title: "Binance TR: BTC | SHIB | DOGE"
altTitle: 
authors:
- danny
appId: com.binanceCloudTR.binance
appCountry: tr
idd: 1548636153
released: 2021-02-18
updated: 2021-11-03
version: "1.3.0"
stars: 4.43682
reviews: 12087
size: 61177856
website: https://www.trbinance.com/
repository: 
issue: 
icon: com.binanceCloudTR.binance.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: BinanceTR
providerLinkedIn: 
providerFacebook: TRBinanceTR
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
