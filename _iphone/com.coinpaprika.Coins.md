---
wsId: coinsonepaprika
title: "COINS: One App For Crypto"
altTitle: 
authors:
- danny
appId: com.coinpaprika.Coins
appCountry: us
idd: 1475233621
released: 2019-12-03
updated: 2021-10-19
version: "2.6.2"
stars: 4.74232
reviews: 749
size: 65758208
website: http://coins.coinpaprika.com
repository: 
issue: 
icon: com.coinpaprika.Coins.jpg
bugbounty: 
verdict: nosource
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: CoinsOneApp
providerLinkedIn: 
providerFacebook: CoinsOneApp
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
