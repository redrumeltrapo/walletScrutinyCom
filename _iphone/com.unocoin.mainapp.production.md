---
wsId: Unocoin
title: "Unocoin Wallet"
altTitle: 
authors:
- leo
appId: com.unocoin.mainapp.production
appCountry: 
idd: 1030422972
released: 2016-05-12
updated: 2021-11-09
version: "6.1.5"
stars: 2.47826
reviews: 23
size: 198586368
website: https://www.unocoin.com
repository: 
issue: 
icon: com.unocoin.mainapp.production.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: Unocoin
providerLinkedIn: unocoin
providerFacebook: unocoin
providerReddit: 

redirect_from:

---

This app appears to be the interface to a trading platform. The description
does not talk about where the keys are stored but it links to their
website and there we read

> AES-256 Encryption<br>
  The address-private key pairs obtained are encrypted using AES-256, sealed in
  envelopes and stored in multiple safe deposit lockers.

which clearly means they have the keys and you don't. As a custodial service,
this app is **not verifiable**.
