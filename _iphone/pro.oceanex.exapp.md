---
wsId: OceanEX
title: "OceanEx"
altTitle: 
authors:
- danny
appId: pro.oceanex.exapp
appCountry: us
idd: 1481311936
released: 2019-10-05
updated: 2021-10-22
version: "1.4.10"
stars: 4.65517
reviews: 29
size: 48142336
website: https://oceanex.pro/
repository: 
issue: 
icon: pro.oceanex.exapp.jpg
bugbounty: 
verdict: custodial
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: OceanexOfficial
providerLinkedIn: 
providerFacebook: OceanExPROOfficial
providerReddit: OceanExOfficial

redirect_from:

---

{% include copyFromAndroid.html %}
