---
wsId: ZamWallet
title: "ZamWallet Buy Crypto with Card"
altTitle: 
authors:
- danny
appId: zamzam.wallet.app
appCountry: ru
idd: 1436344249
released: 2018-10-17
updated: 2021-11-06
version: "2.1.0"
stars: 4
reviews: 28
size: 48569344
website: https://zam.io
repository: 
issue: 
icon: zamzam.wallet.app.jpg
bugbounty: 
verdict: nosource
date: 2021-11-04
signer: 
reviewArchive:


providerTwitter: zam_io
providerLinkedIn: 11770701
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
