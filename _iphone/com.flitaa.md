---
wsId: flitaa
title: "flitaa"
altTitle: 
authors:
- danny
appId: com.flitaa
appCountry: ng
idd: 1566777501
released: 2021-05-25
updated: 2021-09-05
version: "1.7.6"
stars: 4.26316
reviews: 19
size: 60886016
website: https://flitbase.com
repository: 
issue: 
icon: com.flitaa.jpg
bugbounty: 
verdict: custodial
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: getflitaa
providerLinkedIn: 
providerFacebook: getflitaa
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
