---
wsId: mercurycash
title: "Mercury Cash"
altTitle: 
authors:
- leo
appId: com.adenter.mercurycash
appCountry: 
idd: 1291394963
released: 2017-10-07
updated: 2021-09-16
version: "4.3.3"
stars: 4.53731
reviews: 67
size: 88678400
website: https://www.mercury.cash/
repository: 
issue: 
icon: com.adenter.mercurycash.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: mercurycash
providerLinkedIn: 
providerFacebook: mercurycash
providerReddit: 

redirect_from:

---

This app makes no claims about self-custody so we have to assume it is a
custodial product and thus **not verifiable**.
