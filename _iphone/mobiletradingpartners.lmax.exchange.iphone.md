---
wsId: LMAX
title: "LMAX Global Trading"
altTitle:
authors:
- danny
appId: mobiletradingpartners.lmax.exchange.iphone
appCountry: hu
idd: 884042608
released: 2014-06-06
updated: 2021-09-20
version: "4.3.35"
stars:
reviews:
size: 126159872
website: https://www.lmax.com/mobile
repository:
issue:
icon: mobiletradingpartners.lmax.exchange.iphone.jpg
bugbounty:
verdict: nosendreceive
date: 2021-11-10
signer:
reviewArchive:


providerTwitter: LMAX
providerLinkedIn: lmax-group
providerFacebook:
providerReddit:

redirect_from:

---
{% include copyFromAndroid.html %}
