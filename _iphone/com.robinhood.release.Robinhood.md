---
wsId: Robinhood
title: "Robinhood: Investing for All"
altTitle: 
authors:
- danny
appId: com.robinhood.release.Robinhood
appCountry: us
idd: 938003185
released: 2014-12-11
updated: 2021-11-02
version: "9.39.0"
stars: 4.14818
reviews: 3811203
size: 268425216
website: https://robinhood.com/
repository: 
issue: 
icon: com.robinhood.release.Robinhood.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: RobinhoodApp
providerLinkedIn: robinhood
providerFacebook: robinhoodapp
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
