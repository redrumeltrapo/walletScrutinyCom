---
wsId: cobowallet
title: "Cobo Crypto Wallet: BTC & DASH"
altTitle: 
authors:
- leo
appId: cobo.wallet
appCountry: 
idd: 1406282615
released: 2018-08-05
updated: 2021-10-29
version: "5.16.0"
stars: 3.66667
reviews: 3
size: 79941632
website: https://cobo.com
repository: 
issue: 
icon: cobo.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: Cobo_Wallet
providerLinkedIn: coboofficial
providerFacebook: coboOfficial
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
