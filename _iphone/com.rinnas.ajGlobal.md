---
wsId: AjGlobalV
title: "AJGlobalV"
altTitle: 
authors:
- danny
appId: com.rinnas.ajGlobal
appCountry: us
idd: 1519859460
released: 2020-09-08
updated: 2021-10-19
version: "2.0.5"
stars: 3.2381
reviews: 21
size: 50455552
website: https://ajglobalv.com/
repository: 
issue: 
icon: com.rinnas.ajGlobal.jpg
bugbounty: 
verdict: custodial
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: ajglobalv
providerLinkedIn: 
providerFacebook: AJGLOBALVENTURES
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
