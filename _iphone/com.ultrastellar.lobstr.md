---
wsId: lobstrco
title: "LOBSTR Stellar Lumens Wallet"
altTitle: 
authors:
- danny
appId: com.ultrastellar.lobstr
appCountry: us
idd: 1404357892
released: 2018-08-06
updated: 2021-11-07
version: "7.6.2"
stars: 4.53162
reviews: 4080
size: 74646528
website: https://lobstr.co/
repository: 
issue: 
icon: com.ultrastellar.lobstr.jpg
bugbounty: 
verdict: nobtc
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: Lobstrco
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

As the description states, lobstr is a stellar lumens wallet. 

However, looking deeper you'll find that you can purchase other assets including bitcoin. You can also add that to the "Assets". 

You need to deposit 5 XLM in order to add the BTC asset.

However, lobstr presumably only holds btc tokens and not actual bitcoin. Hence, the **nobtc** verdict.

Zendesk support page "[Buying Crypto with LOBSTR wallet](https://lobstr.zendesk.com/hc/en-us/articles/360014741460-Buying-crypto-with-LOBSTR-wallet)"
