---
wsId: ambercrypto
title: "Amber App - Swap & Earn Crypto"
altTitle: 
authors:
- danny
appId: com.ambergroup.amberapp
appCountry: us
idd: 1515652068
released: 2020-09-21
updated: 2021-10-22
version: "1.7.6"
stars: 3.06452
reviews: 31
size: 292851712
website: http://www.ambergroup.io
repository: 
issue: 
icon: com.ambergroup.amberapp.jpg
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: ambergroup_io
providerLinkedIn: amberbtc
providerFacebook: ambergroup.io
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
