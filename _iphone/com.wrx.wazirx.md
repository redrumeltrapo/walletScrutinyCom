---
wsId: wazirx
title: "WazirX -  Bitcoin Exchange"
altTitle: 
authors:

appId: com.wrx.wazirx
appCountry: in
idd: 1349082789
released: 2018-03-07
updated: 2021-10-05
version: "1.10.1"
stars: 4.23313
reviews: 34031
size: 33864704
website: https://support.wazirx.com
repository: 
issue: 
icon: com.wrx.wazirx.jpg
bugbounty: 
verdict: custodial
date: 2021-08-09
signer: 
reviewArchive:


providerTwitter: WazirxIndia
providerLinkedIn: wazirx
providerFacebook: wazirx
providerReddit: 

redirect_from:

---

As this exchange allows holding your BTC in the app such
as sending and receiving them, it is usable as a wallet. A custodial wallet. As
such it is **not verifiable**.
