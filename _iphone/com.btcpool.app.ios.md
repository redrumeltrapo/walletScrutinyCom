---
wsId: BTCcomPool
title: "BTC.com - Leading Mining Pool"
altTitle: 
authors:
- danny
appId: com.btcpool.app.ios
appCountry: us
idd: 1490997527
released: 2020-01-21
updated: 2021-10-24
version: "1.1.1"
stars: 3.22222
reviews: 9
size: 54467584
website: http://btc.com
repository: 
issue: 
icon: com.btcpool.app.ios.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: btccom_official
providerLinkedIn: btc.com
providerFacebook: btccom
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
