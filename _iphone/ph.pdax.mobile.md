---
wsId: pdax
title: "PDAX"
altTitle: 
authors:
- danny
appId: ph.pdax.mobile
appCountry: ph
idd: 1531246346
released: 2020-12-18
updated: 2021-10-12
version: "1.2.169"
stars: 3.1422
reviews: 218
size: 97229824
website: https://pdax.ph
repository: 
issue: 
icon: ph.pdax.mobile.jpg
bugbounty: 
verdict: custodial
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: pdaxph
providerLinkedIn: pdaxph
providerFacebook: pdaxph
providerReddit: 

redirect_from:

---

PDAX or the "Philippine Digital Asset Exchange" is a cryptocurrency exchange.

It allows deposits and withdrawals of different cryptocurrency assets only after KYC. 

In its [Terms Page](https://trade.pdax.ph/pages/terms), under Section 17 "Assumption of Risk", it mentions:

> You accept the risks associated with the use of the PDAX’s online Platform and its Services in transacting Digital Assets such as, but not limited to: **Failure of security of your Wallet address and Private Keys;**

We contacted PDAX support and they confirmed that they do not provide the private keys for the wallets.

Verdict is **custodial**
