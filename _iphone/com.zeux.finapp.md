---
wsId: zeux
title: "Zeux - Banking & Investing"
altTitle: 
authors:
- danny
appId: com.zeux.finapp
appCountry: gb
idd: 1384796487
released: 2019-02-12
updated: 2021-04-06
version: "2.18.0"
stars: 4.85612
reviews: 1112
size: 55801856
website: https://www.zeux.com
repository: 
issue: 
icon: com.zeux.finapp.jpg
bugbounty: 
verdict: defunct
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---
{% include copyFromAndroid.html %}
