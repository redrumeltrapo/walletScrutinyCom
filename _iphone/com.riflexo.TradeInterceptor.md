---
wsId: ThinkTrader
title: "ThinkTrader"
altTitle: 
authors:
- danny
appId: com.riflexo.TradeInterceptor
appCountry: us
idd: 329476057
released: 2009-09-23
updated: 2021-11-04
version: "6.6.3"
stars: 4.78782
reviews: 476
size: 74099712
website: https://www.thinkmarkets.com/uk/about-us/
repository: 
issue: 
icon: com.riflexo.TradeInterceptor.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: ThinkMarketscom
providerLinkedIn: thinkmarkets
providerFacebook: ThinkMarkets
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
