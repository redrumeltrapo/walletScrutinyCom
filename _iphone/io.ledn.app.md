---
wsId: Ledn
title: "Ledn"
altTitle:
authors:
- danny
appId: io.ledn.app
appCountry: ca
idd: 1543035976
released: 2021-01-20
updated: 2021-01-28
version: "0.1.3"
stars: 5
reviews: 11
size: 38786048
website: https://ledn.io
repository:
issue:
icon: io.ledn.app.jpg
bugbounty:
verdict: custodial
date: 2021-11-10
signer:
reviewArchive:


providerTwitter: hodlwithLedn
providerLinkedIn: ledn-inc
providerFacebook: Ledn.io
providerReddit:

redirect_from:

---
{% include copyFromAndroid.html %}
