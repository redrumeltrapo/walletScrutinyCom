---
wsId: bitay
title: "Bitay - Easy Bitcoin Exchange"
altTitle: 
authors:
- danny
appId: com.app.Bitay
appCountry: us
idd: 1458595661
released: 2019-04-14
updated: 2020-11-07
version: "2.1.0"
stars: 4.14286
reviews: 7
size: 13966336
website: 
repository: 
issue: 
icon: com.app.Bitay.jpg
bugbounty: 
verdict: stale
date: 2021-11-03
signer: 
reviewArchive:
- date: 2021-08-27
  version: "2.1.0"
  appHash: 
  gitRevision: 11e04851956c9e541b63a85b959962c9de5fce94
  verdict: custodial

providerTwitter: BitayTurkiye
providerLinkedIn: 
providerFacebook: bitayturkiye
providerReddit: 

redirect_from:

---

Site Description:

> Turkey's Most Trusted Crypto Currency Exchange

Bitay's [Security Policy](https://www.bitay.com/en/security-policy) discusses how its wallets are secured:

> the technology from our trading platform to wallets has been developed within the company in a 100% secure manner. No 3rd party software has been used in case of a security risk. 98% of your platform's crypto-currencies are stored privately in cold wallets that are not connected to the internet.

Bitay also requires [identity verification](https://www.bitay.com/en/how/how-to-do-identity-verification)

The [Support Center > Deposit and Withdraw > Withdraw page](https://www.bitay.com/en/support/deposit-withdraw/withdraw-cryptocurrency-from-bitay) is blank.

However, it has a [status page](https://www.bitay.com/en/status) for the availability of cryptocurrency Withdrawals/Deposits and fees. 

These point to a **custodial** service with an exchange.

