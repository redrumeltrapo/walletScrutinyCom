---
wsId: Monarch
title: "Monarch Wallet"
altTitle: 
authors:
- leo
appId: com.sf.monarch
appCountry: 
idd: 1386397997
released: 2018-06-12
updated: 2021-10-28
version: "1.6.8"
stars: 4.76376
reviews: 436
size: 180037632
website: https://monarchwallet.com
repository: 
issue: 
icon: com.sf.monarch.jpg
bugbounty: 
verdict: nosource
date: 2021-05-22
signer: 
reviewArchive:


providerTwitter: Monarchtoken
providerLinkedIn: monarchtoken
providerFacebook: MonarchWallet
providerReddit: MonarchToken

redirect_from:

---

This app appears to have been created for Monarch Tokens but it also features a
Bitcoin wallet.

On the App Store listing there is no official website mentioned but their
support i at [http://splashfactory.com](http://splashfactory.com/) but there is
no information there and we found [monarchwallet.com](https://monarchwallet.com)
to likely be their website.

There, we found
[this question in their FAQ](https://monarch.freshdesk.com/support/solutions/articles/44001516779-lost-my-seed):

> **Lost My Seed** Wow this is a problem. We're so sorry, we are a decentralized
> wallet, this means you and only you own your seed. If you lost your seed there
> is nothing we can do for you.

which means they claim to be a non-custodial wallet. But can we find the source
code?

As we can't find any source code we consider the app closed source and therefore
**not verifiable**.
