---
wsId: Nominex
title: "Nominex"
altTitle: 
authors:
- danny
appId: com.nominex.app
appCountry: ru
idd: 1572965666
released: 2021-07-17
updated: 2021-10-12
version: "1.3.4"
stars: 4.6087
reviews: 23
size: 66307072
website: https://nominex.io
repository: 
issue: 
icon: com.nominex.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: NominexExchange
providerLinkedIn: nominex
providerFacebook: Nominex
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

