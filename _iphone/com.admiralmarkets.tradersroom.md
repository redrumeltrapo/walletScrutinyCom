---
wsId: AdmiralMarkets
title: "Admirals"
altTitle: 
authors:
- danny
appId: com.admiralmarkets.tradersroom
appCountry: us
idd: 1222861799
released: 2017-06-28
updated: 2021-11-04
version: "5.7.1"
stars: 5
reviews: 4
size: 65402880
website: https://admiralmarkets.com/
repository: 
issue: 
icon: com.admiralmarkets.tradersroom.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: AdmiralsGlobal
providerLinkedIn: -admiral-markets-group
providerFacebook: AdmiralsGlobal
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

