---
wsId: hotbit
title: "Hotbit-Global"
altTitle: 
authors:
- danny
appId: io.chainbase.global
appCountry: 
idd: 1568969341
released: 2021-05-26
updated: 2021-11-08
version: "1.3.33"
stars: 3.62948
reviews: 251
size: 53370880
website: https://www.hotbit.io
repository: 
issue: 
icon: io.chainbase.global.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: Hotbit_news
providerLinkedIn: hotbitexchange
providerFacebook: hotbitexchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
