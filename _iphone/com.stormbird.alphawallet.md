---
wsId: AlphaWallet
title: "AlphaWallet Ethereum Wallet"
altTitle: 
authors:
- danny
appId: com.stormbird.alphawallet
appCountry: us
idd: 1358230430
released: 2018-05-25
updated: 2021-10-29
version: "3.31"
stars: 4.60294
reviews: 136
size: 65274880
website: https://alphawallet.com/
repository: https://github.com/alphawallet
issue: 
icon: com.stormbird.alphawallet.jpg
bugbounty: 
verdict: nobtc
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: Alpha_wallet
providerLinkedIn: 
providerFacebook: 
providerReddit: AlphaWallet

redirect_from:

---

{% include copyFromAndroid.html %}
