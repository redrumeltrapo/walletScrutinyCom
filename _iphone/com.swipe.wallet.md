---
wsId: SwipeWallet
title: "Swipe Wallet"
altTitle: 
authors:
- leo
appId: com.swipe.wallet
appCountry: 
idd: 1476726454
released: 2019-09-10
updated: 2021-05-07
version: "1.900"
stars: 4.58999
reviews: 939
size: 142593024
website: https://swipe.io
repository: 
issue: 
icon: com.swipe.wallet.jpg
bugbounty: 
verdict: defunct
date: 2021-07-06
signer: 
reviewArchive:
- date: 2021-04-26
  version: "1.900"
  appHash: 
  gitRevision: cb228d8cdb269382f7751e003d94523f6e219bc5
  verdict: custodial

providerTwitter: SwipeWallet
providerLinkedIn: 
providerFacebook: Swipe
providerReddit: 

redirect_from:

---

**Update 2021-07-06**: The app looks abandoned. It was:

* Removed from the App Store
* Their Twitter account is suspended
* Most reviews on Play Store are 1-star, some with scam accusations and since 
  July 2020 none got a reply from the provider, with many reporting similar
  silence on support channels

We found some
[Binance link](https://www.binance.com/en/blog/421499824684900723/Binance-and-Swipe-Partner-to-Bridge-Crypto-and-Commerce-Announce-Acquisition-)
to this, too:

> Jul 06 2020<br>
  **Binance and Swipe Partner to Bridge Crypto and Commerce, Announce Acquisition**<br>
  Binance, the global blockchain company behind the world’s largest digital
  asset exchange, today announced the completion of its acquisition of Swipe,
  the industry’s leading multi-asset digital wallet and Visa debit card platform
  that allows users to buy, sell, convert and spend cryptocurrencies, for an
  undisclosed amount.

# Original Review

This app is a custodial offering:

> SECURE STORAGE<br>
  Swipe users can have peace-of-mind knowing their assets are covered under a
  $100M insurance policy with our custodian. All User deposited funds are stored
  in cold storage with a trusted custodian. Having these funds in a cold storage
  wallet ensures our users that their funds are safe and easily accessible
  through the Swipe Network on the Swipe Wallet.

This contradicts itself. Being in cold storage should mean that it's precisely
not easily accessible via network. What's on the network is by definition a
"hot" wallet.

The website lists "coinbase | custody" and "BitGo" as custodians, which means
you not only have to trust them but also two other services to make sure your
and all the other clients' funds are being accounted for with funds in their
custody.

Anyway, this is all **not verifiable**.
