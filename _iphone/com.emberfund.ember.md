---
wsId: ember
title: "Ember Fund - Invest in Crypto"
altTitle: 
authors:
- danny
appId: com.emberfund.ember
appCountry: us
idd: 1406211993
released: 2018-08-04
updated: 2021-11-01
version: "30.5"
stars: 4.53109
reviews: 386
size: 64650240
website: https://emberfund.io/
repository: https://github.com/ember-fund
issue: 
icon: com.emberfund.ember.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: Ember_Fund
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}