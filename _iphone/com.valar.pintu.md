---
wsId: Pintu
title: "Pintu: Buy/Sell Digital Assets"
altTitle: 
authors:
- danny
appId: com.valar.pintu
appCountry: us
idd: 1494119678
released: 2020-01-27
updated: 2021-10-18
version: "3.6.2"
stars: 4.71739
reviews: 46
size: 57576448
website: https://pintu.co.id/
repository: 
issue: 
icon: com.valar.pintu.jpg
bugbounty: 
verdict: custodial
date: 2021-08-27
signer: 
reviewArchive:


providerTwitter: pintuid
providerLinkedIn: 
providerFacebook: pintucrypto
providerReddit: 

redirect_from:

---

Found in the [FAQ:](https://pintu.co.id/en/faq/private-keys)
> **Do I hold a Private Key?**<br>
  Pintu is a custodial crypto exchange, which means Pintu acts as a custodian/keeper of the users’ private keys.  Therefore, Pintu doesn’t share private keys with its users.

Pintu is **custodial** and **not verifiable.**
