---
wsId: BinanceUS
title: "Binance.US - Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: us.binance.fiat
appCountry: 
idd: 1492670702
released: 2020-01-05
updated: 2021-11-05
version: "2.8.6"
stars: 4.19262
reviews: 82515
size: 200669184
website: https://www.binance.us
repository: 
issue: 
icon: us.binance.fiat.jpg
bugbounty: 
verdict: custodial
date: 2021-01-10
signer: 
reviewArchive:


providerTwitter: binanceus
providerLinkedIn: binance-us
providerFacebook: BinanceUS
providerReddit: 

redirect_from:

---

This is the iPhone version of {% include walletLink.html wallet='android/com.binance.us' %} and we
come to the same conclusion for the same reasons. This app is **not verifiable**.
