---
wsId: kardiawallet
title: "KardiaChain Wallet"
altTitle: 
authors:
- danny
appId: com.kardiachain.wallet
appCountry: vn
idd: 1551620695
released: 2021-03-02
updated: 2021-11-05
version: "2.3.39"
stars: 4.37097
reviews: 62
size: 70023168
website: https://kardiachain.io/
repository: 
issue: 
icon: com.kardiachain.wallet.jpg
bugbounty: 
verdict: nobtc
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: KardiaChain
providerLinkedIn: 
providerFacebook: KardiaChainFoundation
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
