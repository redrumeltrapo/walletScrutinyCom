---
wsId: Bitget
title: "Bitget"
altTitle: 
authors:
- danny
appId: com.bitget.exchange.global
appCountry: ua
idd: 1442778704
released: 2018-11-29
updated: 2021-11-09
version: "1.2.18"
stars: 
reviews: 
size: 161408000
website: https://youtu.be/i4DnHKL19dE
repository: 
issue: 
icon: com.bitget.exchange.global.jpg
bugbounty: 
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: bitgetglobal
providerLinkedIn: bitget
providerFacebook: BitgetGlobal
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
