---
wsId: krakent
title: "KrakenPro"
altTitle: 
authors:
- leo
appId: com.kraken.trade.app
appCountry: 
idd: 1473024338
released: 2019-11-12
updated: 2021-10-22
version: "2.2.0"
stars: 4.49125
reviews: 10414
size: 51477504
website: https://www.kraken.com
repository: 
issue: 
icon: com.kraken.trade.app.jpg
bugbounty: 
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive:


providerTwitter: krakenfx
providerLinkedIn: krakenfx
providerFacebook: KrakenFX
providerReddit: 

redirect_from:

---

On their website we read:

> 95% of all deposits are kept in offline, air-gapped, geographically
  distributed cold storage. We keep full reserves so that you can always
  withdraw immediately on demand.

This app is an interface to a custodial exchange and therefore **not
verifiable**.
