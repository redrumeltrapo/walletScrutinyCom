---
wsId: LBank
title: "LBank - Buy Bitcoin & Crypto"
altTitle: 
authors:
- danny
appId: com.LBank.LBankNavApp
appCountry: us
idd: 1437346368
released: 2019-02-22
updated: 2021-10-10
version: "4.9.11"
stars: 4.78469
reviews: 980
size: 208022528
website: https://www.lbank.info/
repository: 
issue: 
icon: com.LBank.LBankNavApp.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: LBank_Exchange
providerLinkedIn: lbank
providerFacebook: LBank.info
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
