---
wsId: bitthumbglobal
title: "BitGlobal (ex: Bithumb Global)"
altTitle: 
authors:
- danny
appId: pro.bithumb.global
appCountry: us
idd: 1467713913
released: 2019-06-25
updated: 2021-08-09
version: "2.6.1"
stars: 2.71831
reviews: 71
size: 75458560
website: https://www.bithumb.pro/alliance
repository: 
issue: 
icon: pro.bithumb.global.jpg
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: BithumbGlobal
providerLinkedIn: bithumbglobal
providerFacebook: bithumb.global
providerReddit: BithumbGlobal

redirect_from:

---

{% include copyFromAndroid.html %}
