---
wsId: forex4you
title: "Forex4you - Online Trading"
altTitle: 
authors:
- danny
appId: com.forex4you.ios
appCountry: th
idd: 1008039704
released: 2015-10-11
updated: 2020-11-11
version: "4.2"
stars: 4.74809
reviews: 262
size: 101718016
website: https://www.forex4you.com/en/contacts/
repository: 
issue: 
icon: com.forex4you.ios.jpg
bugbounty: 
verdict: stale
date: 2021-11-07
signer: 
reviewArchive:
- date: 2021-11-01
  version: "4.2"
  appHash: 
  gitRevision: a5f6ad88ff8926faf6f2ce111aff123860ea1e50
  verdict: nosendreceive

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
