---
wsId: currencycominvesting
title: "Crypto Exchange Currency.com"
altTitle: 
authors:
- danny
appId: com.currency.exchange.prod2
appCountry: by
idd: 1458917114
released: 2019-04-23
updated: 2021-10-26
version: "1.17.4"
stars: 4.79413
reviews: 2186
size: 63082496
website: https://currency.com/
repository: 
issue: 
icon: com.currency.exchange.prod2.jpg
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: currencycom
providerLinkedIn: 
providerFacebook: currencycom
providerReddit: currencycom

redirect_from:

---

{% include copyFromAndroid.html %}
