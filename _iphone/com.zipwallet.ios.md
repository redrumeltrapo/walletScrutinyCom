---
wsId: zipwalletpay
title: "Zipwallet-Bitcoin & Send money"
altTitle: 
authors:
- danny
appId: com.zipwallet.ios
appCountry: us
idd: 1463275408
released: 2019-05-14
updated: 2021-09-05
version: "69"
stars: 1
reviews: 1
size: 59897856
website: https://zipwalletpay.com
repository: 
issue: 
icon: com.zipwallet.ios.jpg
bugbounty: 
verdict: defunct
date: 2021-10-06
signer: 
reviewArchive:
- date: 2021-09-15
  version: "69"
  appHash: 
  gitRevision: 390ad12adfd0448e851c0112bc5cc9c2a11698b4
  verdict: custodial

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

**Update 2021-09-28**: This app is not on the Store anymore.

{% include copyFromAndroid.html %}
