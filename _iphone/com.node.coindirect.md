---
wsId: coindirect
title: "Coindirect"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.node.coindirect
appCountry: 
idd: 1438224938
released: 2018-10-25
updated: 2021-07-21
version: "1.2.7"
stars: 2.8
reviews: 10
size: 34306048
website: https://www.coindirect.com/
repository: 
issue: 
icon: com.node.coindirect.jpg
bugbounty: 
verdict: defunct
date: 2021-10-22
signer: 
reviewArchive:
- date: 2021-04-27
  version: "1.2.7"
  appHash: 
  gitRevision: f9f046037c44e67715b35a4a2fbf64ab6b2244ac
  verdict: custodial
  

providerTwitter: coindirectcom
providerLinkedIn: coindirect
providerFacebook: coindirectcom
providerReddit: 

redirect_from:

---

**Update 2021-10-14**: This app is no more available.

The website states:

> The majority of our users’ Bitcoins are kept securely in cold storage. This is
  equivalent to a digital bank vault.

this leads us to conclude the wallet funds are in control of the provider and
hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
