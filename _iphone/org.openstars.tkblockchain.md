---
wsId: TrustKeys
title: "TrustKeys - TKBlockchain"
altTitle: 
authors:
- danny
appId: org.openstars.tkblockchain
appCountry: us
idd: 1584371814
released: 2021-09-06
updated: 2021-09-06
version: "1.0.0"
stars: 3.25
reviews: 4
size: 230525952
website: https://tkblockchain.net
repository: 
issue: 
icon: org.openstars.tkblockchain.jpg
bugbounty: 
verdict: defunct
date: 2021-11-10
signer: 
reviewArchive:
- date: 2021-10-07
  version: "1.0.0"
  appHash: 
  gitRevision: 1af5b9bcf87d45fe695ccccbca30a4a9d303a0f1
  verdict: nosource

providerTwitter: trustkeysglobal
providerLinkedIn: 
providerFacebook: trustkeys.network
providerReddit: 

redirect_from:

---

**Update 2021-10-30**: This app is no more.

{% include copyFromAndroid.html %}
