---
wsId: blockfolio
title: "FTX (formerly Blockfolio)"
altTitle: 
authors:
- leo
appId: com.blockfolio.blockfolio
appCountry: us
idd: 1095564685
released: 2017-01-05
updated: 2021-11-02
version: "4.2.1"
stars: 4.80244
reviews: 48972
size: 65348608
website: https://blockfolio.com
repository: 
issue: 
icon: com.blockfolio.blockfolio.jpg
bugbounty: 
verdict: custodial
date: 2021-08-09
signer: 
reviewArchive:


providerTwitter: Blockfolio
providerLinkedIn: 
providerFacebook: Blockfolio
providerReddit: 

redirect_from:

---

**Update 2021-08-09**: There are a total of 6 related apps that all appear to belong to the same "FTX":

* {% include walletLink.html wallet='android/com.blockfolio.blockfolio' %}
* {% include walletLink.html wallet='iphone/com.blockfolio.blockfolio' %}
* {% include walletLink.html wallet='android/com.ftxmobile.ftx' %}
* {% include walletLink.html wallet='iphone/org.reactjs.native.example.FTXMobile.FTX' %}
* {% include walletLink.html wallet='android/com.ftxmobile.ftxus' %}
* {% include walletLink.html wallet='iphone/com.ftx.FTXMobile.FTXUS' %}

This app clearly sounds like an exchange that lets
you buy, hold, send and receive BTC but as an exchange it's certainly custodial
and thus **not verifiable**.
