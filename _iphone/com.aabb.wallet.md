---
wsId: AABBWallet
title: "AABB Wallet"
altTitle: 
authors:
- danny
appId: com.aabb.wallet
appCountry: ca
idd: 1557298954
released: 2021-03-14
updated: 2021-11-08
version: "1.0.205"
stars: 4.95556
reviews: 90
size: 36020224
website: https://aabbgoldtoken.com/
repository: 
issue: 
icon: com.aabb.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: AsiaBroadband
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
