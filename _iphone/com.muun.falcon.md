---
wsId: muun
title: "Muun Wallet"
altTitle: 
authors:
- leo
appId: com.muun.falcon
appCountry: 
idd: 1482037683
released: 2019-10-11
updated: 2021-09-22
version: "2.3.7"
stars: 4.46377
reviews: 69
size: 80998400
website: https://www.muun.com
repository: https://github.com/muun/falcon
issue: 
icon: com.muun.falcon.jpg
bugbounty: 
verdict: nonverifiable
date: 2021-07-30
signer: 
reviewArchive:


providerTwitter: muunwallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

**Update 2021-07-30**: While the Android version made some attempts at
reproducibility, the iPhone version, which is in a different code repository,
doesn't claim to be reproducible.

# Old Analysis

This provider claims:

> Muun is a non-custodial wallet: this means you are in full control of your
  money. You remain in control of your private keys, which are stored only on
  your device, using your phone's secure enclave.

and

> Our code is in an open source repository and can be audited by anyone on the
  Internet.

So they claim the right things and we found
[their source code](https://github.com/muun/falcon) but no claims of
reproducibility so we conclude this app is **not verifiable**.
