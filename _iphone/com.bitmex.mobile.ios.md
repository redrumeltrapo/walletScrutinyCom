---
wsId: BitMEX
title: "BitMEX"
altTitle: 
authors:
- danny
appId: com.bitmex.mobile.ios
appCountry: il
idd: 1377855125
released: 2020-08-26
updated: 2021-10-28
version: "1.1.10"
stars: 
reviews: 
size: 19519488
website: https://BitMEX.com/mobile
repository: 
issue: 
icon: com.bitmex.mobile.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-10-08
signer: 
reviewArchive:


providerTwitter: bitmex
providerLinkedIn: 
providerFacebook: 
providerReddit: BitMEX

redirect_from:

---

{% include copyFromAndroid.html %}
