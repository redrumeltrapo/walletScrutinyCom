---
wsId: Coinigy
title: "Coinigy"
altTitle: 
authors:
- danny
appId: com.coinigy
appCountry: us
idd: 1317482120
released: 2018-04-28
updated: 2021-06-08
version: "0.5.30"
stars: 3.95556
reviews: 45
size: 44223488
website: https://www.coinigy.com/
repository: 
issue: 
icon: com.coinigy.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: coinigy
providerLinkedIn: coinigy
providerFacebook: coinigy
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
