---
wsId: sMiles
title: "sMiles: Bitcoin Rewards"
altTitle: 
authors:
- danny
appId: inc.standapp.sMiles
appCountry: us
idd: 1492458803
released: 2020-12-18
updated: 2021-11-05
version: "3.3"
stars: 3.54237
reviews: 118
size: 69850112
website: https://www.smilesbitcoin.com/
repository: 
issue: 
icon: inc.standapp.sMiles.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-11-04
signer: 
reviewArchive:


providerTwitter: smilesbitcoin
providerLinkedIn: 
providerFacebook: smilesbitcoin
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
