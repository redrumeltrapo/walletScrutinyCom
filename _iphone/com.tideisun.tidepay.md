---
wsId: iSunOne
title: "iSunOne"
altTitle:
authors:
- danny
appId: com.tideisun.tidepay
appCountry: us
idd: 1384802533
released: 2018-06-09
updated: 2021-04-22
version: "2.1.2"
stars: 5
reviews: 6
size: 109626368
website: https://isun.one
repository:
issue:
icon: com.tideisun.tidepay.jpg
bugbounty:
verdict: custodial
date: 2021-11-10
signer:
reviewArchive:


providerTwitter: isunone1
providerLinkedIn: isunone
providerFacebook: iSunOne
providerReddit:

redirect_from:

---
{% include copyFromAndroid.html %}
