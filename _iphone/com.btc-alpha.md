---
wsId: BTCAlpha
title: "BTC-Alpha: Buy Sell Bitcoin"
altTitle: 
authors:
- danny
appId: com.btc-alpha
appCountry: us
idd: 1437629304
released: 2019-04-20
updated: 2021-11-02
version: "1.12.4"
stars: 4.27273
reviews: 11
size: 74011648
website: https://btc-alpha.com
repository: 
issue: 
icon: com.btc-alpha.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: btcalpha
providerLinkedIn: btcalpha
providerFacebook: btcalpha
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}


