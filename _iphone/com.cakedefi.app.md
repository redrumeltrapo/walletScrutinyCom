---
wsId: CakeDeFi
title: "Cake DeFi"
altTitle: 
authors:
- danny
appId: com.cakedefi.app
appCountry: la
idd: 1564415526
released: 2021-06-15
updated: 2021-11-06
version: "2.2.1"
stars: 
reviews: 
size: 53413888
website: https://cakedefi.com/
repository: 
issue: 
icon: com.cakedefi.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-25
signer: 
reviewArchive:


providerTwitter: cakedefi
providerLinkedIn: cakedefi
providerFacebook: cakedefi
providerReddit: cakedefi

redirect_from:

---

{% include copyFromAndroid.html %}