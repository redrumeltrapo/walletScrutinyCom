---
wsId: bitpaytrading
title: "Zonda - crypto exchange"
altTitle: 
authors:
- leo
appId: com.bitbay.BitBayTrading
appCountry: 
idd: 1409644952
released: 2018-11-20
updated: 2021-11-08
version: "1.4.0"
stars: 3.56522
reviews: 23
size: 64688128
website: https://zondaglobal.com
repository: 
issue: 
icon: com.bitbay.BitBayTrading.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: BitBay
providerLinkedIn: bitbay
providerFacebook: BitBay
providerReddit: BitBayExchange

redirect_from:

---

This app's description loses no word on who holds the keys to your coins. Their
website is mainly about the exchange and not about the mobile appp but there is
[a site about that](https://bitbay.net/en/mobile), too. There they only talk
about exchange features, too and lose no word about who holds the keys which
probably means this app is a custodial offering and therefore **not verifiable**.
