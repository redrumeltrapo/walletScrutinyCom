---
wsId: tabtrader
title: "TabTrader"
altTitle: 
authors:
- leo
- kiwilamb
appId: com.tabtrader.apps.TabTrader
appCountry: 
idd: 1095716562
released: 2016-09-02
updated: 2021-11-02
version: "3.1.1"
stars: 4.73993
reviews: 4022
size: 25698304
website: https://tab-trader.com
repository: 
issue: 
icon: com.tabtrader.apps.TabTrader.jpg
bugbounty: 
verdict: nowallet
date: 2021-04-17
signer: 
reviewArchive:


providerTwitter: tabtraderpro
providerLinkedIn: tabtrader
providerFacebook: tabtrader
providerReddit: 

redirect_from:

---

This app appears to not function as a wallet. At least we could not see any
documentation about depositing or withdrawing through the app, which makes the
verdict **not a wallet** but the app still has still massive potential for abuse
if the provider front-runs the trades of the users from the insight they gain or
even worse, they could trigger lucrative-to-front-run trades the user never
intended to make.
