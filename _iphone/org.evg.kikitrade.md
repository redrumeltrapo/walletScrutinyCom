---
wsId: kikitrade
title: "Kikitrade: Crypto for Everyone"
altTitle: 
authors:
- danny
appId: org.evg.kikitrade
appCountry: us
idd: 1507120441
released: 2020-07-28
updated: 2021-11-05
version: "3.4.5"
stars: 5
reviews: 3
size: 88675328
website: https://www.kikitrade.com/
repository: 
issue: 
icon: org.evg.kikitrade.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: KikitradeHQ
providerLinkedIn: kikitrade
providerFacebook: kikitrade
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
