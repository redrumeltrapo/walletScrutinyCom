---
wsId: dfox
title: "Dfox - Wallet&DeFi Portfolio"
altTitle: 
authors:
- leo
appId: com.sixpencer.simplework
appCountry: 
idd: 1529717509
released: 2020-10-24
updated: 2021-10-25
version: "1.4.7"
stars: 4.66667
reviews: 15
size: 70266880
website: https://dfox.cc
repository: 
issue: 
icon: com.sixpencer.simplework.jpg
bugbounty: 
verdict: nowallet
date: 2021-03-22
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This app appears not to get access to spend your Bitcoins:

> Dfox is a chain-agnostic crypto portfolio tracker.
