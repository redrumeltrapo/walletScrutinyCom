---
wsId: ownbit
title: "Ownbit: Cold & MultiSig Wallet"
altTitle: 
authors:
- leo
appId: com.bitbill.wallet
appCountry: 
idd: 1321798216
released: 2018-02-07
updated: 2021-10-17
version: "4.32.1"
stars: 4.42593
reviews: 54
size: 118890496
website: http://www.bitbill.com
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
