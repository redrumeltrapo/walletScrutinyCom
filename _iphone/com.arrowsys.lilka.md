---
wsId: lilka
title: "POS LILKA"
altTitle: 
authors:
- danny
appId: com.arrowsys.lilka
appCountry: mn
idd: 1098126251
released: 2016-04-06
updated: 2019-08-02
version: "2.6.15"
stars: 
reviews: 
size: 54119424
website: 
repository: 
issue: 
icon: com.arrowsys.lilka.jpg
bugbounty: 
verdict: obsolete
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
