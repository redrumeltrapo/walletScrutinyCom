---
wsId: Bitstamp
title: "Bitstamp – crypto exchange app"
altTitle: 
authors:
- leo
appId: net.bitstamp
appCountry: 
idd: 1406825640
released: 2019-01-30
updated: 2021-10-28
version: "3.1"
stars: 4.78663
reviews: 4832
size: 104238080
website: https://www.bitstamp.net/
repository: 
issue: 
icon: net.bitstamp.jpg
bugbounty: 
verdict: custodial
date: 2021-01-23
signer: 
reviewArchive:


providerTwitter: Bitstamp
providerLinkedIn: bitstamp
providerFacebook: Bitstamp
providerReddit: 

redirect_from:

---

Just like on Play Store {% include walletLink.html wallet='android/net.bitstamp.app' %}, they claim:

> Convenient, but secure<br>
  ● We store 98% of all crypto assets in cold storage

which means you don't get the keys for your coins. This is a custodial service
and therefore **not verifiable**.
