---
wsId: ascendex
title: "AscendEX - Cryptocurrency App"
altTitle: 
authors:
- kiwilamb
- leo
appId: io.bitmax.bitmax
appCountry: 
idd: 1463917147
released: 2019-06-13
updated: 2021-11-02
version: "2.6.5"
stars: 4.96922
reviews: 2404
size: 145957888
website: 
repository: 
issue: 
icon: io.bitmax.bitmax.jpg
bugbounty: 
verdict: custodial
date: 2021-04-20
signer: 
reviewArchive:


providerTwitter: AscendEX_Global
providerLinkedIn: 
providerFacebook: AscendEXOfficial
providerReddit: AscendEX_Official

redirect_from:

---

The AscendEx mobile app claims on the website help section to manage bitcoins...

> You can withdraw your digital assets to external platforms or wallets via
  their address. Copy the address from the external platform or wallet, and
  paste it into the withdrawal address field on AscendEX to complete the
  withdrawal. 

however their is no evidence of the wallet being non-custodial, this leads us to
conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
