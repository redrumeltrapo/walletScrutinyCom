---
wsId: cryptoComDefi
title: "Crypto.com l DeFi Wallet"
altTitle: 
authors:
- leo
appId: com.defi.wallet
appCountry: 
idd: 1512048310
released: 2020-05-20
updated: 2021-11-05
version: "1.20.0"
stars: 4.47645
reviews: 1677
size: 116202496
website: https://crypto.com/defi-wallet
repository: 
issue: 
icon: com.defi.wallet.jpg
bugbounty: 
verdict: nosource
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

{% include copyFromAndroid.html %}
