---
wsId: goabra
title: "Abra: Buy Bitcoin Instantly"
altTitle: 
authors:
- leo
appId: com.goabra.abra
appCountry: 
idd: 966301394
released: 2015-03-12
updated: 2021-10-26
version: "109.0.0"
stars: 4.57076
reviews: 16676
size: 118383616
website: 
repository: 
issue: 
icon: com.goabra.abra.jpg
bugbounty: 
verdict: custodial
date: 2021-01-04
signer: 
reviewArchive:


providerTwitter: AbraGlobal
providerLinkedIn: abra
providerFacebook: GoAbraGlobal
providerReddit: 

redirect_from:

---

This is the iPhone version of the Android
{% include walletLink.html wallet='android/com.plutus.wallet' %}.

Just like the Android version, this wallet is **not verifiable**.
