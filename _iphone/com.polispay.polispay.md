---
wsId: PolisPay
title: "PolisPay - Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.polispay.polispay
appCountry: 
idd: 1351572060
released: 2019-02-20
updated: 2021-10-22
version: "8.9.3"
stars: 3.83333
reviews: 6
size: 34947072
website: https://www.polispay.com
repository: 
issue: 
icon: com.polispay.polispay.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
