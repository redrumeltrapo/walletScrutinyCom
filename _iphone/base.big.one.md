---
wsId: BigONE
title: "BigONE"
altTitle: 
authors:

appId: base.big.one
appCountry: us
idd: 1485385044
released: 2019-11-06
updated: 2021-11-05
version: "2.1.920"
stars: 4.65455
reviews: 110
size: 172178432
website: https://big.one
repository: 
issue: 
icon: base.big.one.jpg
bugbounty: 
verdict: custodial
date: 2021-09-17
signer: 
reviewArchive:


providerTwitter: BigONEexchange
providerLinkedIn: 
providerFacebook: exBigONE
providerReddit: BigONEExchange

redirect_from:

---

 {% include copyFromAndroid.html %}
