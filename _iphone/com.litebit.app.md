---
wsId: LiteBit
title: "LiteBit – Buy Bitcoin & crypto"
altTitle: 
authors:
- leo
appId: com.litebit.app
appCountry: 
idd: 1448841440
released: 2019-08-20
updated: 2021-11-08
version: "3.1.8"
stars: 4
reviews: 8
size: 72836096
website: https://www.litebit.eu/en/
repository: 
issue: 
icon: com.litebit.app.jpg
bugbounty: 
verdict: custodial
date: 2021-05-30
signer: 
reviewArchive:


providerTwitter: litebiteu
providerLinkedIn: litebit
providerFacebook: litebiteu
providerReddit: 

redirect_from:

---

> All you need is a LiteBit account.

If you need an account, it's probably custodial.

On their website there is no contrary claims so we assume this app is
**not verifiable**.
