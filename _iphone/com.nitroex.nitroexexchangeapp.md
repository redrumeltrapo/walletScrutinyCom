---
wsId: NitroEXexchangeapp
title: "NitroEx Exchange"
altTitle:
authors:
- danny
appId: com.nitroex.nitroexexchangeapp
appCountry: tt
idd: 1569267465
released: 2021-07-05
updated: 2021-07-06
version: "1.0.13"
stars:
reviews:
size: 55815168
website: https://www.nitroex.io/
repository:
issue:
icon: com.nitroex.nitroexexchangeapp.jpg
bugbounty:
verdict: custodial
date: 2021-11-10
signer:
reviewArchive:


providerTwitter: NitroExOfficial
providerLinkedIn: nitroex
providerFacebook: nitroex.io
providerReddit: nitroexchange

redirect_from:

---
{% include copyFromAndroid.html %}
