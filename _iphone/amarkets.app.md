---
wsId: AMarkets
title: "AMarkets Trading"
altTitle: 
authors:
- danny
appId: amarkets.app
appCountry: us
idd: 1495820700
released: 2020-02-12
updated: 2021-10-20
version: "1.4.20"
stars: 4.21739
reviews: 23
size: 63213568
website: https://www.amarkets.com/
repository: 
issue: 
icon: amarkets.app.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-13
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: amarkets
providerFacebook: AMarketsFirm
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
