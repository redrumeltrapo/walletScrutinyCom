---
wsId: AltcoinFantasy
title: "Altcoin Fantasy - Crypto Game"
altTitle: 
authors:
- danny
appId: com.altcoinfantasy.altcoinfantasy
appCountry: es
idd: 1356209063
released: 2018-03-14
updated: 2020-02-06
version: "2.1.3"
stars: 4
reviews: 1
size: 87098368
website: https://altcoinfantasy.com
repository: 
issue: 
icon: com.altcoinfantasy.altcoinfantasy.jpg
bugbounty: 
verdict: stale
date: 2021-11-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

