---
wsId: RocketByChiji
title: "Rocket by Chiji14xchange"
altTitle: 
authors:
- danny
appId: com.chiji14xchange-
appCountry: ng
idd: 1459183957
released: 2019-08-19
updated: 2021-05-23
version: "2.4.2"
stars: 3.99535
reviews: 215
size: 54788096
website: https://chiji14xchange.com
repository: 
issue: 
icon: com.chiji14xchange-.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: myrocketapp
providerLinkedIn: 
providerFacebook: myrocketapp
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
