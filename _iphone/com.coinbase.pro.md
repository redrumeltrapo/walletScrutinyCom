---
wsId: coinbasepro
title: "Coinbase Pro"
altTitle: 
authors:
- leo
appId: com.coinbase.pro
appCountry: 
idd: 1446636681
released: 2019-10-10
updated: 2021-10-29
version: "1.0.81"
stars: 4.7262
reviews: 136083
size: 46786560
website: https://pro.coinbase.com
repository: 
issue: 
icon: com.coinbase.pro.jpg
bugbounty: 
verdict: custodial
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: CoinbasePro
providerLinkedIn: 
providerFacebook: coinbase
providerReddit: 

redirect_from:

---

On the website we read:

> **Insurance protection**<br>
  All digital assets held in online storage are fully insured. All USD balances
  are covered by FDIC insurance, up to a maximum of $250,000 per customer.

which look purely custodial. This app is **not verifiable**.
