---
wsId: Vidulum
title: "Vidulum"
altTitle: 
authors:
- leo
appId: com.vidulum.app
appCountry: 
idd: 1505859171
released: 2020-07-28
updated: 2021-11-02
version: "1.2.6"
stars: 4.33333
reviews: 6
size: 39627776
website: https://vidulum.app
repository: 
issue: 
icon: com.vidulum.app.jpg
bugbounty: 
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: VidulumApp
providerLinkedIn: 
providerFacebook: VidulumTeam
providerReddit: VidulumOfficial

redirect_from:

---

{% include copyFromAndroid.html %}
