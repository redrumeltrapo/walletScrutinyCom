---
wsId: Zipmex
title: "Zipmex"
altTitle: 
authors:
- danny
appId: com.zipmex.app
appCountry: sg
idd: 1485647781
released: 2019-11-06
updated: 2021-11-03
version: "21.11.1"
stars: 4.29412
reviews: 85
size: 49820672
website: https://www.youtube.com/watch?v=iYI01eFjxTg
repository: 
issue: 
icon: com.zipmex.app.jpg
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: ZipmexTH
providerLinkedIn: 
providerFacebook: ZipmexThailand
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
