---
wsId: ChaingeFinance
title: "Chainge Finance"
altTitle: 
authors:
- danny
appId: com.chainge.finance.app
appCountry: us
idd: 1578987516
released: 2021-08-04
updated: 2021-10-20
version: "0.2.12"
stars: 4.25
reviews: 20
size: 54643712
website: https://www.chainge.finance/
repository: 
issue: 
icon: com.chainge.finance.app.jpg
bugbounty: 
verdict: nosource
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: FinanceChainge
providerLinkedIn: chainge-finance
providerFacebook: chainge.finance
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
