---
wsId: ECOS
title: "ECOS: invest in crypto assets"
altTitle: 
authors:
- danny
appId: am.ecos.ios.production
appCountry: us
idd: 1528964374
released: 2020-11-25
updated: 2021-11-08
version: "1.17.1"
stars: 3.88235
reviews: 17
size: 66939904
website: https://ecos.am/
repository: 
issue: 
icon: am.ecos.ios.production.jpg
bugbounty: 
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: ecosmining
providerLinkedIn: 
providerFacebook: ecosdefi
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
