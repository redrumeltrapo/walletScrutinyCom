---
wsId: Afriex
title: "Afriex - Money transfer"
altTitle: 
authors:
- danny
appId: com.afriex.afriex
appCountry: us
idd: 1492022568
released: 2020-03-06
updated: 2021-11-05
version: "11.38"
stars: 4.66733
reviews: 502
size: 54890496
website: https://afriexapp.com
repository: 
issue: 
icon: com.afriex.afriex.jpg
bugbounty: 
verdict: wip
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: afriexapp
providerLinkedIn: afriex
providerFacebook: AfriexApp
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
