---
wsId: nordFX
title: "NordFX"
altTitle: 
authors:
- danny
appId: com.nordfx.mobiletrading
appCountry: lv
idd: 1551642767
released: 2021-02-06
updated: 2021-11-05
version: "13.0"
stars: 5
reviews: 1
size: 29602816
website: https://nordfx.com/
repository: 
issue: 
icon: com.nordfx.mobiletrading.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: NordFX
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
