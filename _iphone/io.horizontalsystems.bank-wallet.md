---
wsId: Unstoppable
title: "Unstoppable Wallet"
altTitle: 
authors:
- leo
appId: io.horizontalsystems.bank-wallet
appCountry: 
idd: 1447619907
released: 2019-01-10
updated: 2021-08-16
version: "0.22"
stars: 4.66667
reviews: 330
size: 41478144
website: https://unstoppable.money/
repository: https://github.com/horizontalsystems/unstoppable-wallet-ios
issue: 
icon: io.horizontalsystems.bank-wallet.jpg
bugbounty: 
verdict: nonverifiable
date: 2020-12-19
signer: 
reviewArchive:


providerTwitter: unstoppablebyhs
providerLinkedIn: 
providerFacebook: 
providerReddit: UNSTOPPABLEWallet

redirect_from:

---

The provider claims:

> A non-custodial wallet without third party risk.

and we found the source code
[here](https://github.com/horizontalsystems/unstoppable-wallet-ios)
but so far nobody reproduced the build, so the claim is **not verifiable**.
