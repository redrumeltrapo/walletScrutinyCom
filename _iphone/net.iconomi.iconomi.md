---
wsId: iconomi
title: "ICONOMI: Buy and Sell Crypto"
altTitle: 
authors:
- danny
appId: net.iconomi.iconomi
appCountry: si
idd: 1238213050
released: 2017-05-25
updated: 2021-11-03
version: "2.0.9"
stars: 4.66216
reviews: 74
size: 77685760
website: http://www.iconomi.com
repository: 
issue: 
icon: net.iconomi.iconomi.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: iconomicom
providerLinkedIn: iconominet
providerFacebook: iconomicom
providerReddit: ICONOMI

redirect_from:

---

{% include copyFromAndroid.html %}
