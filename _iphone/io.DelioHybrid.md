---
wsId: DelioLending
title: "델리오 - Delio"
altTitle: 
authors:
- danny
appId: io.DelioHybrid
appCountry: kr
idd: 1498891184
released: 2020-02-26
updated: 2021-11-02
version: "1.3.3"
stars: 4.25
reviews: 12
size: 74057728
website: https://www.delio.foundation/
repository: 
issue: 
icon: io.DelioHybrid.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: happydelio
providerLinkedIn: 
providerFacebook: delio.io
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
