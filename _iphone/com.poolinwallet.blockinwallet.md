---
wsId: PoolinWallet
title: "Poolin Wallet: Bitcoin"
altTitle: 
authors:
- danny
appId: com.poolinwallet.blockinwallet
appCountry: us
idd: 1495275337
released: 2020-01-23
updated: 2021-11-02
version: "2.1.5"
stars: 4
reviews: 22
size: 68348928
website: https://poolin.fi/
repository: 
issue: 
icon: com.poolinwallet.blockinwallet.jpg
bugbounty: 
verdict: custodial
date: 2021-10-07
signer: 
reviewArchive:


providerTwitter: PoolinWallet
providerLinkedIn: poolinwallet
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
