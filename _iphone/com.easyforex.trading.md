---
wsId: easyMarkets
title: "easyMarkets Online Trading"
altTitle: 
authors:
- danny
appId: com.easyforex.trading
appCountry: us
idd: 348823316
released: 2010-01-05
updated: 2021-11-01
version: "4.28"
stars: 4.63968
reviews: 247
size: 153959424
website: https://www.easymarkets.com/int/platforms/easymarkets-mobile-app/
repository: 
issue: 
icon: com.easyforex.trading.jpg
bugbounty: 
verdict: custodial
date: 2021-09-01
signer: 
reviewArchive:


providerTwitter: easymarkets
providerLinkedIn: easymarkets
providerFacebook: easyMarkets
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
