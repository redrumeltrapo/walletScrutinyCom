---
wsId: ONTO
title: "ONTO-Cross-chain Crypto Wallet"
altTitle: 
authors:
- danny
appId: com.ontology.foundation.onto
appCountry: us
idd: 1436009823
released: 2018-09-21
updated: 2021-10-29
version: "4.1.0"
stars: 4.2459
reviews: 61
size: 216051712
website: https://www.onto.app
repository: 
issue: 
icon: com.ontology.foundation.onto.jpg
bugbounty: 
verdict: nosource
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: ONTOWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}