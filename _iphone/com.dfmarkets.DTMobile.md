---
wsId: DeltaTrading
title: "Delta Trading–FX & Stock CFDs"
altTitle: 
authors:
- danny
appId: com.dfmarkets.DTMobile
appCountry: bg
idd: 649221938
released: 2013-05-22
updated: 2021-08-02
version: "5.9"
stars: 4.59999
reviews: 25
size: 3863552
website: https://www.deltastock.com/english/platforms/delta_trading-mobile.asp
repository: 
issue: 
icon: com.dfmarkets.DTMobile.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-13
signer: 
reviewArchive:


providerTwitter: deltastock
providerLinkedIn: deltastock-ad
providerFacebook: Deltastock
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
