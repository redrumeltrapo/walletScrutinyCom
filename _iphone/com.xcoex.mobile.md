---
wsId: XCOEX
title: "XCOEX: Cryptocurrency Exchange"
altTitle: 
authors:
- kiwilamb
appId: com.xcoex.mobile
appCountry: 
idd: 1447945810
released: 2019-01-22
updated: 2021-07-13
version: "1.18.0"
stars: 
reviews: 
size: 54763520
website: https://xcoex.com/
repository: 
issue: 
icon: com.xcoex.mobile.jpg
bugbounty: 
verdict: defunct
date: 2021-10-03
signer: 
reviewArchive:
- date: 2021-05-04
  version: "1.18.0"
  appHash: 
  gitRevision: e438eb905262a9e88382146da027048d1b346da6
  verdict: custodial
  

providerTwitter: OfficialXcoex
providerLinkedIn: 
providerFacebook: xcoex
providerReddit: 

redirect_from:

---

**Update 2021-09-24**: This app is not on App Store anymore.

This is an exchange based app wallet, meaning it is mainly developed to manage trading on an exchange.
The exchange provider typically stores users bitcoins, partly in cold storage, partly hot.

This leads us to conclude the wallet funds are in control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
