---
wsId: bit2me
title: "Bit2Me - Buy Bitcoin"
altTitle: 
authors:
- leo
appId: com.bit2me.flutter-ios
appCountry: 
idd: 1459809738
released: 2019-06-04
updated: 2021-11-05
version: "2.1.13"
stars: 4.57576
reviews: 33
size: 86654976
website: https://bit2me.com
repository: 
issue: 
icon: com.bit2me.flutter-ios.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: bit2me
providerLinkedIn: bit2me
providerFacebook: bit2me
providerReddit: 

redirect_from:

---

This appears to be the interface for an exchange. We could not find any claims
about you owning your keys. As a custodial service it is **not verifiable**.
