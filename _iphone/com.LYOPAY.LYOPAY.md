---
wsId: lyopay
title: "LYOPAY"
altTitle: 
authors:
- danny
appId: com.LYOPAY.LYOPAY
appCountry: eg
idd: 1537945402
released: 2020-11-06
updated: 2021-11-05
version: "7.1"
stars: 5
reviews: 1
size: 26099712
website: https://lyopay.com/
repository: 
issue: 
icon: com.LYOPAY.LYOPAY.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: lyopayofficial
providerLinkedIn: lyopay
providerFacebook: lyopayofficial
providerReddit: LYOPAY

redirect_from:

---

{% include copyFromAndroid.html %}
