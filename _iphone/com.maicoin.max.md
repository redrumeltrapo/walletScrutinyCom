---
wsId: maxmaicoin
title: "MAX Exchange - Buy Bitcoin"
altTitle: 
authors:
- danny
appId: com.maicoin.max
appCountry: us
idd: 1370837255
released: 2018-07-04
updated: 2021-08-12
version: "3.0.0"
stars: 4.78946
reviews: 19
size: 96899072
website: https://max.maicoin.com
repository: 
issue: 
icon: com.maicoin.max.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: Max_exch
providerLinkedIn: 
providerFacebook: MaiCoinAssetExchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
