---
wsId: bacoorhbwallet
title: "Ethereum Wallet - HB Wallet"
altTitle: 
authors:
- danny
appId: co.bacoor.ios.hbwallet
appCountry: us
idd: 1273639572
released: 2017-08-23
updated: 2021-07-09
version: "3.5.0"
stars: 4.51299
reviews: 154
size: 81245184
website: https://www.bacoor.io
repository: 
issue: 
icon: co.bacoor.ios.hbwallet.jpg
bugbounty: 
verdict: nobtc
date: 2021-09-03
signer: 
reviewArchive:


providerTwitter: HBWallet_Ether
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}