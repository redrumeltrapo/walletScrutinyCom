---
wsId: swft
title: "SWFT Blockchain"
altTitle: 
authors:
- leo
appId: com.SwftBlockChain.client
appCountry: 
idd: 1435569339
released: 2018-09-28
updated: 2021-10-23
version: "5.13.17"
stars: 4.45902
reviews: 122
size: 117955584
website: http://www.swft.pro
repository: 
issue: 
icon: com.SwftBlockChain.client.jpg
bugbounty: 
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: SwftCoin
providerLinkedIn: swftcoin
providerFacebook: SWFTBlockchain
providerReddit: 

redirect_from:

---

The description on App Store is full of buzzwords like big data, machine
learning but no clear words on weather this wallet is custodial or not. Given
its strong emphasis on speed and many coins and no words on the usual seed words,
we have to assume it is indeed custodial. Their [FAQ](https://www.swft.pro/#/FAQ)
also sounds more like a custodial exchange than a wallet. This app is certainly
**not verifiable**.
