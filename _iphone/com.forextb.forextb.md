---
wsId: ForexTB
title: "ForexTB: Online Trading"
altTitle: 
authors:
- danny
appId: com.forextb.forextb
appCountry: dk
idd: 1532850884
released: 2020-10-21
updated: 2021-10-14
version: "2.4.5"
stars: 
reviews: 
size: 131527680
website: https://www.forextb.com/
repository: 
issue: 
icon: com.forextb.forextb.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
