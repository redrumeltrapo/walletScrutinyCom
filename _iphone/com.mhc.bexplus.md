---
wsId: Bexplus
title: "Bexplus- Crypto Margin Trading"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.mhc.bexplus
appCountry: 
idd: 1442189260
released: 2018-11-29
updated: 2021-10-20
version: "2.1.5"
stars: 4.8512
reviews: 1371
size: 78017536
website: https://www.bexplus.com/activity/cash_back
repository: 
issue: 
icon: com.mhc.bexplus.jpg
bugbounty: 
verdict: custodial
date: 2021-04-24
signer: 
reviewArchive:


providerTwitter: BexplusExchange
providerLinkedIn: 
providerFacebook: 
providerReddit: Bexplus

redirect_from:

---

The Bexplus website states under the mobile wallet section "Assets Security"

> Assets are stored in cold storage against stealing and loss

this leads us to conclude the wallet funds are in control of the provider and
hence custodial.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.
