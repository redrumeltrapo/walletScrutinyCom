---
wsId: CoinsField
title: "CoinField"
altTitle: 
authors:
- danny
appId: com.coinfield.csapp
appCountry: ca
idd: 1460170928
released: 2019-05-21
updated: 2021-07-27
version: "2.6.0"
stars: 4.43004
reviews: 486
size: 68434944
website: https://www.coinfield.com
repository: 
issue: 
icon: com.coinfield.csapp.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: CoinFieldEx
providerLinkedIn: coinfield
providerFacebook: coinfieldexchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
