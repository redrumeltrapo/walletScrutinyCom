---
wsId: CoinSwitch
title: "CoinSwitch"
altTitle: 
authors:
- danny
appId: com.coinswitch.kuber
appCountry: in
idd: 1540214951
released: 2020-12-01
updated: 2021-10-18
version: "3.1.1"
stars: 4.35767
reviews: 25157
size: 61172736
website: https://coinswitch.co/in
repository: 
issue: 
icon: com.coinswitch.kuber.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: CoinSwitchKuber
providerLinkedIn: coinswitch
providerFacebook: coinswitch
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
