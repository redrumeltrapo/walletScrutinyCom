---
wsId: cryptocomexchange
title: "Crypto.com Exchange"
altTitle: 
authors:
- leo
- danny
appId: crypto.com.exchange
appCountry: nz
idd: 1569309855
released: 2021-06-15
updated: 2021-11-04
version: "1.4.1"
stars: 4.5
reviews: 2
size: 45863936
website: https://crypto.com/exchange
repository: 
issue: 
icon: crypto.com.exchange.jpg
bugbounty: 
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: cryptocom
providerLinkedIn: cryptocom
providerFacebook: CryptoComOfficial
providerReddit: Crypto_com

redirect_from:

---

{% include copyFromAndroid.html %}