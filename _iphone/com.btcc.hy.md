---
wsId: btcc
title: "BTCC-Trade Bitcoin&Crypto CFD"
altTitle: 
authors:
- danny
appId: com.btcc.hy
appCountry: us
idd: 1462880009
released: 2019-05-11
updated: 2021-10-22
version: "5.6.1"
stars: 3.2
reviews: 5
size: 86861824
website: https://www.btcc.com/
repository: 
issue: 
icon: com.btcc.hy.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: YourBTCC
providerLinkedIn: yourbtcc
providerFacebook: yourbtcc
providerReddit: YourBTCC

redirect_from:

---

 {% include copyFromAndroid.html %}
