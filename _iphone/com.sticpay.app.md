---
wsId: STICPAY
title: "STICPAY"
altTitle: 
authors:
- danny
appId: com.sticpay.app
appCountry: us
idd: 1274956968
released: 2017-09-05
updated: 2021-10-06
version: "3.41"
stars: 4.2
reviews: 5
size: 32781312
website: https://www.sticpay.com/
repository: 
issue: 
icon: com.sticpay.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: SticPay
providerLinkedIn: sticpay
providerFacebook: sticpay.global
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
