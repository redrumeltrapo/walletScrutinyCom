---
wsId: cryptonator
title: "Cryptonator"
altTitle: 
authors:
- leo
appId: com.cryptonator.ios
appCountry: 
idd: 1463793201
released: 2019-06-11
updated: 2021-01-27
version: "4.1.4"
stars: 2.08
reviews: 25
size: 79916032
website: https://www.cryptonator.com
repository: 
issue: 
icon: com.cryptonator.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive:


providerTwitter: cryptonatorcom
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

Cryptonator cryptocurrency wallet makes no claim to be non-custodial but the
[Customer Support](https://www.cryptonator.com/contact/other/)
is pretty unambiguously pointing towards it being custodial:

> **Do you provide private keys?**: No

Absent source code this wallet is **not verifiable**.
