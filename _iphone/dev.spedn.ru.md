---
wsId: Koshelek
title: "Кошелек. Криптовалюты & Токены"
altTitle: 
authors:
- danny
appId: dev.spedn.ru
appCountry: ru
idd: 1524167720
released: 2020-08-05
updated: 2021-10-15
version: "1.3.0"
stars: 4.63636
reviews: 44
size: 55341056
website: https://koshelek.ru/
repository: 
issue: 
icon: dev.spedn.ru.jpg
bugbounty: 
verdict: custodial
date: 2021-11-04
signer: 
reviewArchive:


providerTwitter: koshelek_ru
providerLinkedIn: 
providerFacebook: koshelekru
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
