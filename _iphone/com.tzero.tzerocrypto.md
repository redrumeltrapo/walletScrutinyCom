---
wsId: tzerocrypto
title: "tZERO Crypto: BTC, ETH, & more"
altTitle: 
authors:
- danny
appId: com.tzero.tzerocrypto
appCountry: us
idd: 1468985150
released: 2019-06-28
updated: 2021-11-09
version: "2.0.6"
stars: 4.6875
reviews: 704
size: 69697536
website: https://www.tzero.com/crypto-app
repository: 
issue: 
icon: com.tzero.tzerocrypto.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
