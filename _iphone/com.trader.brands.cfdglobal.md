---
wsId: capex
title: "Capex.com"
altTitle: 
authors:
- danny
appId: com.trader.brands.cfdglobal
appCountry: gb
idd: 1304998844
released: 2018-02-03
updated: 2021-11-01
version: "1.30"
stars: 5
reviews: 8
size: 109659136
website: http://www.capex.com/
repository: 
issue: 
icon: com.trader.brands.cfdglobal.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: capex_en
providerLinkedIn: capexglobal
providerFacebook: CAPEXSeychelles
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
