---
wsId: Bidesk
title: "Bidesk App - Crypto Trading"
altTitle: 
authors:
- danny
appId: com.bideskexchange.app
appCountry: us
idd: 1495664797
released: 2020-02-03
updated: 2021-03-28
version: "3.1.2"
stars: 3.5
reviews: 12
size: 69810176
website: https://www.bidesk.com/
repository: 
issue: 
icon: com.bideskexchange.app.jpg
bugbounty: 
verdict: defunct
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: bideskcom
providerLinkedIn: 
providerFacebook: bideskcom
providerReddit: bidesk 

redirect_from:

---


{% include copyFromAndroid.html %}
