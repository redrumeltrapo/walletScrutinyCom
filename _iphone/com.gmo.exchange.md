---
wsId: ZcomEx
title: "Z.com EX - Buy/Sell Bitcoin"
altTitle: 
authors:
- danny
appId: com.gmo.exchange
appCountry: th
idd: 1525862502
released: 2020-08-09
updated: 2021-11-05
version: "1.1.9"
stars: 3.46154
reviews: 13
size: 107071488
website: https://ex.z.com/
repository: 
issue: 
icon: com.gmo.exchange.jpg
bugbounty: 
verdict: custodial
date: 2021-11-08
signer: 
reviewArchive:


providerTwitter: ZcomExchange
providerLinkedIn: 
providerFacebook: ZcomCrypto
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
