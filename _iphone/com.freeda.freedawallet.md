---
wsId: freeda
title: "Freeda Wallet"
altTitle: 
authors:
- danny
appId: com.freeda.freedawallet
appCountry: us
idd: 1545428547
released: 2021-03-17
updated: 2021-09-17
version: "1.6.11"
stars: 5
reviews: 23
size: 84263936
website: https://www.freeda.io/
repository: 
issue: 
icon: com.freeda.freedawallet.jpg
bugbounty: 
verdict: custodial
date: 2021-11-08
signer: 
reviewArchive:


providerTwitter: FreedaWallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
