---
wsId: AOFEX
title: "aofex-buy & sell bitcoin"
altTitle: 
authors:
- danny
appId: com.aofex.exchange1
appCountry: us
idd: 1477466894
released: 2019-09-19
updated: 2021-11-05
version: "3.0.6"
stars: 4.93689
reviews: 713
size: 238366720
website: https://www.aofex.com/#/
repository: 
issue: 
icon: com.aofex.exchange1.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: Aofex2
providerLinkedIn: 
providerFacebook: AofexDigitalCurrencyExchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

