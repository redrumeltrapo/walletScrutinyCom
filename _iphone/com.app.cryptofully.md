---
wsId: cryptofully
title: "Cryptofully"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.app.cryptofully
appCountry: 
idd: 1533929589
released: 2020-11-15
updated: 2021-09-15
version: "1.3.2"
stars: 4.60417
reviews: 48
size: 50038784
website: https://www.cryptofully.com
repository: 
issue: 
icon: com.app.cryptofully.jpg
bugbounty: 
verdict: nowallet
date: 2021-04-18
signer: 
reviewArchive:


providerTwitter: cryptofully
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This mobile app is an exchange solution aimed at transferring money into Nigerian bank accounts.
The user can use other Bitcoin wallets to send BTC to receive addresses in the
app to initiate deposits to Nigerian Bank accounts.

It is not designed to store BTC, thus is **not a wallet**.
