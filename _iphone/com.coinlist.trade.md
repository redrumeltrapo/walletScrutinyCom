---
wsId: CoinList
title: "CoinList"
altTitle: 
authors:
- danny
appId: com.coinlist.trade
appCountry: us
idd: 1522706079
released: 2020-08-07
updated: 2021-05-19
version: "2.0.3"
stars: 2.54622
reviews: 119
size: 29948928
website: https://coinlist.co
repository: 
issue: 
icon: com.coinlist.trade.jpg
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: coinlist
providerLinkedIn: 
providerFacebook: CoinListOfficial
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
