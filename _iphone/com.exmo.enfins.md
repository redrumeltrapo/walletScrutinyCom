---
wsId: exmo
title: "EXMO Cryptocurrency Exchange"
altTitle: 
authors:
- kiwilamb
- leo
appId: com.exmo.enfins
appCountry: 
idd: 1505496232
released: 2020-04-07
updated: 2021-11-04
version: "2.15.2"
stars: 3.67742
reviews: 31
size: 53526528
website: https://exmo.com/en
repository: 
issue: 
icon: com.exmo.enfins.jpg
bugbounty: 
verdict: custodial
date: 2021-04-21
signer: 
reviewArchive:


providerTwitter: Exmo_com
providerLinkedIn: 
providerFacebook: exmo.market
providerReddit: 

redirect_from:

---

The Exmo [support FAQ](https://info.exmo.com/en/faq/) states under "Where are my
EXMO funds kept?"

> Users cryptocurrency funds are stored on the exchange’s crypto wallets: cold
  and hot vaults.

this leads us to conclude the wallet funds are in control of the provider.

Our verdict: This 'wallet' is **not verifiable**.
