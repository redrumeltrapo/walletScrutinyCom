---
wsId: LVLMoneyApp
title: "LVL - The Money App"
altTitle: 
authors:
- danny
appId: co.lvl.firstapp
appCountry: us
idd: 1494647317
released: 2020-07-08
updated: 2021-09-13
version: "1.0.5"
stars: 4.19292
reviews: 311
size: 116187136
website: https://www.lvl.co
repository: 
issue: 
icon: co.lvl.firstapp.jpg
bugbounty: 
verdict: custodial
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: BankWithLVL
providerLinkedIn: bankwithlvl
providerFacebook: BankWithLVL
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
