---
wsId: Nitrobot
title: "NitroBot - Automated Trading"
altTitle: 
authors:
- danny
appId: com.nitroex.nitrobotapp
appCountry: us
idd: 1541146988
released: 2020-12-09
updated: 2020-12-14
version: "2.0"
stars: 1
reviews: 1
size: 82761728
website: https://www.nitroex.io
repository: 
issue: 
icon: com.nitroex.nitrobotapp.jpg
bugbounty: 
verdict: nowallet
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: NitroExOfficial
providerLinkedIn: nitroex
providerFacebook: nitroex.io
providerReddit: nitroexchange

redirect_from:

---

{% include copyFromAndroid.html %}
