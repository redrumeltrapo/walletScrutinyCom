---
wsId: GenesisVision
title: "Genesis Vision"
altTitle: 
authors:
- danny
appId: vision.genesis.ios.investor
appCountry: us
idd: 1369865290
released: 2018-04-23
updated: 2021-04-20
version: "2.2.3"
stars: 4.85417
reviews: 144
size: 55239680
website: https://genesis.vision/
repository: 
issue: 
icon: vision.genesis.ios.investor.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: genesis_vision
providerLinkedIn: genesis-vision
providerFacebook: GenesisVisionProject
providerReddit: genesisvision

redirect_from:

---

{% include copyFromAndroid.html %}
