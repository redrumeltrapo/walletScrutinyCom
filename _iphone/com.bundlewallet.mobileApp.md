---
wsId: africa.bundle
title: "Bundle Africa"
altTitle: 
authors:
- kiwilamb
appId: com.bundlewallet.mobileApp
appCountry: 
idd: 1506502199
released: 2020-06-03
updated: 2021-07-17
version: "3.0.0"
stars: 4.33654
reviews: 208
size: 100101120
website: https://bundle.africa/
repository: 
issue: 
icon: com.bundlewallet.mobileApp.jpg
bugbounty: 
verdict: defunct
date: 2021-09-08
signer: 
reviewArchive:
- date: 2021-04-19
  version: "3.0.0"
  appHash: 
  gitRevision: 4885e4bdcce2af277a6f0d609d2428430e0f61e6
  verdict: custodial
  

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

**Update 2021-08-31**: This app is no more available.

The [Bundle website](https://bundle.africa/) claims to be a wallet from their description...

> Send, receive, request naria and other ctyptocurrenices for free.

so it claims to manage BTC, however their is no evidence of the wallet being non-custodial, with no source code repository listed or found...

Our verdict: This 'wallet' is probably custodial but does not provide public source and therefore is **not verifiable**.
