---
wsId: bhexExchange
title: "BHEX"
altTitle:
authors:
- danny
appId: io.bhex.app
appCountry: us
idd: 1441395245
released: 2018-12-13
updated: 2021-10-11
version: "4.1.2"
stars: 3.17143
reviews: 35
size: 93606912
website: https://www.hbtc.com/
repository:
issue:
icon: io.bhex.app.jpg
bugbounty:
verdict: defunct
date: 2021-11-08
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
{% include copyFromAndroid.html %}
