---
wsId: atomic
title: "Atomic Wallet"
altTitle: 
authors:
- leo
appId: atomicwallet
appCountry: 
idd: 1478257827
released: 2019-11-05
updated: 2021-10-06
version: "0.78.7"
stars: 4.27901
reviews: 13365
size: 49713152
website: https://atomicwallet.io/
repository: 
issue: 
icon: atomicwallet.jpg
bugbounty: 
verdict: nosource
date: 2021-01-23
signer: 
reviewArchive:


providerTwitter: atomicwallet
providerLinkedIn: 
providerFacebook: atomicwallet
providerReddit: 

redirect_from:

---

> Atomic Wallet is a universal, fully decentralized, multi-currency, and
  convenient app with a simple interface that supports over 300
  cryptocurrencies.

so they claim to be non-custodial but although they feature a link to
[their GitHub account](https://github.com/Atomicwallet), none of the
repositories there looks like an iPhone wallet so the app is **not verifiable**.
