---
wsId: Fiboda
title: "Fiboda - Copy trading platform"
altTitle: 
authors:
- danny
appId: com.fiboda.Fiboda
appCountry: us
idd: 1540952335
released: 2021-02-09
updated: 2021-07-09
version: "1.1.0"
stars: 4.2
reviews: 5
size: 89354240
website: http://fiboda.com
repository: 
issue: 
icon: com.fiboda.Fiboda.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
