---
wsId: whitebit
title: "WhiteBIT:Buy bitcoin securel‪y"
altTitle: 
authors:
- danny
appId: com.whitebit.whitebitapp
appCountry: ua
idd: 1463405025
released: 2019-05-21
updated: 2021-11-03
version: "2.0"
stars: 4.62263
reviews: 106
size: 126968832
website: https://whitebit.com
repository: 
issue: 
icon: com.whitebit.whitebitapp.jpg
bugbounty: 
verdict: custodial
date: 2021-09-10
signer: 
reviewArchive:


providerTwitter: whitebit
providerLinkedIn: whitebit-cryptocurrency-exchange
providerFacebook: whitebit
providerReddit: WhiteBitExchange

redirect_from:

---

{% include copyFromAndroid.html %}