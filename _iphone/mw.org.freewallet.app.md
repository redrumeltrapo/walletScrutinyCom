---
wsId: mwfreewallet
title: "Multi Crypto Wallet－Freewallet"
altTitle: 
authors:
- leo
appId: mw.org.freewallet.app
appCountry: 
idd: 1274003898
released: 2017-09-01
updated: 2021-10-23
version: "1.16.5"
stars: 4.11015
reviews: 1516
size: 45300736
website: https://freewallet.org
repository: 
issue: 
icon: mw.org.freewallet.app.jpg
bugbounty: 
verdict: custodial
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: freewalletorg
providerLinkedIn: 
providerFacebook: freewallet.org
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
