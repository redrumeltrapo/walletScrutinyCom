---
wsId: OneKey
title: "OneKey Wallet: Crypto & DeFi"
altTitle: 
authors:
- danny
appId: com.onekey.wallet
appCountry: us
idd: 1568432215
released: 2021-06-01
updated: 2021-11-02
version: "2.12.1"
stars: 4.75
reviews: 16
size: 245541888
website: https://www.onekey.so
repository: https://github.com/OneKeyHQ/wallet-deprecated-
issue: 
icon: com.onekey.wallet.jpg
bugbounty: 
verdict: wip
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: OneKeyHQ
providerLinkedIn: 
providerFacebook: 
providerReddit: OneKeyHQ

redirect_from:

---

{% include copyFromAndroid.html %}
