---
wsId: MoonXBT
title: "MoonXBT - Trade BTC Up To 150X"
altTitle: 
authors:
- danny
appId: com.nano.moonxbt
appCountry: us
idd: 1566536854
released: 2021-06-08
updated: 2021-06-08
version: "2.2.1"
stars: 4
reviews: 4
size: 49402880
website: https://www.moonxbt.com
repository: 
issue: 
icon: com.nano.moonxbt.jpg
bugbounty: 
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: MoonXBT_Global
providerLinkedIn: 
providerFacebook: MoonXBT
providerReddit: MoonXBT

redirect_from:

---

{% include copyFromAndroid.html %}

