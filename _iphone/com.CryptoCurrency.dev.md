---
wsId: crypterApp
title: "Crypto App - Widgets, Alerts"
altTitle: 
authors:
- danny
appId: com.CryptoCurrency.dev
appCountry: us
idd: 1339112917
released: 2018-02-21
updated: 2021-06-22
version: "2.3.2"
stars: 4.74986
reviews: 1743
size: 70728704
website: https://thecrypto.app
repository: 
issue: 
icon: com.CryptoCurrency.dev.jpg
bugbounty: 
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive:


providerTwitter: TrustSwap
providerLinkedIn: TrustSwap
providerFacebook: TrustSwap
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
