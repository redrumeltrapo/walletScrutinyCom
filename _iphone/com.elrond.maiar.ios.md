---
wsId: maiar
title: "Maiar Browser"
altTitle: 
authors:
- danny
appId: com.elrond.maiar.ios
appCountry: ro
idd: 1458612260
released: 2020-03-05
updated: 2020-07-23
version: "0.1.3"
stars: 4.9278
reviews: 277
size: 74845184
website: https://browser.maiar.com/
repository: 
issue: 
icon: com.elrond.maiar.ios.jpg
bugbounty: 
verdict: stale
date: 2021-08-27
signer: 
reviewArchive:
- date: 2021-08-27
  version: "0.1.3"
  appHash: 
  gitRevision: a9ddd343442f032ffecc0fd75774198ca8fc2501
  verdict: nosource

providerTwitter: getMaiar
providerLinkedIn: getmaiar
providerFacebook: getMaiar
providerReddit: 

redirect_from:

---

> Maiar is a digital wallet and global payments app that is reimagining the way we interact with money, allowing you to exchange and securely store crypto on your mobile phone.

> Create a wallet in seconds. No username, no password, no recovery phrase to backup. Just use your phone number.


It's rather unclear if this means there's *no recovery phrase provided at all* or if it's simply optional.

> **Control your crypto. Be your own bank.** At Maiar we help you protect your crypto with the highest level of security via cryptography and privacy features.

> We're approaching security through a process called progressive security. When having nothing to lose, you start light, and gradually receive security suggestions proportional to the assets you store. The more you use the app, the more sophisticated security features are triggered, to help you stay safe and secure.

This seemed to be a relevant piece of information regarding security.

Maiar.com has an FAQ on the homepage.

> [**Is Maiar a custodial or non-custodial wallet?**](https://maiar.com/)

> The Maiar wallet is completely decentralised and non-custodial. We don't store or have access to the user's funds at any time. The user owns the private key and has full control over their funds.

> **How secure is Maiar?**

> Maiar protects your assets and data using cutting edge encryption and verification techniques to ensure they're securely stored. In the event where your phone is lost or stolen, your private key (recovery phrase) can be safely used to recover your funds.

In this case, Maiar is **non-custodial.** 

As there's no accessible source code, this is **not verifiable.**