---
wsId: DeltaExchange
title: "Delta Exchange: Crypto Trading"
altTitle: 
authors:
- danny
appId: exchange.delta.app
appCountry: nz
idd: 1567218518
released: 2021-06-05
updated: 2021-10-06
version: "1.4.4"
stars: 
reviews: 
size: 21483520
website: https://www.delta.exchange/
repository: 
issue: 
icon: exchange.delta.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-08
signer: 
reviewArchive:


providerTwitter: Delta_Exchange
providerLinkedIn: 
providerFacebook: deltaexchg
providerReddit: Delta_Exchange

redirect_from:

---

{% include copyFromAndroid.html %}
