---
wsId: OWNR
title: "OWNR crypto wallet for PC"
altTitle: 
authors:
- leo
appId: com.ownrwallet.desktop
appCountry: 
idd: 1520395378
released: 2020-08-13
updated: 2021-11-02
version: "2.0.2"
stars: 
reviews: 
size: 113166306
website: https://ownrwallet.com
repository: 
issue: 
icon: com.ownrwallet.desktop.png
bugbounty: 
verdict: nosource
date: 2021-05-31
signer: 
reviewArchive:


providerTwitter: ownrwallet
providerLinkedIn: 
providerFacebook: ownrwallet
providerReddit: ownrwallet

redirect_from:

---

