---
wsId: swissborg
title: "SwissBorg - Invest in Crypto"
altTitle: 
authors:
- danny
appId: com.swissborg.ios
appCountry: gb
idd: 1442483481
released: 2020-03-31
updated: 2021-09-23
version: "1.22.2"
stars: 4.22391
reviews: 786
size: 64336896
website: https://swissborg.com
repository: 
issue: 
icon: com.swissborg.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: swissborg
providerLinkedIn: swissborg
providerFacebook: swissborg
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}