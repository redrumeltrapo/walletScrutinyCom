---
wsId: dcoin
title: "Dcoin - Trade Bitcoin,Ethereum"
altTitle: 
authors:
- danny
appId: com.dcoin.ios
appCountry: us
idd: 1508064925
released: 2018-12-20
updated: 2021-10-19
version: "4.4.1"
stars: 3.3125
reviews: 16
size: 74135552
website: https://www.dcoin.com/
repository: 
issue: 
icon: com.dcoin.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: dcoinexchange
providerLinkedIn: dcoin-exchange
providerFacebook: 
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
