---
wsId: Coinsuper
title: "Coinsuper - Exchange"
altTitle: 
authors:
- danny
appId: com.coinsuper.app
appCountry: us
idd: 1346980481
released: 2018-02-20
updated: 2021-11-04
version: "2.4.13"
stars: 4
reviews: 13
size: 57735168
website: https://www.coinsuper.com/
repository: 
issue: 
icon: com.coinsuper.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-08
signer: 
reviewArchive:


providerTwitter: Coinsuper_OFCL
providerLinkedIn: 
providerFacebook: CoinsuperOFCL
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
