---
wsId: fibermode
title: "Mode: Pay, earn & grow Bitcoin"
altTitle: 
authors:
- danny
appId: com.fibermode.Mode-Wallet
appCountry: gb
idd: 1483284435
released: 2019-11-26
updated: 2021-10-27
version: "5.3.1"
stars: 4.40573
reviews: 907
size: 42048512
website: https://www.modeapp.com
repository: 
issue: 
icon: com.fibermode.Mode-Wallet.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: modeapp_
providerLinkedIn: modeapp-com
providerFacebook: themodeapp
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
