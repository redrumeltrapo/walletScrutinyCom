---
wsId: TitanCoin
title: "Titan Coin"
altTitle: 
authors:
- danny
appId: com.coin.titan
appCountry: us
idd: 1492432350
released: 2021-04-22
updated: 2021-10-29
version: "1.25.0"
stars: 1.5
reviews: 2
size: 87212032
website: https://titanprojects.co
repository: 
issue: 
icon: com.coin.titan.jpg
bugbounty: 
verdict: custodial
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: ProjectsTitan
providerLinkedIn: titanprojects
providerFacebook: titanprojectsco
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

