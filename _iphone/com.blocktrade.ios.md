---
wsId: BlockTrade
title: "Blocktrade"
altTitle: 
authors:
- danny
appId: com.blocktrade.ios
appCountry: us
idd: 1360294403
released: 2018-10-30
updated: 2021-11-06
version: "1.4.1"
stars: 5
reviews: 8
size: 56538112
website: http://blocktrade.com
repository: 
issue: 
icon: com.blocktrade.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: Blocktradecom
providerLinkedIn: blocktradecom
providerFacebook: Blocktradecom
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
