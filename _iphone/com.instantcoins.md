---
wsId: InstantCoins
title: "Instantcoins – Crypto Trade"
altTitle: 
authors:
- danny
appId: com.instantcoins
appCountry: ng
idd: 1519748966
released: 2020-08-17
updated: 2021-04-16
version: "1.0.5"
stars: 3.09524
reviews: 21
size: 75852800
website: https://instantcoins.ng/
repository: 
issue: 
icon: com.instantcoins.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: instantcoins_ng
providerLinkedIn: 
providerFacebook: instantcoins
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

