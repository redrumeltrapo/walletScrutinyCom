---
wsId: FluxPay
title: "Flux Pay"
altTitle: 
authors:
- danny
appId: com.blueloopflux.app
appCountry: us
idd: 1534426282
released: 2020-10-15
updated: 2021-10-28
version: "1.7.7"
stars: 3.125
reviews: 8
size: 64945152
website: https://iflux.app/
repository: 
issue: 
icon: com.blueloopflux.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive:


providerTwitter: ifluxdotapp
providerLinkedIn: iflux-pay
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
