---
wsId: fatBTC
title: "FatBTC Exchange"
altTitle: 
authors:
- danny
appId: com.fatbtc.FatBTCApp
appCountry: us
idd: 1490226195
released: 2019-12-31
updated: 2021-11-08
version: "2.45"
stars: 3.33333
reviews: 3
size: 45196288
website: https://www.fatbtc.com/
repository: 
issue: 
icon: com.fatbtc.FatBTCApp.jpg
bugbounty: 
verdict: custodial
date: 2021-10-10
signer: 
reviewArchive:


providerTwitter: fatbtc
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
