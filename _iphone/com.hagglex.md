---
wsId: hagglex
title: "HaggleX: Buy/Sell BTC and ETH"
altTitle: 
authors:
- danny
appId: com.hagglex
appCountry: us
idd: 1535046179
released: 2021-03-18
updated: 2021-11-01
version: "1.1.1"
stars: 4.08474
reviews: 118
size: 108738560
website: https://www.hagglex.com/
repository: 
issue: 
icon: com.hagglex.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: hagglexintl
providerLinkedIn: hagglex
providerFacebook: hagglex
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
