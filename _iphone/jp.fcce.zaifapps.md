---
wsId: ZaifExchange
title: "Zaif Exchange"
altTitle: 
authors:
- danny
appId: jp.fcce.zaifapps
appCountry: in
idd: 1505715935
released: 2020-05-15
updated: 2021-08-25
version: "1.1.7"
stars: 
reviews: 
size: 26476544
website: https://zaif.jp/
repository: 
issue: 
icon: jp.fcce.zaifapps.jpg
bugbounty: 
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive:


providerTwitter: zaifdotjp
providerLinkedIn: 
providerFacebook: zaifdotjp
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
