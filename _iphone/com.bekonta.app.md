---
wsId: Bekonta
title: "Bekonta"
altTitle: 
authors:
- danny
appId: com.bekonta.app
appCountry: us
idd: 1479400295
released: 2021-09-15
updated: 2021-09-16
version: "1.0.0"
stars: 
reviews: 
size: 12212224
website: http://www.bekonta.com
repository: 
issue: 
icon: com.bekonta.app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: bekonta
providerFacebook: bekontahq
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
