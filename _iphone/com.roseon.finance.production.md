---
wsId: roseon
title: "Roseon Finance"
altTitle: 
authors:
- danny
appId: com.roseon.finance.production
appCountry: vn
idd: 1559440997
released: 2021-05-24
updated: 2021-11-04
version: "2.3.13"
stars: 4.64516
reviews: 31
size: 100806656
website: https://roseon.finance/
repository: 
issue: 
icon: com.roseon.finance.production.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: RoseonFinance
providerLinkedIn: 
providerFacebook: Roseon.Finance
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
