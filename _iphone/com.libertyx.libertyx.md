---
wsId: libertyx
title: "LibertyX - Buy Bitcoin"
altTitle: 
authors:
- danny
appId: com.libertyx.libertyx
appCountry: us
idd: 966538981
released: 2015-02-20
updated: 2021-06-15
version: "4.1.0"
stars: 3.95973
reviews: 149
size: 14478336
website: https://libertyx.com
repository: 
issue: 
icon: com.libertyx.libertyx.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: libertyx
providerLinkedIn: libertyx
providerFacebook: getlibertyx
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
