---
wsId: webull
title: "Webull: Investing & Trading"
altTitle: 
authors:
- danny
- leo
appId: com.webull.trade
appCountry: 
idd: 1179213067
released: 2017-01-18
updated: 2021-10-31
version: "7.3.0"
stars: 4.71668
reviews: 246639
size: 349535232
website: https://www.webull.com
repository: 
issue: 
icon: com.webull.trade.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-08-10
signer: 
reviewArchive:


providerTwitter: WebullGlobal
providerLinkedIn: webullfinancialllc
providerFacebook: 
providerReddit: 

redirect_from:

---

The app allows you to trade BTC but it is only an exchange and not a wallet.

From their website:
> We provide our customers with access to cryptocurrency trading through Apex Crypto. Apex Crypto is not a registered broker-dealer or FINRA member and your cryptocurrency holdings are not FDIC or SIPC insured.

> You can buy and sell cryptocurrency on Webull. However, we do not support transferring crypto into or out of your Webull account at this time.
