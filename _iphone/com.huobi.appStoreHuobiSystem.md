---
wsId: huobi
title: "Huobi - Buy & Sell Bitcoin"
altTitle: 
authors:
- leo
appId: com.huobi.appStoreHuobiSystem
appCountry: 
idd: 1023263342
released: 2015-08-19
updated: 2021-11-06
version: "6.5.8"
stars: 4.69692
reviews: 3085
size: 319521792
website: http://www.hbg.com
repository: 
issue: 
icon: com.huobi.appStoreHuobiSystem.jpg
bugbounty: 
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive:


providerTwitter: HuobiGlobal
providerLinkedIn: 
providerFacebook: huobiglobalofficial
providerReddit: 

redirect_from:

---

Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
