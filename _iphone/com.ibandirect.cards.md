---
wsId: Ibandirect
title: "Ibandirect"
altTitle:
authors:
- danny
appId: com.ibandirect.cards
appCountry: sg
idd: 1538001175
released: 2020-11-09
updated: 2021-10-29
version: "1.4.1"
stars:
reviews:
size: 128165888
website: https://ibandirect.com/
repository:
issue:
icon: com.ibandirect.cards.jpg
bugbounty:
verdict: custodial
date: 2021-11-10
signer:
reviewArchive:


providerTwitter:
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
{% include copyFromAndroid.html %}
