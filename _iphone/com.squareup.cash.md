---
wsId: CashApp
title: "Cash App"
altTitle: 
authors:
- leo
appId: com.squareup.cash
appCountry: 
idd: 711923939
released: 2013-10-16
updated: 2021-11-08
version: "3.52"
stars: 4.74264
reviews: 2025691
size: 222214144
website: https://cash.app
repository: 
issue: 
icon: com.squareup.cash.jpg
bugbounty: 
verdict: custodial
date: 2020-12-22
signer: 
reviewArchive:


providerTwitter: cashapp
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

On their website the provider claims:

> **Coin Storage**<br>
  Your Bitcoin balance is securely stored in our offline system

which means it is custodial.
