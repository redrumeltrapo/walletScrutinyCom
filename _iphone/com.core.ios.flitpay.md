---
wsId: flitpay
title: "Flitpay: Crypto Exchange"
altTitle: 
authors:
- danny
appId: com.core.ios.flitpay
appCountry: dk
idd: 1571975471
released: 2021-08-27
updated: 2021-09-23
version: "1.0.4"
stars: 
reviews: 
size: 44262400
website: https://www.flitpay.com
repository: 
issue: 
icon: com.core.ios.flitpay.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: flitpayofficial
providerLinkedIn: 
providerFacebook: flitpay
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
