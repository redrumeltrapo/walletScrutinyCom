---
wsId: buda
title: "Buda"
altTitle: 
authors:
- leo
appId: com.buda.crypto
appCountry: 
idd: 1321460860
released: 2018-01-04
updated: 2019-12-02
version: "1.9.5"
stars: 3
reviews: 6
size: 31807488
website: 
repository: 
issue: 
icon: com.buda.crypto.jpg
bugbounty: 
verdict: stale
date: 2021-08-17
signer: 
reviewArchive:
- date: 2021-05-30
  version: "1.9.5"
  appHash: 
  gitRevision: d7cf0c0967057bb9f06fb8ec6cbf1097fe1da885
  verdict: custodial

providerTwitter: BudaPuntoCom
providerLinkedIn: budapuntocom
providerFacebook: BudaPuntoCom
providerReddit: 

redirect_from:

---

This app has very poor ratings on Google (2.5 stars) and Apple (3.5 stars),
mainly due to limited functionality and high fees. Caution is advised!

This app is an interface to an exchange and coins are held there and not on the
phone. As a custodial service it is **not verifiable**.
