---
wsId: vita
title: "Vita Wallet"
altTitle: 
authors:
- danny
appId: io.vitawallet.vitawallet
appCountry: cl
idd: 1486999955
released: 2019-11-15
updated: 2021-10-29
version: "3.7.1"
stars: 4.42105
reviews: 19
size: 44824576
website: https://www.vitawallet.io
repository: 
issue: 
icon: io.vitawallet.vitawallet.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: vitawallet
providerLinkedIn: vita-wallet
providerFacebook: vitawallet
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
