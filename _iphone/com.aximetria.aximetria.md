---
wsId: Aximetria
title: "Aximetria Swiss crypto account"
altTitle: 
authors:
- danny
appId: com.aximetria.aximetria
appCountry: gb
idd: 1361781578
released: 2018-07-06
updated: 2021-08-02
version: "2.1.3"
stars: 3.30769
reviews: 13
size: 78513152
website: http://aximetria.com
repository: 
issue: 
icon: com.aximetria.aximetria.jpg
bugbounty: 
verdict: custodial
date: 2021-10-13
signer: 
reviewArchive:


providerTwitter: aximetriagmbh
providerLinkedIn: aximetriagmbh
providerFacebook: aximetriagmbh
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
