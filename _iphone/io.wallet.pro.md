---
wsId: WalletIOPRO
title: "wallet.io Pro"
altTitle: 
authors:
- danny
appId: io.wallet.pro
appCountry: nz
idd: 1475607592
released: 2019-09-17
updated: 2021-08-06
version: "1.2.9"
stars: 
reviews: 
size: 63653888
website: https://pro.wallet.io/
repository: https://github.com/wallet-io
issue: 
icon: io.wallet.pro.jpg
bugbounty: 
verdict: nosource
date: 2021-10-19
signer: 
reviewArchive:


providerTwitter: io_wallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

