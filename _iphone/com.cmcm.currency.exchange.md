---
wsId: bitrue
title: "Bitrue"
altTitle: 
authors:
- leo
appId: com.cmcm.currency.exchange
appCountry: 
idd: 1435877386
released: 2018-09-16
updated: 2021-11-04
version: "5.0.6"
stars: 3.57192
reviews: 292
size: 103452672
website: https://www.bitrue.com
repository: 
issue: 
icon: com.cmcm.currency.exchange.jpg
bugbounty: 
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive:


providerTwitter: BitrueOfficial
providerLinkedIn: 
providerFacebook: BitrueOfficial
providerReddit: 

redirect_from:

---

This app is heavily focused on the "exchange" part which is also in its name.
Nowhere on the App Store can we find claims about self-custody but things like

> - Applies the advanced multi-layer clustered system and the hot/cold wallet
  isolation technology to ensure system security.

only make sense for custodial apps. As a custodial app it is **not verifiable**.
