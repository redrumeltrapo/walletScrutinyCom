---
wsId: QuantDART
title: "QuantDART Defi Digital Assets"
altTitle: 
authors:
- danny
appId: com.quantdart.ios
appCountry: us
idd: 1522245431
released: 2020-10-18
updated: 2021-11-05
version: "1.1.19"
stars: 
reviews: 
size: 182721536
website: https://www.quantdart.com/
repository: 
issue: 
icon: com.quantdart.ios.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: QuantDART
providerLinkedIn: 
providerFacebook: quantdart
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

