---
wsId: bitbuy
title: "Bitbuy: Buy Bitcoin Canada"
altTitle: 
authors:
- danny
appId: com.bitbuy.mobileApp
appCountry: ca
idd: 1476837869
released: 2019-10-21
updated: 2021-09-08
version: "3.58.0"
stars: 4.60106
reviews: 6422
size: 97673216
website: https://bitbuy.ca/
repository: 
issue: 
icon: com.bitbuy.mobileApp.jpg
bugbounty: 
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive:


providerTwitter: bitbuy
providerLinkedIn: bitbuyca
providerFacebook: bitbuyCA
providerReddit: 

redirect_from:

---

 {% include copyFromAndroid.html %}
