---
wsId: Colodax
title: "Colodax-India Crypto Exchange"
altTitle: 
authors:
- danny
appId: com.colodax.colodax
appCountry: us
idd: 1497362345
released: 2020-02-04
updated: 2021-09-17
version: "1.7.2"
stars: 5
reviews: 1
size: 80020480
website: https://colodax.com
repository: 
issue: 
icon: com.colodax.colodax.jpg
bugbounty: 
verdict: custodial
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: colodax
providerLinkedIn: colodax
providerFacebook: colodax
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
