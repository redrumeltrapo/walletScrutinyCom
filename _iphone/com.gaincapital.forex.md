---
wsId: Forexcom
title: "FOREX.com: CFD & Forex Trading"
altTitle: 
authors:
- danny
appId: com.gaincapital.forex
appCountry: gb
idd: 1506581586
released: 2020-10-14
updated: 2021-11-08
version: "1.79.2457"
stars: 3.8125
reviews: 32
size: 131992576
website: https://www.forex.com/en-uk/
repository: 
issue: 
icon: com.gaincapital.forex.jpg
bugbounty: 
verdict: nosendreceive
date: 2021-10-16
signer: 
reviewArchive:


providerTwitter: forexcom
providerLinkedIn: 
providerFacebook: FOREXcom
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

