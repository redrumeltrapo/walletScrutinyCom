---
wsId: DotWallet
title: "DotWallet - BTC/ETH/BSV Wallet"
altTitle:
authors:
- danny
appId: com.boquanhash.dotwallet
appCountry: us
idd: 1509685349
released: 2021-01-05
updated: 2021-10-01
version: "2.8.0"
stars: 5
reviews: 4
size: 76503040
website: https://www.dotwallet.com
repository:
issue:
icon: com.boquanhash.dotwallet.jpg
bugbounty:
verdict: nosource
date: 2021-11-10
signer:
reviewArchive:


providerTwitter: wallet_dot
providerLinkedIn:
providerFacebook:
providerReddit:

redirect_from:

---
{% include copyFromAndroid.html %}
