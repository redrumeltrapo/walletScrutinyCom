---
wsId: Bolsa
title: "Bolsa: Forex Stock Market Game"
altTitle: 
authors:
- danny
appId: com.tiim.tradeon
appCountry: us
idd: 1267910740
released: 2017-08-28
updated: 2021-10-01
version: "2.2"
stars: 5
reviews: 20
size: 44812288
website: https://bolsa.app
repository: 
issue: 
icon: com.tiim.tradeon.jpg
bugbounty: 
verdict: nowallet
date: 2021-10-18
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
