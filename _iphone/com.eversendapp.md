---
wsId: eversend
title: "Eversend - the money app"
altTitle: 
authors:
- danny
appId: com.eversendapp
appCountry: lv
idd: 1438341192
released: 2020-05-28
updated: 2021-11-03
version: "0.2.13"
stars: 
reviews: 
size: 65250304
website: http://www.eversend.co
repository: 
issue: 
icon: com.eversendapp.jpg
bugbounty: 
verdict: nobtc
date: 2021-11-01
signer: 
reviewArchive:


providerTwitter: eversendapp
providerLinkedIn: eversend
providerFacebook: eversendapp
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
