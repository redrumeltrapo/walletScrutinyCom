---
wsId: coinspace
title: "Coin Bitcoin & Crypto Wallet"
altTitle: 
authors:
- leo
appId: com.coinspace.wallet
appCountry: 
idd: 980719434
released: 2015-12-14
updated: 2021-10-12
version: "5.1.1"
stars: 4.48026
reviews: 152
size: 40324096
website: https://coin.space/
repository: https://github.com/CoinSpace/CoinSpace
issue: 
icon: com.coinspace.wallet.jpg
bugbounty: https://www.openbugbounty.org//bugbounty/CoinAppWallet/
verdict: nonverifiable
date: 2020-12-20
signer: 
reviewArchive:


providerTwitter: coinappwallet
providerLinkedIn: coin-space
providerFacebook: coinappwallet
providerReddit: 

redirect_from:

---

On the website the provider claims:

> keys are stored locally, on your device

and there is a public source repository on GitHub but as iPhone apps are
all currently not reproducible, the app remains **not verifiable**.
