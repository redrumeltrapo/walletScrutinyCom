---
wsId: CoinBurp
title: "CoinBurp"
altTitle: 
authors:
- kiwilamb
appId: com.coinburp.mobile
appCountry: 
idd: 1486342307
released: 2020-06-12
updated: 2021-07-29
version: "1.0.15"
stars: 4.4
reviews: 5
size: 42656768
website: https://www.coinburp.com/
repository: 
issue: 
icon: com.coinburp.mobile.jpg
bugbounty: 
verdict: custodial
date: 2021-06-04
signer: 
reviewArchive:


providerTwitter: coinburp
providerLinkedIn: coinburp
providerFacebook: thecoinburp
providerReddit: 

redirect_from:

---

On the provider's website we find how private keys are managed, under [help section "Account Security"](https://help.coinburp.com/hc/en-gb/articles/360017544100-Are-Balances-Stored-on-CoinBurp-Insured-) we find their custodial provider is Bitpay.
This is the typical setup of exchange based apps, they hold a % of coins in hot wallets for daily trade management and store larger % in cold "offline" storage for security purposes.

Our Verdict: This "wallet" is custodial and therefor **not verifiable**
