---
wsId: quantfury
title: "Quantfury"
altTitle: 
authors:
- danny
appId: com.quantfury
appCountry: gb
idd: 1445564443
released: 2018-12-15
updated: 2021-11-03
version: "1.39.1"
stars: 4.7037
reviews: 54
size: 68061184
website: https://quantfury.com/
repository: 
issue: 
icon: com.quantfury.jpg
bugbounty: 
verdict: custodial
date: 2021-11-01
signer: 
reviewArchive:


providerTwitter: quantfury
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
