---
wsId: Paymium
title: "Paymium - Bitcoin Platform"
altTitle: 
authors:
- danny
appId: com.paymium.mobile
appCountry: us
idd: 1055288395
released: 2016-01-18
updated: 2021-10-19
version: "3.1.4"
stars: 4
reviews: 1
size: 115828736
website: https://www.paymium.com/
repository: 
issue: 
icon: com.paymium.mobile.jpg
bugbounty: 
verdict: custodial
date: 2021-11-03
signer: 
reviewArchive:


providerTwitter: paymium
providerLinkedIn: paymium
providerFacebook: Paymium
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
