---
wsId: bitmart
title: "BitMart - Crypto Exchange"
altTitle: 
authors:
- leo
appId: com.bitmart.exchange
appCountry: 
idd: 1396382871
released: 2018-08-02
updated: 2021-11-04
version: "2.8.0"
stars: 4.57094
reviews: 10241
size: 123554816
website: https://www.bitmart.com/
repository: 
issue: 
icon: com.bitmart.exchange.jpg
bugbounty: 
verdict: custodial
date: 2021-05-20
signer: 
reviewArchive:


providerTwitter: BitMartExchange
providerLinkedIn: bitmart
providerFacebook: bitmartexchange
providerReddit: BitMartExchange

redirect_from:

---

On their website we read:

> **Secure**<br>
  Advanced risk control system in the market. Hybrid hot/cold wallet systems and
  multi-signature technologies. 100% secure for trading and digital asset
  management

A "hot" wallet is online, a "cold" wallet is offline. Your phone is certainly
not "cold", so it's them who hold the keys. As a custodial service the app is
**not verifiable**.
