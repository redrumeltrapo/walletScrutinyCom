---
wsId: kelta
title: "KELTA - Buy Bitcoin Now"
altTitle: 
authors:
- danny
appId: com.kelta
appCountry: sk
idd: 1385038150
released: 2018-06-07
updated: 2021-08-01
version: "3.0.60"
stars: 4.1573
reviews: 178
size: 19784704
website: https://kelta.com
repository: 
issue: 
icon: com.kelta.jpg
bugbounty: 
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
