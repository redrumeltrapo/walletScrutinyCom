---
wsId: gate.io
title: "Gate.io-Buy BTC,ETH,SHIB"
altTitle: 
authors:
- danny
appId: com.gateio.app.gateio-app
appCountry: id
idd: 1294998195
released: 2017-11-03
updated: 2021-11-07
version: "3.1.3"
stars: 3.17391
reviews: 46
size: 373575680
website: https://gate.io
repository: 
issue: 
icon: com.gateio.app.gateio-app.jpg
bugbounty: 
verdict: custodial
date: 2021-10-05
signer: 
reviewArchive:


providerTwitter: gate_io
providerLinkedIn: 
providerFacebook: gateioglobal
providerReddit: GateioExchange

redirect_from:

---

{% include copyFromAndroid.html %}
