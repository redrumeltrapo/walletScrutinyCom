---
wsId: Tokenize
title: "Tokenize Trading App"
altTitle: 
authors:
- danny
appId: com.tokenize.exchange.trading
appCountry: us
idd: 1495765876
released: 2020-02-02
updated: 2021-11-01
version: "1.2.16"
stars: 
reviews: 
size: 72688640
website: https://tokenize.exchange
repository: 
issue: 
icon: com.tokenize.exchange.trading.jpg
bugbounty: 
verdict: custodial
date: 2021-11-08
signer: 
reviewArchive:


providerTwitter: TokenizeXchange
providerLinkedIn: 
providerFacebook: tokenize.exchange
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
