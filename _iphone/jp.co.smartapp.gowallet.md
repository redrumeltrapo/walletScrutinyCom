---
wsId: gowallet
title: "GO ! WALLET -  ethereum wallet"
altTitle: 
authors:
- danny
appId: jp.co.smartapp.gowallet
appCountry: us
idd: 1400626330
released: 2018-10-11
updated: 2020-10-09
version: "1.4.8"
stars: 4.63089
reviews: 699
size: 86727680
website: https://www.go-wallet.app/
repository: 
issue: 
icon: jp.co.smartapp.gowallet.jpg
bugbounty: 
verdict: stale
date: 2021-10-04
signer: 
reviewArchive:


providerTwitter: gowallet_app_
providerLinkedIn: 
providerFacebook: gowalletappli
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
