---
wsId: CoinBene
title: "CoinBene"
altTitle: 
authors:
- danny
appId: br.com.coinbene
appCountry: us
idd: 1439224379
released: 2018-11-12
updated: 2021-09-13
version: "4.8.6"
stars: 3.64286
reviews: 42
size: 115953664
website: https://www.coinbene.com/
repository: 
issue: 
icon: br.com.coinbene.jpg
bugbounty: 
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive:


providerTwitter: coinbene
providerLinkedIn: coinbene-official
providerFacebook: CoinBeneOfficial
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}
