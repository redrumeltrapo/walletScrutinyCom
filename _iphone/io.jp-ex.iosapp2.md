---
wsId: JPEX
title: "JPEX Exchange"
altTitle: 
authors:
- danny
appId: io.jp-ex.iosapp2
appCountry: jp
idd: 1559708728
released: 2021-04-22
updated: 2021-11-05
version: "2.23.368"
stars: 
reviews: 
size: 48020480
website: https://jp-ex.io/
repository: 
issue: 
icon: io.jp-ex.iosapp2.jpg
bugbounty: 
verdict: custodial
date: 2021-10-26
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

{% include copyFromAndroid.html %}

